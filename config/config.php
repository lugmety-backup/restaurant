<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 3:35 PM
 */

if(env("APP_ENV") == "production"){
    return array(

        'url' => 'https://api.lugmety.com/auth/v1/oauth/introspect',

        'image_service_base_url' => 'https://api.lugmety.com/cdn/v1',//dont add / after v1

        "image_service_base_url_cdn" => 'https://cdn.lugmety.com',//cdn base url for image display

        "oauth_base_url" => "https://api.lugmety.com/auth/v1",

        "country_url" => "https://api.lugmety.com/location/v1/public/country/",

        "location_url" => "https://api.lugmety.com/location/v1",

        "order_base_url" => "https://api.lugmety.com/order/v1",//donot add /

        "settings_url" => "https://api.lugmety.com/general/v1/public/settings/",

        "error_message_url" => "https://api.lugmety.com/general/v1/error-message/",

        'access_token' => "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjA0YjM4YmRmZDRlNWJlMDJjZGE3MjU4YjFkYjFmOGExOTViYjkzNjMwODQzYjZhNTg5ZTU2YWViYTc2NDgxNDNkNDRmZTUzZmFiZjI0Njg5In0.eyJhdWQiOiI1IiwianRpIjoiMDRiMzhiZGZkNGU1YmUwMmNkYTcyNThiMWRiMWY4YTE5NWJiOTM2MzA4NDNiNmE1ODllNTZhZWJhNzY0ODE0M2Q0NGZlNTNmYWJmMjQ2ODkiLCJpYXQiOjE0OTE1NDA2NzEsIm5iZiI6MTQ5MTU0MDY3MSwiZXhwIjoxNTIzMDc2NjcxLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.bL9_FaUiC7qYRuWkG4Bz4foWaWhFTxZs4cKZYRBNIQEdyg8XN6qG_XHuGe38J8FmD4MWPBlQ2MF7kqt7c9tg1bghH7l8uaaFCYdb5tidmeG_texh_GpekmEirkyLwMGkfbv_hXPb5RyXO6F4wNO0Z9xlUgC451vy6gE0W2E8swQb_-Oih6O9rC7S6ahnA0P3NR-npwL6_8YrGD0A_qvlGfWcg1zCveW7nzNp4WQAdI898NWDModP7yRJgCasHLLnGtI8KlKuiyZC_Owitjjj36F3SVid61v0i5PN_Ux3GTv9NqHpK8Ktxw6Yg5Q-w7dBwQK0IAMuejaN_uMM3eZrLTTPBRDBTAmSH-b-5INu7qDvOqv-0F0mjlXvgKvUPLwDRP1uNe8UT1xUx_5JWLJJzn5Ca-yQUT76s7arPdY6jFfezg8pi8_t-6csBYi_FcXGQnsW5pb12aNK46I5Kzuh1TL1VXtG38WICm27fa-Hj12rHJr66N8kUIb1_txtZOSUghABf1rvE0z9CYZw5JbKQkC0zQtL_YjJlRUOXFV8urzkrPLcrtmLQffnoJe0ol4inPM_863xoWyptZJU6QbTvAqplCfTjd_boxEcw-7K-cCqJEHVb9tUyajgk5E-fQMZax6Mm3p18TNucP8RVBiAl1WL9qUJvocKw9GeLVqG0jM",

        'IMAGE_BASE_PATH' => '/public/images/',

        /*
         * dont set restaurant hour limit to 0
         * */
        "restaurant_hour_limit" => 2,
        /*
         * Note:if you should add shift based on the restaurant_hour limit
         * for eg: if restaurant_hour_limit is 3 do add 3 shift in restaurant_hour_shift inside array
         * like :"restaurant_hour_shift"=>[
            "Morning","Evening","Night"
        ]
         * */
        "restaurant_hour_shift" => [
            "Day", "Evening"
        ],

        "category_image_path" => '/public/images/food_category/',

        "food_image_path" => '/public/images/food/'
    );
}
else {
    return array(

        'url' => 'http://api.stagingapp.io/auth/v1/oauth/introspect',

//    'url'=> 'http://localhost:8888/oauth/introspect',

        'image_service_base_url' => 'http://api.stagingapp.io/cdn/v1',//dont add / after v1


        "image_service_base_url_cdn" => 'http://api.stagingapp.io/cdn/v1',//cdn base url for image display

//    'image_service_base_url'=>'http://localhost:5555',

        "oauth_base_url" => "http://api.stagingapp.io/auth/v1",

//    "oauth_base_url" => "http://localhost:8888",

        "country_url" => "http://api.stagingapp.io/location/v1/public/country/",

//   "country_url" => "http://localhost:9000/public/country/",

        "location_url" => "http://api.stagingapp.io/location/v1",

//    "location_url"=>"http://localhost:9000",

//    "order_base_url"=>"http://localhost:2222",//donot add /
        "order_base_url" => "http://api.stagingapp.io/order/v1",//donot add /

//    "settings_url"=>"http://localhost:8091/public/settings/",

        "settings_url" => "http://api.stagingapp.io/general/v1/public/settings/",

        "error_message_url" => "http://api.stagingapp.io/general/v1/error-message/",

        'access_token' => "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjA0YjM4YmRmZDRlNWJlMDJjZGE3MjU4YjFkYjFmOGExOTViYjkzNjMwODQzYjZhNTg5ZTU2YWViYTc2NDgxNDNkNDRmZTUzZmFiZjI0Njg5In0.eyJhdWQiOiI1IiwianRpIjoiMDRiMzhiZGZkNGU1YmUwMmNkYTcyNThiMWRiMWY4YTE5NWJiOTM2MzA4NDNiNmE1ODllNTZhZWJhNzY0ODE0M2Q0NGZlNTNmYWJmMjQ2ODkiLCJpYXQiOjE0OTE1NDA2NzEsIm5iZiI6MTQ5MTU0MDY3MSwiZXhwIjoxNTIzMDc2NjcxLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.bL9_FaUiC7qYRuWkG4Bz4foWaWhFTxZs4cKZYRBNIQEdyg8XN6qG_XHuGe38J8FmD4MWPBlQ2MF7kqt7c9tg1bghH7l8uaaFCYdb5tidmeG_texh_GpekmEirkyLwMGkfbv_hXPb5RyXO6F4wNO0Z9xlUgC451vy6gE0W2E8swQb_-Oih6O9rC7S6ahnA0P3NR-npwL6_8YrGD0A_qvlGfWcg1zCveW7nzNp4WQAdI898NWDModP7yRJgCasHLLnGtI8KlKuiyZC_Owitjjj36F3SVid61v0i5PN_Ux3GTv9NqHpK8Ktxw6Yg5Q-w7dBwQK0IAMuejaN_uMM3eZrLTTPBRDBTAmSH-b-5INu7qDvOqv-0F0mjlXvgKvUPLwDRP1uNe8UT1xUx_5JWLJJzn5Ca-yQUT76s7arPdY6jFfezg8pi8_t-6csBYi_FcXGQnsW5pb12aNK46I5Kzuh1TL1VXtG38WICm27fa-Hj12rHJr66N8kUIb1_txtZOSUghABf1rvE0z9CYZw5JbKQkC0zQtL_YjJlRUOXFV8urzkrPLcrtmLQffnoJe0ol4inPM_863xoWyptZJU6QbTvAqplCfTjd_boxEcw-7K-cCqJEHVb9tUyajgk5E-fQMZax6Mm3p18TNucP8RVBiAl1WL9qUJvocKw9GeLVqG0jM",

        'IMAGE_BASE_PATH' => '/public/images/',

        /*
         * dont set restaurant hour limit to 0
         * */
        "restaurant_hour_limit" => 2,
        /*
         * Note:if you should add shift based on the restaurant_hour limit
         * for eg: if restaurant_hour_limit is 3 do add 3 shift in restaurant_hour_shift inside array
         * like :"restaurant_hour_shift"=>[
            "Morning","Evening","Night"
        ]
         * */
        "restaurant_hour_shift" => [
            "Day", "Evening"
        ],

        "category_image_path" => '/public/images/food_category/',

        "food_image_path" => '/public/images/food/'
    );
}