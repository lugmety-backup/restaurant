<?php

/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 5/4/2017
 * Time: 1:49 PM
 */
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Testing\DatabaseTransactions;

/**
 * Class RestaurantTest
 */
class RestaurantTest extends TestCase
{
    /*
     * since in phpunit.xml squlite is used so Database MIgration will migrate and automaticall roll back after testing
     * */
    use \Laravel\Lumen\Testing\DatabaseMigrations;
    use DatabaseTransactions;
    public function testSpecificRestaurantWithoutLangParamDisplayEnglish(){
        $restaurant=factory(\App\Restaurant::class)->create();
        $restaurantTranslation=[[
            "restaurant_id"=>$restaurant->id,
            "lang_code"=>"en",
            "name"=>"Bajeko sekwa",
            "description"=>"sekwa corner"
        ],
            [
                "restaurant_id"=>$restaurant->id,
                "lang_code"=>"ar",
                "name"=>"المطاعم",
                "description"=>"مرحبا اسمي اشيش"
            ]];
        $restaurantTranslations=DB::table('restaurant_translation')->insert([
            "restaurant_id"=>$restaurant->id,
            "lang_code"=>"en",
            "name"=>"Bajeko sekwa",
            "description"=>"sekwa corner"
        ],
            [
                "restaurant_id"=>$restaurant->id,
                "lang_code"=>"ar",
                "name"=>"المطاعم",
                "description"=>"مرحبا اسمي اشيش"
            ]);
        $res=$this->get("restaurant/$restaurant->id");
        $this->seeJson(
            [
                'data'=>[
                    "id"=> $restaurant->id,
                    /*
                     * since input user_id is in integer format
                     * the response from databse is in string so we need to convert into string
                     * */
                    "user_id"=> (string)$restaurant->user_id,
                    "address"=> $restaurant->address,
                    "phone_no"=> (string)$restaurant->phone_no,
                    "mobile_no"=> $restaurant->mobile_no,
                    "country_id"=>(string) $restaurant->country_id,
                    "email_address"=> $restaurant->email_address,
                    "commission_rate"=>(string)$restaurant->commission_rate,
                    "website"=> $restaurant->website,
                    "contact_person"=> $restaurant->contact_person,
                    "status"=>(string)$restaurant->status,
                    "name"=>$restaurantTranslation[0]["name"] ,
                    "lang_code"=> $restaurantTranslation[0]["lang_code"]

                ],
                "status"=>"200"
            ])->assertResponseStatus(200);
    }

    public function testSpecificRestaurantLanguageParameterIdIsWrong(){
        $res=$this->get("restaurant/1");
        $this->seeJson([
            "status" => "200",
            "message" => "Empty Record for requested status"
        ])->assertResponseStatus(200);
    }

    public function testSpecificRestaurantLanguageParameterIfNotDefaultEnglishDisplayed(){
        $restaurant=factory(\App\Restaurant::class)->create();
        $restaurantTranslation=[[
            "restaurant_id"=>$restaurant->id,
            "lang_code"=>"en",
            "name"=>"Bajeko sekwa",
            "description"=>"sekwa corner"
        ],
            [
                "restaurant_id"=>$restaurant->id,
                "lang_code"=>"ar",
                "name"=>"المطاعم",
                "description"=>"مرحبا اسمي اشيش"
            ]];

        $restaurantTranslations=DB::table('restaurant_translation')->insert([[
            "restaurant_id"=>$restaurant->id,
            "lang_code"=>"en",
            "name"=>"Bajeko sekwa",
            "description"=>"sekwa corner"
        ],
            [
                "restaurant_id"=>$restaurant->id,
                "lang_code"=>"ar",
                "name"=>"المطاعم",
                "description"=>"مرحبا اسمي اشيش"
            ]
        ]);
        $res=$this->get("restaurant/$restaurant->id?lang=ar");
        $this->seeJson(
            [
                'data'=>[
                    "id"=> $restaurant->id,
                    "user_id"=> (string)$restaurant->user_id,
                    "address"=> $restaurant->address,
                    "phone_no"=> (string)$restaurant->phone_no,
                    "mobile_no"=> $restaurant->mobile_no,
                    "country_id"=>(string) $restaurant->country_id,
                    "email_address"=> $restaurant->email_address,
                    "commission_rate"=>(string)$restaurant->commission_rate,
                    "website"=> $restaurant->website,
                    "contact_person"=> $restaurant->contact_person,
                    "status"=>(string)$restaurant->status,
                    "name"=>$restaurantTranslation[1]["name"] ,
                    "lang_code"=> $restaurantTranslation[1]["lang_code"]

                ],
                "status"=>"200"
            ])->assertResponseStatus(200);
    }

    public function testCreateRestaurant(){
        $restaurant=[
            "user_id"=>1,
            "address"=>"americe",
            "phone_no"=>"4990939",
            "mobile_no"=>"9840072010",
            "country_id"=>1,
            "email_address"=>"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa@gmail.com",
            "website"=>"https://gitlab.codm/lugmetddy/restaurant",
            "contact_person"=>"avash kafle",
            "commission_rate"=>10.99,
            "status"=>0,
            "translation"=>[
                "en"=>[
                    "name"=>"avashsssssssssssss pvt"
                ],
                "AR"=>[
                    "name"=>"نيبال"
                ]
            ]
        ];

        $this->post("restaurant",$restaurant,[])->seeJson([
            "status"=> "200",
            "message"=> "Restaurant Created Successfully"
        ])->assertResponseStatus(200);
    }
    public function testCreateInvalidRestaurant(){
        $restaurant=[
            "user_id"=>"",
            "address"=>"",
            "phone_no"=>"",
            "mobile_no"=>"",
            "country_id"=>"",
            "email_address"=>"as",
            "website"=>"gitlab.codm/lugmetddy/restaurant",
            "contact_person"=>"",
            "commission_rate"=>"",
            "status"=>0,
            "translation"=>[
                "en"=>[
                    "name"=>"avashsssssssssssss pvt"
                ],
                "AR"=>[
                    "name"=>"نيبال"
                ]
            ]
        ];
        $this->post("restaurant",$restaurant,[])->seeJson(
            [
                "status"=> "422",
                "message"=> [
                    "user_id"=> [
                        "The user id field is required."
                    ],
                    "address"=> [
                        "The address field is required."
                    ],
                    "phone_no"=> [
                        "The phone no field is required."
                    ],
                    "mobile_no"=> [
                        "The mobile no field is required."
                    ],
                    "email_address"=> [
                        "The email address must be a valid email address."
                    ],
                    "website"=> [
                        "The website format is invalid."
                    ],
                    "contact_person"=> [
                        "The contact person field is required."
                    ],
                    "commission_rate"=> [
                        "The commission rate field is required."
                    ]
                ]
            ]
        )->assertResponseStatus(422);
    }
    public function testCreateInvalidRestaurantIfTranslationIsNotInArray(){
        $restaurant=[
            "user_id"=>1,
            "address"=>"sss",
            "phone_no"=>"1234567",
            "mobile_no"=>"1234567891",
            "country_id"=>1,
            "email_address"=>"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa@gmail.com",
            "website"=>"https://gitlab.codm/lugmetddy/restaurant",
            "contact_person"=>"ashish kafle",
            "commission_rate"=>10.11,
            "status"=>0,
            "translation"=>"ashish"
        ];
        $this->post("restaurant",$restaurant,[])->seeJson([
                "translation"=> [
                    "The translation must be an array."
                ]]
        )->assertResponseStatus(422);
    }

    public function testUpdateRestaurant(){
        $restaurant=factory(\App\Restaurant::class)->create();
        $restaurantTranslation=[[
            "restaurant_id"=>$restaurant->id,
            "lang_code"=>"en",
            "name"=>"Bajeko sekwa",
            "description"=>"sekwa corner"
        ],
            [
                "restaurant_id"=>$restaurant->id,
                "lang_code"=>"ar",
                "name"=>"المطاعم",
                "description"=>"مرحبا اسمي اشيش"
            ]];

        $restaurantTranslations=DB::table('restaurant_translation')->insert([
            "restaurant_id"=>$restaurant->id,
            "lang_code"=>"en",
            "name"=>"Bajeko sekwa",
            "description"=>"sekwa corner"
        ],
            [
                "restaurant_id"=>$restaurant->id,
                "lang_code"=>"ar",
                "name"=>"المطاعم",
                "description"=>"مرحبا اسمي اشيش"
            ]);

        $restaurants=[
            "user_id"=>1,
            "address"=>"americe",
            "phone_no"=>"4990939",
            "mobile_no"=>"9840072010",
            "country_id"=>1,
            "email_address"=>"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa@gmail.com",
            "website"=>"https://gitlab.codm/lugmetddy/restaurant",
            "contact_person"=>"avash kafle",
            "commission_rate"=>10.99,
            "status"=>0,
            "translation"=>[
                "en"=>[
                    "name"=>"avashsssssssssssss pvt"
                ],
                "AR"=>[
                    "name"=>"نيبال"
                ]
            ]
        ];

        $this->put("restaurant/$restaurant->id",$restaurants,[])
            ->seeJson([
                "status"=> "200",
                "message"=> "Restaurat Updated Successfully"
            ])->assertResponseStatus(400);
    }

    public function testRestaurantInvalidIdOnUpdate(){
        $restaurants=[
            "user_id"=>1,
            "address"=>"americe",
            "phone_no"=>"4990939",
            "mobile_no"=>"9840072010",
            "country_id"=>1,
            "email_address"=>"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa@gmail.com",
            "website"=>"https://gitlab.codm/lugmetddy/restaurant",
            "contact_person"=>"avash kafle",
            "commission_rate"=>10.99,
            "status"=>0,
            "translation"=>[
                "en"=>[
                    "name"=>"avashsssssssssssss pvt"
                ],
                "AR"=>[
                    "name"=>"نيبال"
                ]
            ]
        ];

        $this->put("restaurant/20",$restaurants,[])
            ->seeJson([
                "status" => "200",
                "message" => "Empty Record for requested id"
            ])->assertResponseStatus(200);
    }

    public function testRestaurantDelete(){
        $restaurant=factory(\App\Restaurant::class)->create();
        $restaurantTranslation=[[
            "restaurant_id"=>$restaurant->id,
            "lang_code"=>"en",
            "name"=>"Bajeko sekwa",
            "description"=>"sekwa corner"
        ],
            [
                "restaurant_id"=>$restaurant->id,
                "lang_code"=>"ar",
                "name"=>"المطاعم",
                "description"=>"مرحبا اسمي اشيش"
            ]];

        $restaurantTranslations=DB::table('restaurant_translation')->insert([
            "restaurant_id"=>$restaurant->id,
            "lang_code"=>"en",
            "name"=>"Bajeko sekwa",
            "description"=>"sekwa corner"
        ],
            [
                "restaurant_id"=>$restaurant->id,
                "lang_code"=>"ar",
                "name"=>"المطاعم",
                "description"=>"مرحبا اسمي اشيش"
            ]);
        $res=$this->delete("restaurant/$restaurant->id")->seeJson([ "status"=>"200",
            "message"=>"Requested Restaurant Deleted Successfully"]);
        /*
         * this step is to check whether the id is deleted or not*/
        $rest=$this->delete("restaurant/$restaurant->id")->seeJson([ "status"=>"200",
            "message"=>"Requested Restaurant could not be found"]);

    }

    public function testRestaurantInvalidIdDelete(){
        $restaurant=factory(\App\Restaurant::class)->create();
        $restaurantTranslation=[[
            "restaurant_id"=>$restaurant->id,
            "lang_code"=>"en",
            "name"=>"Bajeko sekwa",
            "description"=>"sekwa corner"
        ],
            [
                "restaurant_id"=>$restaurant->id,
                "lang_code"=>"ar",
                "name"=>"المطاعم",
                "description"=>"مرحبا اسمي اشيش"
            ]];

        $restaurantTranslations=DB::table('restaurant_translation')->insert([
            "restaurant_id"=>$restaurant->id,
            "lang_code"=>"en",
            "name"=>"Bajeko sekwa",
            "description"=>"sekwa corner"
        ],
            [
                "restaurant_id"=>$restaurant->id,
                "lang_code"=>"ar",
                "name"=>"المطاعم",
                "description"=>"مرحبا اسمي اشيش"
            ]);
        $id=$restaurant->id+1;
        $res=$this->delete("restaurant/$id")->seeJson([
            "status" => "200",
            "message" => "Requested Restaurant could not be found"])->assertResponseStatus(200);

    }



}
