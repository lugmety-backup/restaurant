-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 21, 2017 at 06:46 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lugmety_restaurant_service`
--

-- --------------------------------------------------------

--
-- Table structure for table `branch_translation`
--

CREATE TABLE `branch_translation` (
  `id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `lang_code` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `branch_translation`
--

INSERT INTO `branch_translation` (`id`, `branch_id`, `lang_code`, `name`, `description`) VALUES
(1, 5, 'en', 'Nepaliaa', ''),
(2, 5, 'ar', 'NepalInArabic', ''),
(7, 7, 'en', 'Nepaliaa', ''),
(8, 7, 'ar', 'arabic', ''),
(13, 9, 'en', 'Nepaliaa', ''),
(14, 9, 'ar', 'NepalInArabic', ''),
(15, 14, 'en', 'Nepaliaa', ''),
(16, 14, 'ar', 'NepalInArabic', ''),
(19, 16, 'en', 'Nepaliaa', ''),
(20, 16, 'ar', 'NepalInArabic', ''),
(21, 17, 'en', 'Nepaliaa', ''),
(22, 17, 'ar', 'NepalInArabic', ''),
(23, 18, 'en', 'Nepaliaa', ''),
(24, 18, 'ar', 'NepalInArabic', ''),
(33, 19, 'en', 'Nepaliaa', ''),
(34, 19, 'ar', 'NepalInArabic', ''),
(35, 20, 'en', 'Nepaliaa', ''),
(36, 20, 'ar', 'NepalInArabic', ''),
(37, 21, 'en', 'Nepal', ''),
(38, 21, 'ar', 'NepalInArabic', ''),
(39, 22, 'en', 'Nepal', ''),
(40, 22, 'ar', 'Nepal', ''),
(41, 23, 'en', 'Nepal', ''),
(42, 23, 'ar', 'Nepal', ''),
(43, 24, 'en', 'Nepal', ''),
(44, 24, 'ar', 'Nepal', ''),
(45, 25, 'en', 'Nepal', ''),
(46, 25, 'ar', 'Nepal', ''),
(47, 26, 'en', 'Nepal', ''),
(48, 26, 'ar', 'Nepal', ''),
(49, 27, 'en', 'Nepal', ''),
(50, 27, 'ar', 'Nepal', '');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_zones`
--

CREATE TABLE `delivery_zones` (
  `id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `delivery_zones`
--

INSERT INTO `delivery_zones` (`id`, `branch_id`, `district_id`) VALUES
(2, 6, 171),
(12, 21, 171),
(13, 21, 170),
(18, 5, 170);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(14, '2017_04_26_072323_create_restaurants_table', 1),
(15, '2017_04_26_072424_create_restaurant_branches_table', 1),
(16, '2017_04_26_072603_create_branch_translation_table', 1),
(17, '2017_04_26_072644_create_restaurant_translation_table', 1),
(18, '2017_04_26_072718_create_delivery_options_table', 1),
(19, '2017_04_26_072752_create_available_delivery_opt_table', 1),
(21, '2017_04_26_073501_create_delivery_zones_table', 1),
(22, '2017_04_26_073535_create_available_zones_table', 1),
(23, '2017_04_26_073716_create_restaurant_users_table', 1),
(24, '2017_04_26_074645_create_payment_methods_table', 1),
(25, '2017_04_26_074831_create_available_payment_methods_table', 1),
(26, '2017_04_26_074905_create_payment_method_translations_table', 1),
(27, '2017_04_26_073535_create_delivery_zones_table', 2),
(28, '2017_04_26_073350_create_restaurant_hours_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `restaurants`
--

CREATE TABLE `restaurants` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_no` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `mobile_no` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `email_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `commission_rate` double(5,3) NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_person` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `restaurants`
--

INSERT INTO `restaurants` (`id`, `user_id`, `address`, `phone_no`, `mobile_no`, `country_id`, `email_address`, `commission_rate`, `website`, `contact_person`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'americe', '4990939', '9840072010', 2, 'nnnnnnnn@hazesoft.com', 10.990, 'https://gitlab.codm/lugmetddy/restaurant', 'avash kafle', 1, NULL, '2017-05-07 09:25:51', '2017-05-07 09:25:51'),
(2, 2, '', '9999999999', '9999999999', 2, 'pra@gmail.com', 13.330, '', '', 1, NULL, '2017-05-07 10:09:48', '2017-05-07 10:09:48'),
(3, 3, '', '9999999999', '9999999999', 2, 'asis@gmail.com', 13.330, '', '', 1, NULL, '2017-05-07 10:20:35', '2017-05-07 10:20:35'),
(12, 2, 'americe', '4990939', '9840072010', 2, 'nnnnnnn@hazesoft.com', 10.990, 'https://gitlab.codm/lugmetddy/restaurant', 'avash kafle', 1, NULL, '2017-05-07 11:32:33', '2017-05-07 11:32:33'),
(13, 73, 'dafa', '3432', '334', 2, 'asfaf', 99.999, '234', 'afda', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `restaurant_branches`
--

CREATE TABLE `restaurant_branches` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `city_id` int(10) UNSIGNED NOT NULL,
  `district_id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL,
  `logo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cover_image` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `phone_no` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `mobile_no` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `email_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `commission_rate` double(5,3) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `min_purchase_amt` double(9,3) NOT NULL,
  `max_purchase_amt` double(9,3) NOT NULL,
  `no_of_seats` int(10) UNSIGNED NOT NULL,
  `available_seats` int(10) UNSIGNED NOT NULL,
  `machine_ref_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `freelance_drive_supported` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_featured` smallint(6) NOT NULL,
  `pre_order` smallint(6) NOT NULL DEFAULT '0',
  `methods` text COLLATE utf8_unicode_ci NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `timezone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `restaurant_branches`
--

INSERT INTO `restaurant_branches` (`id`, `country_id`, `city_id`, `district_id`, `parent_id`, `logo`, `cover_image`, `phone_no`, `mobile_no`, `email_address`, `commission_rate`, `slug`, `min_purchase_amt`, `max_purchase_amt`, `no_of_seats`, `available_seats`, `machine_ref_key`, `freelance_drive_supported`, `is_featured`, `pre_order`, `methods`, `latitude`, `longitude`, `timezone`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(5, 2, 1, 1, 1, '/images/590ef26c70bac.png', '/images/590ef26c77048.png', '9999999999', '9999999999', 'pra@gmail.com', 13.330, 'a-b', 20000.000, 20000.000, 14, 10, '909', 'yes', 0, 0, '', 0, 0, '', 1, NULL, '2017-05-07 10:09:48', '2017-05-07 10:09:48'),
(6, 2, 1, 1, 1, '/images/590efe09b0021.png', '/images/590efe09b8f1d.png', '9999999999', '9999999999', 'asis@gmail.com', 13.330, 'a-b', 20000.000, 20000.000, 14, 10, '909', 'yes', 0, 0, '', 0, 0, '', 1, NULL, '2017-05-07 10:20:35', '2017-05-09 10:15:28'),
(7, 2, 1, 1, 2, '/images/590f0626bb1ab.png', '/images/590f0626c1c7d.png', '9999999999', '9999999999', 'branchemail@gmail.com', 13.330, 'a-b', 20000.000, 20000.000, 14, 10, '909', 'yes', 0, 0, '', 0, 0, '', 1, NULL, '2017-05-07 11:33:58', '2017-05-07 11:33:58'),
(8, 2, 1, 1, 12, '/images/59117f5aa04d8.png', '/images/59117f5aac586.png', '9999999999', '9999999999', 'praafdafasd@gmail.com', 13.330, 'a-b', 20000.000, 20000.000, 14, 10, '909', 'yes', 0, 0, '', 0, 0, '', 1, NULL, '2017-05-09 08:34:32', '2017-05-09 08:35:49'),
(9, 2, 1, 1, 12, '/images/59118be29ce89.png', '/images/59118be2a30ad.png', '9999999999', '9999999999', 'praaafdafasd@gmail.com', 13.330, 'a-b', 20000.000, 20000.000, 14, 10, '909', 'yes', 0, 0, '', 0, 0, '', 1, NULL, '2017-05-09 09:29:06', '2017-05-09 09:29:06'),
(10, 2, 1, 1, 12, '/images/5911985d859a1.png', '/images/5911985d8c1a4.png', '9999999999', '9999999999', 'delete@gmail.com', 13.330, 'a-b', 20000.000, 20000.000, 14, 10, '909', 'yes', 0, 0, '', 0, 0, '', 1, NULL, '2017-05-09 10:22:21', '2017-05-09 10:32:30'),
(11, 2, 1, 1, 1, '/images/5912bdbe107c4.png', '/images/5912bdbe1625e.png', '9999999999', '9999999999', 'deletetest@gmail.com', 13.330, 'a-b', 20000.000, 20000.000, 14, 10, '909', 'yes', 0, 0, '', 0, 0, '', 1, '2017-05-10 07:18:09', '2017-05-10 07:14:10', '2017-05-10 07:18:09'),
(12, 2, 1, 1, 1, '/images/5912bed9e323e.png', '/images/5912bed9e8e2c.png', '9999999999', '9999999999', 'deletetest1@gmail.com', 13.330, 'a-b', 20000.000, 20000.000, 14, 10, '909', 'yes', 0, 0, '', 0, 0, '', 1, '2017-05-24 06:56:47', '2017-05-10 07:18:50', '2017-05-24 06:56:47'),
(14, 2, 1, 1, 1, '/images/5923d0247e9bc.png', '/images/5923d02487ce1.png', '9999999999', '9999999999', 'pras@gmail.com', 0.000, 'a-b', 20000.000, 20000.000, 14, 10, '909', '', 0, 0, '', 0, 0, '', 1, NULL, '2017-05-23 06:01:08', '2017-05-23 06:01:08'),
(16, 2, 1, 1, 1, '/images/5923d4d0020b4.png', '/images/5923d4d00882d.png', '9999999999', '9999999999', 'praas@gmail.com', 0.000, 'avashsssssssssssss pvt-kathmandu', 20000.000, 20000.000, 14, 10, '909', '', 0, 0, '', 0, 0, '', 1, NULL, '2017-05-23 06:21:04', '2017-05-23 06:21:04'),
(17, 2, 1, 1, 1, '/images/5923d702bf807.png', '/images/5923d702c859a.png', '9999999999', '9999999999', 'praasa@gmail.com', 0.000, 'avashsssssssssssss pvt-kathmdsdsandu', 20000.000, 20000.000, 14, 10, '909', '', 0, 0, '', 0, 0, '', 1, NULL, '2017-05-23 06:30:27', '2017-05-23 06:30:27'),
(18, 2, 1, 1, 1, '/images/5923d9f0aebe4.png', '/images/5923d9f0b5482.png', '9999999999', '9999999999', 'praassa@gmail.com', 0.000, 'avashsssssssssssss pvt-kathmdsdsasndu', 20000.000, 20000.000, 14, 10, '909', '', 0, 0, '', 0, 0, '', 1, NULL, '2017-05-23 06:42:56', '2017-05-23 06:42:56'),
(19, 2, 1, 1, 1, '/images/5923dd6e3052b.png', '/images/5923dd6e36822.png', '9999999999', '9999999999', 'praasssa@gmail.com', 0.000, 'avashsssssssssssss pvt-kathmdsdssasndusdshhhdsfhhdfs', 20000.000, 20000.000, 14, 10, '909', '', 0, 0, '', 0, 0, '', 1, '2017-05-23 07:01:27', '2017-05-23 06:43:20', '2017-05-23 07:01:27'),
(20, 2, 1, 1, 1, '/images/59256ad69a2af.png', '/images/59256ad7922af.png', '9999999999', '9999999999', 'praassssa@gmail.com', 0.000, 'avashsssssssssssss pvt-kantipur', 20000.000, 20000.000, 14, 10, '909', '', 0, 0, '', 0, 0, '', 1, NULL, '2017-05-24 11:13:31', '2017-05-24 11:13:31'),
(21, 2, 1, 1, 1, '/images/593cede499e8d.png', '/images/593cede4afc3e.png', '1234567890', '1234567890', 'example@gmail.com', 0.000, 'avashsssssssssssss pvt-kathmandu-branch', 20000.000, 20000.000, 50, 25, '100', '', 0, 0, '', 0, 0, '', 1, NULL, '2017-06-11 07:14:44', '2017-06-11 07:14:44'),
(22, 2, 1, 1, 1, '/images/593f882c30418.png', '/images/593f882c3f97f.png', '1234567890', '1234567890', 'exampleadfafd@gmail.com', 0.000, 'avashsssssssssssss pvt-kathassdadmandu-branch', 20000.000, 20000.000, 50, 25, '100', '', 0, 0, '', 0, 0, '', 1, NULL, '2017-06-13 06:37:32', '2017-06-13 06:37:32'),
(23, 2, 1, 1, 1, '/images/593f89104f564.png', '/images/593f8910549b0.png', '1234567890', '1234567890', 'exampledadfafd@gmail.com', 0.000, 'avashsssssssssssss pvt-kathassddadmandu-branch', 20000.000, 20000.000, 50, 25, '100', '', 0, 0, '', 0, 0, '', 1, NULL, '2017-06-13 06:41:20', '2017-06-13 06:41:20'),
(24, 2, 1, 1, 1, '/images/593f893ea8cb3.png', '/images/593f893eaea5c.png', '1234567890', '1234567890', 'examplesdadfafd@gmail.com', 0.000, 'avashsssssssssssss pvt-kathasssddadmandu-branch', 20000.000, 20000.000, 50, 25, '100', '', 0, 0, '', 0, 0, '', 1, NULL, '2017-06-13 06:42:06', '2017-06-13 06:42:06'),
(25, 2, 1, 1, 1, '/images/593f9f9a01eae.png', '/images/593f9f9a874fe.png', '1234567890', '1234567890', 'examsfplesdadfafd@gmail.com', 0.000, 'avashsssssssssssss pvt-kathasssddsadmandu-branch', 20000.000, 20000.000, 50, 25, '100', '', 0, 0, '', 20.1111, 80.1111, '', 1, NULL, '2017-06-13 08:17:31', '2017-06-13 08:17:31'),
(26, 2, 1, 1, 2, '', '', '1234567890', '1234567890', 'afdaffage@gmail.com', 0.000, 'two-kathmandu-branch', 20000.000, 20000.000, 50, 25, '100', '', 0, 0, 'a:2:{s:16:\"shipping_methods\";a:2:{i:0;s:9:\"shipping1\";i:1;s:9:\"shipping2\";}s:15:\"payment_methods\";a:2:{i:0;s:8:\"payment1\";i:1;s:8:\"payment2\";}}', 20, 20, '', 1, NULL, '2017-06-15 08:10:27', '2017-06-15 08:10:27'),
(27, 2, 1, 1, 2, '', '', '1234567890', '1234567890', 'afdaffage@gmaissl.com', 0.000, 'two-kathmandu-branchss', 20000.000, 20000.000, 50, 25, '100', '', 0, 0, 'a:2:{s:16:\"shipping_methods\";a:2:{i:0;s:9:\"shipping1\";i:1;s:9:\"shipping2\";}s:15:\"payment_methods\";a:2:{i:0;s:8:\"payment1\";i:1;s:8:\"payment2\";}}', 20, 20, 'UTC -5:30', 1, NULL, '2017-06-20 07:23:19', '2017-06-20 07:23:19');

-- --------------------------------------------------------

--
-- Table structure for table `restaurant_hours`
--

CREATE TABLE `restaurant_hours` (
  `id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `notice` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `day` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shift` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `opening_time` time NOT NULL,
  `closing_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `restaurant_hours`
--

INSERT INTO `restaurant_hours` (`id`, `branch_id`, `notice`, `day`, `shift`, `opening_time`, `closing_time`) VALUES
(5, 5, '', 'Monday', 'Morning', '08:10:00', '10:00:00'),
(6, 5, '', 'Monday', 'Evening', '09:10:00', '10:00:00'),
(7, 5, '', 'Friday', 'Morning', '07:10:00', '10:00:00'),
(8, 5, '', 'Friday', 'Evening', '09:10:00', '10:00:00'),
(9, 21, '', 'Monday', 'Morning', '08:10:00', '10:00:00'),
(10, 21, '', 'Monday', 'Evening', '09:10:00', '10:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `restaurant_translation`
--

CREATE TABLE `restaurant_translation` (
  `id` int(10) UNSIGNED NOT NULL,
  `restaurant_id` int(10) UNSIGNED NOT NULL,
  `lang_code` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `restaurant_translation`
--

INSERT INTO `restaurant_translation` (`id`, `restaurant_id`, `lang_code`, `name`, `description`) VALUES
(1, 1, 'en', 'avashsssssssssssss pvt', NULL),
(2, 1, 'ar', 'نيبال', NULL),
(3, 12, 'en', 'avashsssssssssssss pvt', NULL),
(4, 12, 'ar', 'نيبال', NULL),
(5, 2, 'en', 'two', NULL),
(6, 3, 'en', 'three', NULL),
(7, 13, 'en', 'thirteen', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `restaurant_users`
--

CREATE TABLE `restaurant_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `restaurant_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `restaurant_users`
--

INSERT INTO `restaurant_users` (`id`, `branch_id`, `restaurant_id`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(2, 5, 1, 1, 1, '2017-05-07 12:01:32', '2017-05-07 12:01:32'),
(3, 5, 1, 90, 1, '2017-05-07 12:21:23', '2017-05-07 12:21:23'),
(4, 5, 1, 52, 1, '2017-05-08 07:54:19', '2017-05-08 07:54:19'),
(5, 6, 1, 58, 1, '2017-05-09 06:26:38', '2017-05-09 06:26:38'),
(14, 12, 12, 106, 1, '2017-05-10 07:43:47', '2017-05-10 07:43:47'),
(15, 6, 1, 109, 1, '2017-05-17 05:37:38', '2017-05-17 05:37:38'),
(16, 6, 1, 110, 1, '2017-05-17 06:14:54', '2017-05-17 06:14:54'),
(17, 6, 1, 60, 1, '2017-05-17 06:21:44', '2017-05-17 06:21:44'),
(18, 6, 1, 111, 1, '2017-05-17 08:15:48', '2017-05-17 08:15:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branch_translation`
--
ALTER TABLE `branch_translation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `branch_translation_branch_id_foreign` (`branch_id`);

--
-- Indexes for table `delivery_zones`
--
ALTER TABLE `delivery_zones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `restaurants`
--
ALTER TABLE `restaurants`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `restaurants_email_address_unique` (`email_address`);

--
-- Indexes for table `restaurant_branches`
--
ALTER TABLE `restaurant_branches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `restaurant_branches_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `restaurant_hours`
--
ALTER TABLE `restaurant_hours`
  ADD PRIMARY KEY (`id`),
  ADD KEY `restaurant_hours_branch_id_foreign` (`branch_id`);

--
-- Indexes for table `restaurant_translation`
--
ALTER TABLE `restaurant_translation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `restaurant_translation_restaurant_id_foreign` (`restaurant_id`);

--
-- Indexes for table `restaurant_users`
--
ALTER TABLE `restaurant_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `restaurant_users_branch_id_foreign` (`branch_id`),
  ADD KEY `restaurant_users_restaurant_id_foreign` (`restaurant_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `branch_translation`
--
ALTER TABLE `branch_translation`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `delivery_zones`
--
ALTER TABLE `delivery_zones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `restaurants`
--
ALTER TABLE `restaurants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `restaurant_branches`
--
ALTER TABLE `restaurant_branches`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `restaurant_hours`
--
ALTER TABLE `restaurant_hours`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `restaurant_translation`
--
ALTER TABLE `restaurant_translation`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `restaurant_users`
--
ALTER TABLE `restaurant_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `branch_translation`
--
ALTER TABLE `branch_translation`
  ADD CONSTRAINT `branch_translation_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `restaurant_branches` (`id`);

--
-- Constraints for table `restaurant_branches`
--
ALTER TABLE `restaurant_branches`
  ADD CONSTRAINT `restaurant_branches_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `restaurants` (`id`);

--
-- Constraints for table `restaurant_hours`
--
ALTER TABLE `restaurant_hours`
  ADD CONSTRAINT `restaurant_hours_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `restaurant_branches` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `restaurant_translation`
--
ALTER TABLE `restaurant_translation`
  ADD CONSTRAINT `restaurant_translation_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`);

--
-- Constraints for table `restaurant_users`
--
ALTER TABLE `restaurant_users`
  ADD CONSTRAINT `restaurant_users_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `restaurant_branches` (`id`),
  ADD CONSTRAINT `restaurant_users_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
