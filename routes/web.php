<?php


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$app->group(['country id ,district id and city id and restaurant slug  check route'], function () use ($app) {
    $app->post("restaurant/check", [
        "uses" => "RestaurantController@checkCountryId"
    ]);
    $app->post("restaurant/branch/check", [
        "uses" => "RestaurantBranchController@checkBranchDistrictCityId"
    ]);
    $app->post("check/slug", [
        "uses" => "RestaurantBranchController@checkSlug"
    ]);

});

$app->get("client/ip", [
    "uses" => "IpController@getClientIp"
]);


//$app->get("foo",function (){
//        \Amqp::consume('test', function ($message, $resolver) {
//            var_dump($message->body);
//            \Illuminate\Support\Facades\Log::info($message->body);
//        $resolver->acknowledge($message);
////        $resolver->stopWhenProcessed();
//        }, [
//            'exchange' => 'amq.fanout',
//            'exchange_type' => 'fanout',
//            'queue_force_declare' => true,
//            'queue_exclusive' => false,
//            'persistent' => true// required if you want to listen forever
//        ]);
//});


$app->group(['Restaurant branch sort group'], function () use ($app) {
    $app->get("restaurant/branch/sort", [
        "middleware" => "checkTokenPermission:view-restaurant-branch-sort",
        "uses" => "RestaurantBranchSortingController@viewRestaurantSorting"
    ]);

    $app->post("restaurant/branch/sort", [
        "middleware" => "checkTokenPermission:create-restaurant-branch-sort",
        "uses" => "RestaurantBranchSortingController@createRestaurantSorting"
    ]);

});

$app->group(['Cuisine group'], function () use ($app) {


    $app->get("public/cuisine", [
        "uses" => "CuisineController@getAllCuisineForPublic"
    ]);

    $app->get('cuisine/{id:[0-9]+}/branch', [
        "middleware" => "checkTokenPermission:view-cuisine",
        "uses" => "CuisineController@getNonFeaturedCuisineBranch"
    ]);

    $app->get("datatable/cuisine", [
        "middleware" => "checkTokenPermission:view-cuisine",
        "uses" => "CuisineController@getAllCuisineForAdmin"
    ]);
    $app->get("cuisine/{id:[0-9]+}", [
        "middleware" => "checkTokenPermission:view-cuisine",
        "uses" => "CuisineController@getSpecificCuisineForAdmin"
    ]);
    $app->post("restaurant/{id:[0-9]+}/attach/cuisine", [
        "middleware" => "checkTokenPermission:attach-cuisine",
        "uses" => "CuisineController@attachCuisine"
    ]);

    $app->post("restaurant/{id:[0-9]+}/detach/cuisine", [
        "middleware" => "checkTokenPermission:detach-cuisine",
        "uses" => "CuisineController@detachCuisine"
    ]);
    $app->post("cuisine", [
        "middleware" => "checkTokenPermission:create-cuisine",
        "uses" => "CuisineController@createCuisine"
    ]);

    $app->put("cuisine/{id:[0-9]+}", [
        "middleware" => "checkTokenPermission:edit-cuisine",
        "uses" => "CuisineController@updateCuisine"
    ]);
    $app->delete("cuisine/{id:[0-9]+}", [
        "middleware" => "checkTokenPermission:delete-cuisine",
        "uses" => "CuisineController@deleteCuisine"
    ]);
});


$app->group(['Category Sort group'], function () use ($app) {

    $app->get("category/sort/{branchId:[0-9]+}", [
        "middleware" => "checkTokenPermission",
        "uses" => "BranchCategorySortController@viewCategorySorting"
    ]);

    $app->post("category/sort", [
        "middleware" => "checkTokenPermission",
        "uses" => "BranchCategorySortController@createCategorySorting"
    ]);

    $app->put("category/sort", [
        "middleware" => "checkTokenPermission",
        "uses" => "BranchCategorySortController@removeCategorySorting"
    ]);


});


$app->group(['Branch Sort group'], function () use ($app) {

    $app->get("branch/sort/cuisine/{cuisine_id:[0-9]+}", [
        "middleware" => "checkTokenPermission:view-cuisine-branch-sort",
        "uses" => "CuisineBranchSortingController@viewBranchSorting"
    ]);

    $app->post("branch/sort", [
        "middleware" => "checkTokenPermission:create-cuisine-branch-sort",
        "uses" => "CuisineBranchSortingController@createBranchSorting"
    ]);

    $app->post('add/branch', [
        'middleware' => 'checkTokenPermission:create-cuisine-branch-sort',
        "uses" => "CuisineBranchSortingController@addNewFeaturedRestaurant"
    ]);

    $app->delete("cuisine/{cuisine_id}/branch/{branch_id}", [
        "middleware" => "checkTokenPermission:delete-cuisine-branch-sort",
        "uses" => "CuisineBranchSortingController@removeFeaturedRestaurant"
    ]);


});
$app->group(['feature restaurant sorting route'], function () use ($app) {
    $app->get("admin/featured/restaurant/sorting", [
        'middleware' => 'checkTokenPermission:view-feature-restaurant-sorting',
        "uses" => "FeaturedRestaurantSortingController@viewFeaturedRestaurantSorting"
    ]);

    $app->post('admin/featured/restaurant/sorting',[
        'middleware' => 'checkTokenPermission:create-feature-restaurant-sorting',
        'uses' => 'FeaturedRestaurantSortingController@addBranchInSorting'
    ]);
    $app->put('admin/featured/restaurant/sorting',[
        'middleware' => 'checkTokenPermission:update-feature-restaurant-sorting',
        'uses' => 'FeaturedRestaurantSortingController@createBranchSortingbyAdmin'
    ]);
    $app->delete('admin/featured/restaurant/sorting',[
        'middleware' => 'checkTokenPermission:delete-feature-restaurant-sorting',
        'uses' => 'FeaturedRestaurantSortingController@deleteFeatureRestaurantFromSorting'
    ]);

});

//used to validate role for food display
$app->get('restaurant/branch/{branch_id:[0-9]+}/checkAuthorityForFood/{user_id: [0-9]+}', ['uses' => 'RestaurantBranchController@checkAuthorityForFood']);
// restaurant branch routes
$app->group(['restaurant branch group'], function () use ($app) {

//    $app->get('restaurant/{restaurant_id:[0-9]+}/branch', ['middleware' => 'checkTokenPermission:restaurant-branch-view',
//        'uses' => 'RestaurantBranchController@index']);

    $app->get('datatable/restaurant/{restaurant_id:[0-9]+}/branch', [
        'middleware' => 'checkTokenPermission:restaurant-branch-view',
        'uses' => 'RestaurantBranchController@adminIndex']);

    $app->get('restaurant/{restaurant_id:[0-9]+}/branch', [
        'middleware' => 'checkTokenPermission:restaurant-branch-view',
        'uses' => 'RestaurantBranchController@index']);


    $app->get('notification/restaurant/{branch_id:[0-9]+}/branch', [
//        'middleware' => 'checkTokenPermission:restaurant-branch-view',
        'uses' => 'RestaurantBranchController@getSpecificBranchForNotification']);

    $app->get('restaurant/{restaurant_id:[0-9]+}/branch/{branch_id}', ['middleware' => 'checkTokenPermission:restaurant-branch-view',
        'uses' => 'RestaurantBranchController@show']);

    $app->get('public/restaurant/branch', ['uses' => 'RestaurantBranchController@search']);

    $app->get('search/branch', ['uses' => 'RestaurantBranchController@search']);

    $app->get('public/restaurant/branch/{branch_id:[0-9]+}/detail', ['uses' => 'RestaurantBranchController@publicShowWithAllDetails']);

    //gets list of restaurants for drivers, internal call

    $app->post('public/branch', ['uses' => 'RestaurantBranchController@getAllRestaurantForDrivers']);

    $app->get('public/restaurant/branch/{branch_id}', ['uses' => 'RestaurantBranchController@publicShow']);

    $app->post('restaurant/{restaurant_id:[0-9]+}/branch', ['middleware' => 'checkTokenPermission:restaurant-branch-create',
        'uses' => 'RestaurantBranchController@store']);

    $app->put('restaurant/{restaurant_id:[0-9]+}/branch/{branch_id:[0-9]+}',
        [
            'middleware' => 'checkTokenPermission:restaurant-branch-edit',
            'uses' => 'RestaurantBranchController@update']);

    $app->delete('restaurant/{restaurant_id:[0-9]+}/branch/{branch_id:[0-9]+}', ['middleware' => 'checkTokenPermission:restaurant-branch-delete',
        'uses' => 'RestaurantBranchController@destroy']);

    $app->get('restaurant/{branch_id}/pre-order/time', [
        'uses' => 'RestaurantBranchController@timeListOfOpenRestaurantHours']);
});
$app->group(['Restaurant user route group'], function () use ($app) {

    $app->get('restaurant/{restaurant_id}/user', ['middleware' => 'checkTokenPermission:restaurant-user-view',
        'uses' => 'RestaurantUserController@restaurantUserViewAll']);

    $app->post('restaurant/{restaurant_id}/user', ['middleware' => 'checkTokenPermission:restaurant-user-create',
        'uses' => 'RestaurantUserController@restaurantUserCreate']);

//    $app->put('restaurant/{restaurant_id}/user', ['middleware' => 'checkTokenPermission:restaurant-user-edit',
//        'uses' => 'RestaurantUserController@restaurantUserEdit']); //remaining updating user

    $app->delete('restaurant/{restaurant_id}/user/{id}', ['middleware' => 'checkTokenPermission:restaurant-user-delete',
        'uses' => 'RestaurantUserController@restaurantUserDelete']);


});


$app->group(['Restaurant route group'], function () use ($app) {

    $app->get('datatable/restaurant', ['middleware' => 'checkTokenPermission:restaurant-view',
        'uses' => 'RestaurantController@adminIndex']);

    $app->get('public/restaurant/{countryId:[0-9]+}', [
        'uses' => 'RestaurantController@index']);

    $app->get('restaurant/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:restaurant-view',
        'uses' => 'RestaurantController@show']);

    $app->get('oauth/restaurant/{id:[0-9]+}', [
        'uses' => 'RestaurantController@showForOauthCall']);

    $app->post('restaurant', ['middleware' => 'checkTokenPermission:restaurant-create',
        'uses' => 'RestaurantController@store']);

    $app->put('restaurant/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:restaurant-edit',
        'uses' => 'RestaurantController@update']);

    $app->delete('restaurant/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:restaurant-delete',
        'uses' => 'RestaurantController@destroy']);
});

$app->group(['Restaurant Branch Users route group'], function () use ($app) {

    $app->get('restaurant/{restaurant_id:[0-9]+}/branch/{branch_id:[0-9]+}/user', ['middleware' => 'checkTokenPermission:branch-user-view',
        'uses' => 'BranchUserController@index']);

    $app->post('restaurant/{restaurant_id:[0-9]+}/branch/{branch_id:[0-9]+}/user', ['middleware' => 'checkTokenPermission:branch-user-create',
        'uses' => 'BranchUserController@store']);

    $app->delete('restaurant/{restaurant_id:[0-9]+}/branch/{branch_id:[0-9]+}/user/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:branch-user-delete',
        'uses' => 'BranchUserController@destroy']);
});
$app->group(['Restaurant Delivery Zone route group'], function () use ($app) {

    $app->get('datatable/restaurant/{restaurant_id:[0-9]+}/branch/{branch_id:[0-9]+}/delivery-zone', ['middleware' => 'checkTokenPermission:restaurant-delivery-zone-view',
        'uses' => 'DeliveryZoneController@index']);

    $app->get('public/restaurant/branch/{branch_id:[0-9]+}/delivery-zone', [
        'uses' => 'DeliveryZoneController@indexPublic']);

//    $app->get('restaurant/{restaurant_id:[0-9]+}/branch/{branch_id:[0-9]+}/delivery-zone/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:restaurant-delivery-zone-view',
//        'uses' => 'DeliveryZoneController@show']);
    $app->get('public/restaurant/branch/{branch_id:[0-9]+}/delivery-zone/{id:[0-9]+}', [
        'uses' => 'DeliveryZoneController@showPublic']);

    $app->post('restaurant/{restaurant_id:[0-9]+}/branch/{branch_id:[0-9]+}/delivery-zone', ['middleware' => 'checkTokenPermission:restaurant-delivery-zone-create',
        'uses' => 'DeliveryZoneController@store']);

    $app->put('restaurant/{restaurant_id:[0-9]+}/branch/{branch_id:[0-9]+}/delivery-zone', ['middleware' => 'checkTokenPermission:restaurant-delivery-zone-edit',
        'uses' => 'DeliveryZoneController@update']);

    $app->delete('restaurant/{restaurant_id:[0-9]+}/branch/{branch_id:[0-9]+}/delivery-zone', ['middleware' => 'checkTokenPermission:restaurant-delivery-zone-delete',
        'uses' => 'DeliveryZoneController@destroy']);
});

$app->group(['Restaurant  Hour route group'], function () use ($app) {

    $app->get('public/restaurant/branch/{branch_id:[0-9]+}/hour', [
        'uses' => 'RestaurantHourController@publicIndex']);

    $app->get('datatable/restaurant/branch/{branch_id:[0-9]+}/hour', ['middleware' => 'checkTokenPermission:restaurant-hour-view',
        'uses' => 'RestaurantHourController@adminIndex']);

    $app->get('restaurant/branch/{branch_id:[0-9]+}/hour', ['middleware' => 'checkTokenPermission:restaurant-hour-view',
        'uses' => 'RestaurantHourController@adminShow']);

    $app->get('restaurant/branch/{branch_id:[0-9]+}/hour/v2', ['middleware' => 'checkTokenPermission:restaurant-hour-view',
        'uses' => 'RestaurantHourController@adminShowV2']);

    $app->get('restaurant/branch/{branch_id:[0-9]+}/hour/{day}', ['middleware' => 'checkTokenPermission:restaurant-hour-view',
        'uses' => 'RestaurantHourController@show']);

    $app->post('restaurant/branch/{branch_id:[0-9]+}/hour', ['middleware' => 'checkTokenPermission:restaurant-hour-create',
        'uses' => 'RestaurantHourController@store']);

    $app->put('restaurant/branch/{branch_id:[0-9]+}/hour', ['middleware' => 'checkTokenPermission:restaurant-hour-edit',
        'uses' => 'RestaurantHourController@update']);

    $app->delete('restaurant/branch/hour/{id:[0-9]+}', ['middleware' => 'checkTokenPermission:restaurant-hour-delete',
        'uses' => 'RestaurantHourController@destroy']);
});


/**
 * Prefix 'public' is used for non logged in user.
 *
 */
/**
 * routes related to category
 */
$app->group(['category group'], function () use ($app) {
    /**
     * Get all categories according to role of user.
     * If user is admin or super admin, get all the categories regardless the status.
     * Else display active categories. Laravel pagination is used.
     * Permission used: category-view
     */
    $app->get('category', [
        'middleware' => 'checkTokenPermission:category-view',
        'uses' => 'CategoryController@index'
    ]);

    /**
     * Create new category.
     * Only admin or super admin can access this route.
     * permission used: category-create
     */
    $app->post('category', [
        'middleware' => 'checkTokenPermission:category-create',
        'uses' => 'CategoryController@store'
    ]);

    /**
     * Update existing category.
     * Only admin or super admin can access this route.
     * permission used: category-edit
     */
    $app->put('category/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:category-edit',
        'uses' => 'CategoryController@update'
    ]);

    /**
     * Delete existing category
     * Only admin or super admin access this route.
     * Permission used: category-delete
     */
    $app->delete('category/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:category-delete',
        'uses' => 'CategoryController@destroy'
    ]);

    /**
     * Get specific category of given id
     * If logged in user is admin or super admin, get category although the status is inactive.
     * Else only get active category. If status is inactive, displays 404 error.
     * Permission used: category-view
     */
    $app->get('category/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:category-view',
        'uses' => 'CategoryController@show'
    ]);

    /**
     * Get categories for non logged in user.
     * Only active categories are fetched.
     * No permission used.
     *
     */
    $app->get('public/category', [
        'uses' => 'CategoryController@index'
    ]);

    /**
     * Get specific category for non logged in user.
     * If specific category is inactive or not available, displays 404 error.
     * No permission used.
     */
    $app->get('public/category/{id:[0-9]+}', [
        'uses' => 'CategoryController@show'
    ]);

    /**
     * Get categories for admin dashboard.
     * Categories are fetched in datatable json format.
     * Permission used: admin-category-view
     */
    $app->get('admin/category', [
        'as' => 'category',
        'middleware' => 'checkTokenPermission:admin-category-view',
        'uses' => 'CategoryController@getForDatatable'
    ]);

    /**
     * Get specific category and its all related translations for admin dashboard.
     * Permission used: admin-category-view
     */
    $app->get('admin/category/{id: [0-9]+}', [
        'middleware' => 'checkTokenPermission:admin-category-view',
        'uses' => 'CategoryController@adminShow'
    ]);

    $app->get('admin/categoryList/{countryId: [0-9]+}', [

        'middleware' => 'checkTokenPermission:admin-category-view',
        'uses' => 'CategoryController@activeCategoriesForAdmin'
    ]);
});

/**
 * Routes related to food.
 *
 */

$app->group(['food group'], function () use ($app) {
    /**
     * Get all the food related to given restaurant according to role of logged in user.
     * If user is admin or super admin, get all the food regardless the status.
     * Else display active food. Laravel pagination is used.
     * Permission used: food-view
     */
    $app->get('restaurant/{restaurantId: [0-9]+}/food', [
        'middleware' => 'checkTokenPermission:food-view',
        'uses' => 'FoodController@index'
    ]);

    /**
     * Create new food under the given restaurant.
     * Only authorized personnel can access this route.
     * Permission used: food-create
     */
    $app->post('restaurant/{restaurantId: [0-9]+}/food', [
        'middleware' => 'checkTokenPermission:food-create',
        'uses' => 'FoodController@store'
    ]);

    /**
     * Update existing food related to given restaurant.
     * Only authorized personnel can access this route.
     * Permission used: food-edit
     */
    $app->put('restaurant/{restaurantId: [0-9]+}/food/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:food-edit',
        'uses' => 'FoodController@update'
    ]);

//check if the food is out of stock or not as well as inactive
    $app->post('check/foods', [
//        'middleware' => 'checkTokenPermission:food-create',
        'uses' => 'FoodController@checkFoodStatus'
    ]);

    /**
     * Delete existing food related to given restaurant
     * Only authorized personnel can access this route.
     * Permission used: food-delete
     */
    $app->delete('restaurant/{restaurantId: [0-9]+}/food/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:food-delete',
        'uses' => 'FoodController@destroy'
    ]);

    /**
     * Get specific food related to given restaurant for logged in user.
     * If logged in user is authorized person, get food although the status is inactive.
     * Else only get active food. If status is inactive, displays 404 error.
     * Permission used: food-view
     */
    $app->get('restaurant/{restaurantId: [0-9]+}/food/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:food-view',
        'uses' => 'FoodController@show'
    ]);

    /**
     * Get food related to given restaurant for non logged in user.
     * Only active food are fetched.
     * No permission used.
     */
    $app->get('public/restaurant/{restaurantId: [0-9]+}/food', [
        'uses' => 'FoodController@index'
    ]);

    /**
     * Get specific food related to given restaurant for non logged in user.
     * If specific food is inactive or not available, displays 404 error.
     * No permission used.
     */
    $app->get('public/restaurant/{restaurantId: [0-9]+}/food/{id:[0-9]+}', [
        'uses' => 'FoodController@publicShow'
    ]);

    /**
     * Get food related to given restaurant for admin dashboard.
     * Food are fetched in datatable json format.
     * Permission used: admin-food-view
     */
    $app->get('admin/restaurant/{restaurantId: [0-9]+}/food', [
        'as' => 'food',
        'middleware' => 'checkTokenPermission:admin-food-view',
        'uses' => 'FoodController@getForDatatable'
    ]);

    /**
     * Get specific food of given restaurant and its all related translations for admin dashboard.
     * Permission used: admin-food-view
     */
    $app->get('admin/restaurant/{restaurantId: [0-9]+}/food/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:admin-food-view',
        'uses' => 'FoodController@adminShow'
    ]);

    $app->put('branch-operator/food/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:operator-food-update',
        'uses' => 'FoodController@operatorUpdate'
    ]);
    $app->get('branch-operator/food', [
        'middleware' => 'checkTokenPermission:operator-food-update',
        'uses' => 'FoodController@operatorFoodView'
    ]);

});

/**
 * Routes related to addon category.
 *
 */

$app->group(['addon category group'], function () use ($app) {
    /**
     * Get all the addon category related to given restaurant according to role of logged in user.
     * If user is admin or super admin, get all the addon categories regardless the status.
     * Else display active addon categories. Laravel pagination is used.
     * Permission used: addon-category-view
     */

    $app->get('restaurant/{restaurantId: [0-9]+}/addon-category', [
        'middleware' => 'checkTokenPermission:addon-category-view',
        'uses' => 'AddOnCategoryController@index'
    ]);

    /**
     * Create new addon category under the given restaurant.
     * Only authorized personnel can access this route.
     * Permission used: addon-category-create
     */
    $app->post('restaurant/{restaurantId: [0-9]+}/addon-category', [
        'middleware' => 'checkTokenPermission:addon-category-create',
        'uses' => 'AddOnCategoryController@store'
    ]);

    /**
     * Update existing addon category related to given restaurant.
     * Only authorized personnel can access this route.
     * Permission used: addon-category-edit
     */
    $app->put('restaurant/{restaurantId: [0-9]+}/addon-category/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:addon-category-edit',
        'uses' => 'AddOnCategoryController@update'
    ]);

    /**
     * Delete existing addon category related to given restaurant.
     * Only authorized personnel can access this route.
     * ermission used: addon-category-delete
     */
    $app->delete('restaurant/{restaurantId: [0-9]+}/addon-category/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:addon-category-delete',
        'uses' => 'AddOnCategoryController@destroy'
    ]);

    /**
     * Get specific addon category related to given restaurant for logged in user.
     * If logged in user is authorized person, get food although the status is inactive.
     * Else only get active addon category. If status is inactive, displays 404 error.
     * Permission used: addon-category-view
     */
    $app->get('restaurant/{restaurantId: [0-9]+}/addon-category/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:addon-category-view',
        'uses' => 'AddOnCategoryController@show'
    ]);

    /**
     * Get addon categories related to given restaurant for non logged in user.
     * Only active addon category are fetched.
     * No permission used.
     */
    $app->get('public/restaurant/{restaurantId: [0-9]+}/addon-category', [
        'uses' => 'AddOnCategoryController@index'
    ]);

    /**
     * Get specific addon category of related restaurant for non logged in user.
     * If the addon category is inactive or not persent, 404 error is displayed.
     * No permission used.
     */
    $app->get('public/restaurant/{restaurantId: [0-9]+}/addon-category/{id:[0-9]+}', [
        'uses' => 'AddOnCategoryController@show'
    ]);

    /**
     * Get addon categories related to given restaurant for admin dashboard.
     * Addon Categories are fetched in datatable json format.
     * Permission used: admin-addon-category-view
     */
    $app->get('admin/restaurant/{restaurantId: [0-9]+}/addon-category', [
        'as' => 'addon-category',
        'middleware' => 'checkTokenPermission:admin-addon-category-view',
        'uses' => 'AddOnCategoryController@getForDatatable'
    ]);

    /**
     * Get specific addon category of given restaurant and its all related translations for admin dashboard.
     * Permission used: admin-addon-category-view
     */
    $app->get('admin/restaurant/{restaurantId: [0-9]+}/addon-category/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:admin-addon-category-view',
        'uses' => 'AddOnCategoryController@adminShow'
    ]);


});

/**
 * Routes related to addon.
 *
 */
$app->group(['addon group'], function () use ($app) {

    /**
     * Get all the addon related to given restaurant according to role of logged in user.
     * If user is admin or super admin, get all the addon regardless the status.
     * Else display active food. Laravel pagination is used.
     * Permission used: addon-view
     */
    $app->get('restaurant/{restaurantId: [0-9]+}/addon', [
        'middleware' => 'checkTokenPermission',
        'uses' => 'AddOnController@index'
    ]);

    /**
     * Create new addon under the given restaurant.
     * Only authorized personnel can access this route.
     * Permission used: addon-create
     */
    $app->post('restaurant/{restaurantId: [0-9]+}/addon', [
        'middleware' => 'checkTokenPermission:addon-create',
        'uses' => 'AddOnController@store'
    ]);

    /**
     * Update existing addon under the given restaurant.
     * Only authorized personnel can access this route.
     * Permission used: addon-edit
     */
    $app->put('restaurant/{restaurantId: [0-9]+}/addon/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:addon-edit',
        'uses' => 'AddOnController@update'
    ]);

    /**
     * Delete existing addon of the given restaurant
     * Only authorized personnel can access this route.
     * Permission used: addon-delete
     */
    $app->delete('restaurant/{restaurantId: [0-9]+}/addon/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:addon-delete',
        'uses' => 'AddOnController@destroy'
    ]);

    /**
     * Get specific addon related to given restaurant for logged in user.
     * If logged in user is authorized person, get food although the status is inactive.
     * Else only get active addon. If status is inactive, displays 404 error.
     * Permission used: addon-view
     */
    $app->get('restaurant/{restaurantId: [0-9]+}/addon/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:addon-view',
        'uses' => 'AddOnController@show'
    ]);

    /**
     * Get addons related to given restaurant for non logged in user.
     * Only active addons are fetched.
     * No permission used.
     */
    $app->get('public/restaurant/{restaurantId: [0-9]+}/addon', [
        'uses' => 'AddOnController@index'
    ]);

    /**
     * Get specific addon of related restaurant for non logged in user.
     * If the addon is inactive or not persent, 404 error is displayed.
     * No permission used.
     */
    $app->get('public/restaurant/{restaurantId: [0-9]+}/addon/{id:[0-9]+}', [
        'uses' => 'AddOnController@show'
    ]);

    /**
     * Get addon related to given restaurant for admin dashboard.
     * Addons are fetched in datatable json format.
     * Permission used: admin-addon-view
     */
    $app->get('admin/restaurant/{restaurantId: [0-9]+}/addon', [
        'as' => 'addon',
        'middleware' => 'checkTokenPermission:admin-addon-view',
        'uses' => 'AddOnController@getForDatatable'
    ]);

    /**
     * Get specific addon of given restaurant and its all related translations for admin dashboard.
     * Permission used: admin-addon-view
     */
    $app->get('admin/restaurant/{restaurantId: [0-9]+}/addon/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:admin-addon-view',
        'uses' => 'AddOnController@adminShow'
    ]);


});

/**
 * Routes for food-category relation
 */

$app->group(['food category relation group'], function () use ($app) {

    /**
     * route to get list of categories related to food of given id for logged in users.
     * Permission used: food-category-relation-view
     */
    $app->get('restaurant/{restaurantId: [0-9]+}/food/{id:[0-9]+}/category', [
        'middleware' => 'checkTokenPermission:food-category-relation-view',
        'uses' => 'FoodCategoryController@index'
    ]);

    /**
     * route to attach category_id to the given food id
     * Only authorized personnel have access to this route.
     * Permission used: food-category-attach
     */
    $app->post('restaurant/{restaurantId: [0-9]+}/food/{id:[0-9]+}/category', [
        'middleware' => 'checkTokenPermission:food-category-attach',
        'uses' => 'FoodCategoryController@store'
    ]);

    /**
     * route to detach category_id from given food_id for logged in user
     * Only authorized personnel have access to this route.
     * Permission used: food-category-detach
     */
    $app->delete('restaurant/{restaurantId: [0-9]+}/food/{id: [0-9]+}/category', [
        'middleware' => 'checkTokenPermission:food-category-detach',
        'uses' => 'FoodCategoryController@destroy'
    ]);

    /**
     * route to get list of food related to category of given id for logged in user
     * Permission used: food-category-relation-view
     */
    $app->get('restaurant/{restaurantId: [0-9]+}/category/{id:[0-9]+}/food', [
        'middleware' => 'checkTokenPermission:food-category-relation-view',
        'uses' => 'FoodCategoryController@getFoodByCategory'
    ]);

    /**
     * route to get list of categories related to food of given id for non logged in users.
     * No Permission used
     */
    $app->get('public/restaurant/{restaurantId: [0-9]+}/food/{id:[0-9]+}/category', [
        'uses' => 'FoodCategoryController@index'
    ]);

    /**
     * route to get list of food related to category of given id for logged in user
     * No permission used
     */
    $app->get('public/restaurant/{restaurantId: [0-9]+}/category/{id:[0-9]+}/food', [
        'uses' => 'FoodCategoryController@getFoodByCategory'
    ]);


    $app->post("foods/category", [
        'uses' => 'FoodCategoryController@getFoodsCategory'
    ]);


});

/**
 * Routes for food-addon category relation
 */
$app->group(['food addoncategory relation group'], function () use ($app) {

    /**
     * route to get list of categories related to food of given id for logged in user
     * Permission used: food-addoncategory-view
     */
    $app->get('restaurant/{restaurantId: [0-9]+}/food/{id:[0-9]+}/addon-category', [
        'middleware' => 'checkTokenPermission:food-addoncategory-view',
        'uses' => 'FoodAddonCategoryController@index'
    ]);

    /**
     * route to attach category_id to the given food id for logged in user
     * Only authorized personnel have access to this route.
     * Permission used: food-addoncategory-detach
     */
    $app->post('restaurant/{restaurantId: [0-9]+}/food/{id:[0-9]+}/addon-category', [
        'middleware' => 'checkTokenPermission:food-addoncategory-detach',
        'uses' => 'FoodAddonCategoryController@store'
    ]);

    /**
     * route to detach category_id from given food_id
     * Only authorized personnel have access to this route.
     * Permission used: food-addoncategory-detach
     */
    $app->delete('restaurant/{restaurantId: [0-9]+}/food/{id: [0-9]+}/addon-category', [
        'middleware' => 'checkTokenPermission:food-addoncategory-detach',
        'uses' => 'FoodAddonCategoryController@destroy'
    ]);

    /**
     * route to get list of food related to category of given id
     * Permission used: food-addoncategory-view
     */
    $app->get('restaurant/{restaurantId: [0-9]+}/addon-category/{id:[0-9]+}/food', [
        'middleware' => 'checkTokenPermission:food-addoncategory-view',
        'uses' => 'FoodAddonCategoryController@getFoodByCategory'
    ]);

    /**
     * route to get list of categories related to food for non logged in user
     * No permission used
     */
    $app->get('public/restaurant/{restaurantId: [0-9]+}/food/{id:[0-9]+}/addon-category', [
        'uses' => 'FoodAddonCategoryController@index'
    ]);

    /**
     * route to get list of food related to addon category for non logged in user
     * No permission used
     */
    $app->get('public/restaurant/{restaurantId: [0-9]+}/addon-category/{id:[0-9]+}/food', [
        'uses' => 'FoodAddonCategoryController@getFoodByCategory'
    ]);


});

/**
 * routes for addon-addoncategory relation
 */
$app->group(['addon addoncategory relation group'], function () use ($app) {

    /**
     * route to get list of categories related to addon of given id for logged in user
     * Permission used: addon-addoncategory-view
     */
    $app->get('restaurant/{restaurantId: [0-9]+}/addon/{id:[0-9]+}/addon-category', [
        'middleware' => 'checkTokenPermission:addon-addoncategory-view',
        'uses' => 'AddonAddonCategoryController@index'
    ]);

    /**
     * route to attach category_id to the given addon id for logged in user
     * Only authorized personnel has access to this route
     * Permission used: addon-addoncategory-attach
     */
    $app->post('restaurant/{restaurantId: [0-9]+}/addon/{id:[0-9]+}/addon-category', [
        'middleware' => 'checkTokenPermission:addon-addoncategory-attach',
        'uses' => 'AddonAddonCategoryController@store'
    ]);

    /**
     * route to detach category_id from given addon_id for logged in user
     * Only authorized personnel has access to this route
     * Permission used: addon-addoncategory-detach
     */
    $app->delete('restaurant/{restaurantId: [0-9]+}/addon/{id: [0-9]+}/addon-category', [
        'middleware' => 'checkTokenPermission:addon-addoncategory-detach',
        'uses' => 'AddonAddonCategoryController@destroy'
    ]);

    /**
     * route to get list of addons related to addon category of given id for logged in user
     * Permission used: addon-addoncategory-view
     */
    $app->get('restaurant/{restaurantId: [0-9]+}/addon-category/{id:[0-9]+}/addon', [
        'middleware' => 'checkTokenPermission:addon-addoncategory-view',
        'uses' => 'AddonAddonCategoryController@getAddonByCategory'
    ]);


    /**
     * route to get list of categories related to addon of given id for non logged in user
     * No permission used.
     */
    $app->get('public/restaurant/{restaurantId: [0-9]+}/addon/{id:[0-9]+}/addon-category', [
        'uses' => 'AddonAddonCategoryController@index'
    ]);

    /**
     * route to get list of addons related to addon category of given id for non logged in user
     * No permission used.
     */
    $app->get('public/restaurant/{restaurantId: [0-9]+}/addon-category/{id:[0-9]+}/addon', [
        'uses' => 'AddonAddonCategoryController@getAddonByCategory'
    ]);

});

/**
 * Route for menu clone
 */
$app->group(['menu clone group'], function () use ($app) {
    /**
     * Route to clone food menu of one restaurant to another.
     * Permission used: clone-menu
     */
    $app->post('clone/from/{fromId: [0-9]+}/to/{toId: [0-9]+}', [
        'middleware' => 'checkTokenPermission:clone-menu',
        'uses' => 'CloneController@cloneMenu'
    ]);

});


/**
 * Route to get food price for cart
 */

$app->group(['cart group'], function () use ($app) {
    /**
     * Route to get food price for cart
     */
    $app->post('cart', [
        'uses' => 'CartController@cart'
    ]);

    $app->post('cart/reorder', [
        'uses' => 'CartController@reorderCart'
    ]);

});

/**
 * Routes for restaurant-category relation
 */

$app->group(['restaurant category relation group'], function () use ($app) {

    /**
     * route to get list of categories related to restaurant of given id for logged in users.
     * Permission used: restaurant-category-relation-view
     */
    $app->get('restaurant-branch/{id:[0-9]+}/category', [
        'middleware' => 'checkTokenPermission:restaurant-category-relation-view',
        'uses' => 'RestaurantCategoryController@index'
    ]);

    /**
     * route to attach category_id to the given food id
     * Only authorized personnel have access to this route.
     * Permission used: restaurant-category-attach
     */
    $app->post('restaurant-branch/{id:[0-9]+}/category', [
        'middleware' => 'checkTokenPermission:restaurant-category-attach',
        'uses' => 'RestaurantCategoryController@store'
    ]);

    /**
     * route to detach category_id from given restaurant for logged in user
     * Only authorized personnel have access to this route.
     * Permission used: restaurant-category-detach
     */
    $app->delete('restaurant-branch/{id: [0-9]+}/category', [
        'middleware' => 'checkTokenPermission:restaurant-category-detach',
        'uses' => 'RestaurantCategoryController@destroy'
    ]);

    /**
     * route to get list of food related to category of given id for logged in user
     * Permission used: restaurant-category-relation-view
     */
    $app->get('category/{id:[0-9]+}/restaurant-branch', [
        'middleware' => 'checkTokenPermission:restaurant-category-relation-view',
        'uses' => 'RestaurantCategoryController@getRestaurantByCategory'
    ]);

    /**
     * route to get list of categories related to restaurant of given id for non logged in users.
     * No Permission used
     */
    $app->get('public/restaurant-branch/{id:[0-9]+}/category', [
        'uses' => 'RestaurantCategoryController@index'
    ]);

    /**
     * route to get list of restaurant related to category of given id for logged in user
     * No permission used
     */
    $app->get('public/category/{id:[0-9]+}/restaurant-branch', [
        'uses' => 'RestaurantCategoryController@getRestaurantByCategory'
    ]);
    /**
     * route to get list of menu i.e. categories, food related to each categories
     */
    $app->get('public/restaurant-branch/{id:[0-9]+}/menu', [
        'uses' => 'RestaurantCategoryController@getDetailsByRestaurantCategory'
    ]);

    $app->get('admin/restaurant-branch/{id:[0-9]+}/category', [
        'middleware' => 'checkTokenPermission:admin-restaurant-category-list',
        'uses' => 'RestaurantCategoryController@adminCategoryListByBranch'
    ]);
});

$app->group(['user favourite group'], function () use ($app) {

    $app->get('public/user-favourite', [
        'middleware' => 'checkTokenPermission',
        'uses' => 'UserFavouriteController@index'
    ]);

    $app->get('public/user-all-favourite', [
        'middleware' => 'checkTokenPermission',
        'uses' => 'UserFavouriteController@indexShowingIdOnly'
    ]);

    $app->post('public/user-favourite', [
        'middleware' => 'checkTokenPermission',
        'uses' => 'UserFavouriteController@store'
    ]);

    $app->delete('public/user-favourite/{id: [0-9]+}', [
        'middleware' => 'checkTokenPermission',
        'uses' => 'UserFavouriteController@destroy'
    ]);
});


$app->group(['restaurant review route'], function () use ($app) {

    $app->get('public/restaurant/{restaurantId: [0-9]+}/review', [
        'uses' => 'ReviewController@getAllReviewsForRestaurant'
    ]);

    $app->get('restaurant/review', [
        'middleware' => 'checkTokenPermission',
        'uses' => 'ReviewController@getAllReviewsOfUser'
    ]);

    $app->get("restaurant/review/{id:[0-9]+}", [
        'middleware' => 'checkTokenPermission',
        'uses' => 'ReviewController@getSpecificReview'
    ]);

    $app->get("admin/restaurant/review/{id:[0-9]+}", [//admin
        'middleware' => 'checkTokenPermission',
        'uses' => 'ReviewController@getSpecificReviewForAdmin'
    ]);

    $app->post("review/list", [
        'uses' => 'ReviewController@getReviewListBasedOnOrderId'
    ]);

    $app->get('admin/restaurant/{restaurantId: [0-9]+}/review', [//admin
        'middleware' => 'checkTokenPermission:view-review',
        'uses' => 'ReviewController@getAllReviewsForAdmin'
    ]);

    $app->get('admin/count/review', [//admin
        'middleware' => 'checkTokenPermission',
        'uses' => 'ReviewController@countPendingReview'
    ]);

    $app->get('admin/restaurant/review', [//admin
        'middleware' => 'checkTokenPermission',
        'uses' => 'ReviewController@getAllReviewsForAdmins'
    ]);

    $app->post("restaurant/review", [
        'middleware' => 'checkTokenPermission',
        'uses' => 'ReviewController@storeReview'
    ]);

    $app->put("restaurant/review/{id:[0-9]+}", [
        'middleware' => 'checkTokenPermission',
        'uses' => 'ReviewController@updateReview'
    ]);

    $app->put("admin/restaurant/review/{id:[0-9]+}", [
        'middleware' => 'checkTokenPermission',
        'uses' => 'ReviewController@updateReviewByAdminOnly'
    ]);

    $app->delete("restaurant/review/{id:[0-9]+}", [//admin
        'middleware' => 'checkTokenPermission:delete-review',
        'uses' => 'ReviewController@deleteReview'
    ]);


});

$app->group(['restaurant reservation route'], function () use ($app) {

    $app->get('admin/restaurant/{branch_id:[0-9]+}/reservations', [
        'middleware' => 'checkTokenPermission:view-restaurant-reservation',
        'uses' => 'RestaurantReservationController@adminIndex',
    ]);

    $app->get('admin/restaurant/{branch_id:[0-9]+}/reservations/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:view-restaurant-reservation',
        'uses' => 'RestaurantReservationController@adminShow',
    ]);

    $app->get('user/reservations', [
        'middleware' => 'checkTokenPermission',
        'uses' => 'RestaurantReservationController@selfIndex',
    ]);

    $app->post('public/restaurant/{branch_id:[0-9]+}/reservation/request', [
        'uses' => 'RestaurantReservationController@create',
    ]);

    $app->put('admin/restaurant/{branch_id:[0-9]+}/reservation/{id:[0-9]+}/verify', [
        'middleware' => 'checkTokenPermission:update-restaurant-reservation',
        'uses' => 'RestaurantReservationController@verify',
    ]);

    $app->delete('admin/restaurant/{branch_id:[0-9]+}/reservation/{id:[0-9]+}/delete', [
        'middleware' => 'checkTokenPermission:delete-restaurant-reservation',
        'uses' => 'RestaurantReservationController@destroy',
    ]);

    $app->get('public/restaurant/{branch_id:[0-9]+}/reservation/time', [
        'uses' => 'RestaurantReservationController@timeListOfRestaurantHours',
    ]);

});

$app->group(['customer restaurant route'], function () use ($app) {

    $app->get('customer/{user_id:[0-9]+}/restaurants', [
        'middleware' => 'checkTokenPermission:view-restaurant-customer',
        'uses' => 'CustomerRestaurantController@getRestaurantsOfCustomer',
    ]);
    $app->put('customer/{user_id:[0-9]+}/restaurants', [
        'middleware' => 'checkTokenPermission:create-restaurant-customer',
        'uses' => 'CustomerRestaurantController@updateRestaurantsOfCustomer',
    ]);
    $app->post('customer/{user_id:[0-9]+}/restaurant/attach', [
        'middleware' => 'checkTokenPermission:create-restaurant-customer',
        'uses' => 'CustomerRestaurantController@attachRestaurantToCustomer',
    ]);
    $app->delete('customer/{user_id:[0-9]+}/restaurant/detach', [
        'middleware' => 'checkTokenPermission:delete-restaurant-customer',
        'uses' => 'CustomerRestaurantController@detachRestaurantToCustomer',
    ]);

});



$app->get('export-restaurant', 'ExportController@exportRestaurant'
);
$app->get('export-branch', 'ExportController@exportBranch'
);
$app->get('export-categories', 'ExportController@exportFoodCategories'
);

$app->get('export-addoncategories', 'ExportController@exportAddonCategories'
);
$app->get('export-cuisine', 'ExportController@exportCuisine');
$app->get('export-food', 'ExportController@exportFoodandAddons');

$app->get('export-pivot-tables', 'ExportController@exportCategoryFoodPivot');

$app->get('export-addon-food-pivot', 'ExportController@exportFoodAddonCategoryPivot');

$app->get('export-favourites', 'ExportController@exportFavourites');

$app->get('export-reviews', 'ExportController@exportReviews');

$app->get('export-methods', 'ExportController@exportMethods');

$app->get('export/restaurant/branch/{branch_id:[0-9]+}/detail', ['uses' => 'RestaurantBranchController@ResExportDetails']);


$app->get('get-restaurant-report/{id:[0-9]+}', 'ReportController@getBranchesOfRestaurant');
$app->post('get-restaurant-report', 'ReportController@getBranchesOfRestaurants');
$app->get('get-count', 'ReportController@getRestaurantAndBranches');

