<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
    ];
});
$factory->define(App\Restaurant::class, function (Faker\Generator $faker) {
    return [
        /*
         *
         * */
        'user_id' => $faker->numberBetween(1,10),
        'email_address' => $faker->email,
        "address"=>$faker->address,
        "phone_no"=>"014117894",
        "mobile_no"=>"9840012011",
        "country_id"=>$faker->numberBetween(1,1),
        "website"=>$faker->url,
        "contact_person"=>$faker->name,
        "commission_rate"=>10.11,
        "status"=>$faker->numberBetween(0,1)

    ];
});
