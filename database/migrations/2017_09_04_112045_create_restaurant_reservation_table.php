<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantReservationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('restaurant_id');
            $table->foreign("restaurant_id")->references("id")->on("restaurant_branches")->onDelete("cascade");
            $table->unsignedInteger('user_id');
            $table->string('full_name');
            $table->string('email');
            $table->string('phone',19);
            $table->date('booking_date');
            $table->time('booking_time');
            $table->text('special_instruction');
            $table->enum('status',['new','accept','reject'])->default('new');
            $table->timestamps();
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_reservations');
    }
}
