<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddonTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addon_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('addon_id');
            $table->string( 'lang',5 );
            $table->string( 'name' );
            $table->text( 'description' );

            // set foreign key
            $table->foreign('addon_id')->references('id')->on('addon')->onDelete('cascade');
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('addon_translation');
    }
}
