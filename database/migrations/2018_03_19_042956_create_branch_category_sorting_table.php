<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchCategorySortingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_category_sorting', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("branch_id");
            $table->foreign('branch_id')->references('id')->on('restaurant_branches')->onDelete("cascade");
            $table->unsignedInteger("category_id");
            $table->foreign('category_id')->references('id')->on('category')->onDelete('cascade');
            $table->integer("sort");
            $table->engine = "InnoDB";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_category_sorting');
    }
}
