<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class AlterCuisineRestaurantPivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cuisine_restaurant_pivot', function (Blueprint $table) {
            DB::statement('ALTER TABLE cuisine_restaurant_pivot MODIFY COLUMN id INTEGER NOT NULL UNIQUE AUTO_INCREMENT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cuisine_restaurant_pivot', function (Blueprint $table) {
            //
        });
    }
}
