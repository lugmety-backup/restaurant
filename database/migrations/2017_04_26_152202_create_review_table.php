<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("user_id");
            $table->unsignedInteger("restaurant_id");
            $table->foreign("restaurant_id")->references('id')->on('restaurant_branches');
            $table->unsignedInteger("order_id");
            $table->enum("status",["pending","approved","rejected"]);
            $table->float("rating");
            $table->text("review");
            $table->text("driver_review");
            $table->text("lugmety_review");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
