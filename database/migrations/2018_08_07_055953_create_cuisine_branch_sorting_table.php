<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuisineBranchSortingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuisine_branch_sorting', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("branch_id");
            $table->foreign('branch_id')->references('id')->on('restaurant_branches')->onDelete("cascade");
            $table->unsignedInteger("cuisine_id");
            $table->foreign('cuisine_id')->references('id')->on('cuisine')->onDelete('cascade');
            $table->integer("sort");
            $table->engine = "InnoDB";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuisine_branch_sorting');
    }
}
