<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyAddonAddoncategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addon_addoncategory', function (Blueprint $table) {
            $table->dropPrimary('addon_addoncategory_id_primary');
            DB::statement('ALTER TABLE addon_addoncategory MODIFY COLUMN id INTEGER NOT NULL UNIQUE AUTO_INCREMENT');

            $table->primary(['category_id', 'addon_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addon_addoncategory', function (Blueprint $table) {
            //
        });
    }
}
