<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuisineRestaurantPivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuisine_restaurant_pivot', function (Blueprint $table) {
            $table->integer("id");
            $table->engine = "InnoDB";
            $table->unsignedInteger("restaurant_id");
            $table->unsignedInteger("cuisine_id");
            $table->foreign('restaurant_id')->references('id')->on('restaurant_branches')->onDelete("cascade");
            $table->foreign('cuisine_id')->references('id')->on('cuisine')->onDelete("cascade");
            $table->primary(['restaurant_id', 'cuisine_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuisine_restaurant_pivot');
    }
}
