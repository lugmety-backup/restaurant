<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuisineTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuisine_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("cuisine_id");
            $table->foreign('cuisine_id')->references('id')->on('cuisine')->onDelete("cascade");
            $table->string("name");
            $table->string("lang_code",20);
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuisine_translation');
    }
}
