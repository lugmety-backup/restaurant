<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFoodCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('food_category', function (Blueprint $table) {
            $table->dropPrimary('food_category_id_primary');
            DB::statement('ALTER TABLE food_category MODIFY COLUMN id INTEGER NOT NULL UNIQUE AUTO_INCREMENT');

            $table->primary(['food_id', 'category_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('food_category', function (Blueprint $table) {
            //
        });
    }
}
