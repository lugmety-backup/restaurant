<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodSizeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food_size', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('food_id');
            $table->string('size');
            $table->float('price',8,2);
            $table->enum('is_default',['0', '1']);
            $table->timestamps();

            $table->foreign('food_id')->references('id')->on('food')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('food_size');
    }
}
