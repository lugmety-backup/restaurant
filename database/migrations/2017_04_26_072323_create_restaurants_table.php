<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateRestaurantsTable
 */
class CreateRestaurantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function (Blueprint $table) {
            $table->increments('id');            $table->string('address');
            $table->string("phone_no",15);
            $table->string("mobile_no",15);
            $table->unsignedInteger("country_id");
            $table->string("email_address")->unique();
            $table->float('commission_rate',5,3);
            $table->string("website");
            $table->string('contact_person',50);
            $table->smallInteger("status");
            $table->softDeletes();
            $table->timestamps();
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurants');
    }
}
