<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerRestaurantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_restaurant', function (Blueprint $table) {
                $table->unsignedInteger('user_id');
                $table->unsignedInteger('restaurant_id');
                $table->foreign('restaurant_id')
                    ->references('id')
                    ->on('restaurant_branches')
                    ->onDelete('cascade');
                $table->index(['user_id', 'restaurant_id']);
                $table->primary(['user_id', 'restaurant_id']);
                $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_restaurant');
    }
}
