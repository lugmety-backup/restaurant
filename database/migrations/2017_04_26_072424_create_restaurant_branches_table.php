<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateRestaurantBranchesTable
 */
class CreateRestaurantBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_branches', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('country_id');
            $table->unsignedInteger('city_id');
            $table->unsignedInteger('district_id');
            $table->unsignedInteger('parent_id');
            $table->foreign('parent_id')->references('id')->on('restaurants')->onDelete("cascade");
            $table->string('logo','50');
            $table->string('cover_image','50');
            $table->string("phone_no",15);
            $table->string("mobile_no",15);
            $table->text("additional_contact");
            $table->string('email_address');
            $table->float('commission_rate',5,3);
            $table->string('slug',255);
            $table->float('min_purchase_amt',9,3);
            $table->float('max_purchase_amt',9,3);
            $table->unsignedInteger('no_of_seats');
            $table->unsignedInteger('available_seats');
            $table->string('machine_ref_key');
            $table->smallInteger('freelance_drive_supported')->default(0);
            $table->smallInteger('is_featured')->default(0);
            $table->smallInteger('pre_order')->default(0);
            $table->float('latitude');
            $table->float('longitude');
            $table->float("logistics_fee");
            $table->float("delivery_fee");
            $table->string("tin");
            $table->string("invoice_prefix");
            $table->smallInteger("status");
            $table->string('timezone',255);
            $table->enum(["inclusive","exclusive","none"],"tax_type");
            $table->smallInteger('shift')->default(1);
            $table->smallInteger('free_delivery')->default(0);
            $table->softDeletes();
            $table->timestamps();
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_branches');
    }
}
