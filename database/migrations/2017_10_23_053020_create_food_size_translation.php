<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodSizeTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food_size_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('food_size_id');
            $table->string('lang');
            $table->string('title');

            $table->foreign('food_size_id')->references('id')->on('food_size')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('food_size_transaltion');
    }
}
