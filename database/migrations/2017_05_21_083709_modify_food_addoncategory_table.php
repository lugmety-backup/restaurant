<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyFoodAddoncategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('food_addoncategory', function (Blueprint $table) {
            $table->dropPrimary('food_addoncategory_id_primary');
            DB::statement('ALTER TABLE food_addoncategory MODIFY COLUMN id INTEGER NOT NULL UNIQUE AUTO_INCREMENT');

            $table->primary(['category_id', 'food_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('food_addoncategory', function (Blueprint $table) {
            //
        });
    }
}
