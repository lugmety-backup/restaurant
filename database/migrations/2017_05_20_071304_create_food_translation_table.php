<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('food_id');
            $table->string( 'lang',5 );
            $table->string( 'name' );
            $table->text( 'description' );
            $table->text( 'ingredients' );

            // set foreign key
            $table->foreign('food_id')->references('id')->on('food')->onDelete('cascade');
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('food_translation');
    }
}
