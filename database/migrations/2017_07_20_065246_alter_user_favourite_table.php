<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserFavouriteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_favourite', function (Blueprint $table) {
            $table->dropPrimary('user_favourite_id_primary');
            DB::statement('ALTER TABLE user_favourite MODIFY COLUMN id INTEGER NOT NULL UNIQUE AUTO_INCREMENT');

            $table->primary(['user_id', 'food_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_favourite', function (Blueprint $table) {
            //
        });
    }
}
