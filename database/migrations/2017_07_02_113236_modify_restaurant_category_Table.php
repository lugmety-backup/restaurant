<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyRestaurantCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('restaurant_category', function (Blueprint $table) {
            $table->dropPrimary('restaurant_category_id_primary');
            DB::statement('ALTER TABLE restaurant_category MODIFY COLUMN id INTEGER NOT NULL UNIQUE AUTO_INCREMENT');

            $table->primary(['category_id', 'restaurant_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restaurant_category', function (Blueprint $table) {
            //
        });
    }
}
