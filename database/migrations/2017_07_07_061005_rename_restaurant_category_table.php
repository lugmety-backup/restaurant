<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameRestaurantCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('restaurant_category','restaurant_food_category_pivot');
        Schema::rename('addon_addoncategory', 'addon_addoncategory_pivot');
        Schema::rename('food_category', 'food_category_pivot');
        Schema::rename('food_addoncategory', 'food_addoncategory_pivot');
        Schema::rename('addon_addoncategory', 'addon_addoncategory_pivot');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restaurant_category', function (Blueprint $table) {
            //
        });
    }
}
