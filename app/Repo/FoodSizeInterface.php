<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 10/23/17
 * Time: 12:05 PM
 */

namespace App\Repo;


interface FoodSizeInterface
{
    public function createFoodSize(array $attributes);

    public function updateFoodSize($id, array $attributes);

}