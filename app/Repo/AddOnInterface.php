<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/8/17
 * Time: 3:41 PM
 */

namespace App\Repo;


interface AddOnInterface
{
    public function getAllAddOn($restaurantId,$status,$limit);

    public function getSpecificAddOn($id);

    public function createAddOn(array $attributes);

    public function updateAddOn($id, array $attributes);

    public function deleteAddOn($id);

    public function getSpecificAddonByRestaurant($id, $restaurantId);

    public function getAddonForDatatable($restaurantId);

    public function getDataForAddonValidation($restaurantId);
}