<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 8/4/17
 * Time: 2:00 PM
 */

namespace App\Repo;


interface PivotInterface
{
    public function deletePivotForFood($id, $addon_delete);

    public function deletePivotForAddon($id);

    public function deletePivotForRestaurant($id);
}