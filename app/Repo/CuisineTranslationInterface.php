<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 7/7/2017
 * Time: 2:17 PM
 */

namespace App\Repo;


interface CuisineTranslationInterface
{
    public function getAllCuisineTranslation();

    public function createCuisineTranslation(array $request);

    public function updateCuisineTranslation(array $request);

    public function getSpecificCuisineTranslationByCodeAndName($langCode,$name);

    public function viewCuisineTranslationSpecific($id);
}