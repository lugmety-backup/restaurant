<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 7/2/17
 * Time: 3:55 PM
 */

namespace App\Repo;


interface RestaurantCategoryInterface
{
    public function getAllCategoryOfRestaurant($id);

    public function attachCategoryToRestaurant($id, $request);

    public function detachCategoryFromRestaurant($id, $request);

    public function getAllRestaurantByCategory($id);

    public function getSpecificRestaurantCategory($resId, $categoryId);

    public function getDataForCategoryValidation($id);

}