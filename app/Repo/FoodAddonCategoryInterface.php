<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/10/17
 * Time: 2:38 PM
 */

namespace App\Repo;


interface FoodAddonCategoryInterface
{

    public function attachCategoryToFood($id, $request);

    public function detachCategoryFromFood($id, $request);

}