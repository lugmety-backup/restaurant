<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/3/17
 * Time: 3:53 PM
 */

namespace App\Repo;


interface CategoryTranslationInterface
{
    public function getAllCategoryTranslation();

    public function createCategoryTranslation(array $attributes);

    public function deleteTranslationByCategory($id);

    public function getSpecificCategoryTransalationByLang($id,$lang);

}