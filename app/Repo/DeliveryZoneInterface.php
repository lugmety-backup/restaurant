<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:01 AM
 */

namespace App\Repo;


interface DeliveryZoneInterface
{
    public function getAllDeliveryZone($branch_id);

    public function getAllDeliveryZoneForDataTable($branch_id);

    public function getSpecificDeliveryZone($id);

    public function getDeliveryZoneByDistrictWithBranchId($district_id,$branch_id);

    public function createDeliveryZone(array $attributes);

    public function updateDeliveryZone($id, array $attributes);

    public function deleteDeliveryZone($id);
}