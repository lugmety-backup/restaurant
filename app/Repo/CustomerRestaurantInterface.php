<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/27/2017
 * Time: 11:45 AM
 */

namespace App\Repo;


interface CustomerRestaurantInterface
{
    public function getAllRestaurantByUserId($userId);
    public function createCustomerRestaurant(array $request);
    public function createMultipleCustomerRestaurant(array $request);
    public function deleteAndCreateMultipleCustomerRestaurant($userId, array $request);
    public function updateCustomerRestaurant($customerRestaurant, array $request);
    public function deleteAllRestaurantByUserId($userId);
    public function deleteRestaurantByUserId($userId, $restaurantId);
    public function checkUserBelongsToRestaurant($userId,$restaurantId);

}