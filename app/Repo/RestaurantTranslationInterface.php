<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/28/2017
 * Time: 11:54 AM
 */

namespace App\Repo;


/**
 * Interface RestaurantTranslationInterface
 * @package App\Repo
 */
interface RestaurantTranslationInterface
{
    public function getAllRestaurantTranslation();

    /**
     * @param $id
     * @return mixed
     */
    public function getSpecificRestaurantTranslation($id);

    /**
     * @param $code
     * @param $restaurant_id
     * @return mixed
     */
    public function getSpecificRestaurantTranslationByCodeAndId($code, $restaurant_id);

    /**
     * @param array $attributes
     * @return mixed
     */
    public function createRestaurantTranslation(array $attributes);

    /**
     * @param $id
     * @param array $attributes
     * @return mixed
     */
    public function updateRestaurantTranslation($id, array $attributes);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRestaurantTranslation($id);
}