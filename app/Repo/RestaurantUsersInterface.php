<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:01 AM
 */

namespace App\Repo;


interface RestaurantUsersInterface
{
    public function getAllRestaurantUser($branch_id, $limit);

    public function getSpecificRestaurantUser($id);

    public function getSpecificRestaurantUserByUserId($user_id,$status);

    public function createRestaurantUser(array $attributes);

    public function updateRestaurantUser($id, array $attributes);

    public function updateRestaurantStatusFromRestaurant($restaurantId);

    public function  updateUserStatus($id);

    public function deleteRestaurantUser($id);
}