<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 10/23/17
 * Time: 12:14 PM
 */

namespace App\Repo;


interface FoodSizeTranslationInterface
{
    public function createFoodSizeTranslation(array $attributes);

    public function updateFoodSizeTranslation($id, array $attributes);

}