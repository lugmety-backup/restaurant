<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/8/17
 * Time: 3:47 PM
 */

namespace App\Repo\Eloquent;
use App\Models\AddOnTranslation;
use App\Repo\AddOnCategoryTranslationInterface;
use App\Models\AddOnCategoryTranslation;


class AddOnCategoryTranslationRepo implements AddOnCategoryTranslationInterface
{
    private $addon_cat_translation;

    public function __construct(AddOnCategoryTranslation $addon_cat_translation)
    {
        $this->addon_cat_translation = $addon_cat_translation;
    }

    /**
     * Insert new row to addoncategory_translation
     * @param array $attributes
     * @return mixed
     */

    public function createAddOnCatTranslation(array $attributes)
    {
        $data = $this->addon_cat_translation->create($attributes);
        return $data;

    }

    /**
     * Get specific translation of an addon category for specific language
     * @param $id
     * @param $lang
     * @return mixed
     */

    public function getSpecificAddOnCatTransalationByLang($id, $lang)
    {
        return $this->addon_cat_translation->where('category_id',$id)->where('lang',$lang)->get();
    }
}