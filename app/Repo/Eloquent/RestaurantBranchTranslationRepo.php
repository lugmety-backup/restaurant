<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:02 AM
 */

namespace App\Repo\Eloquent;

use App\Repo\RestaurantBranchTranslationInterface;
use App\BranchTranslation;

/**
 * Class RestaurantBranchTranslationRepo
 * @package App\Repo\Eloquent
 */
class RestaurantBranchTranslationRepo implements RestaurantBranchTranslationInterface
{
    private $restaurantBranchTranslation;

    /**
     * CountryRepo constructor.
     * @param BranchTranslation $restaurantBranchTranslation
     * @internal param $country
     */
    public function __construct(BranchTranslation $restaurantBranchTranslation)
    {
        $this->restaurantBranchTranslation = $restaurantBranchTranslation;
    }


    /**
     * @param $id
     * @return mixed
     */
    public function getAllRestaurantBranchTranslation($id)
    {
        return $this->restaurantBranchTranslation->where('branch_id',$id)->get();
    }

    /**
     * @param $id
     * @return mixed|void
     */
    public function getSpecificRestaurantBranchTranslation($id)
    {
        $data=$this->restaurantBranchTranslation->where('branch_id',$id)->where('lang_code','en')->first();
        return $data;
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function createRestaurantBranchTranslation(array $attributes)
    {
        $restaurantBranchTranslation= $this->restaurantBranchTranslation->create($attributes);
        return $restaurantBranchTranslation;
    }

    /**
     * @param $id
     * @param array $attributes
     * @return mixed|void
     */
    public function updateRestaurantBranchTranslation($id, array $attributes)
    {

    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRestaurantBranchTranslation($id)
    {
        $restaurantBranchTranslation=$this->restaurantBranchTranslation->find($id)->delete();
        return $restaurantBranchTranslation;
    }

    /**
     * @param $id
     * @param $lang
     * @return mixed
     */
    public function getAllRestaurantBranchTranslationByLangCode($id, $lang)
    {
//        $translation = $this->restaurantBranchTranslation->where('branch_id',$id)->get();
//        if(count($translation)==0){
            $translation = $this->restaurantBranchTranslation->where([['branch_id',$id],['lang_code',$lang]])->get();
//        }
        return $translation;

    }

    public function getRestaurantNameBtRestaurantList(array $list)
    {
        return $this->restaurantBranchTranslation->whereIn("branch_id",$list)->where("lang_code","en")->get();
    }

}