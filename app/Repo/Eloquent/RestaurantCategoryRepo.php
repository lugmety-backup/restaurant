<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 7/2/17
 * Time: 3:57 PM
 */

namespace App\Repo\Eloquent;

use App\Models\Category;
use App\RestaurantBranch;
use App\Repo\RestaurantCategoryInterface;
use App\Models\RestaurantCategory;

class RestaurantCategoryRepo implements RestaurantCategoryInterface
{
    private $restaurantBranch;
    private $category;
    private $restaurantCategory;



    /**
     * RestaurantCategoryRepo constructor.
     * @param RestaurantBranch $restaurantBranch
     * @param Category $category
     */
    public function __construct(RestaurantBranch $restaurantBranch,Category $category, RestaurantCategory $restaurantCategory)
    {
        $this->restaurantBranch = $restaurantBranch;
        $this->category = $category;
        $this->restaurantCategory = $restaurantCategory;
    }

    /**
     * Get all categories related for specific restaurant
     * @param $id
     * @return mixed
     */
    public function getAllCategoryOfRestaurant($id)
    {

        return $this->restaurantBranch->find($id)->category;

    }

    /**
     * Attach category to Restaurant
     * @param $id
     * @param $request
     * @return mixed
     */

    public function attachCategoryToRestaurant($id, $request)
    {
        $restaurantBranch = $this->restaurantBranch->find($id);
        return $restaurantBranch->category()->attach($request);

    }

    /**
     * Detach category from Restaurant
     * @param $id
     * @param $request
     * @return mixed
     */

    public function detachCategoryFromRestaurant($id, $request)
    {
        $restaurantBranch = $this->restaurantBranch->find($id);
        return $restaurantBranch->category()->detach($request);
    }

    /**
     * Get all food related to specific category
     * @param $id
     * @return mixed
     */
    public function getAllRestaurantByCategory($id)
    {
        return $this->category->find($id)->restaurantBranch;
    }

    public function getSpecificRestaurantCategory($resId, $categoryId)
    {
        return $this->restaurantCategory->where([
            ['restaurant_id', $resId],
            ['category_id', $categoryId]
        ])->get();
    }


    public function getDataForCategoryValidation($country_id)
    {

        $categories = $this->category->select('category.id')->where([
            ['country_id',$country_id],
            ['status',1]
        ])->get();

        $categoryIds = [];

        foreach ($categories as $category)
        {
            $categoryIds[] = $category['id'];

        }

        $data = [

            'categories' => $categoryIds

        ];
        return $data;

    }
}