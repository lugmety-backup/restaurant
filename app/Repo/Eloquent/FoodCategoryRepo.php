<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/9/17
 * Time: 1:20 PM
 */

namespace App\Repo\Eloquent;
use App\Models\Food;
use App\Models\Category;
use App\Repo\FoodCategoryInterface;

class FoodCategoryRepo implements FoodCategoryInterface
{
    private $food;
    private $category;
    /**
     * FoodCategoryRepo constructor.
     * @param $food,$country
     */
    public function __construct(Food $food,Category $category)
    {
        $this->food = $food;
        $this->category = $category;
    }

    /**
     * Get all categories related for specific food
     * @param $id
     * @return mixed
     */
    public function getAllCategoryOfFood($id)
    {

        return $this->food->find($id)->category;

    }

    /**
     * Attach category to food
     * @param $id
     * @param $request
     * @return mixed
     */

    public function attachCategoryToFood($id, $request)
    {
        $food = $this->food->find($id);
        return $food->category()->attach($request);

    }

    /**
     * Detach category from food
     * @param $id
     * @param $request
     * @return mixed
     */

    public function detachCategoryFromFood($id, $request)
    {
        $food = $this->food->find($id);
        return $food->category()->detach($request);
    }

    /**
     * Get all food related to specific category
     * @param $id
     * @return mixed
     */
    public function getAllFoodByCategory($id)
    {
        return $this->category->find($id)->food;
    }
}