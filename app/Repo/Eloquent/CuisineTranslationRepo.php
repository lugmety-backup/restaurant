<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 7/7/2017
 * Time: 2:17 PM
 */

namespace App\Repo\Eloquent;


use App\Repo\CuisineTranslationInterface;
use App\CuisineTranslation;
class CuisineTranslationRepo implements CuisineTranslationInterface
{
    protected  $cuisineTranslation;

    /**
     * CuisineTranslationRepo constructor.
     * @param $cuisineTranslation
     */
    public function __construct(CuisineTranslation $cuisineTranslation)
    {
        $this->cuisineTranslation = $cuisineTranslation;
    }

    public function getAllCuisineTranslation()
    {
       return $this->cuisineTranslation->all();
    }

    public function createCuisineTranslation(array $request)
    {
        return $this->cuisineTranslation->create($request);
    }

    public function updateCuisineTranslation(array $request)
    {

    }

    public function viewCuisineTranslationSpecific($id)
    {
       return $this->cuisineTranslation->findOrFail($id);
    }

    public function getSpecificCuisineTranslationByCodeAndName($langCode,$name){
        return $this->cuisineTranslation->where([
            ["lang_code", $langCode],
            ["name",$name]
        ])->first();
    }

}