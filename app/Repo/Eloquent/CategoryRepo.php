<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/3/17
 * Time: 1:38 PM
 */

namespace App\Repo\Eloquent;

use App\Repo\CategoryInterface;
use App\Models\Category;
use App\RestaurantBranch;


class CategoryRepo implements CategoryInterface
{
    /**
     * @var Category
     */
    private $category;
    private $branch;

    /**
     * CategoryRepo constructor.
     * @param Category $category
     */
    public function __construct(Category $category, RestaurantBranch $branch)
    {
        $this->category = $category;
        $this->branch = $branch;
    }

    /**
     * Get all categories.
     * If $status = "all", get both active and inactive categories.
     * Else, get either active or inactive categories according to value of $status.
     * No. of categories to be displayed at a time is given by value of $limit.
     * Catgories are ordered according to the value of $sort_by.
     *
     */
    /**
     * @param $status
     * @param $sort_by
     * @param $limit
     * @param $country_id
     * @return mixed
     */
    public function getAllCategory($status,$sortBy,$limit, $countryId)
    {
        if($status=="all")
        {
            if(is_null($countryId))
            {
                return $this->category->orderBy('sort',$sortBy)->paginate($limit);
            }
            return $this->category->where('country_id',$countryId)->orderBy('sort',$sortBy)->paginate($limit);
        }else{
            if(is_null($countryId))
            {
                return $this->category->where('status',$status)->orderBy('sort',$sortBy)->paginate($limit);
            }
            return $this->category->where('country_id',$countryId)->where('status',$status)->orderBy('sort',$sortBy)->paginate($limit);
        }

    }

    /**
     *Get specific category of given id
     * @param $id
     * @return mixed
     */
    public function getSpecificCategory($id)
    {
        return $this->category->findOrFail($id);
    }

    /**
     * Insert new category
     * @param array $attributes
     * @return mixed
     */
    public function createCategory(array $attributes)
    {
        $con= $this->category->create($attributes);
        return $con;
    }

    /**
     * Update existing category of given id
     * @param $id
     * @param array $attributes
     * @return mixed
     */
    public function updateCategory($id, array $attributes)
    {

        $this->category->findOrFail($id)->update($attributes);
        $con = $this->category->findOrFail($id);
        return $con;
    }

    /**
     * Delete category
     * @param $id
     * @return mixed
     */
    public function deleteCategory($id)
    {
       return $this->category->findOrFail($id)->delete();
    }

    /**
     * Fetch data for DataTable
     */

    public function getCategoryForDatatable($countryId)
    {
        if($countryId == "all")
        {
            return $this->category->with(['categoryTranslation'=>function ($query) {
                $query->where('lang', 'en');}])->select('category.*')->get();
        }
        return $this->category->where('country_id',$countryId)->with(['categoryTranslation'=>function ($query) {
            $query->where('lang', 'en');}])->select('category.*')->get();
    }

    public function getCategoriesIdRelatedToResBranch($id)
    {
       $categories = $this->branch->findOrFail($id)->category()->select('category.id')->where('status',1)->get();
       $categoryId = array();
    if(count($categories) > 0)
    {
        foreach ($categories as $category)
        {
            $categoryId[] = $category['id'];
        }
    }


       return $categoryId;
    }

    public function activeCategoriesForAdmin($countryId){
        return $this->category->where([
            ['country_id', $countryId],
            ['status',1]
        ])->get();
    }

}