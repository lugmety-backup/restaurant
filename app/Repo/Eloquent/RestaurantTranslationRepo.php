<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/28/2017
 * Time: 12:30 PM
 */

namespace App\Repo\Eloquent;


use App\Repo\RestaurantTranslationInterface;
use App\RestaurantTranslation;

/**
 * Class RestaurantTranslationRepo
 * @package App\Repo\Eloquent
 */
class RestaurantTranslationRepo implements RestaurantTranslationInterface
{
    protected $restaurant_translation;

    /**
     * RestaurantTranslationRepo constructor.
     * @param $restaurant_translation
     */
    public function __construct(RestaurantTranslation $restaurant_translation)
    {
        $this->restaurant_translation = $restaurant_translation;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllRestaurantTranslation()
    {
        return $this->restaurant_translation->all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getSpecificRestaurantTranslation($id)
    {
        return $this->restaurant_translation->findOrFail($id);
    }

    /**
     * @param $code
     * @param $restaurant_id
     * @return mixed
     */
    public function getSpecificRestaurantTranslationByCodeAndId($code, $restaurant_id)
    {
        return $this->restaurant_translation->where(
            [["lang_code",$code],
                ["restaurant_id",$restaurant_id]
            ])->get();
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function createRestaurantTranslation(array $attributes)
    {
        $this->restaurant_translation->insert($attributes);
        return $this->restaurant_translation->where(
            [["lang_code",$attributes["lang_code"]],
                ["restaurant_id",$attributes["restaurant_id"]]
            ])->get();
    }

    /**
     * @param $id
     * @param array $attributes
     * @return mixed
     */
    public function updateRestaurantTranslation($id, array $attributes)
    {
        $translation = $this->restaurant_translation->updateOrCreate(['restaurant_id'=>$id,$attributes['lang_code']],$attributes);
        $translation = $this->restaurant_translation->findOrFail($id)->fill($attributes);
        return $translation;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRestaurantTranslation($id)
    {
        return $this->getSpecificRestaurantTranslation($id)->delete();
    }

}