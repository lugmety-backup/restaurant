<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:02 AM
 */

namespace App\Repo\Eloquent;



use App\Repo\RestaurantUsersInterface;

class RestaurantUsersRepo implements RestaurantUsersInterface
{
    public function getAllRestaurantUser($branch_id, $limit)
    {
        // TODO: Implement getAllRestaurantUser() method.
    }

    public function getSpecificRestaurantUser($id)
    {
        // TODO: Implement getSpecificRestaurantUser() method.
    }

    public function getSpecificRestaurantUserByUserId($user_id, $status)
    {
        // TODO: Implement getSpecificRestaurantUserByUserId() method.
    }

    public function createRestaurantUser(array $attributes)
    {
        // TODO: Implement createRestaurantUser() method.
    }

    public function updateRestaurantUser($id, array $attributes)
    {
        // TODO: Implement updateRestaurantUser() method.
    }

    public function updateRestaurantStatusFromRestaurant($restaurantId)
    {
        // TODO: Implement updateRestaurantStatusFromRestaurant() method.
    }

    public function updateUserStatus($id)
    {
        // TODO: Implement updateUserStatus() method.
    }

    public function deleteRestaurantUser($id)
    {
        // TODO: Implement deleteRestaurantUser() method.
    }


}