<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/8/17
 * Time: 3:46 PM
 */

namespace App\Repo\Eloquent;
use App\Repo\AddOnInterface;
use App\Models\AddOn;
use App\Models\AddOnCategory;
use App\Repo\RestaurantBranchInterface;


class AddOnRepo implements AddOnInterface
{
    private $addon;
    private $branch;
    private $addonCategory;

    /**
     * AddOnRepo constructor.
     * @param AddOn $addon
     */
    public function __construct(AddOn $addon, RestaurantBranchInterface $branch, AddOnCategory $addonCategory)
    {
        $this->addon = $addon;
        $this->branch = $branch;
        $this->addonCategory = $addonCategory;

    }

    /**
     * Get all addonOns related to specific restaurant
     * If $status = "all", get both active and inactive addons.
     * Else, get either active or inactive addons according to value of $status.
     * No. of addons to be displayed at a time is given by value of $limit.
     * @param $restaurantId
     * @param $status
     * @param $limit
     * @return mixed
     */
    public function getAllAddOn($restaurantId,$status, $limit)
    {
        if($status=="all")
        {
            return $this->addon->where('restaurant_id', $restaurantId)->paginate($limit);
        }
        return $this->addon->where('restaurant_id', $restaurantId)->where('status',$status)->paginate($limit);
    }

    /**
     * Get a specific addon for given id
     * @param $id
     * @return mixed
     */
    public function getSpecificAddOn($id)
    {
        return $this->addon->findOrFail($id);
    }

    /**
     * Insert new addon
     * @param array $attributes
     * @return mixed
     */
    public function createAddOn(array $attributes)
    {
        $result= $this->addon->create($attributes);
        return $result;

    }

    /**
     * Update existing addon of given id
     * @param $id
     * @param array $attributes
     * @return mixed
     */

    public function updateAddOn($id, array $attributes)
    {
        $this->addon->findOrFail($id)->update($attributes);
        $result = $this->addon->findOrFail($id);
        return $result;
    }

    /**
     * Delete addon
     * @param $id
     * @return mixed
     */

    public function deleteAddOn($id)
    {
        return $this->addon->findOrFail($id)->delete();
    }

    /**
     * Get a specific addon of giiven restaurant
     * @param $id
     * @param $restaurantId
     * @return mixed
     */

    public function getSpecificAddonByRestaurant($id,$restaurantId)
    {
        return $this->addon->where('restaurant_id',$restaurantId)->findOrFail($id);
    }

    public function getAddonForDatatable($restaurantId)
    {
        return $this->addon->with(['addonTranslation'=>function ($query) {
            $query->where('lang', 'en');}])->where('restaurant_id',$restaurantId)->select('addon.*')->get();
    }


    public function getDataForAddonValidation($restaurantId)
    {
        $restaurant = $this->branch->getSpecificRestaurantBranch($restaurantId);

        $addonCategories = $this->addonCategory->where([
            ['restaurant_id', $restaurantId],
            ['status',1]
        ])->get();

        $addonCategoriesIds = [];

        foreach ($addonCategories as $category)
        {
            $addonCategoriesIds[] = $category['id'];

        }
        //return $addonCategoies;
        $data = [
            'restaurant' => $restaurant,
            'addonCategories' => $addonCategoriesIds

        ];
        return $data;

    }
}