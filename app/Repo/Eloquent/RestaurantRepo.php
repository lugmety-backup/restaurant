<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:02 AM
 */

namespace App\Repo\Eloquent;

use App\Repo\RestaurantInterface;
use App\Restaurant;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RestaurantRepo
 * @package App\Repo\Eloquent
 */
class RestaurantRepo implements RestaurantInterface
{
    private $restaurant;
    use SoftDeletes;

    /**
     * CountryRepo constructor.
     * @param Restaurant $restaurant
     * @internal param $country
     */
    public function __construct(Restaurant $restaurant)
    {
        $this->restaurant = $restaurant;
    }

    public function getAllRestaurantForDatatableBasedOnCountry($countryId){
        return $this->restaurant->where("country_id",$countryId)->orderBy('id', "desc")->get();
    }

    /**
     * @param $status
     * @param $limit
     * @return mixed
     */
    public function getAllRestaurant($status, $limit)
    {
        if(is_null($status)){
            $restaurants = $this->restaurant->paginate($limit);
            $restaurants->appends(['limit' => $limit])->withPath('/restaurant');
        }else {
            $restaurants = $this->restaurant->where('status', $status)->paginate($limit);

            $restaurants->appends(['limit' => $limit, "status" => $status])->withPath('/restaurant');
        }
        return $restaurants;

    }

    /**
     * @param $status
     * @param $limit
     * @param $country_id
     * @return array
     */
    public function getAllRestaurantByCountryId($status, $limit, $country_id)
    {
        if(is_null($status)){
            $restaurants= $this->restaurant->where("country_id",$country_id)->paginate($limit);
            $restaurants->appends(['limit' => $limit,"country_id" => $country_id])->withPath('/restaurant');
        }else{
            $restaurants=
                $this->restaurant->where([
                    ['status',$status],
                    ["country_id",$country_id]
                ])->paginate($limit);
            $restaurants->appends(['limit' => $limit,"status" => $status,"country_id" => $country_id])->withPath('/restaurant');
        }
        return $restaurants;


    }

    /**
     * @param $id
     * @param $email
     * @return bool
     */
    public function checkEmail($id, $email){
        $restaurant=$this->restaurant->where([["email_address",$email],["id","!=",$id]])->first();
        if($restaurant){
            return false;
        }
        return true;

    }

    /**
     * @param $id
     * @param $status
     * @return mixed
     */
    public function getSpecificRestaurant($id, $status)
    {

        if(is_null($status)){
            return $this->restaurant->findOrFail($id);
        }
        return $this->restaurant->where([['status',$status],['id',$id]])->first();
    }

    public function getSpecificRestaurantCheckForBranch($id, $status){
        return $this->restaurant->where([['status',$status],['id',$id]])->firstOrFail();
    }

    /**
     * @param $id
     * @param $status
     * @return mixed
     */
    public function getSpecificSoftDeletedOrNotSoftDeletedRestaurant($id, $status)
    {

        if(is_null($status)){
            return $this->restaurant->withTrashed()->findOrFail($id);
        }
        return $this->restaurant->withTrashed()->where([['status',$status],['id',$id]])->first();
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function createRestaurant(array $attributes)
    {
        $con= $this->restaurant->create($attributes);
        return $con;
    }

    /**
     * @param $id
     * @param array $attributes
     * @return mixed
     */
    public function updateRestaurant($id, array $attributes)
    {
        $con = $this->restaurant->findOrFail($id)->update($attributes);
        $con = $this->restaurant->findOrFail($id)->fill($attributes);
        return $con;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRestaurant($id)
    {
        return $this->restaurant->findOrFail($id)->delete();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function forceDeleteRestaurant($id)
    {
        return $this->restaurant->findOrFail($id)->forceDelete();
    }

    public function getCountry($countryId)
    {
       return $this->restaurant->where("country_id",$countryId)->first();
    }

}