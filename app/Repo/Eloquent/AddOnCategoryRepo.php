<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/8/17
 * Time: 3:46 PM
 */

namespace App\Repo\Eloquent;

use App\Repo\AddOnCategoryInterface;
use App\Models\AddOnCategory;


class AddOnCategoryRepo implements AddOnCategoryInterface
{
    private $addon_category;

    /**
     * AddOnCategoryRepo constructor.
     * @param AddOnCategory $addon_category
     */

    public function __construct(AddOnCategory $addon_category)
    {
        $this->addon_category = $addon_category;
    }

    /**
     * Get All the addon categories of one specific restaurant from table.
     * If $status = 'all', get both active and inactive data.
     * Else get data according to the status provided.
     * Get no. of data according to the given $Limit.
     * @param $restaurantId
     * @param $status
     * @param $limit
     * @return mixed
     */
    public function getAllAddOnCategory($restaurantId, $status, $limit)
    {
        if ($status == "all") {
            return $this->addon_category->where('restaurant_id', $restaurantId)->paginate($limit);
        }
        return $this->addon_category->where('restaurant_id', $restaurantId)->where('status', $status)->paginate($limit);
    }

    /**
     * Get one specific addon category of given id
     * @param $id
     * @return mixed
     */

    public function getSpecificAddOnCategory($id)
    {
        return $this->addon_category->findOrFail($id);
    }

    /**
     * Insert new addon category to table
     * @param array $attributes
     * @return mixed
     */

    public function createAddOnCategory(array $attributes)
    {
        $result = $this->addon_category->create($attributes);
        return $result;
    }

    /**
     * Update specific addon category of given id
     * @param $id
     * @param array $attributes
     * @return mixed
     */

    public function updateAddOnCategory($id, array $attributes)
    {
        $this->addon_category->findOrFail($id)->update($attributes);
        $result = $this->addon_category->findOrFail($id);
        return $result;
    }

    /**
     * Delete specific addon category of given id
     * @param $id
     * @return mixed
     */
    public function deleteAddOnCategory($id)
    {
        return $this->addon_category->findOrFail($id)->delete();
    }

    /**
     * Get specific addon category of given restaurant
     * @param $id
     * @param $restaurantId
     * @return mixed
     */

    public function getSpecificAddonCategoryByRestaurant($id, $restaurantId)
    {
        return $this->addon_category->where('restaurant_id', $restaurantId)->findOrFail($id);
    }

    public function getAddonCategoryForDatatable($restaurantId)
    {
        return $this->addon_category->with(['categoryTranslation' => function ($query) {
            $query->where('lang', 'en');
        }])->where('restaurant_id', $restaurantId)->select('addoncategory.*')->get();
    }

    public function getAllActiveAddonCategories($restaurantId)
    {
        return $this->addon_category->where('restaurant_id', $restaurantId)->where('status',1)->get();
    }
}