<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 7/20/17
 * Time: 11:50 AM
 */

namespace App\Repo\Eloquent;
use App\Models\UserFavourite;
use App\Repo\UserFavouriteInterface;

use Illuminate\Support\Facades\DB;

class UserFavouriteRepo implements UserFavouriteInterface
{
    private $favourite;

    /**
     * FoodRepo constructor.
     * @param Food $food
     */
    public function __construct(UserFavourite $favourite)
    {
        $this->favourite = $favourite;
    }


    public function createFavourite(array $attributes)
    {
        $result = $this->favourite->create($attributes);
        return $result;
    }

    public function getFavOfUser($userId,$limit)
    {
        $fav = $this->favourite->where('user_id',$userId)->get();
//        $fav->withPath("/public/user-favourite?limit=" . $limit);
        return $fav;
//        $lang = "ar";
//        $check = DB::table('user_favourite')->where('user_id', $userId)
//            ->join('food', 'user_favourite.food_id', '=', 'food.id')
//            ->join('food_translation', 'food_translation.food_id', '=', 'user_favourite.food_id')
//            ->join('restaurant_branches', 'food.restaurant_id', '=', 'restaurant_branches.id')
//            ->join('branch_translation', 'restaurant_branches.id', '=', 'branch_translation.branch_id')
//            ->select('food.price','food.id as food_id','food.featured_image', 'food_translation.name as food_name','food_translation.description as food_description','restaurant_branches.logo','restaurant_branches.id as branch_id'.'branch_translation.name as branch_name','branch_translation.address')
//            ->get();
//foreach ($check as $c)
//{
//
//}
//
//        return $check;
    }

    public function deleteFavourite($id, $userId)
    {
        return $this->favourite->where('user_id', $userId)->findOrFail($id)->delete();
    }

    public function getSpecificFavourite($id, $userId)
    {
        return $this->favourite->where('user_id', $userId)->findOrFail($id);
    }

    public function getSpecificFavourite2($id, $userId)
    {
        return $this->favourite->where([['user_id', $userId],['food_id',$id]])->first();
    }
}