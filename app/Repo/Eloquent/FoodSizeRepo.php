<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 10/23/17
 * Time: 12:06 PM
 */

namespace App\Repo\Eloquent;


use App\Models\FoodSize;
use App\Repo\FoodSizeInterface;

class FoodSizeRepo implements FoodSizeInterface
{
    private $foodSize;

    public function __construct(FoodSize $foodSize)
    {
        $this->foodSize = $foodSize;

    }
    public function createFoodSize(array $attributes)
    {
        $result = $this->foodSize->create($attributes);
        return $result;
    }

    public function updateFoodSize($id, array $attributes)
    {
        $this->foodSize->findOrFail($id)->update($attributes);
        $result = $this->foodSize->findOrFail($id);
        return $result;
    }

}