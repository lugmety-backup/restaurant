<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 5/13/2017
 * Time: 8:20 AM
 */

namespace App\Repo\Eloquent;


use App\Repo\RestaurantHourInterface;
use App\RestaurantHour;
use Carbon\Carbon;

/**
 * Class RestaurantHourRepo
 * @package App\Repo\Eloquent
 */
class RestaurantHourRepo implements RestaurantHourInterface
{
    /**
     * @var RestaurantHour
     */
    protected $restaurantHour;

    /**
     * RestaurantHourRepo constructor.
     * @param $restaurantHour
     */
    public function __construct(RestaurantHour $restaurantHour)
    {
        $this->restaurantHour = $restaurantHour;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllRestaurantHour()
    {
        return $this->restaurantHour->all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getSpecificRestaurantHour($id)
    {
       return $this->restaurantHour->findOrFail($id);
    }

    /**
     * @param $branch_id
     * @param $day
     * @return mixed
     */
    public function getSpecificRestaurantHourByIdRestaurantIdDay($branch_id, $day)
    {
       return $this->restaurantHour->where([
           ["branch_id",$branch_id],
           ["day",$day]
       ])->get();
    }

    /**
     * @param $branch_id
     * @param $day
     * @param $shift
     * @return mixed
     */
    public function getSpecificRestaurantHourByIdRestaurantIdDayShift($branch_id, $day, $shift){
        return $this->restaurantHour->where([
            ["branch_id",$branch_id],
            ["day",$day],
            ["shift",$shift]
        ])->get();
    }

    /**
     * @param $branch_id
     * @return mixed
     */
    public function getSpecificRestaurantHourByIdRestaurantId($branch_id)
    {
        return $this->restaurantHour->where([
            ["branch_id",$branch_id]
        ])->get();
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function createRestaurantHour(array $attributes)
    {
       return $this->restaurantHour->create($attributes);
    }

    /**
     * @param $id
     * @param array $attributes
     * @return mixed
     */
    public function updateRestaurantHour($id, array $attributes)
    {
        $con=$this->restaurantHour->findOrFail($id)->fillable($attributes);
        $this->findOrFail($id)->update($attributes);
        return $con;
    }

    /**
     * @param $branch_id
     * @return mixed
     */
    public function deleteRestaurantHourByBranchId($branch_id)
    {
        return $this->restaurantHour->where("branch_id",$branch_id)->delete();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRestaurantHourById($id){
        return $this->restaurantHour->findOrfail($id)->delete();
    }

    public function getHourByBranchIdAndDay($branchId)
    {
        $day = Carbon::now()->format( 'l' );

        return $this->restaurantHour->where("branch_id", $branchId);
    }

//    public function getSpecificRestaurantHourByIdRestaurantIdDayAsap($branch_id, $day,$time){
//        return $this->restaurantHour->where([
//            ["branch_id",$branch_id],
//            ["day",$day],
//            ["opening_time",">=",$time],
//            ["closing_time",">=",$time]
//        ])->get();
//    }


}