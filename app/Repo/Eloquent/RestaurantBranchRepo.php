<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:02 AM
 */

namespace App\Repo\Eloquent;

use App\BranchTranslation;
use App\Repo\RestaurantBranchInterface;
use App\RestaurantBranch;
use Carbon\Carbon;
use Cocur\Slugify\Slugify;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use RoleChecker;

class RestaurantBranchRepo implements RestaurantBranchInterface
{
    private $restaurantBranch;
    private $branchTranslation;
    private $slugify;

    /**
     * CountryRepo constructor.
     * @param $country
     */
    public function __construct(RestaurantBranch $restaurantBranch, BranchTranslation $branchTranslation, Slugify $slugify)
    {
        $this->restaurantBranch = $restaurantBranch;
        $this->branchTranslation = $branchTranslation;
        $this->slugify = $slugify;
    }

    public function getAllRestaurantBranchByStatus($restaurant_id, $status, $limit, $lang)
    {
        if (RoleChecker::hasRole('branch-admin') || (RoleChecker::get('parentRole') == 'branch-admin')) {
            try {
                if (RoleChecker::get('is_restaurant_admin') != false) {
                    throw new \Exception();
                }
                if (is_null($status)) {
                    return $this->restaurantBranch->where('id', RoleChecker::get('branch_id'))->get();
                }
                return $this->restaurantBranch->where([['id', RoleChecker::get('branch_id')], ['status', $status]])->paginate($limit);
            } catch (\Exception $ex) {
                return null;
            }
        } elseif (RoleChecker::hasRole('restaurant-admin') || (RoleChecker::get('parentRole') == 'restaurant-admin')) {
            $restaurantBranchInfo = RoleChecker::getRestaurantInfo();
            if (!($restaurantBranchInfo["is_restaurant_admin"] && $restaurantBranchInfo["restaurant_id"])) return null;
            if (is_null($status)) {
                $response = $this->restaurantBranch->where('parent_id', $restaurant_id)->paginate($limit);
                $path = "/restaurant/$restaurant_id/branch";
                $response->appends(["limit" => $limit, "lang" => $lang])->withPath($path);
                return $response;

            }
            $response = $this->restaurantBranch->where([['parent_id', $restaurant_id], ['status', $status]])->paginate($limit);
            $path = "/restaurant/$restaurant_id/branch";
            $response->appends(["limit" => $limit, "lang" => $lang, "status" => $status])->withPath($path);
            return $response;
        } else {
            if (is_null($status)) {
                $response = $this->restaurantBranch->where('parent_id', $restaurant_id)->paginate($limit);
                $path = "/restaurant/$restaurant_id/branch";
                $response->appends(["limit" => $limit, "lang" => $lang])->withPath($path);
                return $response;

            }
            $response = $this->restaurantBranch->where([['parent_id', $restaurant_id], ['status', $status]])->paginate($limit);
            $path = "/restaurant/$restaurant_id/branch";
            $response->appends(["limit" => $limit, "lang" => $lang, "status" => $status])->withPath($path);
            return $response;
        }
    }

    public function checkRestaurantBelongsToCity($country_id, $city_id, array $branch_ids){

        return $this->restaurantBranch->whereIn('id',$branch_ids)->where([
            ['country_id',$country_id],
            ['city_id',$city_id]
        ])->get();

    }

    public function getAllRestaurantByIds($restaurants, $filter){
        $columnsList = Schema::getColumnListing("restaurant_branches");
        $order = "id";
        array_push($columnsList,"name");
        foreach ($columnsList as $columnName) {
            if (isset($filter["sort_column"])) {
                if ($columnName == $filter["sort_column"]) {
                    $order = $columnName;
                    break;
                }
            }
        }

        $restaurantBranch = $this->restaurantBranch->join("branch_translation", "branch_translation.branch_id","=", "restaurant_branches.id")
            ->where("branch_translation.lang_code","=", "en")
            ->whereIn("restaurant_branches.id", $restaurants);
        if (!is_null($filter["search"])) {
            $value = "%" . strtolower($filter["search"]) . "%";
            $slugedValue = "%" . $this->slugify->slugify($value) . "%";
            $restaurantBranch = $restaurantBranch->where(function ($query) use ($columnsList, $value, $slugedValue) {
                foreach ($columnsList as $key => $column) {
                    if ($key === 0) {
                        $query->where("restaurant_branches.".$column, "like", $value);

                    } else {
                        if($column == "name"){
                            $query->orWhere($column, "like", $value)->orWhere($column, "like", $slugedValue);
                            continue;
                        }

                        $query->orWhere($column, "like", $value)->orWhere($column, "like", $slugedValue);
                    }
                }

            });

        }
        $restaurantBranch = $restaurantBranch
            ->select("restaurant_branches.*","branch_translation.name")
            ->orderBy($order, $filter['sort_by']);
        $restaurantBranch = $restaurantBranch->paginate($filter['limit']);
        $restaurantBranch = $restaurantBranch->appends($filter);
        return $restaurantBranch;
    }

    public function getAllRestaurantBranchByStatusForDataTable($restaurant_id)
    {

        if (RoleChecker::hasRole('branch-admin') || (RoleChecker::get('parentRole') == 'branch-admin')) {
            try {
                if (RoleChecker::get('is_restaurant_admin') != false) {
                    throw new \Exception();
                }
//                if (is_null($status)) {
//
//                }
                $response = $this->restaurantBranch->where('id', RoleChecker::get('branch_id'))->orderBy('id', "desc")->get();
            } catch (\Exception $ex) {
                $response = null;
            }
        } else {
            $response = $this->restaurantBranch->where('parent_id', $restaurant_id)->orderBy('id', "desc")->get();
        }
        return $response;

    }

    public function getAllRestaurantBranchForPublic($request)
    {

        $parameter_path = "";

        $restaurantBranch = $this->restaurantBranch->where([
            ['country_id', $request["country_id"]],
            ["status", 1]
        ]);
        $restaurantBranch = $restaurantBranch
            ->whereHas('anyRestaurant', function ($query) use ($request) {
                $query->where('status', 1);
            });
        $parameter_path = "$parameter_path" . "country_id=" . $request["country_id"] . "&";
        if (!is_null($request["city_id"])) {
            $restaurantBranch = $restaurantBranch->where('city_id', $request["city_id"]);
            $parameter_path = "$parameter_path" . "city_id=" . $request["city_id"] . "&";
        }
        if (!is_null($request["district_id"])) {
            $restaurantBranch = $restaurantBranch->where('district_id', $request['district_id']);
            $parameter_path = "$parameter_path" . "district_id=" . $request['district_id'] . "&";
        }

        if (!is_null($request["is_featured"])) {

            $restaurantBranch = $restaurantBranch->where('is_featured', $request['is_featured']);
            $parameter_path = "$parameter_path" . "is_featured=" . $request['is_featured'] . "&";
        }

        $restaurantBranch = $restaurantBranch->select(DB::raw('*'), DB::raw('"true" as open'))
            ->whereHas('restaurantHour', function ($query) {
                $now = gmdate(Carbon::now()->format("H:i:s"));
                $query->where(function ($query) use ($now) {
                    $query->select(DB::raw('*'), DB::raw('"a" as open'))->where([['opening_time', '<', $now], ['closing_time', '>', $now], ['difference_checker', "None"], ['day', DB::raw('DAYNAME(NOW())')]]);
                })
                    ->orWhere(function ($query) use ($now) {
                        $query->where([['closing_time', '>', $now], ['difference_checker', 'Greater'], ['day', DB::raw('DAYNAME(NOW())')]])->select("restaurant_branch.*");
                    })
                    ->orWhere(function ($query) use ($now) {
                        $query->where([['opening_time', '<', $now], ['difference_checker', 'Greater'], ['day', DB::raw('DAYNAME(NOW())')]])->select("restaurant_branch.*");
                    });
            });

        $restaurantBranch1 = $this->restaurantBranch->where([
            ['country_id', $request["country_id"]],
            ["status", 1]
        ]);
        if (!is_null($request["city_id"])) {
            $restaurantBranch1 = $restaurantBranch1->where('city_id', $request["city_id"]);
        }
        if (!is_null($request["district_id"])) {
            $restaurantBranc1h = $restaurantBranch1->where('district_id', $request['district_id']);
        }

        if (!is_null($request["is_featured"])) {

            $restaurantBranch1 = $restaurantBranch1->where('is_featured', $request['is_featured']);
        }

        $restaurantBranch1 = $restaurantBranch1->select(DB::raw('*'), DB::raw('"false" as open'))
            ->whereHas('restaurantHour', function ($query) {
                $now = gmdate(Carbon::now()->format("H:i:s"));
                $query->where(function ($query) use ($now) {
                    $query->where([['opening_time', '>', $now], ['closing_time', '<', $now], ['difference_checker', "Greater"], ['day', DB::raw('DAYNAME(NOW())')]]);
                })
                    ->orWhere(function ($query) use ($now) {
                        $query->where([['closing_time', '<', $now], ['difference_checker', 'None'], ['day', DB::raw('DAYNAME(NOW())')]]);
                    })
                    ->orWhere(function ($query) use ($now) {
                        $query->where([['opening_time', '>', $now], ['difference_checker', 'None'], ['day', DB::raw('DAYNAME(NOW())')]]);
                    });
            });
        $restaurantBranch = $restaurantBranch->union($restaurantBranch1)->get();
//        $restaurantBranch = $this->manualPagination($request['limit'], array_values($restaurantBranch), count($restaurantBranch));

//        $page = Input::get('page', 1);
//        $slice = array_slice($restaurantBranch, $request['limit'] * ($page - 1), $request['limit']);
//        $result = Paginator::make($slice, count($restaurantBranch), $request['limit']);
//        return $result;
//        $restaurantBranch = $restaurantBranch->union($restaurantBranch1);

//        $restaurantBranch = $restaurantBranch1->paginate($request["limit"]);
//        $parameter_path = "$parameter_path" . "limit=" . $request["limit"];
//        $restaurantBranch->withPath("/public/restaurant/branch?" . $parameter_path);
        return ["data" => $restaurantBranch, "parameter" => $parameter_path];
    }


    public function getSpecificRestaurantBranchForPublic($id)
    {
        return $this->restaurantBranch->where([['id', $id], ['status', 1]])->get();
    }

    public function getSpecificRestaurantBranchBySlugForPublic($slug)
    {
        return $this->restaurantBranch->where([['slug', $slug], ['status', 1]])->get();
    }


    public function getSpecificRestaurantBranch($id)
    {
        return $this->restaurantBranch->findOrFail($id);
    }

    public function getSpecificRestaurantBranchBySlug($slug)
    {
        return $this->restaurantBranch->where('slug', $slug)->firstOrFail($slug);
    }

    public function getSpecificRestaurantBranchByStatus($id, $status)
    {
        if (is_null($status)) {
            $temp = $this->restaurantBranch->findOrFail($id);
            return $temp;
        }

        return $this->restaurantBranch->where([["id", $id], ["status", $status]])->first();
    }

    public function getSpecificRestaurantBranchByIdWithRestaurantId($id, $restaurant_id, $status)
    {
        if (is_null($status)) {
            $temp = $this->restaurantBranch->where([['id', $id], ['parent_id', $restaurant_id]])->get();
            return $temp;
        }
        return $this->restaurantBranch->where([['id', $id], ['parent_id', $restaurant_id], ['status', $status]])->get();
    }

    public function createRestaurantBranch(array $attributes)
    {
        $con = $this->restaurantBranch->create($attributes);

        return $con;
    }

    public function updateRestaurantBranch($id, array $attributes)
    {
        $con = $this->restaurantBranch->findOrFail($id)->update($attributes);
        $con = $this->restaurantBranch->findOrFail($id)->fill($attributes);
        return $con;
    }

    public function deleteRestaurantBranch($id)
    {
        return $this->restaurantBranch->findOrFail($id)->delete();
    }

    public function getSpecificSoftDeletedOrNotSoftDeletedRestaurantBranch($id, $status)
    {

        if (is_null($status)) {
            return $this->restaurantBranch->withTrashed()->findOrFail($id);
        }
        return $this->restaurantBranch->withTrashed()->where([['status', $status], ['id', $id]])->first();
    }

    public function getCity($cityId)
    {
        return $this->restaurantBranch->where("city_id", $cityId)->first();
    }

    public function getDistrict($districtId)
    {
        return $this->restaurantBranch->where("city_id", $districtId)->first();
    }

    public function getRestaurantListForDrivers($restaurantIds)
    {
        return $this->restaurantBranch->find($restaurantIds);
    }

    public function searchBranch(array $attributes)
    {
        $parameter_path = "";
        $flag = true;

        if (!array_key_exists('sort', $attributes)) {
            if ($attributes['filter_by'] == 'all') {
                $flag = false;
            }
        }


        if(isset($attributes["restaurants"])){
            $restaurantBranch = $this->restaurantBranch->where([
                ['country_id', $attributes['country_id']],
                ["status", 1]
            ])->whereIn("restaurant_branches.id",$attributes["restaurants"]);
        }
        else{
            $restaurantBranch = $this->restaurantBranch->where([
                ['country_id', $attributes['country_id']],
                ["status", 1]
            ]);
        }
        $restaurantBranch = $restaurantBranch
            ->whereHas('anyRestaurant', function ($query) use ($attributes) {
                $query->where('status', 1);
            });
        $parameter_path = "$parameter_path" . "country_id=" . $attributes['country_id'] . "&";
        if ($attributes['city_id'] > 0) {
            $restaurantBranch = $restaurantBranch->where('city_id', $attributes['city_id']);
            $parameter_path = "$parameter_path" . "city_id=" . $attributes['city_id'] . "&";
        }
        if (!is_null($attributes["restaurants"])) {
//            dd($attributes['restaurants']);
            $restaurantBranch = $restaurantBranch->whereIn("restaurant_branches.id", $attributes['restaurants']);
        }
        if ($attributes['district_id'] > 0) {
            $restaurantBranch = $restaurantBranch->where(function ($q) use ($attributes) {
                $q->whereHas('deliveryZone', function ($query) use ($attributes) {
                    $query->where('district_id', $attributes['district_id']);
                })
                    ->orWhere('district_id', $attributes['district_id']);
            });

            $parameter_path = "$parameter_path" . "district_id=" . $attributes['district_id'] . "&";
        }

        if (array_key_exists("shipping", $attributes)) {
            $restaurantBranch = $restaurantBranch
                ->whereHas('shippingMethod', function ($query) use ($attributes) {
                    $query->where('slug', $attributes['shipping']);
                });
            $parameter_path = "$parameter_path" . "shipping=" . $attributes['shipping'] . "&";
        }

        if (array_key_exists("payment", $attributes)) {
            if ($attributes['payment'] == "cash") {
                $restaurantBranch = $restaurantBranch
                    ->whereHas('paymentMethod', function ($query) use ($attributes) {
                        $query->where('slug', "cash-on-delivery")->orWhere('slug', "cash-on-pickup");
                    });
            } else {
                $restaurantBranch = $restaurantBranch
                    ->whereHas('paymentMethod', function ($query) use ($attributes) {
                        $query->where('slug', $attributes['payment']);
                    });
            }

            $parameter_path = "$parameter_path" . "payment=" . $attributes['payment'] . "&";
        }

        if ($attributes['cuisine'] > 0) {
            $restaurantBranch = $restaurantBranch
                ->whereHas('cuisine', function ($query) use ($attributes) {
                    $query->where('cuisine.id', $attributes['cuisine']);
                });

            $parameter_path = "$parameter_path" . "cuisine=" . $attributes['cuisine'] . "&";
        }

        if (array_key_exists("is_featured", $attributes)) {
            $is_featured = $attributes['is_featured'];
            $restaurantBranch = $restaurantBranch->where('is_featured', $is_featured);
            $parameter_path = "$parameter_path" . "is_featured=" . $attributes['is_featured'] . "&";
        }

        if (array_key_exists("q", $attributes)) {
            $name = $attributes['q'];
            $restaurantBranch = $restaurantBranch->where('name', 'like', "%$name%");
            $parameter_path = "$parameter_path" . "q=" . $attributes['q'] . "&";
        }

        if (array_key_exists("filter_by", $attributes)) {
            if ($attributes['filter_by'] == 'pre_order') {
                $restaurantBranch = $restaurantBranch->where('pre_order', "1");
            } elseif ($attributes['filter_by'] == 'open_restaurants') {
                $restaurantBranch = $restaurantBranch
                    ->whereHas('restaurantHour', function ($query) {
                        $now = gmdate(Carbon::now()->format("H:i:s"));
                        $query->where(function ($query) use ($now) {
                            $query->where([['opening_time', '<', $now], ['closing_time', '>', $now], ['difference_checker', "None"], ['day', DB::raw('DAYNAME(NOW())')], ['closed', 0]]);
                        })
                            ->orWhere(function ($query) use ($now) {
                                $query->where([['closing_time', '>', $now], ['difference_checker', 'Greater'], ['day', DB::raw('DAYNAME(NOW())')], ['closed', 0]]);
                            })
                            ->orWhere(function ($query) use ($now) {
                                $query->where([['opening_time', '<', $now], ['difference_checker', 'Greater'], ['day', DB::raw('DAYNAME(NOW())')], ['closed', 0]]);
                            });
                    });
            } elseif ($attributes['filter_by'] == 'free_delivery') {
                $restaurantBranch = $restaurantBranch->where('free_delivery', 1);
            }
            $parameter_path = "$parameter_path" . "filter_by=" . $attributes['filter_by'] . "&";
        }
        $restaurantBranch = $restaurantBranch->join('branch_translation', 'restaurant_branches.id', '=', 'branch_translation.branch_id')
            ->select('restaurant_branches.*', 'branch_translation.name as name', "branch_translation.lang_code", "branch_translation.address")
            ->where("branch_translation.lang_code", "=", $attributes['lang']);

        if (array_key_exists('sort', $attributes)) {
            $parameter_path = "$parameter_path" . "sort=" . $attributes['sort'] . "&";
            if ($attributes['sort'] == 'rating') {

                $restaurantBranch = $restaurantBranch->join('reviews', 'restaurant_branches.id', '=', 'reviews.restaurant_id', 'left')
                    ->selectRaw('restaurant_branches.*,count(reviews.rating) as rev_num, avg(reviews.rating) as average_rating' )
//                    ->Where(function ($query1) {
////                        $query1->whereNUll("reviews.deleted_at")
//////                            ->whereNUll("reviews.deleted_at")
////                        ;
//                    })
                    ->orderBy('average_rating', "desc")->orderBy('rev_num', "desc")
                    ->groupBY('restaurant_branches.id');
            } else
                $restaurantBranch = $restaurantBranch->orderBy('name', $attributes['sort']);
        }
        $restaurantBranch = $restaurantBranch->get();
//        return count($restaurantBranch);
//        $allRestaurantBranch = $this->restaurantBranch->where([
//            ['country_id', $attributes['country_id']],
//            ["status", 1]
//        ]);
//        return count($allRestaurantBranch->get());

//        if($attributes['sort'] == 'rating'){
//            foreach ($restaurantBranch as $data){
//                unset ($data['average_rating']);
//                unset($data['rev_num']);
//            }
//            $allRestaurantBranch = $this->restaurantBranch->where([
//                ['country_id', $attributes['country_id']],
//                ["status", 1]
//            ]);
//            $allRestaurantBranch = $allRestaurantBranch
//                ->whereHas('anyRestaurant', function ($query) use ($attributes) {
//                    $query->where('status', 1);
//                });
//
//            if ($attributes['city_id'] > 0) {
//                $allRestaurantBranch = $allRestaurantBranch->where('city_id', $attributes['city_id']);
//            }
//            if ($attributes['district_id'] > 0) {
//                $allRestaurantBranch = $allRestaurantBranch->where(function ($q) use ($attributes) {
//                    $q->whereHas('deliveryZone', function ($query) use ($attributes) {
//                        $query->where('district_id', $attributes['district_id']);
//                    })
//                        ->orWhere('district_id', $attributes['district_id']);
//                });
//            }
//            if (!is_null($attributes["restaurants"])) {
//                $allRestaurantBranch = $allRestaurantBranch->whereIn("id", $attributes['restaurants']);
//            }
//            if (array_key_exists("shipping", $attributes)) {
//                $allRestaurantBranch = $allRestaurantBranch
//                    ->whereHas('shippingMethod', function ($query) use ($attributes) {
//                        $query->where('slug', $attributes['shipping']);
//                    });
//            }
//
//            if (array_key_exists("payment", $attributes)) {
//                if ($attributes['payment'] == "cash") {
//                    $allRestaurantBranch = $allRestaurantBranch
//                        ->whereHas('paymentMethod', function ($query) use ($attributes) {
//                            $query->where('slug', "cash-on-delivery")->orWhere('slug', "cash-on-pickup");
//                        });
//                } else {
//                    $allRestaurantBranch = $allRestaurantBranch
//                        ->whereHas('paymentMethod', function ($query) use ($attributes) {
//                            $query->where('slug', $attributes['payment']);
//                        });
//                }
//
//            }
//
//            if ($attributes['cuisine'] > 0) {
//                $allRestaurantBranch = $allRestaurantBranch
//                    ->whereHas('cuisine', function ($query) use ($attributes) {
//                        $query->where('cuisine.id', $attributes['cuisine']);
//                    });
//
//            }
//
//            if (array_key_exists("is_featured", $attributes)) {
//                $is_featured = $attributes['is_featured'];
//                $allRestaurantBranch = $allRestaurantBranch->where('is_featured', $is_featured);
//            }
//
//            if (array_key_exists("q", $attributes)) {
//                $name = $attributes['q'];
//                $allRestaurantBranch = $allRestaurantBranch->where('name', 'like', "%$name%");
//            }
//
//            if (array_key_exists("filter_by", $attributes)) {
//                if ($attributes['filter_by'] == 'pre_order') {
//                    $allRestaurantBranch = $allRestaurantBranch->where('pre_order', "1");
//                } elseif ($attributes['filter_by'] == 'open_restaurants') {
//                    $allRestaurantBranch = $allRestaurantBranch
//                        ->whereHas('restaurantHour', function ($query) {
//                            $now = gmdate(Carbon::now()->format("H:i:s"));
//                            $query->where(function ($query) use ($now) {
//                                $query->where([['opening_time', '<', $now], ['closing_time', '>', $now], ['difference_checker', "None"], ['day', DB::raw('DAYNAME(NOW())')], ['closed', 0]]);
//                            })
//                                ->orWhere(function ($query) use ($now) {
//                                    $query->where([['closing_time', '>', $now], ['difference_checker', 'Greater'], ['day', DB::raw('DAYNAME(NOW())')], ['closed', 0]]);
//                                })
//                                ->orWhere(function ($query) use ($now) {
//                                    $query->where([['opening_time', '<', $now], ['difference_checker', 'Greater'], ['day', DB::raw('DAYNAME(NOW())')], ['closed', 0]]);
//                                });
//                        });
//                } elseif ($attributes['filter_by'] == 'free_delivery') {
//                    $allRestaurantBranch = $allRestaurantBranch->where('free_delivery', 1);
//                }
//            }
//
//            $allRestaurantBranch = $allRestaurantBranch->join('branch_translation', 'restaurant_branches.id', '=', 'branch_translation.branch_id')
//                ->select('restaurant_branches.*', 'branch_translation.name as name', "branch_translation.lang_code", "branch_translation.address")
//                ->where("branch_translation.lang_code", "=", $attributes['lang']);
//
//            $allRestaurantBranch = $allRestaurantBranch->get();
//            $remainingRestaurantBranch = collect($allRestaurantBranch)->diff(collect($restaurantBranch))->values();
//            $restaurantBranch = collect($restaurantBranch)->merge(collect($remainingRestaurantBranch))->values();
//        }

        $parameter_path = "$parameter_path" . "limit=" . $attributes['limit'];
        return ["data" => $restaurantBranch, "parameter" => $parameter_path];
    }

    public function getRestaurantsByCity($country_id,$city_id){
        return $this->restaurantBranch->where([
            ['country_id',$country_id],
            ['city_id',$city_id]
        ])->orderBy('id','desc')->get();
    }

    public function checkRestaurantSlug($countryId, $slug)
    {
        return $this->restaurantBranch->where([["country_id", $countryId], ["slug", $slug]])->first();
    }

    public function getSpecificBranchForExport($id)
    {
        return $this->restaurantBranch->withTrashed()->findOrFail($id);
    }
    public function getRestaurantsByCityforBranchSorting($country_id,$city_id){
        return $this->restaurantBranch
            ->leftjoin('featured_restaurant_sorting','featured_restaurant_sorting.branch_id','=','restaurant_branches.id')
            ->where([
                ['restaurant_branches.country_id',$country_id],
                ['restaurant_branches.city_id', $city_id],
                ])
            ->whereNull('featured_restaurant_sorting.branch_id')
                ->select('restaurant_branches.*')->get();

    }

}

/**
 * pre-order
 * free-delivery
 * restaurant-open
 * all
 */

