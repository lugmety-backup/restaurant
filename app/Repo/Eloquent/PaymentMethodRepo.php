<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/27/2017
 * Time: 11:46 AM
 */

namespace App\Repo\Eloquent;


use App\Repo\PaymentMethodInterface;
use App\PaymentMethod;

class PaymentMethodRepo implements PaymentMethodInterface
{
    protected $paymentMethod;

    /**
     * PaymentMethodRepo constructor.
     * @param $paymentMethod
     */
    public function __construct(PaymentMethod $paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    public function createPaymentMethod(array $request)
    {
       return $this->paymentMethod->create($request);
    }

    public function updatePaymentMethod()
    {
        // TODO: Implement updatePaymentMethod() method.
    }

    public function deletePaymentMethodByrestaurantBranchId($branchId){
return $this->paymentMethod->where("restaurant_branch_id",$branchId)->delete();
    }

}