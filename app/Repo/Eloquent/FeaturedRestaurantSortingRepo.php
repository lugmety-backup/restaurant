<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/13/18
 * Time: 3:23 PM
 */

namespace App\Repo\Eloquent;


use App\Models\FeaturedRestaurantSorting;
use App\Repo\FeaturedRestaurantSortingInterface;

class FeaturedRestaurantSortingRepo implements FeaturedRestaurantSortingInterface
{

    protected $featuredRestaurantSorting;

    public function __construct(FeaturedRestaurantSorting $featuredRestaurantSorting)
    {
        $this->featuredRestaurantSorting = $featuredRestaurantSorting;
    }


    public function getSortedBranch($country_id,$city_id){

        return $this->featuredRestaurantSorting->where([
            ['country_id',$country_id],
            ['city_id',$city_id]
        ])->orderBy('sort')->get();

    }


    public function createSorting(array $request)
    {
        return $this->featuredRestaurantSorting->create($request);
    }

    public function updateSorting($data, array $request)
    {
        return $data->update($request);
    }

    public function insertSorting(array $request)
    {
        return $this->featuredRestaurantSorting->insert($request);
    }

    public function bulkDelete(array $branch_ids){
        if(count($branch_ids) == 0) return true;
        return $this->featuredRestaurantSorting->whereIn('branch_id',$branch_ids)->delete();
    }

    public function bulkDeleteByCityId($country_id, $city_id)
    {
        return $this->featuredRestaurantSorting->where([
            ['country_id',$country_id],
            ['city_id',$city_id]
        ])->delete();
    }


    public function getSortedFeaturedRestaurantBranch(array $request){
        return $this->featuredRestaurantSorting->where([
            ['country_id',$request['country_id']],
            ['city_id',$request['city_id']]
        ])->orderBy('sort')->pluck('branch_id')->values()->all();


    }


}