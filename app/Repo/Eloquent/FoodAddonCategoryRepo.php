<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/9/17
 * Time: 1:20 PM
 */

namespace App\Repo\Eloquent;
use App\Models\Food;
use App\Models\AddOnCategory;
use App\Repo\FoodAddonCategoryInterface;

class FoodAddonCategoryRepo implements FoodAddonCategoryInterface
{
    private $food;
    private $category;

    /**
     * FoodAddonCategoryRepo constructor.
     * @param $food ,$country
     */
    public function __construct(Food $food, AddOnCategory $category)
    {
        $this->food = $food;
        $this->category = $category;
    }

    /**
     * Attach category to food
     * @param $id
     * @param $request
     * @return mixed
     */

    public function attachCategoryToFood($id, $request)
    {
        $food = $this->food->find($id);
        return $food->addonCategory()->attach($request);

    }

    /**
     * Detach category from food
     * @param $id
     * @param $request
     * @return mixed
     */

    public function detachCategoryFromFood($id, $request)
    {
        $food = $this->food->find($id);
        return $food->addonCategory()->detach($request);
    }

}