<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/8/17
 * Time: 3:46 PM
 */

namespace App\Repo\Eloquent;
use App\Repo\AddOnTranslationInterface;
use App\Models\AddOnTranslation;


class AddOnTranslationRepo implements AddOnTranslationInterface
{
    /**
     * @var AddOnTranslation
     */
    private $addon_translation;

    /**
     * AddOnTranslationRepo constructor.
     * @param AddOnTranslation $addon_translation
     */
    public function __construct(AddOnTranslation $addon_translation)
    {
        $this->addon_translation = $addon_translation;
    }

    /**
     * Create addon Translation
     * @param array $attributes
     * @return mixed
     */
    public function createAddOnTranslation(array $attributes)
    {
        $data = $this->addon_translation->create($attributes);
        return $data;
    }

    /**
     * Get specific addon translation for addon of given id for specific language
     * @param $id
     * @param $lang
     * @return mixed
     */
    public function getSpecificAddOnTransalationByLang($id, $lang)
    {
        return $this->addon_translation->where('addon_id',$id)->where('lang',$lang)->get();
    }
}