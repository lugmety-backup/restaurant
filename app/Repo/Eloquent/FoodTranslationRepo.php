<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/5/17
 * Time: 5:24 PM
 */

namespace App\Repo\Eloquent;
use App\Models\FoodTranslation;
use App\Repo\FoodTranslationInterface;


class FoodTranslationRepo implements FoodTranslationInterface
{
    /**
     * @var FoodTranslation
     */
    private $food_translation;

    /**
     * FoodTranslationRepo constructor.
     * @param FoodTranslation $food_translation
     */
    public function __construct(FoodTranslation $food_translation)
    {
        $this->food_translation = $food_translation;
    }

    /**
     * Insert new food translation
     * @param array $attributes
     * @return mixed
     */
    public function createFoodTranslation(array $attributes)
    {
        //dd($attributes);
        $data = $this->food_translation->create($attributes);
        return $data;
    }

    /**
     * Delete food transaltion of given food id.
     * @param $food_id
     * @return mixed
     */
    public function deleteTranslationByFood($food_id)
    {
        return $this->food_translation->where('food_id',$food_id)->delete();
    }

    /**
     * Get specific food translation for food of given id for specific language
     * @param $id
     * @param $lang
     * @return mixed
     */

    public function getSpecificFoodTransalationByLang($id, $lang)
    {
        return $this->food_translation->where('food_id',$id)->where('lang',$lang)->get();
    }

}