<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/27/2017
 * Time: 11:46 AM
 */

namespace App\Repo\Eloquent;


use App\Repo\ShippingMethodInterface;
use App\ShippingMethod;


class ShippingMethodRepo implements ShippingMethodInterface
{
    protected $shippingMethod;

    /**
     * ShippingMethodRepo constructor.
     * @param $shippingMethod
     */
    public function __construct(ShippingMethod $shippingMethod)
    {
        $this->shippingMethod = $shippingMethod;
    }

    public function createShippingMethod(array $request)
    {
        $this->shippingMethod->create($request);
    }

    public function updateShippingMethod()
    {

    }

    public function deleteShippingMethodByBranchId($branchId){
        return $this->shippingMethod->where("restaurant_branch_id",$branchId)->delete();

    }

}