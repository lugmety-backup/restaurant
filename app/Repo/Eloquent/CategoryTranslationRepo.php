<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/3/17
 * Time: 1:38 PM
 */

namespace App\Repo\Eloquent;

use App\Repo\CategoryTranslationInterface;
use App\Models\CategoryTranslation;


class CategoryTranslationRepo implements CategoryTranslationInterface
{

    private $category_translation;

    public function __construct(CategoryTranslation $category_translation)
    {
        $this->category_translation = $category_translation;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllCategoryTranslation()
    {
        return $this->category_translation->all();
    }

    /**
     *
     * INsert category translation
     * @param array $attributes
     * @return mixed
     */
    public function createCategoryTranslation(array $attributes)
    {
        $data = $this->category_translation->create($attributes);
        return $data;
    }

    /**
     * Delete category Translation
     * @param $category_id
     * @return mixed
     */
    public function deleteTranslationByCategory($category_id)
    {
        return $this->category_translation->where('category_id',$category_id)->delete();
    }

    /**
     * Get specific category translation for addon of given id for specific language
     * @param $id
     * @param $lang
     * @return mixed
     */
    public function getSpecificCategoryTransalationByLang($id, $lang)
    {
        return $this->category_translation->where('category_id',$id)->where('lang',$lang)->get();
    }
}