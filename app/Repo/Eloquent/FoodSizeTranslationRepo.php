<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 10/23/17
 * Time: 12:13 PM
 */

namespace App\Repo\Eloquent;


use App\Models\FoodSizeTranslation;
use App\Repo\FoodSizeTranslationInterface;

class FoodSizeTranslationRepo implements FoodSizeTranslationInterface
{
    private $translation;

    public function __construct(FoodSizeTranslation $translation)
    {
        $this->translation = $translation;
    }

    public function createFoodSizeTranslation(array $attributes)
    {
        $result = $this->translation->create($attributes);
        return $result;
    }

    public function updateFoodSizeTranslation($id, array $attributes)
    {
        $this->translation->findOrFail($id)->update($attributes);
        $result = $this->translation->findOrFail($id);
        return $result;
    }
}