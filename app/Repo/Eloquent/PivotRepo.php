<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 8/4/17
 * Time: 1:59 PM
 */

namespace App\Repo\Eloquent;
use App\Models\AddonAddonCategoryPivot;
use App\Models\FoodCategoryPivot;
use App\Models\FoodAddonCategoryPivot;
use App\Repo\PivotInterface;
use App\Models\RestaurantCategoryPivot;


class PivotRepo implements PivotInterface
{
    private $addonAddonCategory;
    private $foodCategory;
    private $foodAddonCategory;
    private $restaurantCategory;

public function __construct(AddonAddonCategoryPivot $addonAddonCategory, FoodAddonCategoryPivot $foodAddonCategory, FoodCategoryPivot $foodCategory, RestaurantCategoryPivot $restaurantCategory)
{
    $this->addonAddonCategory = $addonAddonCategory;
    $this->foodAddonCategory = $foodAddonCategory;
    $this->foodCategory = $foodCategory;
    $this->restaurantCategory = $restaurantCategory;
}

    public function deletePivotForFood($id, $addon_delete)
    {
        $this->foodCategory->where('food_id', $id)->delete();
        if($addon_delete == 1)
        {
            $this->foodAddonCategory->where('food_id', $id)->delete();
        }

        return true;
    }

    public function deletePivotForAddon($id)
    {
        $this->addonAddonCategory->where('addon_id', $id)->delete();
        return true;
    }

    public function deletePivotForRestaurant($id){
        $this->restaurantCategory->where('restaurant_id', $id)->delete();
        return true;
    }
}