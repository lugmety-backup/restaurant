<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:02 AM
 */

namespace App\Repo\Eloquent;

use App\Repo\DeliveryZoneInterface;
use App\DeliveryZone;
use App\Restaurant;

class DeliveryZoneRepo implements DeliveryZoneInterface
{
    private $deliveryZone;
    private $restaurant;

    /**
     * CountryRepo constructor.
     * @param $country
     */
    public function __construct(DeliveryZone $deliveryZone, Restaurant $restaurant)
    {
        $this->deliveryZone = $deliveryZone;
        $this->restaurant=$restaurant;
    }

    public function getAllDeliveryZone($branch_id )
    {
            return $this->deliveryZone->where('branch_id',$branch_id)->get();
    }

    public function getAllDeliveryZoneForDataTable($branch_id)
    {
        $response = $this->deliveryZone->where('branch_id',$branch_id)->get();
        return $response;
    }

    public function getDeliveryZoneByDistrictWithBranchId($district_id,$branch_id){
        return $this->deliveryZone->where([['district_id',$district_id],['branch_id',$branch_id]])->get();
    }

    public function getSpecificDeliveryZone($id)
    {
        return $this->deliveryZone->findOrFail($id);
    }

    public function createDeliveryZone(array $attributes)
    {
        $con= $this->deliveryZone->create($attributes);
        return $con;
    }

    public function updateDeliveryZone($id, array $attributes)
    {
        $con = $this->deliveryZone->findOrFail($id)->update($attributes);
        $con = $this->deliveryZone->findOrFail($id)->fill($attributes);
        return $con;
    }

    public function deleteDeliveryZone($id)
    {
        return $this->deliveryZone->findOrFail($id)->delete();
    }

}