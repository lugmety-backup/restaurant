<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 7/7/2017
 * Time: 2:17 PM
 */

namespace App\Repo\Eloquent;


use App\Cuisine;
use App\Repo\CuisineInterface;

class CuisineRepo implements CuisineInterface
{
    protected $cuisine;

    /**
     * CuisineRepo constructor.
     * @param $cuisine
     */
    public function __construct(Cuisine $cuisine)
    {
        $this->cuisine = $cuisine;
    }

    public function getAllCuisineForPublic($countryId)
    {
        return $this->cuisine->where([
            ["status", 1],
            ["country_id", $countryId]
        ])->get();
    }

    public function getAllCuisineForAdmin($countryId)
    {
        return $this->cuisine->where("country_id", $countryId)->get();
    }

    public function getSpecificCuisineForAdmin($id)
    {
        return $this->cuisine->findOrFail($id);
    }


    public function createCuisine(array $request)
    {
        return $this->cuisine->create($request);
    }

    public function updateCuisine($id, array $request)
    {
        $update = $this->cuisine->findOrFail($id);
        $update->update($request);
        return $update;
    }

    public function viewCuisineSpecific($id)
    {
        return $this->cuisine->findOrFail($id);
    }

    public function viewSpecificCuisineBasedOnStatus($id)
    {
        return $this->cuisine->where([
            ["status", 1],
            ["id", $id]
        ])->firstOrFail();
    }

}