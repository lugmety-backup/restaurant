<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/7/2017
 * Time: 9:28 PM
 */

namespace App\Repo\Eloquent;

use App\Models\Review;
use App\Repo\ReviewInterface;
use Illuminate\Support\Facades\DB;

class ReviewRepo implements ReviewInterface
{
    protected $review;

    /**
     * ReviewRepo constructor.
     * @param $review
     */
    public function __construct(Review $review)
    {
        $this->review = $review;
    }
    public function getAllReviewByRestaurantIdAndLimit($restaurantId , $limit){
        return $this->review->where([
            ["restaurant_id",$restaurantId],
            ["review_status","approved"]
        ])->latest()->paginate($limit)->withPath("/public/restaurant/$restaurantId/review?limit=".$limit);
    }


    public function getAllReviewByRestaurantId($restaurant){
        return $this->review->where("restaurant_id",$restaurant)->latest()->get();
    }


    public function getSpecificReviewByUser($id, $userId){
        return $this->review->where([
            ["id", $id],
            ["user_id",$userId]
        ])->firstOrFail();
    }

    public function getSpecificReview($id){
        return $this->review->where("id", $id)->firstOrFail();
    }

    public function getAllReviewOfUser($userId, $limit)
    {
        return $this->review->select("rating","review")->where("user_id", $userId)->paginate($limit)->withPath("/restaurant/review?limit=".$limit);
    }

    public function createReview(array $request)
    {
        return $this->review->create($request);
    }

    public function updateReview($review ,array $request)
    {
        return $review->update($request);
    }

    public function getAllReviewListByOrderId($orderIds)
    {
        return $this->review->whereIn("order_id",$orderIds)->get();
    }

    public function getAllReviewForAdmin($count = false)
    {
        if($count){
            return $this->review->where("review_status","pending")->count();
        }
        else {
            return DB::table('reviews')
                ->join('branch_translation', 'reviews.restaurant_id', '=', 'branch_translation.branch_id')
                ->join('restaurant_branches', 'reviews.restaurant_id', '=', 'restaurant_branches.id')
                ->select('reviews.*', 'branch_translation.name',"restaurant_branches.parent_id")
                ->where("branch_translation.lang_code", "=", "en")
                ->where("reviews.deleted_at", "=", null)
                ->orderBy("reviews.id", "desc")->get();
        }
    }

    public function getReviewForRestaurantAdmin(array $branchList)
    {
        return DB::table('reviews')
            ->join('branch_translation', 'reviews.restaurant_id', '=', 'branch_translation.branch_id')
            ->join('restaurant_branches', 'reviews.restaurant_id', '=', 'restaurant_branches.id')
            ->select('reviews.*', 'branch_translation.name',"restaurant_branches.parent_id")
            ->whereIn("restaurant_id",$branchList)
            ->where("branch_translation.lang_code", "=", "en")
            ->where("reviews.deleted_at", "=", null)
            ->orderBy("reviews.id", "desc")->get();
    }

    public function viewSpecificReview($id)
    {
        return $this->review->findOrFail($id);
    }


}