<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/10/17
 * Time: 2:40 PM
 */

namespace App\Repo\Eloquent;
use App\Models\AddOn;
use App\Models\AddOnCategory;
use App\Repo\AddonAddonCategoryInterface;


class AddonAddonCategoryRepo implements AddonAddonCategoryInterface
{
    private $addon;
    private $category;

    /**
     * FoodAddonCategoryRepo constructor.
     * @param $food ,$country
     */
    /**
     * AddonAddonCategoryRepo constructor.
     * @param AddOn $addon
     * @param AddOnCategory $category
     */
    public function __construct(AddOn $addon, AddOnCategory $category)
    {
        $this->addon = $addon;
        $this->category = $category;
    }

    /**
     *
     * Attach addon Category to food
     * @param $id
     * @param $request
     * @return mixed
     */
    public function attachCategoryToAddon($id, $request)
    {
        $addon= $this->addon->find($id);
        return $addon->addonCategory()->attach($request);

    }

    /**
     *
     * Detach addon category from food
     * @param $id
     * @param $request
     * @return mixed
     */

    public function detachCategoryFromAddon($id, $request)
    {
        $food = $this->addon->find($id);
        return $food->addonCategory()->detach($request);
    }
}