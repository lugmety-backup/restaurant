<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/13/18
 * Time: 3:23 PM
 */

namespace App\Repo\Eloquent;


use App\Repo\BranchSortingInterface;
use App\Models\BranchSorting;
class BranchSortingRepo implements BranchSortingInterface
{

    private $branchSorting;

    public function __construct(BranchSorting $branchSorting)
    {
        $this->branchSorting = $branchSorting;
    }


    public function getSortedBranch($country_id,$city_id){

        return $this->branchSorting->where([
            ['country_id',$country_id],
            ['city_id',$city_id]
        ])->orderBy('sort')->get();
    }


    public function createSorting(array $request)
    {
        return $this->branchSorting->create($request);
    }

    public function updateSorting($data, array $request)
    {
        return $data->update($request);
    }

    public function insertSorting(array $request)
    {
        return $this->branchSorting->insert($request);
    }

    public function bulkDelete(array $branch_ids){
        if(count($branch_ids) == 0) return true;
        return $this->branchSorting->whereIn('branch_id',$branch_ids)->delete();
    }

    public function bulkDeleteByCityId($country_id, $city_id)
    {
        return $this->branchSorting->where([
            ['country_id',$country_id],
            ['city_id',$city_id]
        ])->delete();
    }


}