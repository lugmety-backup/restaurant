<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/5/17
 * Time: 5:24 PM
 */

namespace App\Repo\Eloquent;

use App\Models\Food;
use App\Repo\FoodInterface;
use App\Repo\RestaurantBranchInterface;
use App\Models\AddOnCategory;


class FoodRepo implements FoodInterface
{
    /**
     * @var Food
     */
    private $food, $branch, $addOnCategory;

    /**
     * FoodRepo constructor.
     * @param Food $food
     */
    public function __construct(Food $food, RestaurantBranchInterface $branch, AddOnCategory $addOnCategory)
    {
        $this->food = $food;
        $this->branch = $branch;
        $this->addOnCategory = $addOnCategory;
    }

    /**
     * Get all food related to specific restaurant.
     * If $status = "all", get both active and inactive food.
     * Else, get either active or inactive food according to value of $status.
     * No. of food to be displayed at a time is given by value of $limit.
     * If $veg = "all", get both veg and non-veg food.
     * Else, get either veg or non-veg food according to value of $veg.
     * @param $restaurantId
     * @param $status
     * @param $limit
     * @param $veg
     * @return mixed
     */
    public function getAllFood($restaurantId, $status, $limit, $veg)
    {
        if ($veg == "all") {
            if ($status == "all") {
                return $this->food->where('restaurant_id', $restaurantId)->paginate($limit);
            } else {
                return $this->food->where('restaurant_id', $restaurantId)->where('status', $status)->paginate($limit);
            }
        } else {
            if ($status == "all") {
                return $this->food->where('restaurant_id', $restaurantId)->where('veg', $veg)->paginate($limit);
            } else {
                return $this->food->where('restaurant_id', $restaurantId)->where('status', $status)->where('veg', $veg)->paginate($limit);
            }
        }

    }

    /**
     * Get specific food of given id
     * @param $id
     * @return mixed
     */
    public function getSpecificFood($id)
    {
        return $this->food->findOrFail($id);
    }


    public function getActiveAndNonOutOfStockFoods(array $ids){
        return $this->food->whereIn("id",$ids)->where([
            ["status",1],
            ['out_of_stock','0']
        ])->count();
    }

    /**
     * Insert new food
     * @param array $attributes
     * @return mixed
     */
    public function createFood(array $attributes)
    {
        $result = $this->food->create($attributes);
        return $result;
    }

    /**
     * Update existing food of given id
     * @param $id
     * @param array $attributes
     * @return mixed
     */

    public function updateFood($id, array $attributes)
    {

        $this->food->findOrFail($id)->update($attributes);
        $result = $this->food->findOrFail($id);
        return $result;
    }

    /**
     * Delete food
     * @param $id
     * @return mixed
     */
    public function deleteFood($id)
    {
        return $this->food->findOrFail($id)->delete();
    }

    /**
     * Get specific food of given id for given restaurant
     * @param $id
     * @param $restaurantId
     * @return mixed
     */
    public function getSpecificFoodByRestaurant($id, $restaurantId)
    {
        return $this->food->where('restaurant_id', $restaurantId)->findOrFail($id);
    }

    public function getFoodForDatatable($restaurantId)
    {
        return $this->food->with(['foodTranslation' => function ($query) {
            $query->where('lang', 'en');
        }])->where('restaurant_id', $restaurantId)->select('food.*')->get();
    }


    public function getActiveSpecificFood($id)
    {
        return $this->food->where('status', 1)->findOrFail($id);

    }


    public function getDataForFoodValidation($restaurantId)
    {
        $restaurant = $this->branch->getSpecificRestaurantBranch($restaurantId);
        $categories = $restaurant->category()->select('category.id')->where('status',1)->get();

        $categoryIds = [];
        $addonCategoriesIds = [];

        foreach ($categories as $category)
        {
            $categoryIds[] = $category['id'];

        }

        $addonCategories = $this->addOnCategory->where([
            ['restaurant_id', $restaurantId],
            ['status',1]
        ])->get();

        foreach ($addonCategories as $category)
        {
            $addonCategoriesIds[] = $category['id'];

        }
        //return $addonCategoies;
       $data = [
            'restaurant' => $restaurant,
            'categories' => $categoryIds,
           'addonCategories' => $addonCategoriesIds

            ];
       return $data;

    }

    public function getFoodByFoodIds(array $foodId)
    {
        return $this->food->select("id")->whereIn("id",$foodId)->get();
    }


}