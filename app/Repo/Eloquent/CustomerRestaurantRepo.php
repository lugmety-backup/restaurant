<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/7/2017
 * Time: 9:28 PM
 */

namespace App\Repo\Eloquent;

use App\Models\CustomerRestaurant;
use App\Repo\CustomerRestaurantInterface;
use Illuminate\Support\Facades\DB;

class CustomerRestaurantRepo implements CustomerRestaurantInterface
{
    protected $customerRestaurant;

    /**
     * CustomerRestaurantRepo constructor.
     * @param $customerRestaurant
     */
    public function __construct(CustomerRestaurant $customerRestaurant)
    {
        $this->customerRestaurant = $customerRestaurant;
    }

    public function getAllRestaurantByUserId($userId)
    {
        return $this->customerRestaurant->where([
            ["user_id", $userId]
        ])->get();
    }


    public function checkUserBelongsToRestaurant($userId,$restaurantId){
        return $this->customerRestaurant->where([
            ["user_id", $userId],
            ['restaurant_id',$restaurantId]
        ])->first();
    }

    public function createCustomerRestaurant(array $request)
    {
        return $this->customerRestaurant->create($request);
    }

    public function createMultipleCustomerRestaurant(array $request)
    {
        return $this->customerRestaurant->insert($request);
    }

    public function deleteAndCreateMultipleCustomerRestaurant($userId, array $request)
    {
        $this->customerRestaurant->where("user_id", $userId)->delete();
        return $this->customerRestaurant->insert($request);
    }

    public function updateCustomerRestaurant($customerRestaurant, array $request)
    {
        return $customerRestaurant->update($request);
    }

    public function deleteAllRestaurantByUserId($userId)
    {
        return $this->customerRestaurant->where("user_id", $userId)->delete();
    }

    public function deleteRestaurantByUserId($userId, $restaurantId)
    {
        return $this->customerRestaurant->where([["user_id", $userId],["restaurant_id",$restaurantId]])->delete();
    }


}