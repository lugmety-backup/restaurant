<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/7/2017
 * Time: 9:28 PM
 */

namespace App\Repo\Eloquent;

use App\RestaurantReservation;
use App\Repo\RestaurantReservationInterface;

class RestaurantReservationRepo implements RestaurantReservationInterface
{
    protected $restaurantReservation;

    /**
     * RestaurantReservationRepo constructor.
     * @param $RestaurantReservation
     */
    public function __construct(RestaurantReservation $restaurantReservation)
    {
        $this->restaurantReservation = $restaurantReservation;
    }

    public function getAllRestaurantReservation($status)
    {
        if ($status == "new" || $status == "accept" || $status == "reject") {
            return $this->restaurantReservation->where("status", $status)->get();
        }
        return $this->restaurantReservation->all();
    }

    public function getAllRestaurantReservationOfBranch($branchId, $status)
    {
        if ($status == "new" || $status == "accept" || $status == "reject") {
            return $this->restaurantReservation->where([["restaurant_id",$branchId],["status", $status]])->get();
        }
        return $this->restaurantReservation->where("restaurant_id",$branchId)->get();
    }

    public function getAllRestaurantReservationOfUser($userId)
    {
            return $this->restaurantReservation->where([["user_id",$userId]])->get();
    }

    public function getSpecificRestaurantReservation($id)
    {
        return $this->restaurantReservation->findOrFail($id);
    }

    public function createRestaurantReservation(array $attributes)
    {
        $con = $this->restaurantReservation->create($attributes);

        return $con;
    }

    public function updateRestaurantReservation($id, array $request)
    {
        return $this->restaurantReservation->findOrFail($id)->update($request);
    }

    public function deleteRestaurantReservation($id)
    {
        return $this->restaurantReservation->findOrFail($id)->delete();
    }
}