<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/10/17
 * Time: 2:39 PM
 */

namespace App\Repo;


interface AddonAddonCategoryInterface
{
    public function attachCategoryToAddon($id, $request);

    public function detachCategoryFromAddon($id, $request);

}