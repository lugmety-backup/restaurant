<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 5/13/2017
 * Time: 8:20 AM
 */

namespace App\Repo;


/**
 * Interface RestaurantHourInterface
 * @package App\Repo
 */
interface RestaurantHourInterface
{
    public function getAllRestaurantHour();

    /**
     * @param $id
     * @return mixed
     */
    public function getSpecificRestaurantHour($id);

    /**
     * @param $branch_id
     * @param $day
     * @return mixed
     */
    public function getSpecificRestaurantHourByIdRestaurantIdDay($branch_id, $day);

    /**
     * @param $branch_id
     * @param $day
     * @param $shift
     * @return mixed
     */
    public function getSpecificRestaurantHourByIdRestaurantIdDayShift($branch_id, $day, $shift);

    /**
     * @param $branch_id
     * @return mixed
     */
    public function getSpecificRestaurantHourByIdRestaurantId($branch_id);

    /**
     * @param array $attributes
     * @return mixed
     */
    public function createRestaurantHour(array $attributes);

    /**
     * @param $id
     * @param array $attributes
     * @return mixed
     */
    public function updateRestaurantHour($id, array $attributes);

    /**
     * @param $branch_id
     * @return mixed
     */
    public function deleteRestaurantHourByBranchId($branch_id);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRestaurantHourById($id);

    public function getHourByBranchIdAndDay($branchId);
}