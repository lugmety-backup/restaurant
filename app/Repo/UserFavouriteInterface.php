<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 7/20/17
 * Time: 11:49 AM
 */

namespace App\Repo;


interface UserFavouriteInterface
{
    public function createFavourite(array $attributes);

    public function getFavOfUser($user_id,$limit);

    public function deleteFavourite($id, $userId);

    public function getSpecificFavourite($id, $userId);

}