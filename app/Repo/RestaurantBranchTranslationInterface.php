<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/28/2017
 * Time: 11:54 AM
 */

namespace App\Repo;


/**
 * Interface RestaurantBranchTranslationInterface
 * @package App\Repo
 */
interface RestaurantBranchTranslationInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function getAllRestaurantBranchTranslation($id);

    /**
     * @param $id
     * @param $lang
     * @return mixed
     */
    public function getAllRestaurantBranchTranslationByLangCode($id, $lang);

    /**
     * @param $id
     * @return mixed
     */
    public function getSpecificRestaurantBranchTranslation($id);

    /**
     * @param array $attributes
     * @return mixed
     */
    public function createRestaurantBranchTranslation(array $attributes);

    /**
     * @param $id
     * @param array $attributes
     * @return mixed
     */
    public function updateRestaurantBranchTranslation($id, array $attributes);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRestaurantBranchTranslation($id);

    public function getRestaurantNameBtRestaurantList(array $list);
}