<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:01 AM
 */

namespace App\Repo;


interface RestaurantBranchInterface
{
    public function getAllRestaurantBranchByStatus($restaurant_id, $status, $limit, $lang);

    public function getAllRestaurantByIds($restaurants, $filter);

    public function getSpecificRestaurantBranch($id);

    public function getSpecificRestaurantBranchBySlug($slug);

    public function getAllRestaurantBranchByStatusForDataTable($restaurant_id);

    public function getAllRestaurantBranchForPublic ($request);

    public function getSpecificRestaurantBranchBySlugForPublic($slug);

    public function getSpecificRestaurantBranchForPublic($id);

    public function getSpecificRestaurantBranchByStatus($id, $status);

    public function getSpecificRestaurantBranchByIdWithRestaurantId($id,$restaurant_id, $status);

    public function createRestaurantBranch(array $attributes);

    public function updateRestaurantBranch($id, array $attributes);

    public function deleteRestaurantBranch($id);

    public function getCity($cityId);

    public function getDistrict($districtId);

    public function searchBranch(array $attributes);

    public function getRestaurantsByCity($country_id,$city_id);

    public function checkRestaurantBelongsToCity($country_id, $city_id, array $branch_ids);

    public function getSpecificSoftDeletedOrNotSoftDeletedRestaurantBranch($id, $status);

    public function checkRestaurantSlug($countryId,$slug);

    public function getSpecificBranchForExport($id);
    public function getRestaurantsByCityforBranchSorting($country_id,$city_id);
}