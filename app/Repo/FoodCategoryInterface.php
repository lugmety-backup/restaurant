<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/9/17
 * Time: 1:19 PM
 */

namespace App\Repo;


interface FoodCategoryInterface
{
    public function getAllCategoryOfFood($id);

    public function attachCategoryToFood($id, $request);

    public function detachCategoryFromFood($id, $request);

    public function getAllFoodByCategory($id);

}