<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/8/17
 * Time: 4:17 PM
 */

namespace App\Repo;


interface AddOnCategoryTranslationInterface
{
    public function createAddOnCatTranslation(array $attributes);

    public function getSpecificAddOnCatTransalationByLang($id,$lang);
}