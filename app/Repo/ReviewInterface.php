<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/7/2017
 * Time: 9:28 PM
 */

namespace App\Repo;


interface ReviewInterface
{
    public function getAllReviewOfUser($userId, $limit);

    public function getSpecificReview($id);

    public function getSpecificReviewByUser($id, $userId);

    public function getAllReviewByRestaurantIdAndLimit($restaurantId , $limit);

    public function getAllReviewForAdmin();

    public function viewSpecificReview($id);

    public function getReviewForRestaurantAdmin(array $branchList);

    public function getAllReviewByRestaurantId($restaurant);

    public function createReview(array $request);

    public function getAllReviewListByOrderId($orderIds);

    public function updateReview($review , array $request);
}