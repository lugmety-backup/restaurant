<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/27/2017
 * Time: 11:45 AM
 */

namespace App\Repo;


interface ShippingMethodInterface
{
    public function createShippingMethod(array $request);

    public function updateShippingMethod();

    public function deleteShippingMethodByBranchId($branchId);
}