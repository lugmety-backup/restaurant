<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/8/17
 * Time: 3:42 PM
 */

namespace App\Repo;


interface AddOnCategoryInterface
{
    public function getAllAddOnCategory($restaurantId,$status,$limit);

    public function getSpecificAddOnCategory($id);

    public function createAddOnCategory(array $attributes);

    public function updateAddOnCategory($id, array $attributes);

    public function deleteAddOnCategory($id);

    public function getSpecificAddonCategoryByRestaurant($id, $restaurantId);

    public function getAddonCategoryForDatatable($restaurantId);

    public function getAllActiveAddonCategories($restaurantId);

}