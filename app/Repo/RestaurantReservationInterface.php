<?php
/**
 * Created by PhpStorm.
 * branch: ashish
 * Date: 8/7/2017
 * Time: 9:28 PM
 */

namespace App\Repo;


interface RestaurantReservationInterface
{
    public function getAllRestaurantReservation($status);

    public function getAllRestaurantReservationOfBranch($branchId, $status);

    public function getAllRestaurantReservationOfUser($userId);

    public function getSpecificRestaurantReservation($id);

    public function createRestaurantReservation(array $attributes);

    public function updateRestaurantReservation($id , array $request);

    public function deleteRestaurantReservation($id);
}