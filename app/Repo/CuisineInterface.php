<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 7/7/2017
 * Time: 2:17 PM
 */

namespace App\Repo;


interface CuisineInterface
{
    public function getAllCuisineForPublic($countryId);

    public function getAllCuisineForAdmin($countryId);

    public function getSpecificCuisineForAdmin($id);

    public function createCuisine(array $request);

    public function updateCuisine($id, array $request);

    public function viewCuisineSpecific($id);

    public function viewSpecificCuisineBasedOnStatus($id);
}