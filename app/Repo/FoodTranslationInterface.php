<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/5/17
 * Time: 2:19 PM
 */

namespace App\Repo;


interface FoodTranslationInterface
{
    public function createFoodTranslation(array $attributes);

    public function deleteTranslationByFood($id);

    public function getSpecificFoodTransalationByLang($id,$lang);

}