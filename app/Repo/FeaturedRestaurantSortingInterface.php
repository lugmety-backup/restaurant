<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/13/18
 * Time: 3:09 PM
 */

namespace App\Repo;


interface FeaturedRestaurantSortingInterface
{

    public function getSortedBranch($country_id,$city_id);

    public function createSorting(array $request);

    public function updateSorting($id,array $request);

    public function insertSorting(array $request);

    public function bulkDelete(array $branch_ids);

    public function bulkDeleteByCityId($country_id,$city_id);

    public function getSortedFeaturedRestaurantBranch(array $request);

}