<?php

/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/3/17
 * Time: 1:34 PM
 */

namespace App\Repo;

interface FoodInterface
{
    public function getAllFood($restaurantId, $status, $limit, $veg);

    public function getSpecificFood($id);

    public function createFood(array $attributes);

    public function updateFood($id, array $attributes);

    public function deleteFood($id);

    public function getSpecificFoodByRestaurant($id, $restaurantId);

    public function getFoodForDatatable($restaurantId);

    public function getActiveSpecificFood($id);

    public function getActiveAndNonOutOfStockFoods(array $ids);

    public function getDataForFoodValidation($restaurantId);

    public function getFoodByFoodIds(array $foodId);

}