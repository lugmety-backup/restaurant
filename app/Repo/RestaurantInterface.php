<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:01 AM
 */

namespace App\Repo;


/**
 * Interface RestaurantInterface
 * @package App\Repo
 */
interface RestaurantInterface
{
    /**
     * @param $status
     * @param $limit
     * @return mixed
     */
    public function getAllRestaurant($status, $limit);


    public function getAllRestaurantForDatatableBasedOnCountry($countryId);

    /**
     * @param $status
     * @param $limit
     * @param $country_id
     * @return mixed
     */
    public function getAllRestaurantByCountryId($status, $limit, $country_id);

    /**
     * @param $id
     * @param $status
     * @return mixed
     */
    public function getSpecificRestaurant($id, $status);

      /**
     * @param $id
     * @param $status
     * @return mixed
     */
    public function getSpecificSoftDeletedOrNotSoftDeletedRestaurant($id, $status);

    public function getSpecificRestaurantCheckForBranch($id, $status);

    /**
     * @param $id
     * @param $email
     * @return mixed
     */
    public function checkEmail($id, $email);

    /**
     * @param array $attributes
     * @return mixed
     */
    public function createRestaurant(array $attributes);

    /**
     * @param $id
     * @param array $attributes
     * @return mixed
     */
    public function updateRestaurant($id, array $attributes);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRestaurant($id);

    /**
     * @param $id
     * @return mixed
     */
    public function forceDeleteRestaurant($id);

    public function getCountry($countryId);
}