<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/8/17
 * Time: 3:41 PM
 */

namespace App\Repo;


interface AddOnTranslationInterface
{
    public function createAddOnTranslation(array $attributes);

    public function getSpecificAddOnTransalationByLang($id,$lang);

}