<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/3/17
 * Time: 1:37 PM
 */

namespace App\Repo;


interface CategoryInterface
{

    public function getAllCategory($status,$sort_by,$limit, $country_id);

    public function getSpecificCategory($id);

    public function createCategory(array $attributes);

    public function updateCategory($id, array $attributes);

    public function deleteCategory($id);

    public  function getCategoryForDatatable($countryId);

    public function getCategoriesIdRelatedToResBranch($id);

    public function activeCategoriesForAdmin($countryId);

}