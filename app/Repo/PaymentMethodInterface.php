<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/27/2017
 * Time: 11:45 AM
 */

namespace App\Repo;


interface PaymentMethodInterface
{
    public function createPaymentMethod(array $request);

    public function updatePaymentMethod();

    public function deletePaymentMethodByrestaurantBranchId($branchId);

}