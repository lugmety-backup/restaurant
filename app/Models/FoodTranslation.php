<?php
/**
 * Created by PhpStorm.
 * User: bijayluitel
 * Date: 4/20/17
 * Time: 9:11 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FoodTranslation extends Model
{
    protected $table = 'food_translation';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = [
        'food_id',
        'lang',
        'name',
        'description',
        'ingredients'
    ];

    public function food(){
        return $this->belongsTo('App\Models\Food','food_id');
    }
}