<?php
/**
 * Created by PhpStorm.
 * User: bijayluitel
 * Date: 4/20/17
 * Time: 9:11 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FoodSizeTranslation extends Model
{
    protected $table = 'food_size_translation';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = [
        'food_size_id',
        'lang',
        'title',
    ];

}