<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/7/2017
 * Time: 9:10 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Review extends Model
{
    use SoftDeletes;
    protected $table = "reviews";

    protected $fillable = ["user_id","restaurant_id","order_id","rating","review","review_status",'lugmety_review','driver_review'];

    public function restaurant(){
        return $this->belongsTo('App\RestaurantBranch','restaurant_id');
    }

}