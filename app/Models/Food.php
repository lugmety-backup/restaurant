<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
class Food extends Model
{

    protected $table = 'food';

    protected $hidden = ['pivot','foodTranslation','restaurantBranch'];

    protected $fillable = [
        'restaurant_id',
        'price',
        'veg',
        'featured_image',
        'gallery',
        'status',
        'out_of_stock'
    ];

    public function foodTranslation(){
        return $this->hasMany('App\Models\FoodTranslation');
    }

    public function category(){
        return $this->belongsToMany('App\Models\Category','food_category_pivot','food_id','category_id');
    }

    public function addonCategory(){
        return $this->belongsToMany('App\Models\AddOnCategory','food_addoncategory_pivot','food_id','category_id');
    }

    public function restaurantBranch(){
        return $this->belongsTo('App\RestaurantBranch','restaurant_id');
    }
    public function getFeaturedImageAttribute($value)
    {
        if(empty($value)){
            return $value;
        }
        //todo add setting

        return Config::get("config.image_service_base_url_cdn").$value;
    }

    public function foodSize(){
        return $this->hasMany('App\Models\FoodSize');
    }

    public function favourite(){
        return $this->hasMany('App\Models\UserFavourite');
    }
}