<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 7/27/17
 * Time: 1:48 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class RestaurantCategory extends Model
{
    protected $table = 'restaurant_food_category_pivot';
}