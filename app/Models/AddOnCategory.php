<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/8/17
 * Time: 3:24 PM
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class AddOnCategory extends Model
{
    protected $table = "addoncategory";

    protected $fillable = ['restaurant_id','min_addon','max_addon','status'];

    protected $hidden = ['pivot','categoryTranslation'];

    public function categoryTranslation(){
        return $this->hasMany('App\Models\AddOnCategoryTranslation','category_id');
    }

    public function addon()
    {
        return $this->belongsToMany('App\Models\AddOn','addon_addoncategory_pivot','category_id','addon_id');
    }

    public function food(){
        return $this->belongsToMany('App\Models\Food','food_addoncategory_pivot','category_id','food_id');
    }

}