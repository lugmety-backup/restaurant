<?php
/**
 * Created by PhpStorm.
 * User: RockingCancer
 * Date: 10/11/2018
 * Time: 12:06 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class FeaturedRestaurantSorting extends Model
{
    protected $table = 'featured_restaurant_sorting';

    protected $fillable = [
        'branch_id',
        'country_id',
        'city_id',
        'sort'

    ];
    public function RestaurantBranch(){
        return $this->hasMany('App\RestaurantBranch',"id","branch_id");
    }
}