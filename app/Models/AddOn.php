<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/8/17
 * Time: 3:24 PM
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class AddOn extends Model
{
    protected $table = "addon";

    protected $fillable = ['restaurant_id','price','status'];

    protected $hidden = ['pivot','addonTranslation'];

    public function addonTranslation(){
        return $this->hasMany('App\Models\AddOnTranslation','addon_id');
    }

    public function addonCategory()
    {
        return $this->belongsToMany('App\Models\AddOnCategory','addon_addoncategory_pivot','addon_id','category_id');
    }

}