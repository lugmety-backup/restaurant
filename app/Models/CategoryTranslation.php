<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/3/17
 * Time: 3:56 PM
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;



class CategoryTranslation extends Model
{
    protected $table = 'category_translation';
    protected $hidden = ['category_translation'];
    public $timestamps = false;

    protected $fillable = [
        'category_id',
        'lang',
        'name'

    ];

    public function category(){
        return $this->belongsTo('App\Models\Category','category_id');
    }
}