<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 8/15/17
 * Time: 5:13 PM
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class RestaurantCategoryPivot extends Model
{
    protected $table = "restaurant_food_category_pivot";
}