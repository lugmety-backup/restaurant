<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/13/18
 * Time: 2:55 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class BranchSorting extends Model
{
    protected $table = 'branch_sorting';

    protected $fillable = [ 'branch_id' ,'country_id' ,'city_id' ,'sort'];

    protected $hidden = ['branch'];

    public function branch(){
        return $this->belongsTo('App\RestaurantBranch','branch_id');
    }

}