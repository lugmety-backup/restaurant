<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/8/17
 * Time: 3:24 PM
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class FoodSize extends Model
{
    protected $table = "food_size";

    protected $fillable = ['food_id','price','is_default','size'];

    public function FoodSizeTranslation(){
        return $this->hasMany('App\Models\FoodSizeTranslation','food_size_id');
    }

}