<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/8/17
 * Time: 3:34 PM
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class AddOnCategoryTranslation extends Model
{
    public $timestamps = false;

    protected $table = "addoncategory_translation";

    protected $fillable = ['category_id','lang','name'];

}