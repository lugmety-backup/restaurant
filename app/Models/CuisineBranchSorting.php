<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/7/18
 * Time: 11:49 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CuisineBranchSorting extends Model

{
    protected $table = 'cuisine_branch_sorting';

    protected $fillable = [
        'branch_id',
        'country_id',
        'city_id',
        'sort'

    ];

    public function cuisine(){
        return $this->belongsTo('App\Cuisine');
    }

    public function cuisineBranch(){
        return $this->hasMany('App\RestaurantBranch',"id","branch_id");
    }
}