<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/3/17
 * Time: 1:50 PM
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;


class Category extends Model
{
    protected $table = 'category';

    protected $hidden = ['pivot','categoryTranslation'];

    protected $fillable = [
        'image',
        'sort',
        'status',
        'country_id'

    ];

    public function food(){
        return $this->belongsToMany('App\Models\Food','food_category_pivot');
    }

    public function categoryTranslation()
    {
        return $this->hasMany('App\Models\CategoryTranslation');
    }

    public function restaurantBranch(){
        return $this->belongsToMany('App\RestaurantBranch','restaurant_food_category_pivot','category_id','restaurant_id');
    }

    public function branchCategorySorting()
    {
        return $this->belongsTo('App\Models\BranchCategorySorting');
    }

    public function categoryBranchSort(){
        return $this->belongsToMany('App\Models\RestaurantBranch','branch_category_sorting', 'category_id', 'branch_id');
    }
    public function getImageAttribute($value)
    {
        if(empty($value)){
            return $value;
        }
        return Config::get("config.image_service_base_url_cdn").$value;
    }
}