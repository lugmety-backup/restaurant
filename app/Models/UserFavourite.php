<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 7/20/17
 * Time: 11:49 AM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class UserFavourite extends Model
{
    protected $table = 'user_favourite';

    protected $hidden = ['pivot','food','created_at','updated_at'];

    protected $fillable = [
        'user_id',
        'food_id'
    ];

    public function food(){
        return $this->belongsTo('App\Models\Food');
    }
}