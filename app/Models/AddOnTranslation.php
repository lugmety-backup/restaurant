<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/8/17
 * Time: 3:36 PM
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class AddOnTranslation extends Model
{
    public $timestamps = false;

    protected $table = "addon_translation";

    protected $fillable = ['addon_id','lang','name','description'];


}