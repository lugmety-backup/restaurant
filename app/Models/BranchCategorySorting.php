<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 3/19/2018
 * Time: 12:34 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class BranchCategorySorting extends Model
{
    protected $table = "branch_category_sorting";
    protected $fillable = ["branch_id","category_id","sort"];

    public function restaurantBranch(){
        return $this->belongsTo('App\RestaurantBranch');
    }

    public function restaurantBranchCategory(){
        return $this->hasMany('App\Models\Category',"id","category_id");
    }


}