<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/7/2017
 * Time: 9:10 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerRestaurant extends Model
{
    protected $table = "customer_restaurant";

    public $timestamps = false;

    protected $fillable = ["user_id","restaurant_id"];

    public function restaurant(){
        return $this->belongsTo('App\RestaurantBranch','restaurant_id');
    }

}