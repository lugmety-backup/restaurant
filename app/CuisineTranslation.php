<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 7/7/2017
 * Time: 1:56 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class CuisineTranslation extends Model
{
    protected $table = "cuisine_translation";
    protected $fillable = ["cuisine_id","name","lang_code"];
    public $timestamps = false;

    public function cuisine(){
        return $this->belongsTo('App\Cuisine');
    }
}