<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class FlushRedisListener extends Command
{
    protected $name = 'rabbitmq:flushredislistener';
    public function handle()
    {
        \Amqp::consume('restaurant_flush_redis_queue', function ($message, $resolver) {
            $data=$message->body;
            $data = unserialize($data);
            if($data[0] == "flush_redis_key"){
                Redis::del($data[1]);
            }

            \Log::info("Deleted key : ".$data[1]);
            \Log::info("Redis key : ".Redis::get("apple"));


//             var_dump($data);
            $resolver->acknowledge($message);
        }, [
            'exchange' => 'amq.fanout',
            'exchange_type' => 'fanout',
            'queue_force_declare' => true,
            'queue_exclusive' => false,
            'persistent' => true// required if you want to listen forever
        ]);
    }
}
