<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * Class RestaurantBranch
 * @package App
 */
class RestaurantBranch extends Model
{
    use SoftDeletes;
    protected $fillable=['country_id' , 'city_id' , 'district_id' , 'parent_id' , 'logo' , 'cover_image' ,
        'phone_no' , 'mobile_no' , 'email_address' , 'commission_rate' , 'slug', 'min_purchase_amt' ,
        'max_purchase_amt' ,'no_of_seats' , 'available_seats' , 'machine_ref_key' , 'freelance_drive_supported' ,
        'is_featured', 'pre_order' , 'methods' , 'status' , 'latitude' , 'longitude' , 'timezone','shift',
        "is_approved", 'approximate_delivery_time',"free_delivery","tax_type","tin","cc_commision_rate","additional_contact","logistics_fee","delivery_fee"
    ];
    protected $table='restaurant_branches';
    protected $dates = ['deleted_at'];
    protected $hidden = ['restaurant' , "methods" , "created_at" , "updated_at" , "deleted_at","pivot","logistics_fee","delivery_fee"];
    public $hideTinAndPrefix = true;
    /**
     * @return mixed
     */
    public function restaurant(){
        return $this->belongsTo('App\Restaurant','parent_id')->where("status",1);
    }

    public function anyRestaurant(){
        return $this->belongsTo('App\Restaurant','parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function branchTranslation(){
        return $this->hasMany('App\BranchTranslation','branch_id');
    }

    public function geBranchTranslationAttribute()
    {
        $comments = $this->branchTranslation()->getQuery()->orderBy('name', 'asc');
        return $comments;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function deliveryOption(){
        return $this->belongsToMany('App\DeliveryOption','available_delivery_opt','branch_id', 'delivery_opt_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deliveryZone(){
        return $this->hasMany('App\DeliveryZone','branch_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function restaurantHour(){
        return $this->hasMany('App\RestaurantHour','branch_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function paymentMethod(){
        return $this->hasMany('App\PaymentMethod',"restaurant_branch_id");
    }

    public function shippingMethod(){
        return $this->hasMany('App\ShippingMethod',"restaurant_branch_id");
    }

    public function food(){
        return $this->hasMany('App\Models\Food',"restaurant_id");
    }

    public function category(){
        return $this->belongsToMany('App\Models\Category','restaurant_food_category_pivot','restaurant_id','category_id');
    }

    public function cuisine(){
        return $this->belongsToMany('App\Cuisine',"cuisine_restaurant_pivot","restaurant_id","cuisine_id");
    }

    public function restaurantReservation(){
        return $this->hasMany('App\RestaurantReservation',"restaurant_id");
    }

    public function restaurantReviews(){
        return $this->hasMany('App\Models\Review',"restaurant_id")->where("review_status","approved");
    }

    public function branchCategorySorting(){
        return $this->hasMany('App\Models\BranchCategorySorting',"branch_id")->orderBy("sort","asc");
    }

    public function branchCategory(){
        return $this->belongsToMany('App\Models\Category','branch_category_sorting', 'branch_id', 'category_id');
    }


    public function branchCuisineSorting()
    {
        return $this->belongsTo('App\Models\CuisineBranchSorting');
    }

    public function cuisineBranchSort(){
        return $this->belongsToMany('App\Cuisine','cuisine_branch_sorting', 'branch_id', 'cuisine_id');
    }

    public function branchSorting(){
        return $this->belongsTo('App\Models\CuisineBranchSorting','branch_id');
    }

    public function restaurantBranchSorting(){
        return $this->hasOne('App\Models\BranchSorting');
    }

    public function FeatureRestaurantSorting(){
        return $this->belongsTo('App\Models\FeatureRestaurantSorting');
    }
    public function getLogoAttribute($value)
    {
        if(empty($value)){
            return "https://cdn.lugmety.com/images/default-image-lugmety.png";
        }
        return Config::get("config.image_service_base_url_cdn").$value;
    }


    public function getCoverImageAttribute($value)
    {
        if(empty($value)){
            return "https://cdn.lugmety.com/images/cover_image.jpg";
        }
        return Config::get("config.image_service_base_url_cdn").$value;
    }

//    public function setMachineRefKeyAttribute($value){
//        $this->attributes['machine_ref_key'] = Hash::make($value);
//    }


    /**
     * if there is no restaurant hour found for 4 days from now time then pre order is set to 0
     */

    public function getPreOrderAttribute($value){
        $date = Carbon::now();
        $day[0] = $date->format('l');
        $today_time = $date->format("H:i:s");
        $date = $date->addDay();
        $day[1] = $date->format('l');
        $date = $date->addDay();
        $day[2] = $date->format('l');
        $date = $date->addDay();

        $day[3] = $date->format('l');
        $restaurantBranch = $this->attributes;

        if($value == 1){
            $flag = true;
            $restaurantHours = RestaurantHour::where([['branch_id',$restaurantBranch['id']],['closed',0]])
                ->where(function ($query) use ($restaurantBranch, $day){
                    $query->whereIn('day',$day);
                })->get();
            if(count($restaurantHours) == 0){
                $flag = false;
            }

            $restaurantHours = RestaurantHour::where([['branch_id',$restaurantBranch['id']],['closed',0],['day',$day[0]],['closing_time','>',$today_time]])
                ->get();
            if(count($restaurantHours) > 0){
                $flag = true;
            }
            if($flag){
                return 1;
            }else{
                return 0;
            }

        }else{
            return $value;
        }
    }

    public function getTinAttribute($value){
        if($this->hideTinAndPrefix){
            $this->addHidden("tin");
        }

        return $value;

    }

    public function getAdditionalContactAttribute($value){
        return unserialize($value) == false? [] : unserialize($value);
    }

    public function setAdditionalContactAttribute($value){
        $this->attributes['additional_contact'] = serialize($value);
    }
    

}
