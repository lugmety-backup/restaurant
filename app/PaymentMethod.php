<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/27/2017
 * Time: 11:39 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    protected $table = "payment_method";
    protected $fillable = ["restaurant_branch_id", "slug"];

    public function restaurantBranch(){
        return $this->belongsTo('App\RestaurantBranch', "restaurant_branch_id");
    }
}