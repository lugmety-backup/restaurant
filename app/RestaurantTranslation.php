<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 5/2/2017
 * Time: 11:33 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

/**
 * Class RestaurantTranslation
 * @package App
 */
class RestaurantTranslation extends Model
{
    protected $table="restaurant_translation";
    protected $fillable=["restaurant_id","lang_code","name","description"];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurant(){
        return $this->belongsTo("App/Restaurant","restaurant_id");
    }


}