<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * Class DeliveryZone
 * @package App
 */
class DeliveryZone extends Model
{
    protected $guarded=['id'];
    protected $table='delivery_zones';
    protected $hidden = ["created_at", "updated_at"];

    public $timestamps = false;
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantBranch(){
        return $this->belongsTo('App\RestaurantBranch','branch_id');
    }
}
