<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * Class Restaurant
 * @package App
 */
class Restaurant extends Model
{
    use SoftDeletes;
    protected $fillable = ['address', 'phone_no',
        'mobile_no', 'country_id', 'email_address', 'website', 'contact_person', 'commission_rate', 'status'];
    protected $table = 'restaurants';
    protected $dates = ['deleted_at'];
    protected $hidden = ["created_at", "updated_at", "deleted_at"];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function restaurantTranslation()
    {
        return $this->hasMany('App\RestaurantTranslation', 'restaurant_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function restaurantBranch()
    {
        return $this->hasMany('App\RestaurantBranch', 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function restaurantUsers(){
        return $this->hasManyThrough(RestaurantUser::class,RestaurantBranch::class,"parent_id","branch_id");
    }

    /**
     * @return bool|null
     */
    public function deleteALl()
    {
        $this->restaurantBranch()->delete();
        return parent::delete();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function deliveryZone(){
        return $this->hasManyThrough('App\DeliveryZone','App\RestaurantBranch','parent_id',"branch_id");
    }
}
