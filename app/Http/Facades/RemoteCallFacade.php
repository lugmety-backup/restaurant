<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/25/2017
 * Time: 10:50 PM
 */

namespace App\Http\Facades;


use Illuminate\Support\Facades\Facade;

/**
 * Class RemoteCallFacade
 * @package App\Http\Facades
 */
class RemoteCallFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
{
  return "remoteCall";
}
}