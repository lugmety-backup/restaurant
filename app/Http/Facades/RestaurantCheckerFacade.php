<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/17/17
 * Time: 1:15 PM
 */

namespace App\Http\Facades;
use Illuminate\Support\Facades\Facade;


class RestaurantCheckerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return "restaurantChecker";
    }

}