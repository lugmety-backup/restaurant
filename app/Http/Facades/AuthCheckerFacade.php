<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/17/17
 * Time: 1:45 PM
 */

namespace App\Http\Facades;

use Illuminate\Support\Facades\Facade;


class AuthCheckerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return "authChecker";
    }

}