<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 7/7/17
 * Time: 1:02 PM
 */

namespace App\Http\Facades;

use Illuminate\Support\Facades\Facade;
class LocationServiceFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return "LocationService";
    }
}