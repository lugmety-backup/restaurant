<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/17/17
 * Time: 1:46 PM
 */

namespace App\Http;

use RemoteCall;
use RoleChecker;


class AuthCheckerFacade
{

//    public function check($restaurantId=null)
//    {
        /**
         * If token is persent i.e. in case of logged in user, check role. If user has role -admin or super admin, return true.
         *
         */
//        if (RoleChecker::get('roles')) {
//            if (RoleChecker::hasRole('admin') || RoleChecker::hasRole('super-admin')) {
//                return true;
//            }
//
//            $user_id = RoleChecker::getUser();
//            $user = RemoteCall::getSpecific("http://localhost:8001/restaurant/branch/" . $restaurantId . "/checkAuthorityForFood/" . $user_id);
//            if ($user["status"] == "200" && $user["message"]->message == "true") {
//                return true;
//            }
//
//            return false;
//        }
//
//
//
//
//
//    }

    /**
     *If token is present i.e. in case of logged in user, check role. If user has role admin or super admin, return true.
     * If user has role 'restaurant admin' or parentRole 'restaurant_admin'
     * If user has role or parentRole 'branch-admin',check if branch_id from meta and given $restaurantId is same.
     * If same, return true else return false.
     *
     *all null if not restaurant admin or branch admin
     * @param null $restaurantId
     * @return bool
     *
     *
     */


    public function check($restaurantId = null)
    {
        $restaurantInfo = RoleChecker::getRestaurantInfo();
            if (RoleChecker::hasRole('admin') || RoleChecker::hasRole('super-admin')) {
                return true;
            }elseif (RoleChecker::hasRole('restaurant-admin') || (RoleChecker::get('parentRole') == 'restaurant-admin')){
                if($restaurantInfo['is_restaurant_admin'] =="1"){
                    if (in_array($restaurantId, $restaurantInfo["branch_id"])) {
                        return true;
                    }
                }
            }elseif (RoleChecker::hasRole('branch-admin') || (RoleChecker::get('parentRole') == 'branch-admin')){
                if ($restaurantInfo["branch_id"] == $restaurantId) {
                    return true;
                }
            }


        return false;
    }



}