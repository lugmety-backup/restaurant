<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/7/18
 * Time: 11:35 AM
 */

namespace App\Http\Controllers;


use App\Repo\CuisineInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CuisineBranchSortingController extends Controller
{

    protected $cuisine;
    protected $log;
    public function __construct(CuisineInterface $cuisine, \LogStoreHelper $log)
    {
        $this->cuisine = $cuisine;
        $this->log = $log;
    }

    /**
     * create new featured restaurant to cuisine based on the sort value
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addNewFeaturedRestaurant(Request $request){
        DB::beginTransaction();
        try{
            $this->validate($request,[
                "cuisine_id" => "required|integer|min:1",
                "restaurant_id" => "required|integer|min:1|exists:restaurant_branches,id",
                'sort' => 'required|integer|min:1'
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        try{
            /**
             * fetch cusisine detail
             */
            $fetchCuisine = $this->cuisine->getSpecificCuisineForAdmin($request->cuisine_id);
            /**
             * check if the restaurant is assigned to cuisine
             */
            $restaurant_cuisine = $fetchCuisine->branches()->where('restaurant_id',$request->restaurant_id)->first();
            if(!$restaurant_cuisine){
                return response()->json([
                    'status' => '404',
                    'message' => 'Restaurant does not belongs to provided cuisine.'
                ],404);
            }
            /**
             * detach restaurant id if exist to prevent duplication
             */
            $fetchCuisine->cuisineBranch()->detach([$request->restaurant_id]);
            /**
             * fetch cuisine branch order by ascending order
             */
            $getBranchBySorting = $fetchCuisine->cuisineBranchSorting()->get()->toArray();
            /**
             * slice the array take offset 0 to the requested sort index -1
             */
            $slicedArray = array_slice($getBranchBySorting,0,$request->sort -1);
            /**
             * slice the array take offset  requested sort index -1
             */
            $slicedArray1 = array_slice($getBranchBySorting,$request->sort -1);

            /**
             * add the requested restaurant to the first slice to index equals to requested sort
             */
            $slicedArray[$request->sort] = [
                "branch_id" => $request->restaurant_id
            ];

            /**
             * merge the both sliced array
             * @param $slicedArray
             * @param $slicedArray1
             */
            $restaurant_info = array_merge($slicedArray,$slicedArray1);
            /**
             * loop through each array
             */
            foreach ($restaurant_info as $key => $restaurant){
                /**
                 * attach to cuisine branch sorting whose sort value is based on key
                 */
                $attachBranchToCusine = $fetchCuisine->cuisineBranch()->syncWithoutDetaching([$restaurant["branch_id"] =>["sort" => $key+1,"updated_at" =>Carbon::now("utc")]]);
            }

            /**
             * fetch all the restaurants from cuisine restaurant sort to store key in redis
             */
            $getBranchBySortingBranchId = collect($getBranchBySorting)->sortBy('sort')->pluck('branch_id')->values()->all();
            if(\Redis::exists('cuisine-branch-sorting-'.$request->cuisine_id)){
                \Redis::del('cuisine-branch-sorting-'.$request->cuisine_id);
                \Redis::set('cuisine-branch-sorting-'.$request->cuisine_id,serialize($getBranchBySortingBranchId));

            }
            else{
                \Redis::set('cuisine-branch-sorting-'.$request->cuisine_id,serialize($getBranchBySortingBranchId));
            }
//            return $redis_branch_sorting = unserialize(\Redis::get('cuisine-branch-sorting-'.$request->cuisine_id));
            $this->log->storeLogInfo(["creating branch sorting",[
                "data" => $request->all()
            ]]);
            DB::commit();


            return response()->json([
                "status" => "200",
                "message" => "Branch sorting created successfully."
            ]);
        }
        catch (ModelNotFoundException $ex){
            $this->log->storeLogError(["Cuisine could not be found",[
                "data" => $request->all(),
                "message" => $ex->getMessage()
            ]]);
            return response()->json([
                "status" => "404",
                "message" => "Could not found requested cuisine."
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError(["creating branch sorting error",[
                "data" => $request->all(),
                "message" => $ex->getMessage()
            ]]);
            return response()->json([
                "status" => "500",
                "message" => "Error attaching branches sorting."
            ],500);
        }
    }

    /**
     * create restaurant branch sorting .This will be helpful for displaying the restaurant branches in top or button based on the sort value
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createBranchSorting(Request $request){
        DB::beginTransaction();
        try{
            $this->validate($request,[
                "cuisine_id" => "required|integer|min:1",
                "restaurant_info" => "required|array",
                "restaurant_info.*.id" => "required|integer|min:1",
                "restaurant_info.*.branch_id" => "required|integer|min:1",
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        try{
            $fetchCuisine = $this->cuisine->getSpecificCuisineForAdmin($request->cuisine_id);

            foreach ($request->restaurant_info as $key => $restaurant){
                $attachBranchToCusine = $fetchCuisine->cuisineBranch()->syncWithoutDetaching([$restaurant["branch_id"] =>["sort" => $key+1,"updated_at" =>Carbon::now("utc")]]);
            }

            $getBranchBySorting = $fetchCuisine->cuisineBranchSorting()->get();
            $getBranchBySortingBranchId = collect($getBranchBySorting)->sortBy('sort')->pluck('branch_id')->values()->all();
            if(\Redis::exists('cuisine-branch-sorting-'.$request->cuisine_id)){
                \Redis::del('cuisine-branch-sorting-'.$request->cuisine_id);
                \Redis::set('cuisine-branch-sorting-'.$request->cuisine_id,serialize($getBranchBySortingBranchId));

            }
            else{
                \Redis::set('cuisine-branch-sorting-'.$request->cuisine_id,serialize($getBranchBySortingBranchId));
            }
//            return $redis_branch_sorting = unserialize(\Redis::get('cuisine-branch-sorting-'.$request->cuisine_id));
            $this->log->storeLogInfo(["creating branch sorting",[
                "data" => $request->all()
            ]]);
            DB::commit();


            return response()->json([
                "status" => "200",
                "message" => "Branch sorting altered successfully."
            ]);
        }
        catch (ModelNotFoundException $ex){
            $this->log->storeLogError(["Cuisine could not be found",[
                "data" => $request->all(),
                "message" => $ex->getMessage()
            ]]);
            return response()->json([
                "status" => "404",
                "message" => "Could not found requested cuisine."
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError(["creating branch sorting error",[
                "data" => $request->all(),
                "message" => $ex->getMessage()
            ]]);
            return response()->json([
                "status" => "500",
                "message" => "Error attaching branches sorting."
            ],500);
        }
    }

    /**
     * listing of specific restaurants by allowed user only
     * @param $branchId
     * @return \Illuminate\Http\JsonResponse
     */
    public function viewBranchSorting($cuisine_id){
        DB::beginTransaction();
        try{
            /**
             * fetch cuisine details
             */
            $getCuisine = $this->cuisine->getSpecificCuisineForAdmin($cuisine_id);


            $getBranchBySorting = $getCuisine->cuisineBranchSorting()->get();

            $getBranchBySortingBranchId = collect($getBranchBySorting)->sortBy('sort')->pluck('branch_id')->values()->all();
            if(\Redis::exists('cuisine-branch-sorting-'.$cuisine_id)){
                \Redis::del('cuisine-branch-sorting-'.$cuisine_id);
                \Redis::set('cuisine-branch-sorting-'.$cuisine_id,serialize($getBranchBySortingBranchId));

            }
            else{
                \Redis::set('cuisine-branch-sorting-'.$cuisine_id,serialize($getBranchBySortingBranchId));
            }

//            return $redis_branch_sorting = unserialize(\Redis::get('cuisine-branch-sorting-'.$cuisine_id));
            /**
             * sort order by updated first and then by sort value
             */
            $getBranchBySorting = collect($getBranchBySorting)->sortByDesc("updated_at")->sortBy("sort")->values()->all();
            foreach ($getBranchBySorting as $branch){
                /**
                 * fetch branches
                 */
                $branchList = $branch->cuisineBranch()->first();
                /**
                 * fecth branch name in english form branch translation
                 */
                $branchTranslation = $branchList->branchTranslation()->where("lang_code","en")->first();
                if($branchTranslation){
                    $branch["name"] = $branchTranslation->name;
                }
                else{
                    $branch["name"] = "";
                }
            }
            DB::commit();
            return response()->json([
                "status" => "200",
                "data" => $getBranchBySorting
            ]);
        }
        catch (ModelNotFoundException $ex){
            $this->log->storeLogError(["Cuisine could not be found",[
                "data" => ["cuisine_id" =>$cuisine_id ],
                "message" => $ex->getMessage()
            ]]);
            return response()->json([
                "status" => "404",
                "message" => "Could not found requested cuisine."
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError(["creating branch sorting error",[
                "data" => ["cuisine_id" => $cuisine_id ],
                "message" => $ex->getMessage()
            ]]);
            return response()->json([
                "status" => "500",
                "message" => "Error viewing branches sorting."
            ],500);
        }
    }

    /**
     * @param $cuisine_id
     * @param $restaurant_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeFeaturedRestaurant(Request $request,$cuisine_id,$restaurant_id){
        try{
            /**
             * fetch cusisine detail
             */
            $fetchCuisine = $this->cuisine->getSpecificCuisineForAdmin($cuisine_id);
            /**
             * check if the restaurant is assigned to cuisine
             */
            $restaurant_cuisine = $fetchCuisine->branches()->where('restaurant_id',$restaurant_id)->first();
            if(!$restaurant_cuisine){
                return response()->json([
                    'status' => '404',
                    'message' => 'Restaurant does not belongs to provided cuisine.'
                ],404);
            }
            /**
             * detach restaurant id if exist to prevent duplication
             */
            $fetchCuisine->cuisineBranch()->detach([$restaurant_id]);
            /**
             * fetch cuisine branch order by ascending order
             */
            $getBranchBySorting = $fetchCuisine->cuisineBranchSorting()->get()->toArray();
            /**
             * loop through each array
             */
            foreach ($getBranchBySorting as $key => $restaurant){
                /**
                 * attach to cuisine branch sorting whose sort value is based on key
                 */
                $attachBranchToCusine = $fetchCuisine->cuisineBranch()->syncWithoutDetaching([$restaurant["branch_id"] =>["sort" => $key+1,"updated_at" =>Carbon::now("utc")]]);
            }

            /**
             * fetch all the restaurants from cuisine restaurant sort to store key in redis
             */
            $getBranchBySortingBranchId = collect($getBranchBySorting)->sortBy('sort')->pluck('branch_id')->values()->all();
            if(\Redis::exists('cuisine-branch-sorting-'.$cuisine_id)){
                \Redis::del('cuisine-branch-sorting-'.$cuisine_id);
                \Redis::set('cuisine-branch-sorting-'.$cuisine_id,serialize($getBranchBySortingBranchId));

            }
            else{
                \Redis::set('cuisine-branch-sorting-'.$cuisine_id,serialize($getBranchBySortingBranchId));
            }
//            return $redis_branch_sorting = unserialize(\Redis::get('cuisine-branch-sorting-'.$request->cuisine_id));
            $this->log->storeLogInfo(["deleting branch sorting",[
                "data" => $request->all()
            ]]);
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Branch sorting deleted successfully."
            ]);
        }
        catch (ModelNotFoundException $ex){
            $this->log->storeLogError(["Cuisine could not be found",[
                "data" => ["cuisine_id" =>$cuisine_id ],
                "message" => $ex->getMessage()
            ]]);
            return response()->json([
                "status" => "404",
                "message" => "Could not found requested cuisine."
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError(["creating branch sorting error",[
                "data" => ["cuisine_id" => $cuisine_id ],
                "message" => $ex->getMessage()
            ]]);
            return response()->json([
                "status" => "500",
                "message" => "Error deleting branch from sorting."
            ],500);
        }
    }
}