<?php
/**
 * Created by PhpStorm.
 * User: bijayluitel
 * Date: 4/20/17
 * Time: 8:00 PM
 */

namespace App\Http\Controllers;

use App\Models\FoodSize;
use App\Repo\FoodSizeInterface;
use App\Repo\FoodSizeTranslationInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Repo\FoodInterface;
use App\Repo\FoodTranslationInterface;
use LogStoreHelper;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Input;
use Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use RestaurantChecker;
use AuthChecker;
use Datatables;
use Illuminate\Support\Facades\Config;
use ImageUploader;
use App\Repo\RestaurantBranchInterface;
use LocationService;
use App\Repo\FoodCategoryInterface;
use App\Repo\FoodAddonCategoryInterface;
use App\Repo\PivotInterface;
use RoleChecker;
use RemoteCall;

class FoodController extends Controller
{
    /*
     * Food Model
     */
    protected $food;
    protected $foodTranslation;
    protected $checker;
    protected $logStoreHelper;
    protected $restaurantBranch;
    protected $foodCategory;
    protected $foodAddonCategory;
    protected $pivot;
    protected $foodSize;
    protected $foodSizeTranslation;

    /**
     * FoodController constructor.
     * @param FoodInterface $food
     * @param FoodTranslationInterface $foodTranslation
     * @param LogStoreHelper $logStoreHelper
     */
    public function __construct(FoodInterface $food,
                                FoodTranslationInterface $foodTranslation,
                                LogStoreHelper $logStoreHelper, RestaurantBranchInterface $restaurantBranch, FoodCategoryInterface $foodCategory, FoodAddonCategoryInterface $foodAddonCategory, PivotInterface $pivot, FoodSizeInterface $foodSize, FoodSizeTranslationInterface $foodSizeTranslation)
    {
        $this->food = $food;
        $this->foodTranslation = $foodTranslation;
        $this->logStoreHelper = $logStoreHelper;
        $this->restaurantBranch = $restaurantBranch;
        $this->foodCategory = $foodCategory;
        $this->foodAddonCategory = $foodAddonCategory;
        $this->pivot = $pivot;
        $this->foodSize = $foodSize;
        $this->foodSizeTranslation = $foodSizeTranslation;
    }

    /**
     * Display the list of food
     * If 'lang' parameter is not supplied,then default language is set to 'en'.
     * If 'status' parameter is not supplied, Authorized personnel can view all the data i.e. of both status 0 and 1.
     * If 'status' parameter is supplied, Authorized personnel can view data having requested status. Others can view data having status 1 in any case.
     * 'limit' parameter is used to get no. of records, user want to view in per page.
     * If 'limit' parameter is not supplied, only 10 records are shown per page. Else, no. of records is equal to limit supplied.
     * If 'veg' parameter is not suppied, data having both veg value:0(non-veg) and 1(veg).. Else, data is displayed according to value of 'veg'.
     * @param $restaurantId
     * @return \Illuminate\Http\JsonResponse
     */

    public function index($restaurantId)
    {
        try {
            if (!is_null(Input::get('status'))) {
                $request['status'] = Input::get("status");

            }
            $request['lang'] = Input::get("lang", "en");
            $request['limit'] = Input::get("limit", 10);
            if (!is_null(Input::get('veg'))) {
                $request['veg'] = Input::get("veg");
            }

            /*
             * Validate the get request
             * 'status' and 'veg' are validated only when the supplied.
             *
             * Validate the status and limit
             * Status must be 0 or 1.
             * veg must be 0 or 1.
             * limit must be integer.
             *
             *
             * */
            $rules = [
                'status' => 'sometimes|required|integer|min:0|max:1',
                'limit' => 'required|integer',
                'veg' => 'sometimes|required|integer|min:0|max:1'
            ];
            $validator = Validator::make($request, $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status' => '422',
                    "message" => $validator->errors()
                ], 422);
            }


            /*
             *
             * check the authority of the logged in user and set the status according to the output of check function of AuthChecker facade.
             * if output is "true", set status to all ie user can view all food records according to restaurant
             *if output  is "false", set status to 1 i.e. user can only view food records of status 1.
             * */
            if (AuthChecker::check($restaurantId)) {
                /**
                 * If status is not given or empty, then status is set to "all".
                 * It means all the available data is to fetched.
                 * If status is supplied, filter data accordingly
                 */
                if (!isset($request['status']) || empty($request['status'])) {
                    $request['status'] = "all";

                }
                /**
                 * If the user is not a authorized personnel, check the status of restaurant.
                 * If restaurant is active, fetch active food of that restaurant.
                 * If restaurant is inactive, throw exception
                 */

            } else {
                if (RestaurantChecker::check($restaurantId)) {
                    $request['status'] = 1;
                } else {
                    return response()->json([
                        "status" => "404",
                        "message" => "Restaurant you entered is not found"

                    ], 404);
                }

            }

            if (!isset($request['veg']) || empty($request['veg'])) {
                $request['veg'] = "all";
            }
            $data = $this->food->getAllFood($restaurantId, $request['status'], $request['limit'], $request['veg']);

            try {
                if (!$data->first()) {
                    throw  new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Empty Record"
                ], 404);
            }

            foreach ($data as $key => $food) {
                /*
                * Get food Translation based on @param lang
                * if empty record is obtained default en lang is returned
                * If default en lang is not available, throw exception
                * */

                $foodTranslation = $this->foodTranslation->getSpecificFoodTransalationByLang($food->id, $request['lang']);
                try {
                    if ($foodTranslation->count() == 0) {
                        if ($request['lang'] == 'en') {
                            throw new \Exception();
                        } else {
                            $foodTranslation = $this->foodTranslation->getSpecificFoodTransalationByLang($food->id, 'en');
                            if ($foodTranslation->count() == 0) {
                                throw new \Exception();
                            }
                        }

                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Default language english not found in database"
                    ], 404);
                }

                /*
                 * attach name and lang_code in $food variable
                 * */
                $food['lang'] = $foodTranslation[0]['lang'];
                $food['name'] = $foodTranslation[0]['name'];
                $food['description'] = $foodTranslation[0]['description'];
                $food['ingredients'] = $foodTranslation[0]['ingredients'];
                if (!empty($food->gallery)) {
                    unset($foos, $foodNormal);
                    $food["gallery"] = unserialize($food->gallery);
                    foreach ($food["gallery"] as $key => $foo) {
                        /**
                         * get position of last occurance of / in fetaured_image and added the postion by 1
                         */
                        $getpositionOfImageInUrl = strrpos($foo, "/") + 1;
                        //todo add from setting
                        $normalImage = Config::get("config.image_service_base_url_cdn") . substr_replace($foo, "normal-", $getpositionOfImageInUrl, 0);
                        $foos[] = Config::get("config.image_service_base_url_cdn") . $foo;
                        $foodNormal[] = $normalImage;
                    }
                    $food["gallery"] = $foos;
                    $food["gallery_resized"] = $foodNormal;
                } else {
                    $food["gallery"] = [];
                    $food["gallery_resized"] = [];
                }
                //todo add from setting
                if (!empty($food->featured_image)) {
                    /**
                     * get position of last occurance of / in fetaured_image and added the postion by 1
                     */
                    $getpositionOfImageInUrl = strrpos($food->featured_image, "/") + 1;
                    /**
                     * add normal- prefix in $getpositionOfImageInUrl postition
                     */
                    $food["featured_image_resized"] = substr_replace($food->featured_image, "normal-", $getpositionOfImageInUrl, 0);
                } else {
                    $food["featured_image_resized"] = [];
                }
            }

            return response()->json([
                'status' => "200",
                "data" => $data
            ], 200);

        } catch (\Exception $ex) {

            return response()->json([
                "status" => "404",
                "message" => $ex->getMessage()
            ], 404);
        }

    }

    /**
     * Store food of specific restaurant
     * @param Request $request
     * @param $restaurantId
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $restaurantId)
    {
        try {
            $dataValidation = $this->food->getDataForFoodValidation($restaurantId);


            /**
             * Check if logged in user has authority to insert data.
             * If 'true', continue. Else abort with exception message.
             *
             * Validate the request
             * price field is required and must be numeric and between 0 to 9999.99.
             * veg field is required and must be boolean.
             * status field is required and must be integer. Only 0 or 1 is acceptable.
             * translation field is required and must be an array.
             * gallery field is an optional field and should be an array if present.
             * If validation fails throw error message and abort.
             */
            if (AuthChecker::check($restaurantId)) {
                $request['out_of_stock'] = (string)$request['out_of_stock'];
                $rule = [//validating the request
                    "price" => "required|numeric|between:0,9999.99",
                    "veg" => "required|boolean",
                    "status" => "required|integer|min:0|max:1",
                    "translation" => "required|array",
                    "gallery" => "sometimes|array",
                    "food_categories" => "required|array|in:" . implode($dataValidation['categories'], ","),
                    "addon_categories" => "sometimes|array|in:" . implode($dataValidation['addonCategories'], ","),
                    "food_size" => 'sometimes|required|check_default:is_default',
                    "food_size.*.size" => "required_with:food_size|distinct",
                    "food_size.*.price" => "required_with:food_size",
                    "food_size.*.size_translation" => "required_with:food_size",
                    "out_of_stock" => "required|integer|min:0|max:1"
                ];
                $message = [

                    "food_size.check_default" => "Select one size as default size",
                    "food_size.*.size.distinct" => "Sizes must be distinct"
                ];

                $validator = Validator::make($request->toArray(), $rule, $message);
                if ($validator->fails()) {
                    $error = $validator->errors();
                    $this->logStoreHelper->storeLogError([
                        'Validation Error',
                        [
                            'status' => '422',
                            "message" => $error
                        ]]);
                    return response()->json([
                        'status' => '422',
                        "message" => $error
                    ], 422);
                }

                try {

                    /**
                     * Use remote location service to verify the translation request.
                     * If the translation lang codes are not same as country's lang codes, error message is returned.
                     */
                    $countryValidate = LocationService::checkLanguageCode($dataValidation['restaurant']['country_id'], $request['translation']);

                    if ($countryValidate['status'] != 200) {
                        return response()->json([
                            "status" => $countryValidate["status"],
                            "message" => $countryValidate["message"]
                        ], $countryValidate["status"]);

                    }

                    /**
                     * Start DB transaction to ensure the operation is reversed in case not successfully committed.
                     *
                     */
                    DB::beginTransaction();

                    /**
                     * check if featured_image is present. if present, get location of image using saveImage() function
                     *
                     * */


                    if ($request->has('featured_image')) {
                        $imageUpload = $this->saveImage($request['featured_image'], "featured_image");
                        if ($imageUpload["status"] != 200) {
                            return response()->json(
                                $imageUpload["message"]
                                , $imageUpload["status"]);
                        }
                        $request['featured_image'] = $imageUpload["message"]["data"];
                    }
                    /**
                     * check if gallery is present. if present, get location of image using saveImage() function and save as an array
                     */

                    if ($request->has('gallery')) {

                        foreach ($request['gallery'] as $key => $gallery) {
                            $imageUpload = $this->saveImage($gallery, "gallery");
                            if ($imageUpload["status"] != 200) {
                                return response()->json(
                                    $imageUpload["message"]
                                    , $imageUpload["status"]);
                            }
                            $photo[] = $imageUpload["message"]["data"];

                        }
                        $request['gallery'] = serialize($photo);


                    }

                    $request['restaurant_id'] = $restaurantId;
                    /**
                     * Insert new food record in food table.
                     */

                    $food = $this->food->createFood($request->only('restaurant_id', 'price', 'veg', 'featured_image', 'gallery', 'status', 'out_of_stock'));

                    foreach ($request['translation'] as $key => $translation) {

                        /**
                         * Validate lang, name, description and ingredients fields.
                         * lang field is required and must be alphabets.
                         * name field is required.
                         * description field is required.
                         * ingredients field is required.
                         * If validation fails, abort with validation error message.
                         * Else insert data in addon_translation table.
                         */

                        $translation['food_id'] = $food->id;
                        $translation['lang'] = $key;
                        $rules = [
                            "lang" => 'required|alpha',
                            "name" => "required",
                            "description" => 'sometimes',
                            "ingredients" => 'sometimes'
                        ];
                        $translation['lang'] = str_slug($translation['lang']);

                        $validator = Validator::make($translation, $rules);
                        if ($validator->fails()) {
                            $error = $validator->errors();
                            $this->logStoreHelper->storeLogError([
                                'Validation Error',
                                [
                                    'status' => '422',
                                    "message" => [$key => $error]
                                ]]);
                            return response()->json([
                                'status' => '422',
                                "message" => [$key => $error]
                            ], 422);
                        }
                        /**
                         * Insert translation record of food in food_translation table
                         */

                        $createTranslation = $this->foodTranslation->createFoodTranslation($translation);


                    }

                    if ($request->has('food_size')) {

                        foreach ($request['food_size'] as $key => $size) {
                            //dd($key);
                            $countryValidate1 = LocationService::checkLanguageCode($dataValidation['restaurant']['country_id'], $size['size_translation']);

                            if ($countryValidate1['status'] != 200) {
                                return response()->json([
                                    "status" => $countryValidate1["status"],
                                    "message" => $countryValidate1["message"]
                                ], $countryValidate1["status"]);

                            }

                            /**
                             * Validate lang, name, description and ingredients fields.
                             * lang field is required and must be alphabets.
                             * name field is required.
                             * If validation fails, abort with validation error message.
                             */

                            //return $size;
                            $size['food_id'] = $food->id;
                            $size['size'] = str_slug($size['size']);

                            /**
                             * Insert translation record of food in food_translation table
                             */

                            $updateSize = $this->foodSize->createFoodSize($size);
                            //dd($updateSize);

                            foreach ($size['size_translation'] as $key => $size_translation) {

                                /**
                                 * Validate lang, name, description and ingredients fields.
                                 * lang field is required and must be alphabets.
                                 * name field is required.
                                 * If validation fails, abort with validation error message.
                                 */


                                $size_translation['lang'] = $key;
                                $size_translation['food_size_id'] = $updateSize->id;

                                $rules = [

                                    "title" => 'required'
                                ];
                                $size_translation['lang'] = str_slug($size_translation['lang']);

                                $validator = Validator::make($size_translation, $rules);
                                if ($validator->fails()) {
                                    $error = $validator->errors();
                                    $this->logStoreHelper->storeLogError([
                                        'Validation Error',
                                        [
                                            'status' => '422',
                                            "message" => [$key => $error]
                                        ]]);
                                    return response()->json([
                                        'status' => '422',
                                        "message" => ['size_translation' => [$key => $error]]
                                    ], 422);
                                }

                                $createSizeTranslation = $this->foodSizeTranslation->createFoodSizeTranslation($size_translation);

                            }
                        }
                    }

                    $foodCategory = $this->foodCategory->attachCategoryToFood($food->id, $request['food_categories']);
                    $foodAddonCategory = $this->foodAddonCategory->attachCategoryToFood($food->id, $request['addon_categories']);

                    $this->logStoreHelper->storeLogInfo([
                        'Food',
                        [
                            "status" => "200",
                            "message" => "Food Created Successfully"
                        ]
                    ]);

                    /**
                     * If operation is successfully executed, commit it, else revert whole operation.
                     */

                    DB::commit();
                    return response()->json([
                        'status' => "200",
                        "message" => "Food Created Successfully"
                    ], 200);
                } catch (\Exception $e) {
                    return response()->json([
                        "status" => "422",
                        "message" => $e->getMessage()
                    ], 422);
                }
            } else {
                return response()->json([
                    'status' => "401",
                    "message" => "Only Authorized Personnel can create Food "
                ], 401);
            }
        } catch (ModelNotFoundException $notFoundException) {
            return response()->json([
                'status' => '404',
                'message' => "Requested Restaurant not found"
            ], 404);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => '500',
                'message' => "Something went wrong"
            ], 500);
        }


    }


    public function checkFoodStatus(Request $request){
        try{
            $this->validate($request,[
                "ids" => "required|array",
                "ids.*" => "required|integer|min:1"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => '422',
                'message' => $ex->response->original
            ],422);
        }
        try{
            $lang = $request->get('lang','en');
            $requestFoodCount = count($request["ids"]);
            $getFoodCount = $this->food->getActiveAndNonOutOfStockFoods($request["ids"]);
            if($requestFoodCount !== $getFoodCount){
                return \Settings::getErrorMessageOrDefaultMessage('restaurant-foods-are-unavailable',
                    "Error! Some of the foods are out of stock. Please clear your cart and try again.", 403, $lang);
            }
            return response()->json([
                "status" => '200',
                'message' => "Foods are present for ordering."
            ],200);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => '500',
                'message' => 'Error getting foods records'
            ]);
        }
    }

    /**
     * Upload the image to folder of the given path and returns the path of the saved image
     * @param $image
     * @param $type
     * @return string
     */
    protected function saveImage($image, $type)
    {

        $imaged['avatar'] = $image;
        $cdnUrl = Config::get("config.image_service_base_url_cdn");
        if ((strpos($imaged['avatar'], "data:image/") === false)) {
//            dd(["status" =>strpos( $imaged['avatar'], "data:image/") ]);
            $url = str_replace($cdnUrl, "", $imaged['avatar']);
            $data = ["message" => ["data" => $url], "status" => "200"];
            return $data;
        } else {
            $imaged["type"] = $type;
            $imaged["service"] = "food";
            $imageUpload = ImageUploader::upload($imaged);
            return $imageUpload;
        }

    }


    public function destroy($restaurantId, $id)
    {
        /**
         * Check if logged in user has authority to delete data.
         * If 'true', continue. Else abort with exception message.
         */

        if (AuthChecker::check($restaurantId)) {

            try {

                /*
                 * delete food along with its child rows
                 * */

                $food = $this->food->getSpecificFoodByRestaurant($id, $restaurantId);

                $this->food->deleteFood($id);

                $food->favourite()->delete();
//                //check if featured image is present and delete featured image from folder if present
//                if (!empty($food->featured_image)) {
//                    $this->deleteFile($food->featured_image);
//                }

                //check if gallery field is present and delete images of gallery from folder if present
//                if (!empty($food->gallery)) {
//                    foreach (unserialize($food->gallery) as $image) {
//                        $this->deleteFile($image);
//                    }
//                }

                return response()->json([
                    "status" => "200",
                    "message" => "Requested Food Deleted Successfully"
                ], 200);


            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => $ex->getMessage()
                ], 404);

            }
        } else {
            return response()->json([
                'status' => "401",
                "message" => "Only Authorized personnel can delete Food"
            ], 401);
        }

    }

    /**
     *
     * Display food of given id.
     * If status of required data is 0, then only authorized personnel can view the record. Unauthorized can only view data having status 1.
     * @param $restaurantId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function show($restaurantId, $id)
    {
        $request['lang'] = Input::get("lang", "en");

        /*
         *
         * food of requested id is displayed
        */
        if (!AuthChecker::check($restaurantId)) {
            if (!RestaurantChecker::check($restaurantId)) {
                return response()->json([

                    "status" => "404",
                    "message" => "Restaurant you entered is not found"

                ], 404);
            }
        }

        try {

            $food = $this->food->getSpecificFoodByRestaurant($id, $restaurantId);

            if ($food['status'] == 0) {
                if (!AuthChecker::check($restaurantId)) {
                    throw new \Exception();
                }
            }


        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Empty Record for requested food"
            ], 404);
        }
        /*
         * Get category Translation based on @param lang
         * if empty record is obtained default en lang is returned
         * */
        $foodTranslation = $this->foodTranslation->getSpecificFoodTransalationByLang($food->id, $request['lang']);
        try {
            if ($foodTranslation->count() == 0) {
                if ($request['lang'] == 'en') {
                    throw new \Exception();
                } else {
                    $foodTranslation = $this->foodTranslation->getSpecificFoodTransalationByLang($food->id, 'en');
                    if ($foodTranslation->count() == 0) {
                        throw new \Exception();
                    }
                }

            }
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Default language english not found in database"
            ], 404);
        }


        /*
         * attach name and lang in $restaurant variable
         * */
        $food['lang'] = $foodTranslation[0]['lang'];
        $food['name'] = $foodTranslation[0]['name'];
        $food['description'] = $foodTranslation[0]['description'];
        $food['ingredients'] = $foodTranslation[0]['ingredients'];
        if (!empty($food->gallery)) {
            unset($foos, $foodNormal);
            $food["gallery"] = unserialize($food->gallery);
            foreach ($food["gallery"] as $key => $foo) {
                /**
                 * get position of last occurance of / in fetaured_image and added the postion by 1
                 */
                $getpositionOfImageInUrl = strrpos($foo, "/") + 1;
                //todo add from setting
                $normalImage = Config::get("config.image_service_base_url_cdn") . substr_replace($foo, "normal-", $getpositionOfImageInUrl, 0);
                $foos[] = Config::get("config.image_service_base_url_cdn") . $foo;
                $foodNormal[] = $normalImage;
            }
            $food["gallery"] = $foos;
            $food["gallery_resized"] = $foodNormal;
        } else {
            $food["gallery"] = [];
            $food["gallery_resized"] = [];
        }
        //todo add from setting
        if (!empty($food->featured_image)) {
            /**
             * get position of last occurance of / in fetaured_image and added the postion by 1
             */
            $getpositionOfImageInUrl = strrpos($food->featured_image, "/") + 1;
            /**
             * add normal- prefix in $getpositionOfImageInUrl postition
             */
            $food["featured_image_resized"] = substr_replace($food->featured_image, "normal-", $getpositionOfImageInUrl, 0);
        } else {
            $food["featured_image_resized"] = [];
        }
        return response()->json([
            'status' => "200",
            "data" => $food
        ], 200);


    }

    /**
     * Update category of given id. Only admin or super admin can update category
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function update($restaurantId, Request $request, $id)
    {

        try {

            /**
             * Check if logged in user has authority to update data.
             * If 'true', continue. Else abort with exception message.
             *
             * Validate the request
             * price field is required and must be numeric and between 0 to 9999.99.
             * veg field is required and must be boolean.
             * status field is required and must be integer. Only 0 or 1 is acceptable.
             * translation field is required and must be an array.
             * gallery field is an optional field and should be an array if present.
             * If validation fails throw error message and abort.
             */
            $dataValidation = $this->food->getDataForFoodValidation($restaurantId);


            if (AuthChecker::check($restaurantId)) {
                $request['out_of_stock'] = (string)$request['out_of_stock'];
                $rule = [//validating the request
                    "price" => "required|numeric|between:0,9999.99",
                    "veg" => "required|boolean",
                    "status" => "required|integer|min:0|max:1",
                    "translation" => "required|array",
                    "gallery" => "sometimes|array",
                    "food_categories" => "required|array|in:" . implode($dataValidation['categories'], ","),
                    "addon_categories" => "sometimes|array|in:" . implode($dataValidation['addonCategories'], ","),
                    "food_size" => 'sometimes|required|check_default:is_default',
                    "food_size.*.size" => "required_with:food_size|distinct",
                    "food_size.*.price" => "required_with:food_size",
                    "food_size.*.size_translation" => "required_with:food_size",
                    "out_of_stock" => "required|integer|min:0|max:1",
                ];
                $message = [

                    "food_size.check_default" => "Select one size as default size",
                    "food_size.*.size.distinct" => "Sizes must be distinct"
                ];

                $validator = Validator::make($request->toArray(), $rule, $message);
                if ($validator->fails()) {
                    $error = $validator->errors();
                    $this->logStoreHelper->storeLogError([
                        'Validation Error',
                        [
                            'status' => '422',
                            "message" => $error
                        ]]);
                    return response()->json([
                        'status' => '422',
                        "message" => $error
                    ], 422);
                }


                /*
               * Deleting related translations of given food using relationships and re-enter data
               * */


                /**
                 * Use remote location service to verify the translation request.
                 * If the translation lang codes are not same as country's lang codes, error message is returned.
                 */
                $countryValidate = LocationService::checkLanguageCode($dataValidation['restaurant']['country_id'], $request['translation']);

                if ($countryValidate['status'] != 200) {
                    return response()->json([
                        "status" => $countryValidate["status"],
                        "message" => $countryValidate["message"]
                    ], $countryValidate["status"]);

                }
                try {

                    DB::beginTransaction();
                    /**
                     * Start DB transaction to ensure the operation is reversed in case not successfully committed.
                     *
                     */


                    $food = $this->food->getSpecificFoodByRestaurant($id, $restaurantId);

                    /*
                   * Deleting related translations of given food using relationships and re-enter data
                   * */

                    $food->foodTranslation()->delete();
                    /*
                    *
                    * translation field validation
                    *
                    * */

                    foreach ($request['translation'] as $key => $translation) {
                        $translation['food_id'] = $food->id;
                        $translation['lang'] = $key;
                        $rules = [
                            "lang" => 'required|alpha',
                            "name" => "required",
                            "description" => 'sometimes',
                            "ingredients" => 'sometimes'
                        ];
                        $translation['lang'] = str_slug($translation['lang']);

                        $validator = Validator::make($translation, $rules);
                        if ($validator->fails()) {
                            $error = $validator->errors();
                            $this->logStoreHelper->storeLogError([
                                "Validation Error",
                                [
                                    'status' => '422',
                                    "message" => [$key => $error]
                                ]

                            ]);
                            return response()->json([
                                'status' => '422',
                                "message" => [$key => $error]
                            ]);
                        }

                        $createTranslation = $this->foodTranslation->createFoodTranslation($translation);
                    }


                    $food->foodSize()->delete();

                    if ($request->has('food_size')) {
                        foreach ($request['food_size'] as $key => $size) {
                            //dd($key);

                            /**
                             * Validate lang, name, description and ingredients fields.
                             * lang field is required and must be alphabets.
                             * name field is required.
                             * If validation fails, abort with validation error message.
                             */

                            //return $size;
                            $size['food_id'] = $food->id;

                            $countryValidate1 = LocationService::checkLanguageCode($dataValidation['restaurant']['country_id'], $size['size_translation']);

                            if ($countryValidate1['status'] != 200) {
                                return response()->json([
                                    "status" => $countryValidate1["status"],
                                    "message" => $countryValidate1["message"]
                                ], $countryValidate1["status"]);

                            }

                            $size['size'] = str_slug($size['size']);

                            /**
                             * Insert translation record of food in food_translation table
                             */

                            $updateSize = $this->foodSize->createFoodSize($size);
                            //dd($updateSize);

                            foreach ($size['size_translation'] as $key => $size_translation) {

                                /**
                                 * Validate lang, name, description and ingredients fields.
                                 * lang field is required and must be alphabets.
                                 * name field is required.
                                 * If validation fails, abort with validation error message.
                                 */


                                $size_translation['lang'] = $key;
                                $size_translation['food_size_id'] = $updateSize->id;

                                $rules = [

                                    "title" => 'required'
                                ];
                                $size_translation['lang'] = str_slug($size_translation['lang']);

                                $validator = Validator::make($size_translation, $rules);
                                if ($validator->fails()) {
                                    $error = $validator->errors();
                                    $this->logStoreHelper->storeLogError([
                                        'Validation Error',
                                        [
                                            'status' => '422',
                                            "message" => [$key => $error]
                                        ]]);
                                    return response()->json([
                                        'status' => '422',
                                        "message" => ['size_translation' => [$key => $error]]
                                    ], 422);
                                }

                                $createSizeTranslation = $this->foodSizeTranslation->createFoodSizeTranslation($size_translation);

                            }
                        }
                    }


                    /**
                     * check if featured image is present. If present, get location of image using saveImage()
                     * */

                    if ($request->has('featured_image')) {
                        $imageUpload = $this->saveImage($request['featured_image'], "featured_image");

                        if ($imageUpload["status"] != 200) {
                            return response()->json(
                                $imageUpload["message"]
                                , $imageUpload["status"]);
                        } else {
                            $request['featured_image'] = $imageUpload["message"]["data"];
                        }


//                    /**
//                     * check if featured image is stored in db.it present delete the image from folder
//                     * */
//
//                    if (!empty($food->featured_image)) {
//                        $this->deleteFile($food->featured_image);
//                    }

                    }

                    /**
                     * check if gallery is present. if present, get location of image using saveImage() function and save as an array
                     * */
                    if ($request->has('gallery')) {
                        foreach ($request['gallery'] as $key => $gallery) {
                            $imageUpload = $this->saveImage($gallery, "gallery");
                            if ($imageUpload["status"] != 200) {
                                return response()->json(
                                    $imageUpload["message"]
                                    , $imageUpload["status"]);
                            }
                            $photo[] = $imageUpload["message"]["data"];

                        }
                        $request['gallery'] = serialize($photo);
//                    if (!empty($food->gallery)) {
//                        foreach (unserialize($food->gallery) as $image) {
//                            $this->deleteFile($image);
//                        }
//                    }


                    }
                    /**
                     * Update data of food for given id
                     */

                    if ($request->has('addon_categories')) {
                        $addon_delete = 1;
                    } else {
                        $addon_delete = 0;
                    }
                    $this->pivot->deletePivotForFood($id, $addon_delete);
                    $foodCategory = $this->foodCategory->attachCategoryToFood($food->id, $request['food_categories']);
                    $foodAddonCategory = $this->foodAddonCategory->attachCategoryToFood($food->id, $request['addon_categories']);

                    $food = $this->food->updateFood($id, $request->only('price', 'veg', 'featured_image', 'gallery', 'status', 'out_of_stock'));

                    $this->logStoreHelper->storeLogInfo([
                        'Food',
                        [
                            "status" => "200",
                            "message" => "Food Updated Successfully",
                            "data" => $food
                        ]

                    ]);
                    /**
                     * If operation is successfully executed, commit it, else revert whole operation.
                     */

                    DB::commit();

                    return response()->json([
                        'status' => "200",
                        "message" => "Food updated Successfully"
                    ], 200);

                } catch (ModelNotFoundException $notFoundException) {
                    return response()->json([
                        'status' => '404',
                        'message' => "Requested food not found"
                    ], 404);
                } catch (\Exception $e) {
                    return response()->json([
                        "status" => "500",
                        "message" => "There were some problem in updating Food "
                    ], 422);
                }

            } else {
                return response()->json([
                    'status' => "401",
                    "message" => "Only Authorized Personnel can update Food "
                ], 401);
            }
        } catch
        (ModelNotFoundException $exception) {
            return response()->json([

                "status" => "404",
                "message" => "Restaurant you entered is not found"

            ], 404);
        }

    }

    /**
     *Delete image from the folder incase data related to that image is deleted.
     * @param $image
     * @return bool
     */

    public function deleteFile($image)
    {
        $image = substr($image, 1);
        File::delete($image);
        return true;
    }

    /**
     * Get all the food data for datatable.
     * Food name is only available in english.
     * @param Request $request
     * @return mixed
     */

    public function getForDatatable($restaurantId, Request $request)
    {
        //if ($request->ajax()) {
        try {
            $foodData = new Collection($this->food->getFoodForDatatable($restaurantId));
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage()
            ], 500);
        }
        return Datatables::of($foodData)
            ->addColumn('name', function ($food) {

                return $food->foodTranslation->map(function ($translation) {
                    return $translation->name;
                })->implode('');
            })
//            ->editColumn('gallery', function ($food){
//                if(!empty($food->gallery)){
//                    unset($foos);
//                    $food["gallery"] = unserialize($food->gallery);
//                    foreach ($food["gallery"] as $key=> $foo){
//                        //todo add from setting
//                        $foos[] = Config::get("config.image_service_base_url_cdn").$foo;
//                    }
//                    $food["gallery"] =  $foos;
//                }
//                else
//                    $food['gallery'] = [];
//                return $food['gallery'];
//            })
            ->make(true);
        //}
    }

    /**
     * Display all the translations for given category to display in the admin dashboard
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    /**
     *
     * Display food of given id with all related translations for admin dashboard.
     * @param $restaurantId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function adminShow($restaurantId, $id)
    {

        /*
         *
         * food of requested id is displayed
        */
        if (AuthChecker::check($restaurantId)) {

            try {

                $food = $this->food->getSpecificFoodByRestaurant($id, $restaurantId);
                if (!empty($food->gallery)) {
                    unset($foos, $foodNormal);
                    $food["gallery"] = unserialize($food->gallery);
                    foreach ($food["gallery"] as $key => $foo) {
                        /**
                         * get position of last occurance of / in fetaured_image and added the postion by 1
                         */
                        $getpositionOfImageInUrl = strrpos($foo, "/") + 1;
                        //todo add from setting
                        $normalImage = Config::get("config.image_service_base_url_cdn") . substr_replace($foo, "normal-", $getpositionOfImageInUrl, 0);
                        $foos[] = Config::get("config.image_service_base_url_cdn") . $foo;
                        $foodNormal[] = $normalImage;
                    }
                    $food["gallery"] = $foos;
                    $food["gallery_resized"] = $foodNormal;
                } else {
                    $food["gallery"] = [];
                    $food["gallery_resized"] = [];
                }
                //todo add from setting
                if (!empty($food->featured_image)) {
                    /**
                     * get position of last occurance of / in fetaured_image and added the postion by 1
                     */
                    $getpositionOfImageInUrl = strrpos($food->featured_image, "/") + 1;
                    /**
                     * add normal- prefix in $getpositionOfImageInUrl postition
                     */
                    $food["featured_image_resized"] = substr_replace($food->featured_image, "normal-", $getpositionOfImageInUrl, 0);
                } else {
                    $food["featured_image_resized"] = [];
                }

            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Empty Record for requested food"
                ], 404);
            }
            /*
             * Get category Translation based on @param lang
             * if empty record is obtained default en lang is returned
             * */
            try {


                $foodTranslation = $food->foodTranslation;

                $foodSize = $food->foodSize()->get();

                if ($foodSize->count() == 0) {

                    $food['food_size'] = [];

                } else {

                    foreach ($foodSize as $key => $size) {
                        $sizeTranslation = $size->FoodSizeTranslation()->get();
                        $size['translation'] = $sizeTranslation;

                    }

                    $food['food_size'] = $foodSize;
                }


                $foodCategory = $food->category()->select('category.id')->get();

                if (!$foodCategory->first()) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Food categories not found"
                    ], 404);
                }
                foreach ($foodCategory as $category) {
                    $translation = $category->categoryTranslation()->where('lang', "en")->get();
                    try {
                        if ($translation->count() == 0) {
                            throw new \Exception();
                        }
                    } catch (\Exception $ex) {
                        return response()->json([
                            "status" => "404",
                            "message" => "Default language english not found in database"
                        ], 404);
                    }

                    $category['name'] = $translation[0]['name'];

                }

                $food['addon_categories'] = [];
                $foodAddonCategory = $food->addonCategory()->select('addoncategory.id')->get();


                if ($foodAddonCategory->first()) {
                    foreach ($foodAddonCategory as $category) {
                        $translation = $category->categoryTranslation()->where('lang', "en")->get();
                        try {
                            if ($translation->count() == 0) {
                                throw new \Exception();
                            }
                        } catch (\Exception $ex) {
                            return response()->json([
                                "status" => "404",
                                "message" => "Default language english not found in database"
                            ], 404);
                        }

                        $category['name'] = $translation[0]['name'];

                    }
                    $food['addon_categories'] = $foodAddonCategory;
                }

                /*
                 * attach name and lang in $restaurant variable
                 * */
                $food['translation'] = $foodTranslation;
                $food['food_categories'] = $foodCategory;


                return response()->json([
                    'status' => "200",
                    "data" => $food
                ], 200);

            } catch (\Exception $ex) {
                return response()->json([
                    'status' => "404",
                    "message" => "Data Related to food could not be found"
                ], 404);
            }
        } else {
            return response()->json([
                'status' => "401",
                "message" => "Unauthorized action"
            ], 401);
        }

    }

    /**
     * Public route to display specific food and its related addon category and addons.
     * @param $restaurantId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function publicShow($restaurantId, $id)
    {
        $request['lang'] = Input::get("lang", "en");

        /*
         *
         * Check if the restaurant is active.
        */
        if (!RestaurantChecker::check($restaurantId)) {

            return response()->json([

                "status" => "404",
                "message" => "Restaurant you entered is not found"

            ], 404);
        }
        /**
         * If food is not active or available, throw 404 exception error.
         */
        try {

            $food = $this->food->getSpecificFoodByRestaurant($id, $restaurantId);

            if ($food['status'] == 0) {

                throw new \Exception();

            }

        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Empty Record for requested food"
            ], 404);
        }
        /*
         * Get category Translation based on @param lang
         * if empty record is obtained default en lang is returned
         * */

        $foodTranslation = $this->foodTranslation->getSpecificFoodTransalationByLang($food->id, $request['lang']);
        try {
            if ($foodTranslation->count() == 0) {
                if ($request['lang'] == 'en') {
                    throw new \Exception();
                } else {
                    $foodTranslation = $this->foodTranslation->getSpecificFoodTransalationByLang($food->id, 'en');
                    if ($foodTranslation->count() == 0) {
                        throw new \Exception();
                    }
                }

            }
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Default language english not found in database"
            ], 404);
        }
        $foodSize = $food->foodSize()->get();

        if ($foodSize->count() == 0) {

            $food['size'] = [];

        } else {

            foreach ($foodSize as $key => $size) {

                if ($size['is_default'] == "1") {
                    $food['price'] = $size['price'];

                }
                $sizeTranslation = $size->FoodSizeTranslation()->where('lang', $request['lang'])->first();
                if (count($sizeTranslation) == 0) {
                    if ($request['lang'] == 'en') {
                        unset($foodSize[$key]);
                    } else {
                        $sizeTranslation = $size->FoodSizeTranslation()->where('lang', 'en')->first();
                        if (count($sizeTranslation) == 0) {
                            unset($foodSize[$key]);
                        }
                    }

                } else {
                    $size['title'] = $sizeTranslation['title'];
                }
                $size['is_default'] = (integer)$size['is_default'];
            }

            $food['size'] = $foodSize;
        }


        /*
         * attach name and lang in $food variable
         * */
        $food['lang'] = $foodTranslation[0]['lang'];
        $food['name'] = $foodTranslation[0]['name'];
        $food['description'] = $foodTranslation[0]['description'];
        $food['ingredients'] = $foodTranslation[0]['ingredients'];
        if (!empty($food->gallery)) {
            unset($foos, $foodNormal);
            $food["gallery"] = unserialize($food->gallery);
            foreach ($food["gallery"] as $key => $foo) {
                /**
                 * get position of last occurance of / in fetaured_image and added the postion by 1
                 */
                $getpositionOfImageInUrl = strrpos($foo, "/") + 1;
                //todo add from setting
                $normalImage = Config::get("config.image_service_base_url_cdn") . substr_replace($foo, "normal-", $getpositionOfImageInUrl, 0);
                $foos[] = Config::get("config.image_service_base_url_cdn") . $foo;
                $foodNormal[] = $normalImage;
            }
            $food["gallery"] = $foos;
            $food["gallery_resized"] = $foodNormal;
        } else {
            $food["gallery"] = [];
            $food["gallery_resized"] = [];
        }
        //todo add from setting
        if (!empty($food->featured_image)) {
            /**
             * get position of last occurance of / in fetaured_image and added the postion by 1
             */
            $getpositionOfImageInUrl = strrpos($food->featured_image, "/") + 1;
            /**
             * add normal- prefix in $getpositionOfImageInUrl postition
             */
            $food["featured_image_resized"] = substr_replace($food->featured_image, "normal-", $getpositionOfImageInUrl, 0);
        } else {
            $food["featured_image_resized"] = [];
        }

        /**
         * Get active addon categories related to food of same restaurant.
         */
        $addonCategory = $food->addonCategory()->where('status', 1)->where('restaurant_id', $restaurantId)->get();

        $addonCat = array();
        if (count($addonCategory) > 0) {

            foreach ($addonCategory as $addcategory) {
                $categoryTranslation = $addcategory->categoryTranslation()->where('lang', $request['lang'])->get();

                try {
                    if ($categoryTranslation->count() == 0) {
                        if ($request['lang'] == 'en') {
                            throw new \Exception();
                        } else {
                            $categoryTranslation = $addcategory->categoryTranslation()->where('lang', 'en')->get();
                            if ($categoryTranslation->count() == 0) {

                                throw new \Exception();
                            }
                        }

                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Default language english not found in database"
                    ], 404);
                }
                /*
                               * attach name and lang in $category variable
                               * */
                $addcategory['name'] = $categoryTranslation[0]['name'];
                $addcategory['lang'] = $categoryTranslation[0]['lang'];
                /**
                 * Get addon related to addon category and restaurant branch.
                 * If addon is not active or available, throw 404 error message.
                 */
                $addons = $addcategory->addon()->where('status', 1)->where('restaurant_id', $restaurantId)->get();

                if (count($addons) > 0) {
                    foreach ($addons as $addon) {
                        $addonTranslation = $addon->addonTranslation()->where('lang', $request['lang'])->get();
                        try {
                            if ($addonTranslation->count() == 0) {
                                if ($request['lang'] == 'en') {
                                    throw new \Exception();
                                } else {
                                    $addonTranslation = $addon->addonTranslation()->where('lang', 'en')->get();
                                    if ($addonTranslation->count() == 0) {
                                        throw new \Exception();
                                    }
                                }

                            }
                        } catch (\Exception $ex) {
                            return response()->json([
                                "status" => "404",
                                "message" => "Default language english not found in database"
                            ], 404);
                        }

                        $addon['lang'] = $addonTranslation[0]['lang'];
                        $addon['name'] = $addonTranslation[0]['name'];
                        $addon['description'] = $addonTranslation[0]['description'];
                    }

                    $addcategory['addons'] = $addons;

                    $addonCat[] = $addcategory;
                }
            }

            $food['addon_category'] = $addonCat;
        }


        return response()->json([
            'status' => "200",
            "data" => $food
        ], 200);


    }

    /**
     * Public route to display specific food and its related addon category and addons.
     * @param $restaurantId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
//    public function publicShow($restaurantId, $id)
//    {
//        $request['lang'] = Input::get("lang", "en");
//
//        /*
//         *
//         * Check if the restaurant is active.
//        */
//        if (!RestaurantChecker::check($restaurantId)) {
//
//            return response()->json([
//
//                "status" => "404",
//                "message" => "Restaurant you entered is not found"
//
//            ], 404);
//        }
//        /**
//         * If food is not active or available, throw 404 exception error.
//         */
//        try {
//
//            $food = $this->food->getSpecificFoodByRestaurant($id, $restaurantId);
//
//            if ($food['status'] == 0) {
//
//                throw new \Exception();
//
//            }
//
//        } catch (\Exception $ex) {
//            return response()->json([
//                "status" => "404",
//                "message" => "Empty Record for requested food"
//            ], 404);
//        }
//        /*
//         * Get category Translation based on @param lang
//         * if empty record is obtained default en lang is returned
//         * */
//
//        $foodTranslation = $this->foodTranslation->getSpecificFoodTransalationByLang($food->id, $request['lang']);
//        try {
//            if ($foodTranslation->count() == 0) {
//                if ($request['lang'] == 'en') {
//                    throw new \Exception();
//                } else {
//                    $foodTranslation = $this->foodTranslation->getSpecificFoodTransalationByLang($food->id, 'en');
//                    if ($foodTranslation->count() == 0) {
//                        throw new \Exception();
//                    }
//                }
//
//            }
//        } catch (\Exception $ex) {
//            return response()->json([
//                "status" => "404",
//                "message" => "Default language english not found in database"
//            ], 404);
//        }
//
//        /*
//         * attach name and lang in $food variable
//         * */
//        $food['lang'] = $foodTranslation[0]['lang'];
//        $food['name'] = $foodTranslation[0]['name'];
//        $food['description'] = $foodTranslation[0]['description'];
//        $food['ingredients'] = $foodTranslation[0]['ingredients'];
//        $food['gallery'] = unserialize($food->gallery);
//
//        /**
//         * Get active addon categories related to food of same restaurant.
//         */
//        $addonCategory = $food->addonCategory()->where('status', 1)->where('restaurant_id', $restaurantId)->get();
//
//        $addonCat = array();
//        if (count($addonCategory) > 0) {
//
//            foreach ($addonCategory as $addcategory) {
//                $categoryTranslation = $addcategory->categoryTranslation()->where('lang', $request['lang'])->get();
//                try {
//                    if ($categoryTranslation->count() == 0) {
//                        if ($request['lang'] == 'en') {
//                            throw new \Exception();
//                        } else {
//                            $categoryTranslation = $addcategory->categoryTranslation()->where('lang', $request['lang'])->get();
//                            if ($categoryTranslation->count() == 0) {
//                                throw new \Exception();
//                            }
//                        }
//
//                    }
//                } catch (\Exception $ex) {
//                    return response()->json([
//                        "status" => "404",
//                        "message" => "Default language english not found in database"
//                    ], 404);
//                }
//
//
//                /*
//                 * attach name and lang in $category variable
//                 * */
//                $category['name'] = $categoryTranslation[0]['name'];
//                $category['lang'] = $categoryTranslation[0]['lang'];
//                /**
//                 * Get addon related to addon category and restaurant branch.
//                 * If addon is not active or available, throw 404 error message.
//                 */
//                $addons = $addcategory->addon()->where('status', 1)->where('restaurant_id', $restaurantId)->get();
//
//                if (count($addons) > 0) {
//                    foreach ($addons as $addon) {
//                        $translation = $addon->addonTranslation()->where('lang', $request['lang'])->get();
//                        try {
//                            if ($categoryTranslation->count() == 0) {
//                                if ($request['lang'] == 'en') {
//                                    throw new \Exception();
//                                } else {
//                                    $categoryTranslation = $addon->addonTranslation()->where('lang', $request['lang'])->get();
//                                    if ($categoryTranslation->count() == 0) {
//                                        throw new \Exception();
//                                    }
//                                }
//
//                            }
//                        } catch (\Exception $ex) {
//                            return response()->json([
//                                "status" => "404",
//                                "message" => "Default language english not found in database"
//                            ], 404);
//                        }
//
//                        $addon['lang'] = $translation[0]['lang'];
//                        $addon['name'] = $translation[0]['name'];
//                        $addon['description'] = $translation[0]['description'];
//                    }
//
//                    $addcategory['addons'] = $addons;
//
//                    $addonCat[] = $addcategory;
//                }
//            }
//
//            $food['addon_category'] = $addonCat;
//        }
//
//
//        return response()->json([
//            'status' => "200",
//            "data" => $food
//        ], 200);
//
//
//    }
    /**
     * Update function for branch operators only
     * @param $restaurantId
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function operatorUpdate($id, Request $request)
    {
        $restaurantInfo = RoleChecker::getRestaurantInfo();

        if(RoleChecker::hasRole('branch-operator'))
        {
            try{
                $rules = [
                    'status' => 'required|integer|min:0|max:1',
                    'out_of_stock' => 'required|integer|min:0|max:1',
                ];
                $validator = Validator::make($request->toArray(), $rules);
                if ($validator->fails()) {
                    return response()->json([
                        'status' => '422',
                        "message" => $validator->errors()
                    ], 422);
                }

                $food = $this->food->getSpecificFoodByRestaurant($id, $restaurantInfo['branch_id']);

                $request['out_of_stock'] = (string)$request['out_of_stock'];

                $this->food->updateFood($id, $request->only('status','out_of_stock'));

                return response()->json([
                    'status' => "200",
                    "message" => "Food Updated Successfully"
                ], 200);
            }catch( \Exception $ex)
            {
                return response()->json([
                    'status' => "404",
                    "message" => "Empty Record for requested food "
                ], 404);
            }

        }else{
            return response()->json([
                'status' => "401",
                "message" => "Unauthorized action"
            ], 401) ;
        }

    }

    /**
     * Food list for branch operator
     * @return \Illuminate\Http\JsonResponse
     */

    public function operatorFoodView()
    {

        try {
            $restaurantInfo = RoleChecker::getRestaurantInfo();

            if(RoleChecker::hasRole('branch-operator')) {
                try{
                    $restaurantResponse =  $this->restaurantBranch->getSpecificRestaurantBranch($restaurantInfo['branch_id']);
                }catch(\Exception $ex)
                {
                    return response()->json([
                        'status' => '404',
                        "message" => "Restaurant not found"
                    ], 404);
                }


                $country = RemoteCall::getSpecific(Config::get("config.country_url")."/".$restaurantResponse['country_id']);

                if ($country['status'] != 200) {
                    throw new \Exception("Currency code not found");
                }
                if (!is_null(Input::get('status'))) {
                    $request['status'] = Input::get("status");

                }
                $request['lang'] = Input::get("lang", "en");
                $request['limit'] = Input::get("limit", 10);


                /*
                 * Validate the get request
                 * 'status' and 'veg' are validated only when the supplied.
                 *
                 * Validate the status and limit
                 * Status must be 0 or 1.
                 * veg must be 0 or 1.
                 * limit must be integer.
                 *
                 *
                 * */
                $rules = [
                    'status' => 'sometimes|required|integer|min:0|max:1',
                    'limit' => 'required|integer',

                ];
                $validator = Validator::make($request, $rules);
                if ($validator->fails()) {
                    return response()->json([
                        'status' => '422',
                        "message" => $validator->errors()
                    ], 422);
                }

                if (!isset($request['status']) || empty($request['status'])) {
                    $request['status'] = "all";

                }
                $data = $this->food->getAllFood($restaurantInfo['branch_id'], $request['status'], $request['limit'], "all");

                try {
                    if (!$data->first()) {
                        throw  new \Exception();
                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Empty Record"
                    ], 404);
                }

                foreach ($data as $key => $food) {
                    /*
                    * Get food Translation based on @param lang
                    * if empty record is obtained default en lang is returned
                    * If default en lang is not available, throw exception
                    * */

                    $foodTranslation = $this->foodTranslation->getSpecificFoodTransalationByLang($food->id, $request['lang']);
                    try {
                        if ($foodTranslation->count() == 0) {
                            if ($request['lang'] == 'en') {
                                throw new \Exception();
                            } else {
                                $foodTranslation = $this->foodTranslation->getSpecificFoodTransalationByLang($food->id, 'en');
                                if ($foodTranslation->count() == 0) {
                                    throw new \Exception();
                                }
                            }

                        }
                    } catch (\Exception $ex) {
                        return response()->json([
                            "status" => "404",
                            "message" => "Default language english not found in database"
                        ], 404);
                    }

                    /*
                     * attach name and lang_code in $food variable
                     * */
                    $food['lang'] = $foodTranslation[0]['lang'];
                    $food['name'] = $foodTranslation[0]['name'];
                    $food['description'] = $foodTranslation[0]['description'];
                    $food['ingredients'] = $foodTranslation[0]['ingredients'];
                    if (!empty($food->gallery)) {
                        unset($foos, $foodNormal);
                        $food["gallery"] = unserialize($food->gallery);
                        foreach ($food["gallery"] as $key => $foo) {
                            /**
                             * get position of last occurance of / in fetaured_image and added the postion by 1
                             */
                            $getpositionOfImageInUrl = strrpos($foo, "/") + 1;
                            //todo add from setting
                            $normalImage = Config::get("config.image_service_base_url_cdn") . substr_replace($foo, "normal-", $getpositionOfImageInUrl, 0);
                            $foos[] = Config::get("config.image_service_base_url_cdn") . $foo;
                            $foodNormal[] = $normalImage;
                        }
                        $food["gallery"] = $foos;
                        $food["gallery_resized"] = $foodNormal;
                    } else {
                        $food["gallery"] = [];
                        $food["gallery_resized"] = [];
                    }
                    //todo add from setting
                    if (!empty($food->featured_image)) {
                        /**
                         * get position of last occurance of / in fetaured_image and added the postion by 1
                         */
                        $getpositionOfImageInUrl = strrpos($food->featured_image, "/") + 1;
                        /**
                         * add normal- prefix in $getpositionOfImageInUrl postition
                         */
                        $food["featured_image_resized"] = substr_replace($food->featured_image, "normal-", $getpositionOfImageInUrl, 0);
                    } else {
                        $food["featured_image_resized"] = [];
                    }

                    $food['currency'] = trim($country["message"]["data"]['currency_symbol']);
                }

                return response()->json([
                    'status' => "200",
                    "data" => $data->appends(["limit" => $request["limit"],"lang" => $request["lang"]])->withPath('/branch-operator/food')
                ], 200);
            }else{
                return response()->json([
                    'status' => "401",
                    "message" => "Unauthorized action"
                ], 401) ;
            }

        } catch (\Exception $ex) {

            return response()->json([
                "status" => "404",
                "message" => $ex->getMessage()
            ], 404);
        }


    }

}