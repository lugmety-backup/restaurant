<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/28/2017
 * Time: 12:38 PM
 */

namespace App\Http\Controllers;

use App\Repo\DeliveryZoneInterface;
use App\Repo\RestaurantBranchInterface;
use App\Repo\RestaurantInterface;
use App\Repo\RestaurantUsersInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use RoleChecker;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use LogStoreHelper;
use Validator;
use RemoteCall;
use Yajra\Datatables\Facades\Datatables;


class DeliveryZoneController extends Controller
{
    protected $checker;
    protected $logStoreHelper;
    protected $deliveryZone;
    protected $restaurantBranch;
    protected $restaurant;
    protected $restaurantUser;

    /**
     * DeliveryZoneController constructor.
     * @param DeliveryZoneInterface $deliveryZone
     * @param RoleChecker $checker
     * @param RestaurantBranchInterface $restaurantBranch
     * @param RestaurantUsersInterface $restaurantUser
     * @param RestaurantInterface $restaurant
     * @param LogStoreHelper $logStoreHelper
     */
    public function __construct(DeliveryZoneInterface $deliveryZone,
                                RoleChecker $checker,
                                RestaurantBranchInterface $restaurantBranch,
                                RestaurantUsersInterface $restaurantUser,
                                RestaurantInterface $restaurant,
                                LogStoreHelper $logStoreHelper)
    {
        $this->deliveryZone=$deliveryZone;
        $this->checker=$checker;
        $this->logStoreHelper=$logStoreHelper;
        $this->restaurantBranch=$restaurantBranch;
        $this->restaurant=$restaurant;
        $this->restaurantUser=$restaurantUser;
    }

    /**
     * Display a listing of the resource from table named .
     *
     * @param $restaurant_id
     * @param $branch_id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($restaurant_id,$branch_id,Request $request)
    {
        $request['status']=Input::get("status");
        /**
         * if limit param is other than integer and
         */
        if (!is_null($request['status'])) {
            $rules = [
                "status"=>"numeric|min:0|max:1"];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $request['status']=1;
            }
        }
        /**
         * validation for restaurant_id
         */
        try{
            $restaurant=$this->restaurant->getSpecificRestaurant($restaurant_id,1);
        }catch (\Exception $e)
        {
            $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser",[
                'status'=>'404',
                'message'=>'Requested Restaurant could not be found'
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Restaurant could not be found'], 404);
        }
        /**
         * validation for branch_id
         */
        try{
            $restaurantBranch=$this->restaurantBranch->getSpecificRestaurantBranchByStatus($branch_id,$request['status']);
        }catch (\Exception $e)
        {
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Restaurant branch could not be found'], 404);
        }
        try{

            if($restaurantBranch['parent_id'] != $restaurant_id){
                throw new \Exception();
            }
        }catch (\Exception $e)
        {
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Branch is not found in requested restaurant'], 404);
        }
        try{
            $deliveryZone= $this->deliveryZone->getAllDeliveryZoneForDataTable($branch_id);
            if(count($deliveryZone)==0){
                throw new \Exception();
            }
        }
        catch (\Exception $ex){
            return response()->json([
                'status'=>'404',
                "message"=>'Empty Record'
            ],404);
        }

        $districts =[];
        foreach ($deliveryZone as $zone){
            $districts['districts'][]=$zone['district_id'];
        }
        $districts['lang']=$request['lang'];

        $url = Config::get("config.location_url");
        $districtNames = RemoteCall::store($url."/public/district/getnames" , $districts);
        if ($districtNames["status"] != "200") {
            return response()->json([
                $districtNames["message"]
            ], $districtNames["status"]);
        }
        $results = $districtNames['message']['data']['districts'];
        foreach ($results as $key => $result){
            foreach ($deliveryZone as $zone){
                if($zone['district_id'] == $result['district_id']){
                    $results[$key]['id']=$zone['id'];
                }
            }
        }

        return Datatables::of($results)->make('true');
    }

    public function indexPublic($branch_id,Request $request)
    {
        /**
         *validation for branch_id
         */
        try{
            $restaurantBranch=$this->restaurantBranch->getSpecificRestaurantBranchByStatus($branch_id,1);
        }catch (\Exception $e)
        {
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Restaurant branch could not be found'], 404);
        }

        try{
            $restaurantBranch=$this->restaurantBranch->getSpecificRestaurantBranch($branch_id );
        }catch (\Exception $e)
        {
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Restaurant branch could not be found'], 404);
        }

        $request['lang'] = Input::get('lang', 'en');

        /**
         * get all delivery zone of branch
         */
        try{
            $deliveryZone= $this->deliveryZone->getAllDeliveryZone($branch_id);
            if(count($deliveryZone)==0){
                throw new \Exception();
            }
        }
        catch (\Exception $ex){
            return response()->json([
                'status'=>'200',
                "message"=>'Empty Record'
            ],200);
        }
        $districts =[];
        foreach ($deliveryZone as $zone){
            $districts['districts'][]=$zone['district_id'];
        }
        $districts['lang']=$request['lang'];

        $url = Config::get("config.location_url");
        $districtNames = RemoteCall::store($url."/public/district/getnames" , $districts);
        if ($districtNames["status"] != "200") {
            return response()->json([
                $districtNames["message"]
            ], $districtNames["status"]);
        }
        $results = $districtNames['message']['data']['districts'];
        foreach ($results as $key => $result){
            foreach ($deliveryZone as $zone){
                if($zone['district_id'] == $result['district_id']){
                    $results[$key]['id']=$zone['id'];
                }
            }
        }

        return response()->json([
            'status'=>'200',
            "data"=>$results
        ],200);
    }


    /**
     * Store a newly created resource in table name .
     *
     * Any restaurant only can have delivery zone with in it's city.
     *
     * @param Request $request
     * @param $restaurant_id
     * @param $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request,$restaurant_id,$branch_id)
    {
        /**
         * validation for restaurant_id
         */
        try{
            $restaurant=$this->restaurant->getSpecificRestaurantCheckForBranch($restaurant_id,1);
        }catch (\Exception $e)
        {
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Restaurant could not be found or Restaurant status is disabled'], 404);
        }
        /**
         * validation for branch_id
         */
        try{
            $restaurantBranch=$this->restaurantBranch->getSpecificRestaurantBranchByStatus($branch_id,1);
            if(!$restaurantBranch){
                throw new \Exception();
            }
        }catch (\Exception $e)
        {
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Restaurant branch could not be found'], 404);
        }
        try{
            if($restaurantBranch['parent_id'] != $restaurant_id){
                throw new \Exception();
            }
        }catch (\Exception $e)
        {
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Branch is not found in requested restaurant'], 404);
        }

        /**
         * checking roles potential
         */
        if(!($this->checkRolePotention($restaurant_id,$branch_id))){
            return response()->json([
                'status' => '401',
                'message' => 'Unauthorized'], 401);
        }
        try {
            $this->validate($request, [
                'district_id'=>'required|array'
            ]);
        }catch (\Exception $ex) {
            $this->logStoreHelper->storeLogError(array("Error in Validation in delivery zone index", [
                "status" => "422",
                "message" => $ex->response->original
            ]));
            return response()->json([
                "status" => "422",
                "message" =>$ex->response->original
            ],422);
        }

        /**
         * checking the given district lies in the city or not
         */
        $districtCheck=$this->checkDistrict($restaurantBranch['city_id'],$request);
        switch ($districtCheck){

            case false:
                return response()->json([
                    "status"=>"422",
                    "message"=>['district_id'=>["Input Districts does not match with Available districts of City"]]
                ],422);
                break;

            case is_array($districtCheck) :
                return response()->json([
                    "status"=>$districtCheck["status"],
                    "message"=>$districtCheck["message"]
                ],$districtCheck["status"]);
                break;

            case true:
                $this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
                    "status" => "200",
                    "message" => "Valid Translation"
                ]));
                break;
        }
        DB::beginTransaction();
        foreach ($request['district_id'] as $req['district_id']){
            $req['branch_id']=$branch_id;
            try{
                $deliveryZone=$this->deliveryZone->getDeliveryZoneByDistrictWithBranchId($req['district_id'],$branch_id);
                if(count($deliveryZone)!=0){
                    throw new \Exception();
                }
            }catch (\Exception $e){
                DB::rollBack();
                return response()->json([
                    'status'=>'409',
                    'message'=> 'Requested Delivery Zone already Exists'
                ],409);
            }
            try{
                $deliveryZone=$this->deliveryZone->createDeliveryZone($req);
            }catch (\Exception $e){
                DB::rollBack();
                return response()->json([
                    'status'=>'403',
                    'message'=> 'Requested Delivery Zone could not be created'
                ],403);
            }
        }
        DB::commit();
        return response()->json([
            'status'=>'200',
            'message'=> 'Delivery Zone created successfully'
        ],200);

    }
    /**
     * Showing Specific record
     *
     * @param $id
     * @param $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showPublic($id, $branch_id)
{
    /**
     * validation for branch_id
     */
    try{
        $restaurantBranch=$this->restaurantBranch->getSpecificRestaurantBranchByStatus($branch_id,1);
    }catch (\Exception $e)
    {
        return response()->json([
            'status'=>'404',
            'message'=>'Requested Restaurant branch could not be found'], 404);
    }
    /**
     * retriving specific the delivery zone of branch
     */
    try{
        $deliveryZone= $this->deliveryZone->getSpecificDeliveryZone($id);
    }
    catch (\Exception $ex){
        return response()->json([
            'status'=>'404',
            "message"=>'Empty Record'
        ],404);
    }
    return response()->json([
        'status'=>'200',
        "data"=>$deliveryZone
    ],200);
}


    /**
     * Update the specified resource in storage in table named.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$restaurant_id,$branch_id)
    {
        /**
          *validation for restaurant_id
          */
        try{
            $restaurant=$this->restaurant->getSpecificRestaurantCheckForBranch($restaurant_id,1);
        }catch (\Exception $e)
        {
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Restaurant could not be found or Restaurant status is disabled'], 404);
        }
        /**
         *validation for branch_id
         */
        try{
            $restaurantBranch=$this->restaurantBranch->getSpecificRestaurantBranchByStatus($branch_id,1);
            if(!$restaurantBranch){
                throw new \Exception();
            }
        }catch (\Exception $e)
        {
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Restaurant could not be found or Restaurant status is disabled'], 404);
        }
        try{
            if($restaurantBranch['parent_id'] != $restaurant_id){
                throw new \Exception();
            }
        }catch (\Exception $e)
        {
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Branch is not found in requested restaurant'], 404);
        }
        /**
         * checking roles potential
         */
        if(!($this->checkRolePotention($restaurant_id,$branch_id))){

            return response()->json([
                'status' => '401',
                'message' => 'Unauthorized'], 401);
        }
        /**
         * validation of request
         */
        try {
            $this->validate($request, [
                'district_id'=>'required|array'
            ]);
        }catch (\Exception $ex) {
            $this->logStoreHelper->storeLogError(array("Error in Validation in delivery zone index", [
                "status" => "422",
                "message" => $ex->response->original
            ]));
            return response()->json([
                "status" => "422",
                "message" =>$ex->response->original
            ],422);
        }
        $districtCheck=$this->checkDistrict($restaurantBranch['city_id'],$request);
        switch ($districtCheck){

            case false:{
                return response()->json([
                    "status"=>"422",
                    "message"=>"Input Districts does not match with Available districts of City"
                ],422);
                break;

            }
            case is_array($districtCheck) :{
                return response()->json([
                    "status"=>$districtCheck["status"],
                    "message"=>$districtCheck["message"]
                ],$districtCheck["status"]);
                break;
            }

            case true:{
                $this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
                    "status" => "200",
                    "message" => "Valid Translation"
                ]));
                break;
            }
        }
        DB::beginTransaction();
        /**
         * deletion of all delivery zones
         */
        try{
            $deliveryZone= $this->deliveryZone->getAllDeliveryZone($branch_id,null);
            if(count($deliveryZone)!=0){
                foreach ($deliveryZone as $zone){
                    $deliveryZone=$this->deliveryZone->deleteDeliveryZone($zone->id);
                }
            }
        }catch(\Exception $exception){
            DB::rollBack();
            return response()->json([
                'status'=>'403',
                'message'=> 'Delivery Zone could not be updated'
            ],403);
        }
        /**
         *  createing delivery zones
         */
        foreach ($request['district_id'] as $req['district_id']){
            $req['branch_id']=$branch_id;
            try{
                $deliveryZone=$this->deliveryZone->createDeliveryZone($req);
            }catch (\Exception $e){
                DB::rollBack();
                return response()->json([
                    'status'=>'403',
                    'message'=> 'Requested Delivery Zone could not be created'
                ],403);
            }
        }
        DB::commit();
        return response()->json([
            'status'=>'200',
            'message'=> 'Delivery Zone updated successfully'
        ],200);
    }

    /**
     * Deleting all delivery zones of restaurant branch
     *
     * @param $restaurant_id
     * @param $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($restaurant_id,$branch_id)
    {
        /**
         *validation for restaurant_id
         */
        try{
            $restaurant=$this->restaurant->getSpecificRestaurant($restaurant_id,null);
        }catch (\Exception $e)
        {
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Restaurant could not be found'], 404);
        }
        /**
         *validation for branch_id
         */
        try{
            $restaurantBranch=$this->restaurantBranch->getSpecificRestaurantBranch($branch_id);
        }catch (\Exception $e)
        {
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Restaurant branch could not be found'], 404);
        }
        try{
            if($restaurantBranch['parent_id'] != $restaurant_id){
                throw new \Exception();
            }
        }catch (\Exception $e)
        {
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Branch is not found in requested restaurant'], 404);
        }
        /**
         * validation for delivery-zone
         */
        try{
            $deliveryZones=$this->deliveryZone->getAllDeliveryZone($branch_id,null);
        }catch (\Exception $e)
        {
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Delivery zone could not be found'], 404);
        }
        /**
         * checking roles potential
         */
        if(!($this->checkRolePotention($restaurant_id,$branch_id))){
            return response()->json([
                'status' => '401',
                'message' => 'Unauthorized'], 401);
        }
        try{
            foreach ($deliveryZones as $deliveryZone){
                $this->deliveryZone->deleteDeliveryZone($deliveryZone['id']);
            }

          }catch (ModelNotFoundException $exception){
            return response()->json([
                'status' => '404',
                'message' => "Requested Delivery Zone not found"
            ], 404);
        }
          catch (\Exception $ex){
            return response()->json([
                'status' => '403',
                'message' => "Requested Delivery Zone could not be deleted"
            ], 403);
        }
        return response()->json([
            'status'=>'200',
            'message'=>"Delivery Zone deleted successfully"
        ]);
    }

    public function checkDistrict($city_id,$requestedDistricts)
    {
        $district = RemoteCall::getSpecific("http://api.stagingapp.io/location/v1/public/city/" . $city_id . "/district");
        if ($district["status"] != "200") {
            return $district;
        }
        $requested = $requestedDistricts["district_id"];
        $available=array();
        foreach ($district['message']['data']['data'] as $key => $data) {
            $available[] = $data['id'];
        }
        $intersectArray = array_intersect($available, $requested);
        if (count($requested) == count($intersectArray)) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Verify current User Role
     *
     *
     * if .....   user1 is restaurant-admin or parent-role restaurant-admin with id1
     * id1 has 3 branches branch1, branch2, branch3
     * then restaurant-admin can only access delivery zones in branch1,branch2,branch3 not other branches like: branch4
     *
     * if ..... user1 is branch-admin or parent-role branch-admin with id1
     * id1 has one branch branch1
     * the branch-admin can only access delivery zones in branch1 not other
     *
     * else other can access
     *
     * @param $branch_id
     * @return bool
     * */
    private function checkRolePotention($restaurant_id,$branch_id){
        // restaurant-admin has access to  his restaurant only
        if ((RoleChecker::hasRole('restaurant-admin') || (RoleChecker::get('parentRole') == 'restaurant_admin'))) {
            try {
                if(RoleChecker::get('is_restaurant_admin') != true){
                    throw new \Exception();
                }
                if ($restaurant_id == RoleChecker::get('restaurant_id')) {
                    return true;
                } else {
                    throw new \Exception();
                }

            } catch (\Exception $ex) {
                return false;
            }
        }
        elseif (RoleChecker::hasRole('branch-admin') || (RoleChecker::get('parentRole') == 'branch_admin') ) {
            try {
                if(RoleChecker::get('is_restaurant_admin') != false){
                    throw new \Exception();
                }
                if (RoleChecker::get('branch_id') == $branch_id) {
                    return true;
                } else {
                    throw new \Exception();
                }

            } catch (\Exception $ex) {
                return false;
            }
        }
        return true;
    }


}

