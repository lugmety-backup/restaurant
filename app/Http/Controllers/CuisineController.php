<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 7/7/2017
 * Time: 2:16 PM
 */

namespace App\Http\Controllers;


use App\Repo\CuisineInterface;
use App\Repo\CuisineTranslationInterface;
use App\Repo\RestaurantBranchInterface;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use LogStoreHelper;
use RemoteCall;
use RoleChecker;
use Illuminate\Support\Facades\Config;
use ImageUploader;
use Validator;
use Slugify;
use Illuminate\Support\Facades\DB;
use Datatables;

/**
 * Class CuisineController
 * @package App\Http\Controllers
 */
class CuisineController extends Controller
{

    /**
     * @var RestaurantBranchInterface
     */
    protected  $restaurantBranch;

    /**
     * @var CuisineInterface
     */
    protected $cuisine;

    /**
     * @var CuisineTranslationInterface
     */
    protected $cuisineTranslation;

    /**
     * @var LogStoreHelper
     */
    protected $log;

    /**
     * CuisineController constructor.
     * @param RestaurantBranchInterface $restaurantBranch
     * @param CuisineInterface $cuisine
     * @param CuisineTranslationInterface $cuisineTranslation
     * @param LogStoreHelper $log
     */
    public function __construct(RestaurantBranchInterface $restaurantBranch, CuisineInterface $cuisine , CuisineTranslationInterface $cuisineTranslation, LogStoreHelper $log)
    {
        $this->cuisine = $cuisine;
        $this->cuisineTranslation = $cuisineTranslation;
        $this->log = $log;
        $this->restaurantBranch = $restaurantBranch;
    }

    /**
     * returns all cuisine based on country_id whose status is 1
     * default en language is displayed otherwise based on lang param,
     * default pagination is 10
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllCuisineForPublic(){
        $request["lang"] = Input::get("lang","en");
        $request["country_id"] = Input::get("country_id");
//        $request["limit"] = Input::get("limit",10);
//        if (!is_null($request['limit'])) {
//            $rules = [
//                "limit" => "numeric|min:1"];
//            $validator = Validator::make($request, $rules);
//
//            if ($validator->fails()) {
//                $request['limit'] = 10;
//            }
//        }
        $rules = [
            "country_id" => "numeric|min:1"];
        $validator = Validator::make($request, $rules);

        if ($validator->fails()) {
            $this->log->storeLogInfo(array("Cuisine", [
                'status' => '422',
                'message' => ["country_id" => ['The country_id parameter is required']]
            ]));
            return response()->json([
                'status' => '422',
                "message" => ["country_id" => ['The country_id parameter is required']]
            ],422);
        }

        try{
            $cuisines = $this->cuisine->getAllCuisineForPublic($request["country_id"]);
            if(count($cuisines) == 0){
                return response()->json([
                    "status" => "404",
                    "message" => "Empty Cuisines"
                ],404);
            }
            foreach ($cuisines as $cuisine){
                /*
           * Get cuisine Translation based on @param lang
           * if empty record is obtained default en lang is returned
           * */
                $cuisineTranslation = $cuisine->cuisineTranslations()->where('lang_code', $request['lang'])->get();
                if ($cuisineTranslation->count() == 0) {
                    $cuisineTranslation = $cuisine->cuisineTranslations()->where('lang_code', "en")->get();
                    try {
                        if ($cuisineTranslation->count() == 0) {
                            throw new \Exception();
                        }
                    } catch (\Exception $ex) {
                        return response()->json([
                            "status" => "404",
                            "message" => "Default language english not found in database"
                        ], 404);
                    }
                }
                $cuisine["lang_code"] = $cuisineTranslation[0]["lang_code"];
                $cuisine["name"] = $cuisineTranslation[0]["name"];
            }
            return response()->json([
                "status" => "200",
                "data" => $cuisines
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => '500',
                "message" => $ex->getMessage()
            ]);
        }
    }

    /**
     * fetch all the restaurant branches which is non featured and assigned to the cuisine
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNonFeaturedCuisineBranch(Request $request,$id){

        try{
            $this->validate($request,[
                'country_id' => 'required|integer|min:1',
                'city_id' => 'required|integer|min:1'
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => "422",
                'message' => $ex->response->original
            ],422);
        }
        try{
            /**
             * check cuisine exists in database or not
             */
            $get_cuisine_detail = $this->cuisine->getSpecificCuisineForAdmin($id);

            /**
             *  fetch all assigned featured restaurant in cuisine
             */
            $fetch_featured_branch = $get_cuisine_detail->cuisineBranchSorting()->pluck('branch_id')->values()->all();

            /**
             * fetch restaurant branches which belongs to the cuisine and is not featured
             */
            $fetch_cuisine_non_featured_branches = $get_cuisine_detail
                ->branches()
                ->whereNotIn('restaurant_id',$fetch_featured_branch)
                ->join('branch_translation','restaurant_branches.id','=','branch_translation.branch_id')
                ->where('branch_translation.lang_code','en')
                ->where('restaurant_branches.country_id',$request->country_id)
                ->where('restaurant_branches.city_id',$request->city_id)
                ->get(['restaurant_branches.id','branch_translation.name']);

            return response()->json([
                'status' => '200',
                'data' => $fetch_cuisine_non_featured_branches
            ]);

        }

        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => '404',
                "message" => "Cuisine not found"
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => '500',
                "message" => 'Error Viewing Cuisine Branches.'
            ]);
        }
    }

    /**
     * returns all cuisines for admin based on country_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllCuisineForAdmin(){
        $request["lang"] = "en";
        $request["country_id"] = Input::get("country_id");
        $rules = [
            "country_id" => "numeric|min:1"];
        $validator = Validator::make($request, $rules);

        if ($validator->fails()) {
            $this->log->storeLogInfo(array("Cuisine", [
                'status' => '422',
                'message' => ["country_id" => ['The country_id parameter is required']]
            ]));
            return response()->json([
                'status' => '422',
                "message" => ["country_id" => ['The country_id parameter is required']]
            ],422);
        }

        try{
            $cuisines = $this->cuisine->getAllCuisineForAdmin($request["country_id"]);
            foreach ($cuisines as $cuisine){
                /*
           * Get restaurant Translation based on @param en
           *
           * */
                $cuisineTranslation = $cuisine->cuisineTranslations()->where('lang_code', "en")->get();
                try {
                    if ($cuisineTranslation->count() == 0) {
                        throw new \Exception();
                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Default language english not found in database"
                    ], 404);
                }

                $cuisine["lang_code"] = $cuisineTranslation[0]["lang_code"];
                $cuisine["name"] = $cuisineTranslation[0]["name"];
            }
            return Datatables::of($cuisines)->make(true);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => '500',
                "message" => $ex->getMessage()
            ]);
        }
    }

    /**
     * returns all cuisines for admin based on country_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSpecificCuisineForAdmin($id){
        try{
            $cuisine = $this->cuisine->getSpecificCuisineForAdmin($id);
            $cuisineTranslation = $cuisine->cuisineTranslations()->get();
            $cuisine['translation']=$cuisineTranslation;
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => '404',
                "message" => "Cuisine not found"
            ],404);
        }
        return response()->json([
            "status" => '200',
            "data" => $cuisine
        ],200);
    }

    /**
     * create cuisine
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createCuisine(Request $request){
        try{
            $this->validate($request,[
                "status" => "min:0|max:1",
                'country_id' => "required|integer|min:1|max:999",
                "translation" => "required|array"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => '422',
                "message" => $ex->response->original
            ],422);
        }

        $request["status"] = $request->get("status",1);
        $languageCheck = $this->checkLanguageCode($request["country_id"], $request["translation"]);

        switch ($languageCheck) {

            case null: {
                return response()->json([
                    "status" => "422",
                    "message" => "Input Translation does not match with specified country language code"
                ], 422);
                break;

            }

            case is_array($languageCheck) : {

                return response()->json([
                    "status" => $languageCheck["status"],
                    "message" => $languageCheck["message"]
                ], $languageCheck["status"]);
                break;
            }

            default: {
                DB::beginTransaction();
                if (isset($request["icon"])) {
                    $image['avatar'] = $request["icon"];
                    $image["type"] = "icon";
                    $imageUpload = ImageUploader::upload($image);
                    if ($imageUpload["status"] != 200) {
                        return response()->json(
                            $imageUpload["message"]
                            , $imageUpload["status"]);
                    }
                    $request['icon'] = $imageUpload["message"]["data"];
                }
                $cuisine = $this->cuisine->createCuisine($request->all());
                $this->log->storeLogInfo(array("Cuisine", [
                    "status" => "200",
                    "message" => "Cuisine Created Successfully",
                    "data" => $cuisine
                ]));
                break;
            }
        }
        $cuisineId = $cuisine->id;
        foreach ($request['translation'] as $key => $translation) {
            $translation['cuisine_id'] = $cuisineId;
            $translation['lang_code'] = $key;
            $rules = [
                "lang_code" => 'required|alpha',
                "name" => "required|unique:cuisine_translation"
            ];
            $translation['lang_code'] = Slugify::slugify($translation['lang_code'], ['regexp' => '/([^A-Za-z]|-)+/']);
            if (empty($translation['lang_code'])) {
                return response()->json([
                    "status" => "422",
                    "message" => ["lang_code" => "The lang_code may not only contain numbers,special characters."]
                ], 422);
            }
            $validator = Validator::make($translation, $rules);
            if ($validator->fails()) {
                $error = $validator->errors();
                $this->log->storeLogError(array('Cusine Translation', [
                    'status' => '422',
                    "message" => [$key => $error]
                ]));
                return response()->json([
                    'status' => '422',
                    "message" => [$key => $error]
                ], 422);
            }
            /*
             * checking if the same restaurant id have existing lang_code or not
             * */
            try {
                $checkExistingTranslation = $this->cuisineTranslation
                    ->getSpecificCuisineTranslationByCodeAndName($translation["lang_code"], $translation['name']);
                switch ($checkExistingTranslation) {
                    case !null: {
                        throw new \Exception();
                        break;
                    }
                    default: {
                        $createTranslation = $this->cuisineTranslation->createCuisineTranslation($translation);
                        $this->log->storeLogInfo(array("Cuisine Translation", [
                            "status" => "200",
                            "message" => "Cuisine Translation Created Successfully",
                            "data" => $createTranslation
                        ]));
                    }
                }
            } catch (\Exception $ex) {
                $this->log->storeLogError(array("Cuisine Translation", [
                    "status" => "409",
                    "message" => "Cuisine translation for given cuisine already exist"
                ]));
                return response()->json([
                    "status" => "409",
                    "message" => "Cuisine translation for given cuisine already exist"
                ], 409);
            }
        }
        DB::commit();
        $this->log->storeLogInfo(array("Cuisine Created"), [
            "status" => "200",
            "message" => "Cuisine created successfully"
        ]);

        return response()->json([
            "status" => "200",
            "message" => "Cuisine created successfully"
        ]);

    }

    /**
     * update cuisine
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateCuisine($id, Request $request){
        try{
            $this->validate($request,[
                "status" => "min:0|max:1",
                'country_id' => "required|integer|min:1|max:999",
                "translation" => "required|array"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => '422',
                "message" => $ex->response->original
            ],422);
        }

        $request["status"] = $request->get("status",1);
        $languageCheck = $this->checkLanguageCode($request["country_id"], $request["translation"]);

        switch ($languageCheck) {

            case null: {
                return response()->json([
                    "status" => "422",
                    "message" => "Input Translation does not match with specified country language code"
                ], 422);
                break;

            }

            case is_array($languageCheck) : {

                return response()->json([
                    "status" => $languageCheck["status"],
                    "message" => $languageCheck["message"]
                ], $languageCheck["status"]);
                break;
            }

            default: {
                DB::beginTransaction();
                if (isset($request["icon"])) {
                    $image['avatar'] = $request["icon"];
                    $image["type"] = "icon";
                    $imageUpload = ImageUploader::upload($image);
                    if ($imageUpload["status"] != 200) {
                        return response()->json(
                            $imageUpload["message"]
                            , $imageUpload["status"]);
                    }
                    $request['icon'] = $imageUpload["message"]["data"];
                }
                try {
                    $cuisine = $this->cuisine->updateCuisine($id, $request->all());
                    $cuisine->cuisineTranslations()->delete();
                }
                catch (\Exception $ex){
                    return response()->json([
                        "status" => "404",
                        "message" => "Cuisine does not exist"
                    ],404);
                }
                $this->log->storeLogInfo(array("Cuisine", [
                    "status" => "200",
                    "message" => "Cuisine Updated Successfully",
                    "data" => $cuisine
                ]));
                break;
            }
        }
        $cuisineId = $cuisine->id;
        foreach ($request['translation'] as $key => $translation) {
            $translation['cuisine_id'] = $cuisineId;
            $translation['lang_code'] = $key;
            $rules = [
                "lang_code" => 'required|alpha',
                "name" => "required"
            ];
            $translation['lang_code'] = Slugify::slugify($translation['lang_code'], ['regexp' => '/([^A-Za-z]|-)+/']);
            if (empty($translation['lang_code'])) {
                return response()->json([
                    "status" => "422",
                    "message" => ["lang_code" => "The lang_code may not only contain numbers,special characters."]
                ], 422);
            }
            $validator = Validator::make($translation, $rules);
            if ($validator->fails()) {
                $error = $validator->errors();
                $this->log->storeLogError(array('Cusine Translation', [
                    'status' => '422',
                    "message" => [$key => $error]
                ]));
                return response()->json([
                    'status' => '422',
                    "message" => [$key => $error]
                ], 422);
            }
            /*
             * checking if the same cuisine id have existing lang_code or not
             * */
            try {
                $checkExistingTranslation = $this->cuisineTranslation
                    ->getSpecificCuisineTranslationByCodeAndName($translation["lang_code"], $translation['name']);
                switch ($checkExistingTranslation) {
                    case !null: {
                        throw new \Exception();
                        break;
                    }
                    default: {
                        $createTranslation = $this->cuisineTranslation->createCuisineTranslation($translation);
                        $this->log->storeLogInfo(array("Cuisine Translation", [
                            "status" => "200",
                            "message" => "Cuisine Translation Created Successfully",
                            "data" => $createTranslation
                        ]));
                    }
                }
            } catch (\Exception $ex) {
                $this->log->storeLogError(array("Cuisine Translation", [
                    "status" => "409",
                    "message" => "Cuisine translation for given cuisine already exist"
                ]));
                return response()->json([
                    "status" => "409",
                    "message" => "Cuisine translation for given cuisine already exist"
                ], 409);
            }
        }
        DB::commit();
        $this->log->storeLogInfo(array("Cuisine Created"), [
            "status" => "200",
            "message" => "Cuisine updated successfully"
        ]);
        return response()->json([
            "status" => "200",
            "message" => "Cuisine update successfully"
        ]);


    }

    /**
     * delete cuisine based on $id param
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteCuisine($id){
        try{
            $this->cuisine->viewCuisineSpecific($id)->delete();
            return response()->json([
                "status" => "200",
                "message" => "Cuisine deleted successfully"
            ]);
        }
        catch (ModelNotFoundException $exception ){
            return response()->json([
                "status" => "404",
                "message" => "Cuisine does not exist"
            ],404);
        }
    }

    /**
     * assign list of cuisine to restaurant branch
     * if any of cuisines is invalid or doesnot belongs to same country of restaurant error is thrown
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function attachCuisine($id, Request $request){
        try{
            $this->validate($request,[
                "cuisines" => "array|required"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        try {
            $restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($id);
            DB::beginTransaction();
            foreach ($request["cuisines"] as $cuisine) {
                try {
                    $getCuisine = $this->cuisine->viewSpecificCuisineBasedOnStatus($cuisine);
                } catch (ModelNotFoundException $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Cuisine with id $cuisine could not be found or status is disabled"
                    ], 404);
                }
                if ($restaurantBranch->country_id != $getCuisine->country_id) {
                    return response()->json([
                        "status" => "403",
                        "message" => "Cuisine and Restaurant country is different"
                    ], 403);
                }
            }
            try {
                $restaurantBranch->cuisine()->attach($request["cuisines"]);
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "409",
                    "message" => "Cuisine could not be attached to Restaurant Branch due to duplicate entry"
                ], 409);
            }
            DB::commit();
            $this->log->storeLogInfo(array("Cuisine attached to restaurant", [
                "status" => "200",
                "message" => "Cuisines successfully attached to restaurant branch",
                "request" => $request->all()
            ]));
            return response()->json([
                "status" => "200",
                "message" => "Cuisines successfully attached to restaurant branch"
            ]);

        }

        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "Restaurant Branch could not be found"
            ],404);
        }

        catch (\Exception $ex){
            return response()->json([
                "status" => "403",
                "message" => "Cuisine could not be attached to Restaurant Branch"
            ],403);
        }

    }

    /**
     * removes list of cuisines from specified restaurant branch
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function detachCuisine($id, Request $request){
        try{
            $this->validate($request,[
                "cuisines" => "array|required"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        try {
            $restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($id);
            DB::beginTransaction();
            try {
                $restaurantBranch->cuisine()->detach($request["cuisines"]);
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "403",
                    "message" => "Cuisine could not be detached from Restaurant Branch "
                ], 403);
            }
            DB::commit();
            $this->log->storeLogInfo(array("Cuisine detached from restaurant", [
                "status" => "200",
                "message" => "Cuisines successfully detached from restaurant branch",
                "request" => $request->all()
            ]));
            return response()->json([
                "status" => "200",
                "message" => "Cuisines successfully detached from restaurant branch"
            ]);

        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "Restaurant Branch could not be found"
            ],404);
        }
    }

    /**
     * Check if the country lang_code matches with input lang_code in translation array
     *
     * @param $country_id
     * @param $languageTranslations
     * @return bool|void
     */
    private function checkLanguageCode($country_id, $languageTranslations){
        $url=config::get("config.country_url");
        /**
         * Calls location service to get language translation based on $country_id
         * @param $url
         * @param $country_id
         * @return \Illuminate\Http\Response
         */
        $locationResponseFromServiceCommunication=RemoteCall::getSpecific($url.$country_id);
        if($locationResponseFromServiceCommunication["status"]!="200"){
            return $locationResponseFromServiceCommunication;
        }
        //return response()->json($locationResponseFromServiceCommunication["message"]["data"]["language"]);
        foreach ($locationResponseFromServiceCommunication["message"]["data"]["language"] as $language){
            $languages[]=$language['code'];
        }
        $countLanguage=count($languages);
        foreach ($languageTranslations as $key => $translation) {
            $langlist[] = $key;
        }
        /*
         * if input lang_code count is not equal to obtained country lang_code count ,then it would not compare array since count is not same
         * */
        $countLangList=count($langlist);
        if($countLangList!== $countLanguage) return;
        /*
         * compare input languages array with obtained country language array count
         * */
        $countIntersect=count($intersectArray=array_intersect($languages,$langlist));
        if($countLanguage!==$countIntersect){
            return ;
        }
        return true;
    }



}