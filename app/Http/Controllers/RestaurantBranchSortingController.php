<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/13/18
 * Time: 3:09 PM
 */

namespace App\Http\Controllers;


use App\Repo\BranchSortingInterface;
use App\Repo\RestaurantBranchInterface;
use App\RestaurantBranch;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RestaurantBranchSortingController extends Controller
{
    private $restaurant_branch;
    private $branch_sorting;
    private $log;

    public function __construct(BranchSortingInterface $branch_sorting, RestaurantBranchInterface $restaurantBranch, \LogStoreHelper $log)
    {
        $this->branch_sorting = $branch_sorting;
        $this->restaurant_branch = $restaurantBranch;
        $this->log = $log;
    }



    public function getUnassignedRestaurantBranch(Request $request){

    }

    /**
     * view the branch on the sorted order
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function viewRestaurantSorting(Request $request)
    {
        try{
            $this->validate($request,[
                'country_id' => 'required|integer|min:1',
                'city_id' => 'required|integer|min:1',
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        DB::beginTransaction();
        try{
            $branch_info = [];
            /**
             * fetch sorted branches from branch_sorting table in ascending order based on country and city id
             */
            $last_sort_value = 0;
            $fetch_sorted_restaurant_sort = $this->branch_sorting->getSortedBranch($request->country_id,$request->city_id);
            $last_sort_value = collect($fetch_sorted_restaurant_sort)->sortByDesc('sort')->pluck('sort')->first();

            /**
             * fetch all the restaurant which belongs to particular city of the country
             */
            $restaurant_branches = $this->restaurant_branch->getRestaurantsByCity($request->country_id,$request->city_id)->toArray();
            /**
             * pick the ids of the restaurant branches in an array
             */
            $branch_info = collect($restaurant_branches)->pluck('id')->values()->all();
//dd($branch_info);
            /**
             * if the @param $fetch_sorted_restaurant_sort is empty then create the sorting of all the restaurant
             */
            if(count($fetch_sorted_restaurant_sort) === 0){
                $this->createBranchSorting($request->country_id, $request->city_id,$branch_info);
            }
            /**
             * else delete or add branch from sorting
             */
            else{
                /**
                 * pick the ids of the @param $fetched_sorted_branch in an array
                 */
                $fetched_sorted_branch = collect($fetch_sorted_restaurant_sort)->pluck('branch_id')->values()->all();

                /**
                 * if the array count of @param $fetched_sorted_branch is more than count of
                 * @param $restaurant_branches which means some of the branch are unavialable . then on that case delete the inactive branches from the sorting
                 */
                if(count($fetched_sorted_branch) > count($restaurant_branches)){
                    $diff_in_branch = array_diff($fetched_sorted_branch,$branch_info);
                    $this->branch_sorting->bulkDelete($diff_in_branch);
                }
                /**
                 * else create the missing or new restaurant branches on the sorting which sorting value start with the count of sorted branch added by 1
                 */
                else{
//                $sorted_branch_count = count($fetched_sorted_branch);
//                return $restaurant_branches;
                    $diff_in_branch = array_diff($branch_info,$fetched_sorted_branch );
//                return $last_sort_value;
                    $this->createSpecificSortBranches($request->country_id, $request->city_id, $diff_in_branch, $last_sort_value + 1);
                }
                /**
                 * fetch sorted branches from branch_sorting table in ascending order based on country and city id
                 */
//



            }

            $fetch_sorted_restaurant_sort = $this->branch_sorting->getSortedBranch($request->country_id,$request->city_id);
            $branches_ids = collect($fetch_sorted_restaurant_sort)->pluck('branch_id')->values()->all();
            if(\Redis::exists("restaurant-branch-sorting-country-$request->country_id-city-$request->city_id")){
                \Redis::del("restaurant-branch-sorting-country-$request->country_id-city-$request->city_id");
                \Redis::set("restaurant-branch-sorting-country-$request->country_id-city-$request->city_id",serialize($branches_ids));

            }
            else{
                \Redis::set("restaurant-branch-sorting-country-$request->country_id-city-$request->city_id",serialize($branches_ids));
            }

            foreach ($fetch_sorted_restaurant_sort as $sorted_branch){
                $branch_name = $sorted_branch->branch->branchTranslation()->where('lang_code','en')->first();
                if($branch_name) $sorted_branch->name = $branch_name->name;
                else $sorted_branch->name = '';
            }
            DB::commit();
            return response()->json([
                'status' => '200',
                'data' => $fetch_sorted_restaurant_sort
            ]);
        }
        catch (\Exception $ex){
            $this->log->storeLogError(["viewing restaurant branch sorting error",[
                "data" => $request->all(),
                "message" => $ex->getMessage()
            ]]);
            return response()->json([
                "status" => "500",
                "message" => "Error viewing restaurant branches sorting."
            ],500);
        }
    }

    public function createRestaurantSorting(Request $request){
        try{
            $this->validate($request,[
                'country_id' => 'required|integer|min:1',
                'city_id' => 'required|integer|min:1',
                "branch_info" => "required|array",
                "branch_info.*.id" => "required|integer|min:1",
                "branch_info.*.branch_id" => "required|integer|min:1"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }

        DB::beginTransaction();
        try{
            $branches_ids = [];
            foreach ($request['branch_info'] as $branch_info){
                $branches_ids[] = $branch_info['branch_id'];
            }
            $request->merge([
                'branch_info' => array_values(array_unique($branches_ids))
            ]);
            $fetch_restaurant_branch = $this->restaurant_branch->checkRestaurantBelongsToCity($request->country_id, $request->city_id,$request->branch_info);

            /**
             * check if the request restaurant count match with the restaurants fetched from database
             */

            if(count($request->branch_info) !== count($fetch_restaurant_branch)){
                return response()->json([
                    'status' => '403',
                    'message' => 'One of the restaurant does not belongs to the provided city.'
                ],403);
            }
            $this->branch_sorting->bulkDeleteByCityId($request->country_id,$request->city_id);
            $this->createBranchSorting($request->country_id, $request->city_id,$request->branch_info);

            DB::commit();
            $this->log->storeLogInfo(["creating restaurant branch sorting",[
                "data" => $request->all()
            ]]);

            return response()->json([
                "status" => "200",
                "message" => "Branch sorting created successfully."
            ]);

        }
        catch (\Exception $ex){
            $this->log->storeLogError(["creating restaurant branch sorting error",[
                "data" => $request->all(),
                "message" => $ex->getMessage()
            ]]);
            return response()->json([
                "status" => "500",
                "message" => "Error attaching restaurant branches sorting."
            ],500);
        }
    }


    /**
     * create sorting of restauranant based on the index value
     * @param $country_id
     * @param $city_id
     * @param $branches_ids
     * @return bool
     */
    private function createBranchSorting($country_id,$city_id,$branches_ids){
        $current_date = Carbon::now('utc')->format('Y-m-d H:i:s');

        $request_branches = [];
//dd('here');
        foreach ($branches_ids as $key => $branch){
            $request_branches[] =[
                'country_id' => $country_id,
                'city_id' => $city_id,
                'branch_id' => $branch,
                'sort' => $key + 1,
                'created_at' => $current_date,
                'updated_at' => $current_date
            ];
        }

//        return $request_branches;
        /**
         * bulk insert branches
         */
        $this->branch_sorting->insertSorting($request_branches);

        if(\Redis::exists("restaurant-branch-sorting-country-$country_id-city-$city_id")){
            \Redis::del("restaurant-branch-sorting-country-$country_id-city-$city_id");
            \Redis::set("restaurant-branch-sorting-country-$country_id-city-$city_id",serialize($branches_ids));

        }
        else{
            \Redis::set("restaurant-branch-sorting-country-$country_id-city-$city_id",serialize($branches_ids));
        }

        return true;
    }

    /**
     * create sorting of restaurant branches based starting with the sort value
     * @param $country_id
     * @param $city_id
     * @param $branches_ids
     * @param int $sort
     * @return bool
     */
    private function createSpecificSortBranches($country_id,$city_id,$branches_ids,$sort = 1){
        $current_date = Carbon::now();

        $request_branches = [];

        foreach ($branches_ids as $key => $branch){

            $request_branches[] =[
                'country_id' => $country_id,
                'city_id' => $city_id,
                'branch_id' => $branch,
                'sort' => $sort,
                'created_at' => $current_date,
                'updated_at' => $current_date
            ];
            $sort++;
        }
        /**
         * bulk insert branches
         */
        $this->branch_sorting->insertSorting($request_branches);
        return true;
    }
}