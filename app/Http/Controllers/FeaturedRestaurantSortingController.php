<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/7/18
 * Time: 11:35 AM
 */

namespace App\Http\Controllers;


use App\Repo\FeaturedRestaurantSortingInterface;
use App\Repo\RestaurantBranchInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Repo\RestaurantBranchTranslationInterface;

class FeaturedRestaurantSortingController extends Controller
{

    protected $featuredRestaurantSorting;
    protected $restaurant_branch;
    protected $log;
    protected $branchTranslation;

    public function __construct(FeaturedRestaurantSortingInterface $featuredRestaurantSorting, RestaurantBranchInterface $restaurantBranch, RestaurantBranchTranslationInterface $branchTranslation, \LogStoreHelper $log)
    {

        $this->featuredRestaurantSorting = $featuredRestaurantSorting;
        $this->restaurant_branch = $restaurantBranch;
        $this->branchTranslation = $branchTranslation;
        $this->log = $log;
    }

    /**
     * create new featured restaurant to cuisine based on the sort value
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */


    /**
     * create restaurant branch sorting .This will be helpful for displaying the restaurant branches in top or button based on the sort value
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addBranchInSorting(Request $request)
    {
        try {
            $this->validate($request, [
                'country_id' => 'required|integer',
                'city_id' => 'required|integer',
                'branch_id' => 'required|integer',
                'sort' => 'required|integer'
            ]);

        } catch (\exception $ex) {
            $this->log->storeLogError([
                'error creating ', [
                    'data' => $request->all(),
                    'message' => $ex->getMessage()
                ]
            ]);
            return response()->json([
                'status' => '204',
                'message' => $ex->response->original
            ]);
        }
        try {

            DB::beginTransaction();
            $this->featuredRestaurantSorting->createSorting($request->all());
            DB::commit();
            $this->log->storeLogInfo(['feature restaurant sorting success creating',
                [
                    'status' => '200',
                    'message' => 'The restaurant branch is added successfully'
                ]]);
            return response()->json([
                'status' => '200',
                'message' => 'The restaurant branch is added successfully'
            ], 200);
        } catch (\Exception $ex) {
            $this->log->storeLogError([
                'feature restaurant sorting error creating', [
                    'data' => $request->all(),
                    'message' => $ex->getMessage(),
                ]
            ]);

            return response()->json([
                'status' => '500',
                'message' => 'something went wrong'
            ]);
        }

    }

    public function createBranchSortingbyAdmin(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->validate($request, [
                'country_id' => 'required|integer|min:1',
                'city_id' => 'required|integer|min:1',
                "branch_info" => "required|array",
                "branch_info.*.id" => "required|integer|min:1",
                "branch_info.*.branch_id" => "required|integer|min:1"
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        try {
            $branches_ids = [];
            foreach ($request['branch_info'] as $branch_info) {
                $branches_ids[] = $branch_info['branch_id'];
            }
            $request->merge([
                'branch_info' => array_values(array_unique($branches_ids))
            ]);
            $fetch_restaurant_branch = $this->restaurant_branch->checkRestaurantBelongsToCity($request->country_id, $request->city_id, $request->branch_info);

            /**
             * check if the request restaurant count match with the restaurants fetched from database
             */

            if (count($request->branch_info) !== count($fetch_restaurant_branch)) {
                return response()->json([
                    'status' => '403',
                    'message' => 'One of the restaurant does not belongs to the provided city.'
                ], 403);
            }
            $this->featuredRestaurantSorting->bulkDeleteByCityId($request->country_id, $request->city_id);
            $this->createBranchSorting($request->country_id, $request->city_id, $request->branch_info);
            DB::commit();


            return response()->json([
                "status" => "200",
                "message" => "Feature Restaurant sorting updated successfully."
            ]);
        } catch (ModelNotFoundException $ex) {
            $this->log->storeLogError(["Cuisine could not be found", [
                "data" => $request->all(),
                "message" => $ex->getMessage()
            ]]);
            return response()->json([
                "status" => "404",
                "message" => "Could not found requested cuisine."
            ], 404);
        } catch (\Exception $ex) {
            $this->log->storeLogError(["creating branch sorting error", [
                "data" => $request->all(),
                "message" => $ex->getMessage()
            ]]);
            return response()->json([
                "status" => "500",
                "message" => "Error attaching branches sorting."
            ], 500);
        }
    }

    private function createBranchSorting($country_id, $city_id, $branches_ids)
    {
        $current_date = Carbon::now('utc')->format('Y-m-d H:i:s');
        $request_branches = [];

        foreach ($branches_ids as $key => $branch) {
            $request_branches[] = [
                'country_id' => $country_id,
                'city_id' => $city_id,
                'branch_id' => $branch,
                'sort' => $key + 1,
                'created_at' => $current_date,
                'updated_at' => $current_date
            ];
        }

//        return $request_branches;
        /**
         * bulk insert branches
         */
        $this->featuredRestaurantSorting->insertSorting($request_branches);
        return true;

    }

    public function viewFeaturedRestaurantSorting(Request $request)
    {
        try {
            $this->validate($request, [
                'country_id' => 'required|integer|min:1',
                'city_id' => 'required|integer|min:1',
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }

        try {
            DB::beginTransaction();

//            $branch_info = [];
            /**
             * fetch sorted branches from branch_sorting table in ascending order based on country and city id
             */
//            $last_sort_value = 0;
            $fetch_sorted_restaurant_sort['sorted_branch'] = $this->featuredRestaurantSorting->getSortedBranch($request->country_id, $request->city_id);
//            $last_sort_value = collect($fetch_sorted_restaurant_sort)->sortByDesc('sort')->pluck('sort')->first();

            /**
             * fetch all the restaurant which belongs to particular city of the country
             */


            /**
             * if the @param $fetch_sorted_restaurant_sort is empty then create the sorting of all the restaurant
             */

            foreach ($fetch_sorted_restaurant_sort['sorted_branch'] as $sorted_branches) {
                $branch_name = $this->branchTranslation->getSpecificRestaurantBranchTranslation($sorted_branches['branch_id']);
                if ($branch_name != null) {
                    $sorted_branches->name = $branch_name['name'];
                }
            }
            $fetch_sorted_restaurant_sort['restaurant_branches'] = $this->restaurant_branch->getRestaurantsByCityforBranchSorting($request->country_id, $request->city_id);
            foreach ($fetch_sorted_restaurant_sort['restaurant_branches'] as $sorted_branch) {
                $branch_name = $sorted_branch->branchTranslation()->where('lang_code', 'en')->first();
                if ($branch_name) $sorted_branch->name = $branch_name->name;
                else $sorted_branch->name = '';
            }
            DB::commit();
            return response()->json([
                'status' => '200',
                'data' => $fetch_sorted_restaurant_sort
            ]);
        } catch (\Exception $ex) {
            $this->log->storeLogError(["viewing feature restaurant branch sorting error", [
                "data" => $request->all(),
                "message" => $ex->getMessage()
            ]]);
            return response()->json([
                "status" => "500",
                "message" => "Error viewing feature restaurant branches sorting."
            ], 500);
        }
    }

    public function deleteFeatureRestaurantFromSorting(Request $request)
    {
        try{
            $this->validate($request,[
                'branch_id' => 'required|array'
            ]);
        }
        catch (\Exception $ex){
            $this->log->storeLogError(["Feature restaurant sorting list",[
                    'data' => $request->all(),
                    'message' => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        try {
            DB::beginTransaction();
            $this->featuredRestaurantSorting->bulkDelete($request->all());
            DB::commit();

            return response()->json([
                'status' => '200',
                'message' => 'Feature restaurant removed from list successfully'
            ]);

        } catch (\Exception $ex) {
            $this->log->storeLogError(["Restaurants could not be found in the sorting list", [
                "data" => $request->all(),
                "message" => $ex->getMessage()
            ]]);

            return response()->json([
                'status' => '500',
                'message' => 'Something went wrong'
            ]);

        }
    }

}