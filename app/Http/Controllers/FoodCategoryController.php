<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/8/17
 * Time: 3:24 PM
 */

namespace App\Http\Controllers;


use App\Repo\FoodInterface;
use App\Repo\CategoryInterface;
use Illuminate\Http\Request;
use App\Repo\FoodCategoryInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use AuthChecker;
use LogStoreHelper;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Cocur\Slugify\Slugify;
class FoodCategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $foodCategory;
    private $food;
    private $category;
    private $logStoreHelper;
    private $slugify;

    /**
     * FoodCategoryController constructor.
     * @param FoodCategoryInterface $foodCategory
     * @param CategoryInterface $category
     * @param FoodInterface $food
     */

    public function __construct(FoodCategoryInterface $foodCategory, CategoryInterface $category, FoodInterface $food, LogStoreHelper $logStoreHelper, Slugify $slugify)
    {
        $this->foodCategory = $foodCategory;
        $this->category = $category;
        $this->food = $food;
        $this->logStoreHelper = $logStoreHelper;
        $this->slugify = $slugify;
    }

    /**
     * Display categories related to food of a specific restaurant.
     * If 'lang' parameter is not supplied,then default language is set to 'en'.
     * @param $restaurantId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($restaurantId, $id)
    {
        $lang = Input::get("lang", "en");

        try {
            $food = $this->food->getSpecificFoodByRestaurant($id, $restaurantId);
            /**
             * Check count of result. If result count is 0, throw exception. Else check if the status of result is 0.
             * If user is not logged in or unauthorized, throw exception if status is zero.
             * If the user is logged in and authorized personnel, continue.
             */


            if (count($food) == 0) {
                throw new \Exception();
            } elseif ($food['status'] == 0) {
                if (!AuthChecker::check($restaurantId))
                    throw new \Exception();
            }

            try {

                /**
                 * If user is logged in and authorized personnel, get all the categories related to specific food.
                 * S/He can view categories of both status i.e. 1 or 0.
                 * S/He can view  categories of only 1 or 0 using filter.
                 * If user is not logged in or unauthorized personnel, S/He can only view active categories (status = 1)
                 */

                if (AuthChecker::check($restaurantId)) {
                    if (!is_null(Input::get('status'))) {
                        $foodCategory = $food->category()->where('status', Input::get('status'))->get();
                    } else {
                        $foodCategory = $food->category()->get();
                    }

                } else {
                    $foodCategory = $food->category()->where('status', 1)->get();
                }

                /**
                 * Get translation of each category according to the language desired through 'lang' parameter.
                 * Store translation in $category array along with other deatils.
                 * If translation is not available in requested language, default language english is used.
                 * If translation is not avaliable in default language, throw exception and abort.
                 */

                foreach ($foodCategory as $category) {
                    $translation = $category->categoryTranslation()->where('lang', $lang)->get();
                    try {
                        if ($translation->count() == 0) {
                            if ($lang == 'en') {
                                throw new \Exception();
                            } else {
                                $translation = $category->categoryTranslation()->where('lang', 'en')->get();
                                if ($translation->count() == 0) {
                                    throw new \Exception();
                                }
                            }

                        }
                    } catch (\Exception $ex) {
                        return response()->json([
                            "status" => "404",
                            "message" => "Default language english not found in database"
                        ], 404);
                    }

                    $category['lang'] = $translation[0]['lang'];
                    $category['name'] = $translation[0]['name'];

                }
                if (!$foodCategory->first()) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    "message" => 'Empty Record'
                ], 404);
            }
            return response()->json([
                'status' => '200',
                'data' => $foodCategory
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '404',
                'message' => 'Food Could not found'
            ], 404);
        }
    }

    /**
     * Attach categories to food of a specific restaurant
     * Only authorized user can attach category to food
     * @param $restaurantId
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function store($restaurantId, $id, Request $request)
    {
        /**
         * Check if logged in user has authority to attach data.
         * If 'true', continue. Else abort with exception message.
         */

        if (AuthChecker::check($restaurantId)) {
            try {
                $food = $this->food->getSpecificFoodByRestaurant($id, $restaurantId);


                if (count($food) == 0 || $food['status'] == 0) {
                    throw new \Exception();

                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    'message' => "Food of id: " . $id . " may be removed or disabled"
                ], 404);
            }
            try {
                $this->validate($request, [//validating the request
                    "category_id" => "required|array",

                ]);

            } catch (\Exception $e) {
                /*
                 *creates log for storing error
                 * */

                return response()->json([
                    "status" => "422",
                    "message" => $e->response->original
                ], 422);


            }
            /**
             * Start DB transaction to ensure the operation is reversed in case not successfully committed.
             *
             */
            DB::beginTransaction();
            try {
                /**
                 * Fetch all the categories related to the restaurant branch.
                 * If the categories in the request does not match with the fetched categories, throw error 404.
                 * Else continue.
                 */

                $categoriesRelatedToBranch = $this->category->getCategoriesIdRelatedToResBranch($restaurantId);
                if (count(array_diff($request['category_id'], $categoriesRelatedToBranch)) != 0) {

                    throw new \Exception();

                }

                foreach ($request['category_id'] as $categoryId) {
                    try {
                        $category = $this->category->getSpecificCategory($categoryId);
                        if (count($category) == 0 || $category['status'] == 0) {
                            throw new \Exception();
                        }
                    } catch (\Exception $ex) {
                        return response()->json([
                            'status' => '404',
                            'message' => "Category of id:" . $categoryId . " may be removed or disabled"
                        ], 404);
                    }
                    $request = $categoryId;
                    try {
                        $foodCategory = $this->foodCategory->attachCategoryToFood($id, $request);
                    } catch (\Exception $ex) {
                        return response()->json([
                            'status' => '409',
                            'message' => "Duplicate Entry for food_id " . $id . " and category_id " . $categoryId
                        ], 409);

                    }

                }
            }catch (\Exception $ex)
            {
                return response()->json([
                    'status' => "404",
                    "message" => "Given categories are not available"
                ], 401);
            }
            /**
             * If operation is successfully executed, commit it, else revert whole operation.
             */
            DB::commit();

            $this->logStoreHelper->storeLogInfo([
                "Food-Category Relation", [
                    "status" => "200",
                    "message" => "Categories of id are attached to food id: " . $id
                ]
            ]);

            return response()->json([
                'status' => '200',
                'message' => 'Category attached successfully to food'
            ], 200);
        } else {
            return response()->json([
                'status' => "401",
                "message" => "Only Authorized personnel can attach category to food"
            ], 401);
        }

    }

    /**
     * Detach categories from food of a specific restaurant
     * @param $restaurantId
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function destroy($restaurantId, $id, Request $request)
    {
        /**
         * Check if logged in user has authority to detach data.
         * If 'true', continue. Else abort with exception message.
         */

        if (AuthChecker::check($restaurantId)) {
            try {
                $food = $this->food->getSpecificFoodByRestaurant($id, $restaurantId);

                /**
                 * Check if food is present and active.
                 * If food is not present or inactive, throw exception, else continue.
                 */

                if (count($food) == 0 || $food['status'] == 0) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    'message' => "Food of id: " . $id . "may be removed or disabled"
                ], 404);
            }
            /**
             * Validate the request.
             * Request for field 'category_id' is required and should be an array
             */

            try {
                $this->validate($request, [//validating the request
                    "category_id" => "required|array",

                ]);

            } catch (\Exception $e) {
                /*
                 *creates log for storing error
                 * */

                return response()->json([
                    "status" => "422",
                    "message" => $e->response->original
                ]);


            }
            /**
             * Start DB transaction to ensure the operation is reversed in case not successfully committed.
             *
             */
            DB::beginTransaction();

            foreach ($request['category_id'] as $categoryId) {
                try {
                    $category = $this->category->getSpecificCategory($categoryId);
                    if (count($category) == 0 || $category['status'] == 0) {
                        throw new \Exception();
                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => '404',
                        "message" => "Category of id:" . $categoryId . " could not be found"
                    ], 404);
                }
                $request = $categoryId;

                $foodCategory = $this->foodCategory->detachCategoryFromFood($id, $request);


            }
            /**
             * If operation is successfully executed, commit it, else revert whole operation.
             */
            DB::commit();

            $this->logStoreHelper->storeLogInfo([
                "Food-Category Relation", [
                    "status" => "200",
                    "message" => "Categories of detached from food id: " . $id
                ]
            ]);
            return response()->json([
                "status" => '200',
                "message" => 'Category detached successfully from food'
            ], 200);
        } else {
            return response()->json([
                "status" => "401",
                "message" => "Only Authorized personnel can detach category from food"
            ], 401);
        }

    }

    /**
     * Display food related to given category of a specific restaurant.
     * If 'lang' parameter is not supplied,then default language is set to 'en'.
     * @param $restaurantId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function getFoodByCategory($restaurantId, $id)
    {
        $lang = Input::get("lang", "en");

        try {

            $category = $this->category->getSpecificCategory($id);
            /**
             * Check count of result. If result count is 0, throw exception. Else check if the status of result is 0.
             * If user is not logged in or unauthorized, throw exception if status is 0.
             * If the user is logged in and authorized personnel, continue.
             */
            if (count($category) == 0) {
                throw new \Exception();
            } elseif ($category['status'] == 0) {
                if (!AuthChecker::check($restaurantId))
                    throw new \Exception();
            }
            /**
             * If user is logged in and authorized personnel, get all the food related to specific category.
             * S/He can veiw food of both status i.e. 1 or 0.
             * S/He can view food of only 1 or 0 using filter.
             * If user is not logged in or unauthorized personnel, S/He can only view active food (status = 1)
             */

            try {
                if (AuthChecker::check($restaurantId)) {
                    if (!is_null(Input::get('status'))) {
                        $foodCategory = $category->food()->where('restaurant_id', $restaurantId)->where('status', Input::get('status'))->get();
                    } else {
                        $foodCategory = $category->food()->where('restaurant_id', $restaurantId)->get();
                    }

                } else {
                    $foodCategory = $category->food()->where('restaurant_id', $restaurantId)->where('status', 1)->get();
                }

                /**
                 * Get translation of each food according to the language desired through 'lang' parameter.
                 * Store translation in $food array along with other deatils.
                 * If translation is not available in requested language, default language english is used.
                 * If translation is not avaliable in default language, throw exception and abort.
                 */
                foreach ($foodCategory as $food) {
                    $translation = $food->foodTranslation()->where('lang', $lang)->get();
                    try {
                        if ($translation->count() == 0) {
                            if ($lang == 'en') {
                                throw new \Exception();
                            } else {
                                $translation = $food->foodTranslation()->where('lang', 'en')->get();
                                if ($translation->count() == 0) {
                                    throw new \Exception();
                                }
                            }

                        }
                    } catch (\Exception $ex) {
                        return response()->json([
                            "status" => "404",
                            "message" => "Default language english not found in database"
                        ], 404);
                    }

                    $food['lang'] = $translation[0]['lang'];
                    $food['name'] = $translation[0]['name'];
                    $food['description'] = $translation[0]['description'];
                    $food['ingredients'] = $translation[0]['ingredients'];
                    if(!empty($food['gallery']))
                    {
                        $food['gallery'] = unserialize($food->gallery);
                    }

                }

                if (!$foodCategory->first()) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Empty Record"
                ], 404);
            }
            return response()->json([
                "status" => "200",
                "data" => $foodCategory
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "404",
                "message" => "Category Could not found"
            ], 404);
        }

    }


    public function getFoodsCategory(Request $request){
        try{
            $this->validate($request,[
                "food_ids" => "required|array"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        try{
            $this->logStoreHelper->storeLogInfo(["food id from order",[
                "data" => $request->all(),
            ]]);
             $requestFoodsId = array_values(array_unique($request->food_ids));
            $foodDetailListDetails = $this->food->getFoodByFoodIds($requestFoodsId);//gets the food infor from database,it is possible that the passed response can be be equal to the id passed

            $foodIdsAfterDatabase = $foodDetailListDetails->pluck("id")->values()->all();//
            $this->logStoreHelper->storeLogInfo(["food id from restaurant database",[
                "data" => $foodIdsAfterDatabase,
            ]]);
            $foodIdNotObtainedFromDatabase = array_values(array_diff($requestFoodsId,$foodIdsAfterDatabase));//list the foood ids which cannot be obtained from database
            $array = [];
            foreach ($foodIdNotObtainedFromDatabase as $excludedFood){
                $array[] = ["id" =>$excludedFood,"name" => "miscellaneous"];
            }

            foreach ($foodDetailListDetails as $foodDetailListDetail){
                $category = $foodDetailListDetail->category()->first();
                if(!$category) {
                    $foodDetailListDetail["name"] = "miscellaneous";
                    continue;
                }
                $categoryName = $category->categoryTranslation()->where("lang","en")->pluck("name")->first();
                if(empty($categoryName) || !$categoryName){
                    $foodDetailListDetail["name"] = "miscellaneous";
                    continue;
                }
                $foodDetailListDetail["name"] = $this->slugify->slugify($categoryName);
            }
            $foodWithCategory =  collect($foodDetailListDetails)->merge($array)->all();
            return response()->json([
                "status" => '200',
                "data" => $foodWithCategory
            ]);
        }
        catch (\Exception $ex){
            $this->logStoreHelper->storeLogError(["Error listing food categories",[
                "data" => $request->all(),
                "message" => $ex->getMessage()
            ]]);
            return response()->json([
                "status" => "500",
                "message" => "Error viewing food categories "
            ],500);
        }
    }

}
