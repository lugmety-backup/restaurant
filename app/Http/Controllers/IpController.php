<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/24/18
 * Time: 3:38 PM
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class IpController extends Controller
{
    public function getClientIp(Request $request){
        return $_SERVER["HTTP_X_REAL_IP"] ?? $_SERVER["HTTP_X_FORWARDED_FOR"] ?? $_SERVER["HTTP_CLIENT_IP"] ?? $_SERVER["REMOTE_ADDR"] ?? '192.168.0.1';
    }
}