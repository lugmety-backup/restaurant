<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/28/2017
 * Time: 12:38 PM
 */

namespace App\Http\Controllers;

use App\Events\EmailEvent;
use App\Http\RoleCheckerFacade;
use App\Repo\RestaurantUsersInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Repo\RestaurantInterface;
use App\Repo\RestaurantTranslationInterface;
use Illuminate\Support\Facades\Input;
use RoleChecker;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use LogStoreHelper;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Validator;
use Slugify;
use RemoteCall;
use Yajra\Datatables\Facades\Datatables;
use ImageUploader;

/**
 * Class RestaurantController
 * @package App\Http\Controllers
 */
class RestaurantController extends Controller
{
    /**
     * @var RestaurantInterface
     */
    protected $restaurant;
    /**
     * @var RestaurantTranslationInterface
     */
    protected $restaurant_translation;
    /**
     * @var RoleChecker
     */
    protected $checker;
    /**
     * @var LogStoreHelper
     */
    protected $logStoreHelper;
    /**
     * @var Slugify
     */
    protected $slugify;
    /**
     * @var
     */
    protected $restaurantUser;
    /**
     * @var
     */
    protected $oauthBaseUrl;

    /**
     * RestaurantController constructor.
     * @param RestaurantInterface $restaurant
     * @param RoleChecker $checker
     * @param RestaurantTranslationInterface $restaurant_translation
     * @param Slugify $slugify
     * @param LogStoreHelper $logStoreHelper
     */
    public function __construct(RestaurantInterface $restaurant,
                                RoleChecker $checker,
                                RestaurantTranslationInterface $restaurant_translation,
                                Slugify $slugify,
                                LogStoreHelper $logStoreHelper)
    {
        $this->restaurant = $restaurant;
        $this->restaurant_translation = $restaurant_translation;
        $this->checker = $checker;
        $this->logStoreHelper = $logStoreHelper;
        $this->slugify = $slugify;
        $this->oauthBaseUrl = Config::get('config.oauth_base_url');
    }

    /**
     * Display a listing of the resource for admin .
     *
     * @return \Illuminate\Http\Response
     */
    public function adminIndex()
    {
        /*
         * get all restaurant
         * */
        $request['country_id'] = Input::get("country_id");
        $request['lang'] = "en";
        /*
         *  validation of country_id
         * */
//        if (!is_null($request['country_id'])) {
        $rules = [
            "country_id" => "required|integer|min:1"];
        $validator = Validator::make($request, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => '422',
                "message" => $validator->errors()
            ], 422);
        }
//
        /**
         * if the key has role and the user is admin or super-admin
         * */
        if (RoleChecker::get('roles') && (RoleChecker::hasRole('admin') || RoleChecker::hasRole('super-admin'))) {

            try {
                $restaurants = $this->restaurant->getAllRestaurantForDatatableBasedOnCountry($request['country_id']);
                if (!$restaurants->first()) {
                    throw  new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Empty Restaurant  for requested parameter"
                ], 404);
            }
            foreach ($restaurants as $key => $restaurant) {
                /*
             * Get restaurant Translation based on @param lang
             * if empty record is obtained default en lang is returned
             * */
                $restaurantTranslation = $restaurant->restaurantTranslation()->where('lang_code', $request['lang'])->get();
                if ($restaurantTranslation->count() == 0) {
                    $restaurantTranslation = $restaurant->restaurantTranslation()->where('lang_code', "en")->get();
                    try {
                        if ($restaurantTranslation->count() == 0) {
                            throw new \Exception();
                        }
                    } catch (\Exception $ex) {
                        return response()->json([
                            "status" => "404",
                            "message" => "Default language english not found in database"
                        ], 404);
                    }
                }
                /*
                 * attach name and lang_code in $restaurant variable
                 * */

                $restaurant['name'] = $restaurantTranslation[0]['name'];
                $restaurant['lang_code'] = $restaurantTranslation[0]['lang_code'];
            }
            return \Datatables::of($restaurants)->make(true);
        }
//      elseif (RoleChecker::hasRole("restaurant-admin") || (RoleChecker::get('parentRole') == "restaurant-admin")) {
//            $restaurantInfo = RoleChecker::getRestaurantInfo();
//
//            if (!is_null($restaurantInfo) && ($restaurantInfo["is_restaurant_admin"] == true)) {
//                $restaurant = $this->restaurant->getSpecificRestaurant($restaurantInfo["restaurant_id"], $status = 1);
//
//                try {
//                    if (!$restaurant->first()) {
//                        throw  new \Exception();
//                    }
//                } catch (\Exception $ex) {
//                    return response()->json([
//                        "status" => "404",
//                        "message" => "Restaurant could not be found"
//                    ], 404);
//                }
//
//
//            /*
//         * Get restaurant Translation based on @param lang
//         * if empty record is obtained default en lang is returned
//         * */
//            $restaurantTranslation = $restaurant->restaurantTranslation()->where('lang_code', "en")->get();;
//
//            if ($restaurantTranslation->count() == 0) {
//                $restaurantTranslation = $restaurant->restaurantTranslation()->where('lang_code', "en")->get();
//
//                try {
//                    if ($restaurantTranslation->count() == 0) {
//                        throw new \Exception();
//                    }
//                } catch (\Exception $ex) {
//                    return response()->json([
//                        "status" => "404",
//                        "message" => "Default language english not found in database"
//                    ], 404);
//                }
//            }
//
//            /*
//             * attach name and lang_code in $restaurant variable
//             * */
//            $restaurant['name'] = $restaurantTranslation[0]['name'];
//            $restaurant['lang_code'] = $restaurantTranslation[0]['lang_code'];
//
//            return Datatables::of($restaurant)->make(true);
//            }
        else {
            return response()->json([
                'status' => "403",
                "message" => "Only Admin or Super Admin  can View Restaurant "
            ], 403);
        }

    }


    /**
     * returns list of main restaurant based on country id
     * @param $countryId
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($countryId)
    {
        /*
         * get all restaurant
         * */
        $request['status'] = 1;
        $request['lang'] = Input::get("lang", "en");
        $request['limit'] = Input::get("limit", 10);
        $request['country_id'] = $countryId;
        /*
         *  validation of country_id
         * */
//        if (!is_null($request['country_id'])) {
        $rules = [
            "country_id" => "required|integer|min:1"];
        $validator = Validator::make($request, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => '422',
                "message" => $validator->errors()
            ], 422);
        }
//        }
        /*
         * validation of limit param if limit param is not null
         * */
        if (!is_null($request['limit'])) {
            $rules = [
                "limit" => "sometimes|numeric|min:1",
            ];
            $validator = Validator::make($request, $rules);
            if ($validator->fails()) {
                $request['limit'] = 10;
            }
        }
        try {

           $restaurants = $this->restaurant->getAllRestaurantByCountryId($request['status'], $request['limit'], $request["country_id"]);
            if (!$restaurants->first()) {
                throw  new \Exception();
            }

        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Empty Restaurant  for requested parameter"
            ],404);
        }
        foreach ($restaurants as $key => $restaurant) {
            /*
         * Get restaurant Translation based on @param lang
         * if empty record is obtained default en lang is returned
         * */
            $restaurantTranslation = $restaurant->restaurantTranslation()->where('lang_code', $request['lang'])->get();
            $branchRestaurants = $restaurant->restaurantBranch()->get();

            if ($restaurantTranslation->count() == 0) {
                $restaurantTranslation = $restaurant->restaurantTranslation()->where('lang_code', "en")->get();
                try {
                    if ($restaurantTranslation->count() == 0) {
                        throw new \Exception();
                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Default language english not found in database"
                    ], 404);
                }
            }
            /**
             * get review of the restaurant
             */
            $count = 0;
            $ratings =0;
            $reviewCount =0;
            foreach ($branchRestaurants as $branch) {
                $reviews = $branch->restaurantReviews();
                $restaurantBranch['average_rating'] = $reviews->avg("rating");
                $reviewCount += $reviews->count();
                if($restaurantBranch["average_rating"] == null){
                    $restaurantBranch["average_rating"] =0;
                }
                else {
                    $count++;
                    $ratings += $restaurantBranch["average_rating"];
                }
                $restaurantBranch['review_count'] = $reviews->count();
            }
            /*
             * attach name and lang_code in $restaurant variable
             * */
            if($count == 0){
                $overallRating =0;
            }
            else {
                $overallRating = $ratings / $count;
            }
            $restaurant['name'] = $restaurantTranslation[0]['name'];
            $restaurant['lang_code'] = $restaurantTranslation[0]['lang_code'];
            $restaurant["rating"] = $overallRating;
            $restaurant["review_count"] = $reviewCount;
        }
        return response()->json([
            'status' => "200",
            "data" => $restaurants->appends(["lang" => $request['lang']])
        ], 200);


    }


    /**
     * Store a newly created resource in table name .
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
         * function for storing restaurant
         * translation
         *  and restaurant*/
        try {
            $this->validate($request, [//validating the request
                "address" => "required",
                "phone_no" => "required|min:7",
                "mobile_no" => "required|min:7",
                "email_address" => "required|email|unique:restaurants",
                "website" => "required|url",
                "contact_person" => "required",
                "commission_rate" => 'numeric|between:0,99.999',
                "translation" => "required|array",
                "status" => "integer|min:0|max:1",
                "country_id" => "required|integer",
                "user" => "required|array"
            ]);
        } catch (\Exception $e) {
            /*
             *creates log for storing error
             * */
            $this->logStoreHelper->storeLogError(array("Error in Validation", [
                "status" => "422",
                "message" => $e->response->original
            ]));
            return response()->json([
                "status" => "422",
                "message" => $e->response->original
            ], 422);
        }
        try {
            $message =[
                "domain_validation" => "The mail server for domain does not exist."
            ];
            $rules = [
                'user.first_name' => 'required|max:50',
                'user.last_name' => 'required|max:50',
                'user.email' => 'required|email|domain_validation|max:255',
                'user.password' => 'required|max:255',
                'user.gender' => 'required|in:male,female',
                'user.mobile' => 'required|min:7',
                "user.birthday" => "date_format:Y-m-d",
                "user.status" => "integer|min:0|max:1"
            ];

            $validator = $this->validate($request, $rules,$message);
        } catch (\Exception $ex) {
            $this->logStoreHelper->storeLogError(array("Error in Validation in user index", [
                "status" => "422",
                "message" => $ex->response->original
            ]));

            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        /*
         * RoleChecker facade to check if roles key exist or not
         * and check admin or super-admin role exist or not
         * this mean only admin or super-admin can create restaurant
         * */
        DB::beginTransaction();
        $request['status'] = Input::get('status', 1);
        if (RoleChecker::get('roles') && (RoleChecker::hasRole('admin') || RoleChecker::hasRole('super-admin'))) {
            $url = config::get("config.country_url");
            /**
             * Calls location service to get language translation based on $country_id
             * @param $url
             * @param $country_id
             * @return \Illuminate\Http\Response
             */
            $country = RemoteCall::getSpecific($url . $request["country_id"]);
            if ($country["status"] != "200") {
                return response()->json([
                    "status" => $country["status"],
                    "message" => $country["message"]
                ], $country["status"]);
            }

            $languageCheck = $this->checkLanguageCode($country["message"]["data"]["language"], $request["translation"]);

            switch ($languageCheck) {

                case null: {
                    return response()->json([
                        "status" => "422",
                        "message" => ["translation" => ["Input Translation does not match with specified country language code"]]
                    ], 422);
                    break;

                }

                case is_array($languageCheck) : {

                    return response()->json([
                        "status" => $languageCheck["status"],
                        "message" => $languageCheck["message"]
                    ], $languageCheck["status"]);
                    break;
                }

                default: {
                    $restaurants = $this->restaurant->createRestaurant($request->all());
                    $this->logStoreHelper->storeLogInfo(array("Restaurant", [
                        "status" => "200",
                        "message" => "Restaurant Created Successfully",
                        "data" => $restaurants
                    ]));
                    break;
                }
            }
            foreach ($request['translation'] as $key => $translation) {
                $translation['restaurant_id'] = $restaurants->id;
                $translation['lang_code'] = $key;
                $restaurants_id = $restaurants->id;
                $rules = [
                    "lang_code" => 'required|alpha',
                    "name" => "required"
                ];
                $translation['lang_code'] = Slugify::slugify($translation['lang_code'], ['regexp' => '/([^A-Za-z]|-)+/']);
                if (empty($translation['lang_code'])) {
                    return response()->json([
                        "status" => "422",
                        "message" => ["translation" => ["lang_code" => "The lang_code may not only contain numbers,special characters."]]
                    ], 422);
                }
                $validator = Validator::make($translation, $rules);
                if ($validator->fails()) {
                    $error = $validator->errors();
                    $this->logStoreHelper->storeLogError(array('Restaurant Translation', [
                        'status' => '422',
                        "message" => ["translation" => ["$key" => $error]]
                    ]));
                    return response()->json([
                        'status' => '422',
                        "message" => ["translation" => ["$key" => $error]]
                    ], 422);
                }
                /*
                 * checking if the same restaurant id have existing lang_code or not
                 * */
                try {
                    $checkExistingTranslation = $this->restaurant_translation
                        ->getSpecificRestaurantTranslationByCodeAndId($translation["lang_code"], $restaurants_id);
                    switch ($checkExistingTranslation->first()) {
                        case !null: {
                            throw new \Exception();
                            break;
                        }
                        default: {
                            $createTranslation = $this->restaurant_translation->createRestaurantTranslation($translation);
                            $this->logStoreHelper->storeLogInfo(array("Restaurant Translation", [
                                "status" => "200",
                                "message" => "Restaurant Translation Created Successfully",
                                "data" => $createTranslation
                            ]));
                        }
                    }
                } catch (\Exception $ex) {
                    $this->logStoreHelper->storeLogError(array("Restaurant Translation", [
                        "status" => "409",
                        "message" => "Language translation for given restaurant already exist"
                    ]));
                    return response()->json([
                        "status" => "409",
                        "message" => "Language translation for given restaurant already exist"
                    ], 409);
                }
            }
            $this->logStoreHelper->storeLogInfo(array("Restaurant", [
                "status" => "200",
                "message" => "Restaurant Created Successfully"
            ]));
            /*
             * this variable stores the user related information which is send to oauth
             * for creating as well as assigning created user as restaurant-admin
             * */
            $user = $request["user"];
            /*
             * user role is set to restaurant admin when creating restaurant by default
             * */
            $user['role'] = "restaurant-admin";
            $user['meta'] = [
                'is_restaurant_admin' => true,
                'restaurant_id' => $restaurants->id
            ];
            $user = RemoteCall::store("$this->oauthBaseUrl/user/create", $user);

            if ($user['status'] != 200) {
                return response()->json(
                    $user['message'], $user['status']);
            }
            //$countryNotification = $request->except("user");
            $notification['country'] = $country["message"]["data"]["name"];
            $notification['id'] = $restaurants->id;
            $notification['email_address']= $request->email_address;
            $notification['address']= $request->address;
            $notification['phone_no']= $request->phone_no;
            $notification['mobile_no']= $request->mobile_no;
            $notification['website'] = $request->website;
            $notification['translation'] = $request->translation;
            $notification['country_id'] = $request->country_id;

            $message = [
                "service" => "restaurant service",
                "message" => "restaurant created",
                "data" => [
                "user_details" => [],
                    "restaurant" => $notification
                ]
            ];

            event(new EmailEvent($message));


//            $message1 = ["service" => "oauth service", "message" => "user create", "data" => $request->user];
//            event(new EmailEvent($message1));

//            \Amqp::publish('service1', $message, [
//                'exchange_type' => 'direct',
//                'exchange' => 'amq.direct',
//                'queue' => 'notice'
//            ]);

            DB::commit();
            return response()->json([
                'status' => "200",
                "message" => "Restaurant Created Successfully"
            ], 200);
        } else {
            $this->logStoreHelper->storeLogError(array("Restaurant", [
                'status' => "403",
                "message" => "Only Admin or Super Admin can create Restaurant "
            ]));
            return response()->json([
                'status' => "403",
                "message" => "Only Admin or Super Admin can create Restaurant "
            ], 403);
        }
//        } catch (\Exception $e) {
//            return response()->json([
//                "status" => "422",
//                "message" => "There were some problem in creating restaurant"
//            ], 422);
//        }
    }

    /**
     * Display the specified resource based on role .Only admin and super-admin can have access to this.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $request['status'] = null;//get restaurant independent of status
//        $request['lang'] = Input::get("lang", "en");
//        /*
//         * if the status is not empty then validation of status
//         * */
//        if (!is_null($request['status'])) {
//            $rules = ['status' => 'integer|min:0|max:1'];
//            $validator = Validator::make($request, $rules);
//            if ($validator->fails()) {
//                return response()->json([
//                    'status' => '422',
//                    "message" => $validator->errors()
//                ], 422);
//            }
//        }
        /*
         * if the key has role and the user is admin or super-admin
         * */
        if (RoleChecker::get('roles') && (RoleChecker::hasRole('admin') || RoleChecker::hasRole('super-admin'))) {
            /*
             * if status is not null then based on status and id restaurant is dislpayed
             * otherwise based on id restaurant in displayed
             *
             * */
            try {
                $restaurant = $this->restaurant->getSpecificRestaurant($id, $request['status']);

                if (!$restaurant) {
                    throw  new \Exception();
                }

            } catch (ModelNotFoundException $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Restaurant could not be found"
                ]);
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Restaurant could not be found"
                ], 404);
            }
            /*
             * Get restaurant Translation based on @param lang
             * if empty record is obtained default en lang is returned
             * */
            $restaurantTranslation = $restaurant->restaurantTranslation()->get();
            if ($restaurantTranslation->count() == 0) {
                $restaurantTranslation = $restaurant->restaurantTranslation()->get();
                try {
                    if ($restaurantTranslation->count() == 0) {
                        throw new \Exception();
                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Restaurant language not found"
                    ], 404);
                }
            }
            /*
             * attach name and lang_code in $restaurant variable
             * */
            $restaurant['translation'] = $restaurantTranslation;

            $url = Config::get("config.location_url");
            $country = RemoteCall::getSpecificWithoutOauth($url . "/location/names?country_id=".$restaurant['country_id']);
            if ($country["status"] != "200") {
                return response()->json([
                    "status" => $country["status"],
                    "message" => $country["message"]
                ], $country["status"]);
            }
            $restaurant['country'] = $country['message']['data']['country'];

            return response()->json([
                'status' => "200",
                "data" => $restaurant
            ], 200);
        } elseif (RoleChecker::hasRole("restaurant-admin") || (RoleChecker::get('parentRole') == "restaurant-admin")) {
            $restaurantInfo = RoleChecker::getRestaurantInfo();

            if (!is_null($restaurantInfo) && ($restaurantInfo["is_restaurant_admin"] == true) && $restaurantInfo["restaurant_id"] == $id) {
                $restaurant = $this->restaurant->getSpecificRestaurant($restaurantInfo["restaurant_id"], $status = 1);
                try {
                    if (!$restaurant) {
                        throw  new \Exception();
                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Restaurant could not be found"
                    ], 404);
                }

            } else {
                return response()->json([
                    "status" => "403",
                    "message" => "You are only allowed to view your restaurant only"
                ], 403);
            }


            /*
         * Get restaurant Translation based on @param lang
         * if empty record is obtained default en lang is returned
         * */
            $restaurantTranslation = $restaurant->restaurantTranslation()->get();
            if ($restaurantTranslation->count() == 0) {
                $restaurantTranslation = $restaurant->restaurantTranslation()->get();
                try {
                    if ($restaurantTranslation->count() == 0) {
                        throw new \Exception();
                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Restaurant language not found"
                    ], 404);
                }
            }
            /*
             * attach name and lang_code in $restaurant variable
             * */
            $restaurant['translation'] = $restaurantTranslation;

            $url = Config::get("config.location_url");
            $country = RemoteCall::getSpecificWithoutOauth($url . "/location/names?country_id=".$restaurant['country_id']);
            if ($country["status"] != "200") {
                return response()->json([
                    "status" => $country["status"],
                    "message" => $country["message"]
                ], $country["status"]);
            }
            $restaurant['country'] = $country['message']['data']['country'];

            return response()->json([
                'status' => "200",
                "data" => $restaurant
            ], 200);
        } else {
            return response()->json([
                "status" => "403",
                "message" => "Only Admin ,Super Admin or Restaurant admin can view restaurant "
            ], 403);
        }
    }

    /**
     * show country id for oauth user response
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showForOauthCall($id)
    {
        try {
            $getSpecificData = $this->restaurant->getSpecificRestaurant($id,null);
            return response()->json([
                "status" => "200",
                "data" => $getSpecificData->country_id
            ]);
        }
        catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => $ex->getMessage()
            ],404);
        }

    }

    /**
     * Update the specified resource in storage in table named.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            $this->validate($request, [//validating the request
                "address" => "required",
                "phone_no" => "required|min:7",
                "mobile_no" => "required|min:7",
                "email_address" => "required|email|max:255|unique:restaurants,email_address," . $id,
                "website" => "required|url",
                "contact_person" => "required",
                "commission_rate" => 'numeric|between:0,99.999',
                "translation" => "required|array",
                "status" => "integer|min:0|max:1",
//                "country_id"=>"required|integer", removed country updating
//                "user"=>"array"
            ]);
        } catch (\Exception $e) {
            /*
             *creates log for storing error
             * */
            $this->logStoreHelper->storeLogError(array("Error in Validation", [
                "status" => "422",
                "message" => $e->response->original
            ]));
            return response()->json([
                "status" => "422",
                "message" => $e->response->original
            ], 422);
        }

        DB::beginTransaction();
        if (RoleChecker::get('roles') && !(RoleChecker::hasRole('admin') || RoleChecker::hasRole('super-admin'))) {
            return response()->json([
                'status' => "403",
                "message" => "Only Admin or Super Admin can update Restaurant "
            ], 403);
        }
//            if($request->has("user")){
//                try {
//
//                    $rules=[
//                        'user.user_id' => "required|integer",
//                        'user.first_name' => 'required|max:50',
//                        'user.last_name' => 'required|max:50',
//                        'user.email' => 'required|email|max:255',
//                        'user.password' => 'required|max:255',
//                        'user.gender'=>'required|in:male,female',
//                        'user.mobile'=>'required|digits:10',
//                        "user.birthday"=>"date_format:Y-m-d",
//                        "user.status" => "integer|min:0|max:1"
//                    ];
//                    $validator=$this->validate($request,$rules);
//                }
//                catch (\Exception $ex){
//
//                    $this->logStoreHelper->storeLogError(array("Error in Validation in user index", [
//                        "status" => "422",
//                        "message" => $ex->response->original
//                    ]));
//                    return response()->json([
//                        "status" => "422",
//                        "message" =>$ex->response->original
//                    ],422);
//                }
//                $user=RemoteCall::update("http:/localhost:8888/user/".$request["user"]['user_id'],$request['user']);
//                if($user['status']!=200) {
//                    return response()->json(
//                        $user['message'], $user['status']);
//                }
//            }
        try {

            $oldRestaurantData = $this->restaurant->getSpecificRestaurant($id, $status = null);
            $url = config::get("config.country_url");
            /**
             * Calls location service to get language translation based on $country_id
             * @param $url
             * @param $country_id
             * @return \Illuminate\Http\Response
             */
            $country = RemoteCall::getSpecific($url . $request["country_id"]);
            if ($country["status"] != "200") {
                return response()->json([
                    "status" => $country["status"],
                    "message" => $country["message"]
                ], $country["status"]);
            }

            $languageCheck = $this->checkLanguageCode($country["message"]["data"]["language"], $request["translation"]);


            switch ($languageCheck) {

                case null: {
                    return response()->json([
                        "status" => "422",
                        "message" => ["translation" => ["Input Translation does not match with specified country language code"]]
                    ], 422);
                    break;

                }

                case is_array($languageCheck) : {

                    return response()->json([
                        "status" => $languageCheck["status"],
                        "message" => $languageCheck["message"]
                    ], $languageCheck["status"]);
                    break;
                }

                default: {
                    $updateRestaurant = $this->restaurant->updateRestaurant($id, $request->except("country_id"));
                    $this->logStoreHelper->storeLogInfo(array("Restaurant", [
                        "status" => "200",
                        "message" => "Restaurant Updated Successfully",
                        "data" => $updateRestaurant
                    ]));
                    break;
                }
            }

            /*
             * get the specific updated data
             * */
            $restaurants = $this->restaurant->getSpecificRestaurant($id, $status = null);
            /*
             * delete the specific restaurant translation
             * */
            $deleteTranslation = $restaurants->restaurantTranslation()->delete();
            foreach ($request['translation'] as $key => $translation) {
                $translation['restaurant_id'] = $restaurants->id;
                $translation['lang_code'] = $key;
                $restaurants_id = $restaurants->id;
                $rules = [
                    "lang_code" => 'required|alpha',
                    "name" => "required"
                ];
                /*
                 * slugify package to slugify lang_code
                 * */
                $translation['lang_code'] = Slugify::slugify($translation['lang_code'], ['regexp' => '/([^A-Za-z]|-)+/']);
                if (empty($translation['lang_code'])) {
                    return response()->json([
                        "status" => "422",
                        "message" => ["translation" => ["lang_code" => "The lang_code may not only contain numbers,special characters."]]
                    ], 422);
                }
                $validator = Validator::make($translation, $rules);
                if ($validator->fails()) {
                    $error = $validator->errors();
                    $this->logStoreHelper->storeLogError(array('Restaurant Translation', [
                        'status' => '422',
                        "message" => ["translation" => ["$key" => $error]]
                    ]));
                    return response()->json([
                        'status' => '422',
                        "message" => ["translation" => ["$key" => $error]]
                    ], 422);
                }
                $createTranslation = $this->restaurant_translation->createRestaurantTranslation($translation);
                $this->logStoreHelper->storeLogInfo(array("Restaurant Translation", [
                    "status" => "200",
                    "message" => "Restaurant Translation Updated Successfully",
                    "data" => $createTranslation
                ]));
            }
        if($restaurants->status != $oldRestaurantData->status) {
            $notification['country'] = $country["message"]["data"]["name"];
            $notification['id'] = $restaurants->id;
            $notification['email_address'] = $request->email_address;
            $notification['address'] = $request->address;
            $notification['phone_no'] = $request->phone_no;
            $notification['mobile_no'] = $request->mobile_no;
            $notification['website'] = $request->website;
            $notification['translation'] = $request->translation;
            $notification['country_id'] = $request->country_id;
            $notification['status'] = $request->status;

            $message = [
                "service" => "restaurant service",
                "message" => "restaurant updated",
                "data" => [
                    "user_details" => [],
                    "restaurant" => $notification
                ]
            ];
            event(new EmailEvent($message));
        }

            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Restaurant Updated Successfully"
            ]);
        } catch (ModelNotFoundException $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Empty Record for requested id"
            ], 404);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Record not found"
            ], 404);
        }

    }


    /**
     * Remove the specified resource from storage in table named .
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            if (RoleChecker::get('roles') && RoleChecker::hasRole('admin') || RoleChecker::hasRole('super-admin')) {
                $status = null;
                /*
                 * this method will display specific restaurant wheather the restaurant is soft deleted or not
                 * @param $id ,$status=null
                 * @returns $restaurants
                 * */
                $restaurant = $this->restaurant->getSpecificSoftDeletedOrNotSoftDeletedRestaurant($id, $status);
                $translation = $restaurant->restaurantTranslation()->where('lang_code','en')->get();

                /*
                 * application of has many through relationship between restaurant and restaurnat user which displays
                 * all the user based on the restaurant id
                 * */

                /*
                 * if the deleted_at row of restaurant table is  null then the restaurant and all its associated child
                 * row of restaurant_branch are soft deleted while not changing restaurant_translation table
                 * and changing status of all rows of restaurant_user to 0 based on the restaurant_id
                 * */
                if (is_null($restaurant->deleted_at)) {
                    /*
                * this deleteALL method will use soft delete all the child and parent row if the soft delete features
                * is used and set all the restaurant_user to status 0 based on the specified restaurant id */
                    $restaurant->deleteALL();
                    $restaurantData["method"] = "soft";
                } /*
                 * if the deleted_at row of restaurant table is not null then the restaurant and all its associated child
                 * rows are hard deleted
                 * */
                else {
                    /*hard delete
                    this will delete all its child rows as well its parent row
                    */
                    $restaurant->forceDelete();
                    $restaurantData["method"] = "hard";

                }
                $restaurantData["is_restaurant_admin"] = true;
                $restaurantData["restaurant_id"] = $id;
                $user = RemoteCall::delete("$this->oauthBaseUrl/user/delete", $restaurantData);
                if ($user['status'] != 200) {
                    return response()->json(
                        $user['message'], $user['status']);
                }

                $restaurant = $restaurant->toArray();
                $restaurant['name'] = $translation[0]->name;
                $message = [
                    "service" => "restaurant service",
                    "message" => "restaurant deleted",
                    "data" => [
                        "user_details" => [],
                        "restaurant" => $restaurant
                    ]
                ];
                event(new EmailEvent($message));

                DB::commit();
                return response()->json([
                    "status" => "200",
                    "message" => "Requested Restaurant Deleted Successfully"
                ], 200);

            }
        } catch (ModelNotFoundException $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Requested Restaurant could not be found"
            ], 404);

        } catch (\Exception $ex) {
            return response()->json([
                "status" => "403",
                "message" => "There were problem deleting Restaurant"
            ], 403);
        }


    }

    /**
     * checks if country id exist . this is used for validating before deleting  country in location service
     * @param Request $request
     */
    public function checkCountryId(Request $request)
    {
        try {
            $this->validate($request, [
                "country_id" => "sometimes|integer"
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        try {
            if ($this->restaurant->getCountry($request["country_id"])) {
                return response()->json([
                    "status" => "403",
                    "data" => true
                ], 200);
            } else {
                return response()->json([
                    "status" => "200",
                    "data" => false
                ]);
            }
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "500",
                "message" => "Error in query"
            ], 500);
        }
    }


    /**
     * Check if the country lang_code matches with input lang_code in translation array
     *
     * @param $country_id
     * @param $languageTranslations
     * @return bool|void
     */
    public function checkLanguageCode($countryLanguage, $languageTranslations)
    {
        foreach ($countryLanguage as $language) {
            $languages[] = $language['code'];
        }
        $countLanguage = count($languages);
        foreach ($languageTranslations as $key => $translation) {
            $langlist[] = $key;
        }
        /*
         * if input lang_code count is not equal to obtained country lang_code count ,then it would not compare array since count is not same
         * */
        $countLangList = count($langlist);
        if ($countLangList !== $countLanguage) return;
        /*
         * compare input languages array with obtained country language array count
         * */
        $countIntersect = count($intersectArray = array_intersect($languages, $langlist));
        if ($countLanguage !== $countIntersect) {
            return;
        }
        return true;
    }
}
