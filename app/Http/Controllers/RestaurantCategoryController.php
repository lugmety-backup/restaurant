<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/8/17
 * Time: 3:24 PM
 */

namespace App\Http\Controllers;


use App\Repo\RestaurantBranchInterface;
use App\Repo\CategoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Repo\RestaurantCategoryInterface;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use AuthChecker;
use LogStoreHelper;
use Illuminate\Support\Facades\DB;
use App\Repo\PivotInterface;


class RestaurantCategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $restaurantCategory;
    private $restaurantBranch;
    private $category;
    private $logStoreHelper;
    private $pivot;

    /**
     * RestaurantCategoryController constructor.
     * @param RestaurantCategoryInterface $restaurantCategory
     * @param CategoryInterface $category
     * @param RestaurantBranchInterface $restaurantBranch
     * @param LogStoreHelper $logStoreHelper
     */

    public function __construct(RestaurantCategoryInterface $restaurantCategory, CategoryInterface $category, RestaurantBranchInterface $restaurantBranch, LogStoreHelper $logStoreHelper, PivotInterface $pivot)
    {
        $this->restaurantBranch = $restaurantBranch;
        $this->category = $category;
        $this->restaurantCategory = $restaurantCategory;
        $this->logStoreHelper = $logStoreHelper;
        $this->pivot = $pivot;
    }

    /**
     * Display categories related to a specific restaurant.
     * If 'lang' parameter is not supplied,then default language is set to 'en'.
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($id)
    {
        $lang = Input::get("lang", "en");

        try {
            $restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($id);
            /**
             * Check count of result. If result count is 0, throw exception. Else check if the status of result is 0.
             * If user is not logged in or unauthorized, throw exception if status is zero.
             * If the user is logged in and authorized personnel, continue.
             */

            if (count($restaurantBranch) == 0) {
                throw new \Exception();
            } elseif ($restaurantBranch['status'] == 0) {
                if (!AuthChecker::check())
                    throw new \Exception();
            }

            try {

                /**
                 * If user is logged in and authorized personnel, get all the categories related to specific restaurant.
                 * S/He can view categories of both status i.e. 1 or 0.
                 * S/He can view  categories of only 1 or 0 using filter.
                 * If user is not logged in or unauthorized personnel, S/He can only view active categories (status = 1)
                 */

                if (AuthChecker::check()) {
                    if (!is_null(Input::get('status'))) {
                        $restaurantCategory = $restaurantBranch->category()->where('status', Input::get('status'))->get();
                    } else {
                        $restaurantCategory = $restaurantBranch->category()->get();
                    }

                } else {
                    $restaurantCategory = $restaurantBranch->category()->where('status', 1)->get();
                }

                /**
                 * Get translation of each category according to the language desired through 'lang' parameter.
                 * Store translation in $category array along with other details.
                 * If translation is not available in requested language, default language english is used.
                 * If translation is not available in default language, throw exception and abort.
                 */

                foreach ($restaurantCategory as $category) {
                    $translation = $category->categoryTranslation()->where('lang', $lang)->get();
                    try {
                        if ($translation->count() == 0) {
                            if ($lang == 'en') {
                                throw new \Exception();
                            } else {
                                $translation = $category->categoryTranslation()->where('lang', 'en')->get();
                                if ($translation->count() == 0) {
                                    throw new \Exception();
                                }
                            }

                        }
                    } catch (\Exception $ex) {
                        return response()->json([
                            "status" => "404",
                            "message" => "Default language english not found in database"
                        ], 404);
                    }

                    $category['lang'] = $translation[0]['lang'];
                    $category['name'] = $translation[0]['name'];

                }
                if (!$restaurantCategory->first()) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    "message" => 'Empty Record'
                ], 404);
            }
            return response()->json([
                'status' => '200',
                'data' => $restaurantCategory
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '404',
                'message' => 'Restaurant Could not found'
            ], 404);
        }
    }

    /**
     * Attach categories to a specific restaurant
     * Only authorized user can attach category to restaurant
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store($id, Request $request)
    {

        /**
         * Check if logged in user has authority to attach data.
         * If 'true', continue. Else abort with exception message.
         */

        if (AuthChecker::check()) {
            try {
                $dataValidation = $this->restaurantCategory->getDataForCategoryValidation($id);

                try {
                    $this->validate($request, [//validating the request
                        "category_id" => "required|array|in:" . implode($dataValidation['categories'], ","),

                    ]);

                } catch (\Exception $e) {
                    /*
                     *creates log for storing error
                     * */

                    return response()->json([
                        "status" => "422",
                        "message" => $e->response->original
                    ], 422);


                }
                /**
                 * Start DB transaction to ensure the operation is reversed in case not successfully committed.
                 *
                 */
                DB::beginTransaction();

                $this->pivot->deletePivotForRestaurant($id);
                $restaurantCategory = $this->restaurantCategory->attachCategoryToRestaurant($id, $request['category_id']);


                /**
                 * If operation is successfully executed, commit it, else revert whole operation.
                 */
                DB::commit();

                $this->logStoreHelper->storeLogInfo([
                    "Restaurant-Category Relation", [
                        "status" => "200",
                        "message" => "Categories are attached to restaurant id: " . $id
                    ]
                ]);

                return response()->json([
                    'status' => '200',
                    'message' => 'Category attached successfully to restaurant'
                ], 200);
            }catch (ModelNotFoundException $ex)
            {

            }

        } else {
            return response()->json([
                'status' => "401",
                "message" => "Only Authorized personnel can attach category to restaurant"
            ], 401);
        }

    }
    public function oldStore($id, Request $request)
    {
        /**
         * Check if logged in user has authority to attach data.
         * If 'true', continue. Else abort with exception message.
         */

        if (AuthChecker::check()) {
            try {
                $restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($id);


                if (count($restaurantBranch) == 0 || $restaurantBranch['status'] == 0) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    'message' => "Restaurant of id: " . $id . " may be removed or disabled"
                ], 404);
            }
            try {
                $this->validate($request, [//validating the request
                    "category_id" => "required|array",

                ]);

            } catch (\Exception $e) {
                /*
                 *creates log for storing error
                 * */

                return response()->json([
                    "status" => "422",
                    "message" => $e->response->original
                ], 422);


            }
            /**
             * Start DB transaction to ensure the operation is reversed in case not successfully committed.
             *
             */
            DB::beginTransaction();

            foreach ($request['category_id'] as $categoryId) {
                try {
                    $category = $this->category->getSpecificCategory($categoryId);
                    if (count($category) == 0 || $category['status'] == 0) {
                        throw new \Exception();
                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        'status' => '404',
                        'message' => "Category of id:" . $categoryId . " may be removed or disabled"
                    ], 404);
                }
                $request = $categoryId;
                //dd($category['country_id'] == $restaurantBranch['country_id']);
                if($category['country_id'] == $restaurantBranch['country_id'])
                {
                    try {
                        $restaurantCategory = $this->restaurantCategory->attachCategoryToRestaurant($id, $request);
                    } catch (\Exception $ex) {
                        return response()->json([
                            'status' => '409',
                            'message' => "Duplicate Entry for restaurantBranchId " . $id . " and category_id " . $categoryId
                        ], 409);

                    }
            }else{
                    return response()->json([
                        'status' => '403',
                        'message' => 'Category of id: '.$categoryId.' belongs to different country. Remove this id and try again'
                    ], 200);
                }

            }
            /**
             * If operation is successfully executed, commit it, else revert whole operation.
             */
            DB::commit();

            $this->logStoreHelper->storeLogInfo([
                "Restaurant-Category Relation", [
                    "status" => "200",
                    "message" => "Categories are attached to restaurant id: " . $id
                ]
            ]);

            return response()->json([
                'status' => '200',
                'message' => 'Category attached successfully to restaurant'
            ], 200);
        } else {
            return response()->json([
                'status' => "401",
                "message" => "Only Authorized personnel can attach category to restaurant"
            ], 401);
        }

    }

    /**
     * Detach categories from a specific restaurant
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function destroy($id, Request $request)
    {
        /**
         * Check if logged in user has authority to detach data.
         * If 'true', continue. Else abort with exception message.
         */

        if (AuthChecker::check()) {
            try {
                $restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($id);

                /**
                 * Check if restaurantBranch is present and active.
                 * If restaurantBranch is not present or inactive, throw exception, else continue.
                 */

                if (count($restaurantBranch) == 0 || $restaurantBranch['status'] == 0) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    'message' => "Restaurant Branch of id: " . $id . "may be removed or disabled"
                ], 404);
            }
            /**
             * Validate the request.
             * Request for field 'category_id' is required and should be an array
             */

            try {
                $this->validate($request, [//validating the request
                    "category_id" => "required|array",

                ]);

            } catch (\Exception $e) {
                /*
                 *creates log for storing error
                 * */

                return response()->json([
                    "status" => "422",
                    "message" => $e->response->original
                ]);


            }
            /**
             * Start DB transaction to ensure the operation is reversed in case not successfully committed.
             *
             */
            DB::beginTransaction();

            foreach ($request['category_id'] as $categoryId) {
                try {
                    $category = $this->category->getSpecificCategory($categoryId);
                    if (count($category) == 0 || $category['status'] == 0) {
                        throw new \Exception();
                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => '404',
                        "message" => "Category of id:" . $categoryId . " could not be found"
                    ], 404);
                }
                $request = $categoryId;

                $restaurantCategory = $this->restaurantCategory->detachCategoryFromRestaurant($id, $request);


            }
            /**
             * If operation is successfully executed, commit it, else revert whole operation.
             */
            DB::commit();

            $this->logStoreHelper->storeLogInfo([
                "Restaurant-Category Relation", [
                    "status" => "200",
                    "message" => "Categories are detached from restaurant id: " . $id
                ]
            ]);
            return response()->json([
                "status" => '200',
                "message" => 'Category detached successfully from restaurant'
            ], 200);
        } else {
            return response()->json([
                "status" => "401",
                "message" => "Only Authorized personnel can detach category from restaurant"
            ], 401);
        }

    }

    /**
     * Display restaurant related to given category.
     * If 'lang' parameter is not supplied,then default language is set to 'en'.
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function getRestaurantByCategory($id)
    {
        $lang = Input::get("lang", "en");

        try {

            $category = $this->category->getSpecificCategory($id);
            /**
             * Check count of result. If result count is 0, throw exception. Else check if the status of result is 0.
             */
            if (count($category) == 0) {
                throw new \Exception();
            } elseif ($category['status'] == 0) {
                if (!AuthChecker::check())
                    throw new \Exception();
            }
            /**
             * If user is logged in and authorized personnel, get all the restaurant related to specific category.
             * S/He can veiw restaurant of both status i.e. 1 or 0.
             * S/He can view restaurant of only 1 or 0 using filter.
             * If user is not logged in or unauthoried personnel, S/He can only view active restaurant (status = 1)
             */

            try {
                if (AuthChecker::check()) {
                    if (!is_null(Input::get('status'))) {
                        $restaurantCategory = $category->restaurantBranch()->where('status', Input::get('status'))->get();
                    } else {
                        $restaurantCategory = $category->restaurantBranch()->get();
                    }

                } else {
                    $restaurantCategory = $category->restaurantBranch()->where('status', 1)->get();
                }

                /**
                 * Get translation of each $restaurant according to the language desired through 'lang' parameter.
                 * Store translation in $restaurant array along with other details.
                 * If translation is not available in requested language, default language english is used.
                 * If translation is not available in default language, throw exception and abort.
                 */
                foreach ($restaurantCategory as $restaurant) {
                    $translation = $restaurant->branchTranslation()->where('lang_code', $lang)->get();

                    try {
                        if ($translation->count() == 0) {
                            if ($lang == 'en') {
                                throw new \Exception();
                            } else {
                                $translation = $restaurant->branchTranslation()->where('lang_code', 'en')->get();
                                if ($translation->count() == 0) {
                                    throw new \Exception();
                                }
                            }

                        }
                    } catch (\Exception $ex) {
                        return response()->json([
                            "status" => "404",
                            "message" => "Default language english not found in database"
                        ], 404);
                    }

                    $restaurant['lang'] = $translation[0]['lang_code'];
                    $restaurant['name'] = $translation[0]['name'];
                    $restaurant['description'] = $translation[0]['description'];

                }

                if (!$restaurantCategory->first()) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Empty Record"
                ], 404);
            }
            return response()->json([
                "status" => "200",
                "data" => $restaurantCategory
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "404",
                "message" => "Category Could not found"
            ], 404);
        }

    }


    /**
     * Get all the menu related to a restaurant branch.
     * All the categories related to restaurant branch is fetched first.
     * Then all the food related to each category is fetched.
     * Check if each food has addon.
     * If addon is present, set has_addon to true, else false.
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetailsByRestaurantCategory($id)
    {
        $lang = Input::get("lang", "en");

        try {
            /**
             * Get specific restaurant branch of given id.
             * If restaurant is not active or available, throw 404 error message.
             */
            $restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($id);

            if (count($restaurantBranch) == 0 || $restaurantBranch['status'] == 0) {
                throw new \Exception();
            }
            /**
             * Get translation of each restaurant branch according to the language desired through 'lang' parameter.
             * Store translation in $restaurantBranch array along with other details.
             * If translation is not available in requested language, default language english is used.
             * If translation is not available in default language, throw exception and abort.
             */
            $translation = $restaurantBranch->branchTranslation()->where('lang_code', $lang)->get();

            try {
                if ($translation->count() == 0) {
                    if ($lang == 'en') {
                        throw new \Exception();
                    } else {
                        $translation = $restaurantBranch->branchTranslation()->where('lang_code', 'en')->get();
                        if ($translation->count() == 0) {
                            throw new \Exception();
                        }
                    }

                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Default language english not found in database"
                ], 404);
            }

            $restaurantBranch['lang'] = $translation[0]['lang_code'];
            $restaurantBranch['name'] = $translation[0]['name'];
            $restaurantBranch['description'] = $translation[0]['description'];
            /**
             * Check count of result. If result count is 0, throw exception. Else check if the status of result is 0.
             * If user is not logged in or unauthorized, throw exception if status is zero.
             * If the user is logged in and authorized personnel, continue.
             */

            try {
                /**
                 * Get categories related to restaurant branch.
                 * If categories is not active or available, throw 404 error message.
                 */
                    $restaurantCategory = $restaurantBranch->category()->where('status', 1)->get();


                /**
                 * Get translation of each category according to the language desired through 'lang' parameter.
                 * Store translation in $category array along with other details.
                 * If translation is not available in requested language, default language english is used.
                 * If translation is not available in default language, throw exception and abort.
                 */

                foreach ($restaurantCategory as $category) {
                    $translation = $category->categoryTranslation()->where('lang', $lang)->get();
                    try {
                        if ($translation->count() == 0) {
                            if ($lang == 'en') {
                                throw new \Exception();
                            } else {
                                $translation = $category->categoryTranslation()->where('lang', 'en')->get();
                                if ($translation->count() == 0) {
                                    throw new \Exception();
                                }
                            }

                        }
                    } catch (\Exception $ex) {
                        return response()->json([
                            "status" => "404",
                            "message" => "Default language english not found in database"
                        ], 404);
                    }

                    $category['lang'] = $translation[0]['lang'];
                    $category['name'] = $translation[0]['name'];

                    $foodData = $category->food()->where('status', 1)->where('restaurant_id', $id)->get();
                    /**
                     * Get Food related to  category and restaurant branch.
                     * If food is not active or available, throw 404 error message.
                     */
                    foreach ($foodData as $food)
                    {
                        /**
                         * Get translation of each food according to the language desired through 'lang' parameter.
                         * Store translation in $food array along with other details.
                         * If translation is not available in requested language, default language english is used.
                         * If translation is not available in default language, throw exception and abort.
                         */
                        $translation = $food->foodTranslation()->where('lang', $lang)->get();
                        try {
                            if ($translation->count() == 0) {
                                if ($lang == 'en') {
                                    throw new \Exception();
                                } else {
                                    $translation = $food->foodTranslation()->where('lang', 'en')->get();
                                    if ($translation->count() == 0) {
                                        throw new \Exception();
                                    }
                                }

                            }
                        } catch (\Exception $ex) {
                            return response()->json([
                                "status" => "404",
                                "message" => "Default language english not found in database"
                            ], 404);
                        }

                        $foodSize = $food->foodSize()->get();

                        if ($foodSize->count() == 0) {

                            $food['size'] = [];

                        }else{

                            foreach ($foodSize as $key=>$size)
                            {
                                if($size['is_default'] == "1")
                                {
                                    $food['price'] = $size['price'];

                                }
                                $sizeTranslation = $size->FoodSizeTranslation()->where('lang',$lang)->first();
                                if (count($sizeTranslation) == 0) {
                                    if ($lang == 'en') {
                                        unset($foodSize[$key]);
                                    } else {
                                        $sizeTranslation = $size->FoodSizeTranslation()->where('lang','en')->first();
                                        if (count($sizeTranslation) == 0) {
                                            unset($foodSize[$key]);
                                        }
                                    }

                                }
                                else{
                                    $size['title'] = $sizeTranslation['title'];
                                    $size['is_default'] = (integer)$size['is_default'];
                                }
                            }

                            $food['size'] = $foodSize;
                        }
                        if(!empty($food->gallery)){
                            unset($foos);
                            $food["gallery"] = unserialize($food->gallery);
                            foreach ($food["gallery"] as $key=> $foo){
                                //todo add from setting
                                $foos[] = Config::get("config.image_service_base_url_cdn").$foo;
                            }
                            $food["gallery"] =  $foos;
                        }

                        $food['lang'] = $translation[0]['lang'];
                        $food['name'] = $translation[0]['name'];
                        $food['description'] = $translation[0]['description'];
                        $food['ingredients'] = $translation[0]['ingredients'];
                        $food['has_addon'] = false;

                        /**
                         * Get addon category related to food and restaurant branch.
                         * If addon category is not active or available, throw 404 error message.
                         */
                        $addonCategory = $food->addonCategory()->where('status', 1)->where('restaurant_id', $id)->get();

                        if(count($addonCategory) > 0)
                        {
                            foreach ($addonCategory as $addcategory )
                            {
                                /**
                                 * Get addon related to addon category and restaurant branch.
                                 * If addon is not active or available, throw 404 error message.
                                 */
                                $addon = $addcategory->addon()->where('status', 1)->where('restaurant_id', $id)->get();

                                if(count($addon) > 0)
                                {

                                    $food['has_addon'] = true;
                                }
                            }
                        }


                    }
                  $category['foodData'] = $foodData;


                }
                $restaurantBranch['categories'] = $restaurantCategory;

                if (!$restaurantCategory->first()) {
                    throw new \Exception();
                }

            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    "message" => 'Empty Record'
                ], 404);
            }
            return response()->json([
                'status' => '200',
                'data' => $restaurantBranch
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '404',
                'message' => 'Restaurant Could not found'
            ], 404);
        }

    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     *
     */

    public function adminCategoryListByBranch($id)
    {
        $lang = Input::get("lang", "en");

        try {
            $restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($id);
            /**
             * Check count of result. If result count is 0, throw exception. Else check if the status of result is 0.
             * If the user is logged in and authorized personnel, continue.
             */

            if (count($restaurantBranch) == 0) {
                throw new \Exception();
            } elseif (!AuthChecker::check($id)) {

                    throw new \Exception();
            }

            try {

                /**
                 * If user is logged in and authorized personnel, get all the categories related to specific restaurant.
                 * S/He can view categories of both status i.e. 1 or 0.
                 * S/He can view  categories of only 1 or 0 using filter.
                 * If user is not logged in or unauthorized personnel, S/He can only view active categories (status = 1)
                 */


                        $restaurantCategory = $restaurantBranch->category()->get();


                /**
                 * Get translation of each category according to the language desired through 'lang' parameter.
                 * Store translation in $category array along with other details.
                 * If translation is not available in requested language, default language english is used.
                 * If translation is not available in default language, throw exception and abort.
                 */

                foreach ($restaurantCategory as $category) {
                    $translation = $category->categoryTranslation()->where('lang', $lang)->get();
                    try {
                        if ($translation->count() == 0) {
                            if ($lang == 'en') {
                                throw new \Exception();
                            } else {
                                $translation = $category->categoryTranslation()->where('lang', 'en')->get();
                                if ($translation->count() == 0) {
                                    throw new \Exception();
                                }
                            }

                        }
                    } catch (\Exception $ex) {
                        return response()->json([
                            "status" => "404",
                            "message" => "Default language english not found in database"
                        ], 404);
                    }

                    $category['lang'] = $translation[0]['lang'];
                    $category['name'] = $translation[0]['name'];

                }
                if (!$restaurantCategory->first()) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    "message" => 'Empty Record'
                ], 404);
            }
            return response()->json([
                'status' => '200',
                'data' => $restaurantCategory
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '404',
                'message' => 'Restaurant Could not found'
            ], 404);
        }
    }


}
