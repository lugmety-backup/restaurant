<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/28/2017
 * Time: 12:38 PM
 */

namespace App\Http\Controllers;

use App\Http\RoleCheckerFacade;
use App\Repo\RestaurantBranchInterface;
use App\Repo\RestaurantInterface;
use App\RestaurantBranch;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Repo\RestaurantUsersInterface;
use Illuminate\Support\Facades\Input;
use RoleChecker;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use LogStoreHelper;
use Validator;
use RemoteCall;


/**
 * Class RestaurantUserController
 * @package App\Http\Controllers
 */
class BranchUserController extends Controller
{
    protected $checker;
    protected $logStoreHelper;
    protected $restaurantBranch;
    protected $restaurant;
    protected $oauthBaseUrl;

    /**
     * RestaurantController constructor.
     * @param RestaurantUsersInterface $restaurantUsers
     * @param RoleChecker $checker
     * @param RestaurantBranchInterface $restaurantBranch
     * @param LogStoreHelper $logStoreHelper
     * @internal param $restaurant
     * @internal param $restaurant_translation
     */
    public function __construct(RoleChecker $checker,
                                RestaurantBranchInterface $restaurantBranch,
                                LogStoreHelper $logStoreHelper,
                                RestaurantInterface $restaurant)
    {
        $this->checker=$checker;
        $this->logStoreHelper=$logStoreHelper;
        $this->restaurantBranch=$restaurantBranch;
        $this->restaurant=$restaurant;
        $this->oauthBaseUrl=Config::get('config.oauth_base_url');
    }

    /**
     * Display a listing of the resource from table named .
     *
     * @param $branch_id
     * @return \Illuminate\Http\Response
     */
    public function index($restaurant_id,$branch_id, Request $request)
    {
        /*
           *validation for restaurant_id
           * */
        try{
            $restaurant=$this->restaurant->getSpecificRestaurant($restaurant_id,1);
        }catch (\Exception $e)
        {
            $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser",[
                'status'=>'404',
                'message'=>'Requested Restaurant could not be found',
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Restaurant could not be found'], 404);
        }
        /*
         *validation for branch_id
         * */
        try{
            $restaurantBranch=$this->restaurantBranch->getSpecificRestaurantBranch($branch_id);
        }catch (\Exception $e)
        {
            $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser",[
                'status'=>'404',
                'message'=>'Requested Restaurant branch could not be found',
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Restaurant branch could not be found'], 404);
        }
        try{
            if($restaurantBranch['parent_id'] != $restaurant_id){
                throw new \Exception();
            }
        }catch (\Exception $e)
        {
            $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser",[
                'status'=>'404',
                'message'=>'Requested Branch is not found in requested restaurant',
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Branch is not found in requested restaurant'], 404);
        }

        /*
        * checking roles potential
        * */
        if(!($this->checkRolePotention($restaurant_id,$branch_id))){
            $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser",[
                'status'=>'401',
                'message'=>'Unauthorized',
            ]));
            return response()->json([
                'status' => '401',
                'message' => 'Unauthorized'], 401);
        }

        $user=RemoteCall::getSpecific("$this->oauthBaseUrl/user/restaurant/$restaurant_id/branch/$branch_id");
        if($user['status']!=200) {
            $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser",[
                $user['message']
            ]));
            return response()->json(
                $user['message'], $user['status']);
        }
        $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser",[
            'status'=>'200',
            'message'=>$user['message']
        ]));
        return \Datatables::of(collect($user['message']['data']))->make(true);

    }


    /**
     * Store a newly created resource in table name .
     *
     * @param  \Illuminate\Http\Request $request
     * @param $branch_id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$branch_id, $restaurant_id)
    {
        /*
           *validation for restaurant_id
           * */
        try{
            $restaurant=$this->restaurant->getSpecificRestaurant($restaurant_id,1);
        }catch (\Exception $e)
        {
            $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser",[
                'status'=>'404',
                'message'=>'Requested Restaurant could not be found'
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Restaurant could not be found'], 404);
        }
        /*
         *validation for branch_id
         * */
        try{
            $restaurantBranch=$this->restaurantBranch->getSpecificRestaurantBranch($branch_id);
        }catch (\Exception $e)
        {
            $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser",[
                'status'=>'404',
                'message'=>'Requested Restaurant branch could not be found'
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Restaurant branch could not be found'], 404);
        }
        try{
            if($restaurantBranch['parent_id'] != $restaurant_id){
                throw new \Exception();
            }
        }catch (\Exception $e)
        {
            $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser",[
                'status'=>'404',
                'message'=>'Requested Branch is not found in requested restaurant'
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Branch is not found in requested restaurant'], 404);
        }

        /*
        * checking roles potential
        * */
        if(!($this->checkRolePotention($restaurant_id,$branch_id))){
            $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser",[
                'status'=>'401',
                'message'=>'Unauthorized'
            ]));
            return response()->json([
                'status' => '401',
                'message' => 'Unauthorized'], 401);
        }

        try {
            $message =[
                "domain_validation" => "The mail server for domain does not exist."
            ];
            $this->validate($request, [
                'status'=>'integer|min:0|max:1',
                'first_name' => 'required|max:50',
                'last_name' => 'required|max:50',
                'email' => 'required|email|domain_validation|max:255',
                'password' => 'required|max:255',
                'gender'=>'required',
                'mobile'=>'required|min:7',
                "birthday"=>"date_format:Y-m-d",
                'role'=>'required'
            ],$message);
        }catch (\Exception $ex) {
            $this->logStoreHelper->storeLogError(array("Error in Validation in user index", [
                "status" => "422",
                "message" => $ex->response->original
            ]));
            return response()->json([
                "status" => "422",
                "message" =>$ex->response->original
            ],422);
        }
        $request=$request->all();
        $request['meta']['is_restaurant_admin']=false;
        $request['meta']['restaurant_id']=$restaurant_id;
        $request['meta']['branch_id']=$branch_id;
        $user=RemoteCall::store("$this->oauthBaseUrl/branch/user/create",$request);
        if($user['status']!=200) {
            $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser",[
                $user['message']
            ]));
            return response()->json(
                $user['message'], $user['status']);
        }
        $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser",[
            'status'=>'200',
            'message'=>'User Created Successfully'
        ]));
        return response()->json([
            "status" => "200",
            "message" =>"User Created Successfully"
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }


    /**
     * Update the specified resource in storage in table named.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @param $branch_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $branch_id)
    {

    }

    /**
     * Remove the specified resource from storage in table named .
     *
     * @param  int $id
     * @param $branch_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,$branch_id,$restaurant_id)
    {
        /*
           *validation for restaurant_id
           * */
        try{
            $restaurant=$this->restaurant->getSpecificRestaurant($restaurant_id,1);
        }catch (\Exception $e)
        {
            $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser",[
                'status'=>'404',
                'message'=>'Requested Restaurant could not be found'
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Restaurant could not be found'], 404);
        }

        /*
         *validation for branch_id
         * */
        try{
            $restaurantBranch=$this->restaurantBranch->getSpecificRestaurantBranch($branch_id);
        }catch (\Exception $e)
        {
            $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser",[
                'status'=>'404',
                'message'=>'Requested Restaurant branch could not be found'
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Restaurant branch could not be found'], 404);
        }
        try{
            if($restaurantBranch['parent_id'] != $restaurant_id){
                throw new \Exception();
            }
        }catch (\Exception $e)
        {
            $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser",[
                'status'=>'404',
                'message'=>'Requested Branch is not found in requested restaurant'
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Branch is not found in requested restaurant'], 404);
        }

        /*
        * checking roles potential
        * */
        if(!($this->checkRolePotention($restaurant_id,$branch_id))){
            $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser",[
                'status'=>'401',
                'message'=>'Unauthorized'
            ]));
            return response()->json([
                'status' => '401',
                'message' => 'Unauthorized'], 401);
        }

        $request['method']='hard'; /// soft indicates  changing status of user from 1 to 0 and hard indicates deletion of user.

        $user=RemoteCall::deleteSpecific("$this->oauthBaseUrl/branch/user/$id");
        if($user['status']!=200) {
            $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser",[
                $user['message']
            ]));
            return response()->json(
                $user['message'], $user['status']);
        }
        $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser",[
            'status'=>'200',
            'message'=>'Restaurant User deleted successfully'
        ]));
        return response()->json([
            'status'=>'200',
            'message'=>"Restaurant User deleted successfully"
        ]);
    }

    private function checkRolePotention($restaurant_id,$branch_id){
        // restaurant-admin has access to  his restaurant only
        if ((RoleChecker::hasRole('restaurant-admin') || (RoleChecker::get('parentRole') == 'restaurant_admin'))) {
            try {
                if(RoleChecker::get('is_restaurant_admin') != true){
                    throw new \Exception();
                }
                if ($restaurant_id == RoleChecker::get('restaurant_id')) {
                    return true;
                } else {
                    throw new \Exception();
                }

            } catch (\Exception $ex) {
                return false;
            }
        }
        elseif (RoleChecker::hasRole('branch-admin') || (RoleChecker::get('parentRole') == 'branch_admin') ) {
            try {
                if(RoleChecker::get('is_restaurant_admin') != false){
                    throw new \Exception();
                }
                if (RoleChecker::get('branch_id') == $branch_id) {
                    return true;
                } else {
                    throw new \Exception();
                }

            } catch (\Exception $ex) {
                return false;
            }
        }
        return true;
    }
}