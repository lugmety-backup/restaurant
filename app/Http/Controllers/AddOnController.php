<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/8/17
 * Time: 3:23 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repo\AddOnInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repo\AddOnTranslationInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;
use AuthChecker;
use RestaurantChecker;
use LogStoreHelper;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Datatables;
use LocationService;
use App\Repo\RestaurantBranchInterface;
use App\Repo\PivotInterface;
use App\Repo\AddonAddonCategoryInterface;


class AddOnController extends Controller
{
    protected $addon;
    protected $addonTranslation;
    protected $logStoreHelper;
    protected $restaurantBranch;
    protected $pivot;
    protected $addonAddonCategory;

    /**
     * AddOnController constructor.
     * @param AddOnInterface $addon
     * @param AddOnTranslationInterface $addonTranslation
     * @param LogStoreHelper $logStoreHelper
     */

    public function __construct(AddOnInterface $addon,
                                AddOnTranslationInterface $addonTranslation,
                                LogStoreHelper $logStoreHelper, RestaurantBranchInterface $restaurantBranch, PivotInterface $pivot, AddonAddonCategoryInterface $addonAddonCategory)
    {
        $this->addon = $addon;
        $this->addonTranslation = $addonTranslation;
        $this->logStoreHelper = $logStoreHelper;
        $this->restaurantBranch = $restaurantBranch;
        $this->pivot = $pivot;
        $this->addonAddonCategory = $addonAddonCategory;
    }

    /**
     * Display the list of addon related to specific restaurant
     * If 'lang' parameter is not supplied,then default language is set to 'en'.
     * If 'status' parameter is not supplied, Authorized personnel can view all the data i.e. of both status 0 and 1.
     * If 'status' parameter is supplied, Authorized personnel can view data having requested status. Others can view data having status 1 in any case.
     * 'limit' parameter is used to get no. of records, user want to view in per page.
     * If 'limit' parameter is not supplied, only 10 records are shown per page. Else, no. of records is equal to limit supplied.
     *'page' parameter is used to get page no. of current record display
     * @param $restaurantId
     * @return \Illuminate\Http\JsonResponse
     */

    public function index($restaurantId)
    {
        try {
            if (!is_null(Input::get('status'))) {
                $request['status'] = Input::get('status');

            }

            $request['lang'] = Input::get("lang", "en");
            $request['limit'] = Input::get("limit", 10);
            /*
             * Validate the status and limit
             * Status must be 0 or 1.
             * limit must be integer.
             *
             * */
            $rules = [
                'status' => 'sometimes|required|integer|min:0|max:1',
                'limit' => 'integer'
            ];
            $validator = Validator::make($request, $rules);
            if ($validator->fails()) {
                return response()->json([
                    "status" => "422",
                    "message" => $validator->errors()
                ], 422);
            }
            /**
             *
             * check the authority of the logged in user and set the status according to the output of check function of AuthChecker facade.
             * if output is "true", set status to all ie user can view all addon according to restaurant
             *if output  is "false", set status to 1 i.e. user can only view addon of status 1.
             *
             */

            if (AuthChecker::check($restaurantId)) {
                /**
                 * If status is not given or empty, then status is set to "all".
                 * It means all the available data is to fetched.
                 * If status is supplied, filter data accordingly
                 */
                if (!isset($request['status'])) {
                    $request['status'] = "all";

                }
                /**
                 * If the user is not a authorized personnel, check the status of restaurant.
                 * If restaurant is active, fetch active addon of that restaurant.
                 * If restaurant is inactive, throw exception
                 */

            } else {
                if (RestaurantChecker::check($restaurantId)) {
                    $request['status'] = 1;
                } else {
                    return response()->json([

                        "status" => "404",
                        "message" => "Restaurant you entered is not found"

                    ], 404);
                }

            }

            $addons = $this->addon->getAllAddOn($restaurantId, $request['status'], $request['limit']);
            try {
                if (!$addons->first()) {
                    throw  new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Empty Record"
                ], 404);
            }
            foreach ($addons as $key => $addon) {
                /**
                 * Get addon Translation based on @param lang
                 * if empty record is obtained, default en lang is returned
                 * If default en lang is not available, throw exception
                 * */

                $addonTranslation = $this->addonTranslation->getSpecificAddOnTransalationByLang($addon->id, $request['lang']);
                try {
                    if ($addonTranslation->count() == 0) {
                        if ($request['lang'] == 'en') {
                            throw new \Exception();
                        } else {
                            $addonTranslation = $this->addonTranslation->getSpecificAddOnTransalationByLang($addon->id, 'en');
                            if ($addonTranslation->count() == 0) {
                                throw new \Exception();
                            }
                        }

                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Default language english not found in database"
                    ], 404);
                }

                /*
                 * attach name and lang_code in $addon variable
                 * */
                $addon['name'] = $addonTranslation[0]['name'];
                $addon['lang'] = $addonTranslation[0]['lang'];
                $addon['description'] = $addonTranslation[0]['description'];
            }

            return response()->json([
                'status' => "200",
                "data" => $addons
            ], 200);

        } catch (\Exception $ex) {

            return response()->json([
                "status" => "404",
                "message" => "something went wrong"
            ], 404);
        }
    }

    /**
     * Store Addon related to specific restaurant
     * @param $restaurantId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     */

    public function store($restaurantId, Request $request)
    {
        try{
        /**
         * Check if logged in user has authority to insert data.
         * If 'true', continue. Else abort with exception message.
         *
         * Validate the request
         * price field is required and must be numeric and between 0 to 9999.99.
         * status field is required and must be integer. Only 0 or 1 is acceptable.
         * translation field is required and must be an array
         * If validation fails throw error message and abort.
         */

        $dataValidation = $this->addon->getDataForAddonValidation($restaurantId);

            if (AuthChecker::check($restaurantId)) {
                try {
                    $this->validate($request, [
                        "price" => "required|numeric|between:0,9999.99",
                        "status" => "required|integer|min:0|max:1",
                        "translation" => "required|array",
                        "addon_categories" => "required|array|in:" . implode($dataValidation['addonCategories'], ",")
                    ]);

                } catch (\Exception $e) {

                    return response()->json([
                        "status" => "422",
                        "message" => $e->response->original
                    ], 422);
                }

                try {
                    /**
                     * Use remote location service to verify the translation request.
                     * If the translation lang codes are not same as country's lang codes, error message is returned.
                     */
                    $countryValidate = LocationService::checkLanguageCode($dataValidation['restaurant']['country_id'], $request['translation']);

                    if ($countryValidate['status'] != 200) {
                        return response()->json([
                            "status" => $countryValidate["status"],
                            "message" => $countryValidate["message"]
                        ], $countryValidate["status"]);

                    }
                    /**
                     * Start DB transaction to ensure the operation is reversed in case not successfully committed.
                     *
                     */
                    DB::beginTransaction();

                    $request['restaurant_id'] = $restaurantId;

                    /**
                     * Insert data into addon table
                     */

                    $addon = $this->addon->createAddOn($request->all());

                    /**
                     * Validate each item of an array.
                     * For addon_id, use id of recently created category
                     */

                    foreach ($request['translation'] as $key => $translation) {
                        $translation['addon_id'] = $addon->id;
                        $translation['lang'] = $key;
                        /**
                         * Validate lang and name fields.
                         * lang field is required and must be alphabets.
                         * name field is required.
                         * description field is required.
                         * If validation fails, abort with validation error message.
                         * Else insert data in addon_translation table.
                         */
                        $rules = [
                            "lang" => 'required|alpha',
                            "name" => "required",
                            "description" => 'sometimes',
                        ];
                        $translation['lang'] = str_slug($translation['lang']);

                        $validator = Validator::make($translation, $rules);
                        if ($validator->fails()) {
                            $error = $validator->errors();
                            $this->logStoreHelper->storeLogError([
                                'Addon Translation',
                                [
                                    'status' => '422',
                                    "message" => [$key => $error]
                                ]
                            ]);
                            return response()->json([
                                'status' => '422',
                                "message" => [$key => $error]
                            ], 422);
                        }
                        /**
                         * Insert $translation into addon_translation table
                         */

                        $createTranslation = $this->addonTranslation->createAddOnTranslation($translation);


                    }
                    $foodAddonCategory = $this->addonAddonCategory->attachCategoryToAddon($addon->id, $request['addon_categories']);

                    $this->logStoreHelper->storeLogInfo([
                        "AddOn",
                        [
                            "status" => "200",
                            "message" => "Addon Created Successfully"
                        ]
                    ]);

                    /**
                     * If operation is successfully executed, commit it, else revert whole operation.
                     */

                    DB::commit();
                    return response()->json([
                        'status' => "200",
                        "message" => "Addon Created Successfully"
                    ], 200);

                } catch (\Exception $e) {
                    return response()->json([
                        "status" => "422",
                        "message" => $e->getMessage()
                    ], 422);
                }
            } else {
                return response()->json([
                    'status' => "401",
                    "message" => "Only Authorized Personnel can create addon"
                ], 401);
            }
        } catch (ModelNotFoundException $ex) {
            return response()->json([

                "status" => "404",
                "message" => "Restaurant you entered is not found"

            ], 404);
        } catch (\Exception $ex)
        {
            return response()->json([

                "status" => "500",
                "message" => $ex->getMessage()

            ], 500);
        }


    }

    /**
     * Delete addon of given id
     * @param $restaurantId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function destroy($restaurantId, $id)
    {
        /**
         * Check if logged in user has authority to delete data.
         * If 'true', continue. Else abort with exception message.
         */

        if (AuthChecker::check($restaurantId)) {

            try {
                /**
                 * delete addon along with its translation
                 */
                $this->addon->getSpecificAddonByRestaurant($id, $restaurantId);

                $this->addon->deleteAddOn($id);

                return response()->json([
                    "status" => "200",
                    "message" => "Requested Addon Deleted Successfully"
                ], 200);


            } catch
            (ModelNotFoundException $notFoundException) {
                /**
                 * If requested data is not found, return 404 error with message.
                 */
                return response()->json([
                    "status" => '404',
                    "message" => "Requested Addon not found"
                ], 404);

            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "422",
                    "message" => "There were problem deleting Addon"
                ], 422);
            }
        } else {
            return response()->json([
                "status" => "401",
                "message" => "Only Authorized Personnel can delete addon "
            ], 401);
        }


    }

    /**
     * Show addon of given id
     * @param $restaurantId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function show($restaurantId, $id)
    {
        try {

            $request['lang'] = Input::get("lang", "en");
            /**
             *
             * Check if restaurant is active, for unauthorized user.
             * if restaurant is not active, throw exception message.
             */
            if (!AuthChecker::check($restaurantId)) {
                if (!RestaurantChecker::check($restaurantId)) {
                    return response()->json([

                        "status" => "404",
                        "message" => "Restaurant you entered is not found"

                    ], 404);
                }
            }

            /*
             *
             * Fetch data of addon having specific id of any restaurant.
            */
            try {

                $addon = $this->addon->getSpecificAddonByRestaurant($id, $restaurantId);
                /**
                 * Check if addon is active.
                 * If addon is inactive, throw exception for unauthorized user.
                 * Display data for authorized users.
                 */

                if ($addon['status'] == 0) {
                    if (!AuthChecker::check($restaurantId)) {
                        throw new \Exception();
                    }
                }


            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Empty Record for requested addon"
                ], 404);
            }
            /*
             * Get addon Translation based on @param lang
             * if empty record is obtained default en lang is returned
             * */
            $addonTranslation = $this->addonTranslation->getSpecificAddOnTransalationByLang($addon->id, $request['lang']);
            try {
                if ($addonTranslation->count() == 0) {
                    if ($request['lang'] == 'en') {
                        throw new \Exception();
                    } else {
                        $addonTranslation = $this->addonTranslation->getSpecificAddOnTransalationByLang($addon->id, 'en');
                        if ($addonTranslation->count() == 0) {
                            throw new \Exception();
                        }
                    }

                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Default language english not found in database"
                ], 404);
            }


            /*
             * attach name and lang in $addon variable
             * */
            $addon['name'] = $addonTranslation[0]['name'];
            $addon['lang'] = $addonTranslation[0]['lang'];
            $addon['description'] = $addonTranslation[0]['description'];
            return response()->json([
                'status' => "200",
                "data" => $addon
            ], 200);

        } catch (ModelNotFoundException $notFoundException) {
            return response()->json([
                'status' => '404',
                'message' => "Empty Record for requested addon"
            ], 404);
        }
    }

    /**
     * update addon of given id
     * @param $restaurantId
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function update($restaurantId, Request $request, $id)
    {
        try{
        /**
         * Check if logged in user has authority to update data.
         * If 'true', continue. Else abort with exception message.
         *
         * Validate the request
         * price field is required and must be numeric and between 0 to 9999.99.
         * status field is required and must be integer. Only 0 or 1 is acceptable.
         * translation field is required and must be an array
         * If validation fails throw error message and abort.
         */
        $dataValidation = $this->addon->getDataForAddonValidation($restaurantId);


            if (AuthChecker::check($restaurantId)) {
                try {
                    $this->validate($request, [//validating the request
                        "price" => "required|numeric|between:0,9999.99",
                        "status" => "required|integer|min:0|max:1",
                        "translation" => "required|array",
                        "addon_categories" => "required|array|in:" . implode($dataValidation['addonCategories'], ",")
                    ]);

                } catch (\Exception $e) {

                    return response()->json([
                        "status" => "422",
                        "message" => $e->response->original
                    ],422);
                }

                try {
                    /**
                     * Use remote location service to verify the translation request.
                     * If the translation lang codes are not same as country's lang codes, error message is returned.
                     */
                    $countryValidate = LocationService::checkLanguageCode($dataValidation['restaurant']['country_id'], $request['translation']);

                    if ($countryValidate['status'] != 200) {
                        return response()->json([
                            "status" => $countryValidate["status"],
                            "message" => $countryValidate["message"]
                        ], $countryValidate["status"]);

                    }
                    /**
                     * Start DB transaction to ensure the operation is reversed in case not successfully committed.
                     *
                     */
                    DB::beginTransaction();


                    $addon = $this->addon->getSpecificAddonByRestaurant($id, $restaurantId);


                    /*
                   * Deleting related translations of given addon using relationships and re-enter data
                   * */

                    $addon->addonTranslation()->delete();
                    /*
                    * translation field validation
                    * */
                    foreach ($request['translation'] as $key => $translation) {
                        $translation['addon_id'] = $addon->id;
                        $translation['lang'] = $key;
                        /**
                         * Validate lang and name fields.
                         * lang field is required and must be alphabets.
                         * name field is required.
                         * description field is required.
                         * If validation fails, abort with validation error message.
                         * Else insert data in addon_translation table.
                         */
                        $rules = [
                            "lang" => 'required|alpha',
                            "name" => "required",
                            "description" => 'sometimes',
                        ];
                        $translation['lang'] = str_slug($translation['lang']);

                        $validator = Validator::make($translation, $rules);
                        if ($validator->fails()) {
                            $error = $validator->errors();
                            $this->logStoreHelper->storeLogError([
                                'Addon Translation',
                                [
                                    'status' => '422',
                                    "message" => [$key => $error]
                                ]
                            ]);
                            return response()->json([
                                'status' => '422',
                                "message" => [$key => $error]
                            ], 422);
                        }
                        $createTranslation = $this->addonTranslation->createAddOnTranslation($translation);
                    }

                    /**
                     * update data of the given addon id
                     */
                    $this->pivot->deletePivotForAddon($id);

                    $foodAddonCategory = $this->addonAddonCategory->attachCategoryToAddon($id, $request['addon_categories']);
                    $addon = $this->addon->updateAddOn($id, $request->all());

                    $this->logStoreHelper->storeLogInfo([
                        "addon",
                        [
                            "status" => "200",
                            "message" => "Addon Updated Successfully",
                            "data" => $addon
                        ]
                    ]);
                    /**
                     * If operation is successfully executed, commit it, else revert whole operation.
                     */

                    DB::commit();
                    return response()->json([
                        "status" => "200",
                        "message" => "Addon updated Successfully"
                    ], 200);

                } catch (ModelNotFoundException $notFoundException) {
                    return response()->json([
                        "status" => '404',
                        "message" => "Requested Addon not found"
                    ], 404);
                } catch (\Exception $e) {
                    return response()->json([
                        "status" => "422",
                        "message" => $e->getMessage()
                    ], 422);
                }
            } else {
                return response()->json([
                    "status" => "401",
                    "message" => "Only Authorized Personnel can update addon"
                ], 401);
            }
        } catch(ModelNotFoundException $ex) {
            return response()->json([

                "status" => "404",
                "message" => "Restaurant you entered is not found"

            ], 404);
        } catch (\Exception $ex)
        {
            return response()->json([

                "status" => "500",
                "message" => "Something went wrong"

            ], 500);
        }

    }

    /**
     * Get all the addon data for datatable.
     * Addon name is only available in english.
     * @param Request $request
     * @return mixed
     */

    public function getForDatatable($restaurantId, Request $request)
    {
        try {
            $addonData = new Collection($this->addon->getAddonForDatatable($restaurantId));
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage()
            ],500);
        }
        return Datatables::of($addonData)
            ->addColumn('name', function ($addon) {
                return $addon->addonTranslation->map(function ($translation) {
                    return $translation->name;
                })->implode('');
            })->make(true);
        //}
    }

    /**
     * Show addon of given id with all related translations for admin dashboard
     * @param $restaurantId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function adminShow($restaurantId, $id)
    {
        if (AuthChecker::check($restaurantId)) {
            /*
             *
             * Fetch data of addon having specific id of any restaurant.
            */


                $addon = $this->addon->getSpecificAddonByRestaurant($id, $restaurantId);


                /*
                 * Get addon Translation
                 * */
                $addonTranslation = $addon->addonTranslation;
                $addonCategory = $addon->addonCategory()->select('addoncategory.id')->get();

                /**
                 * Get translation of each addon category according to the language desired through 'lang' parameter.
                 * Store translation in $category array along with other deatils.
                 * If translation is not available in requested language, default language english is used.
                 * If translation is not avaliable in default language, throw exception and abort.
                 */

                foreach ($addonCategory as $category) {
                    $translation = $category->categoryTranslation()->where('lang', 'en')->get();
                    try {
                        if ($translation->count() == 0) {

                                throw new \Exception();

                            }

                    } catch (\Exception $ex) {
                        return response()->json([
                            "status" => "404",
                            "message" => "Default language english not found in database"
                        ], 404);
                    }

                    $category['name'] = $translation[0]['name'];

                }


                if (!$addonCategory->first()) {
                    throw new \Exception();
                }
                /*
                 * attach translation variable
                 *
                 */
                $addon['translation'] = $addonTranslation;
                $addon['addon_categories'] = $addonCategory;

                return response()->json([
                    "status" => "200",
                    "data" => $addon
                ], 200);

            try {} catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => $ex->getMessage()
                ], 404);
            }

        } else {
            return response()->json([
                "status" => "401",
                "message" => "Unauthorized action"
            ], 401);
        }
    }

}
