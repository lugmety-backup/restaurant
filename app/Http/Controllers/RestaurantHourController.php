<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 5/13/2017
 * Time: 8:18 AM
 */

namespace App\Http\Controllers;


use App\Http\SettingsFacade;
use App\Repo\RestaurantHourInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Validator;
use Illuminate\Support\Facades\Config;
use LogStoreHelper;
use App\Repo\RestaurantBranchInterface;
use RoleChecker;
use Settings;

class RestaurantHourController extends Controller
{
	/**
	 * @var RestaurantHourInterface
	 */
	protected $restaurantHour;
	/**
	 * @var RestaurantBranchInterface
	 */
	protected $restaurantBranch;
	/**
	 * @var LogStoreHelper
	 */
	protected $logStoreHelper;

	protected $settings;

	/**
	 * RestaurantHourController constructor.
	 * @param LogStoreHelper $logStoreHelper
	 * @param RestaurantHourInterface $restaurantHour
	 * @param RestaurantBranchInterface $restaurantBranch
	 */
	public function __construct(LogStoreHelper $logStoreHelper,
		RestaurantHourInterface $restaurantHour,
		RestaurantBranchInterface $restaurantBranch)
	{
		$this->logStoreHelper = $logStoreHelper;
		$this->restaurantHour = $restaurantHour;
		$this->restaurantBranch = $restaurantBranch;
	}


	/**
	 * Display a listing of the restaurant hours from table named restaurant_hours
	 * The current day working hour is displayed
	 *
	 *
	 * @param $id
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function publicIndex($id)
	{
		/**
		 *validation for branch_id
		 */
		try {
			$restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($id);
		} catch (\Exception $e) {
			return response()->json([
				'status' => '404',
				'message' => 'Requested Restaurant branch could not be found'], 404);
		}
		try {
			$filter = Input::get("type");
			$day = date("l");
			$restaurantHour = $this->restaurantHour->getSpecificRestaurantHourByIdRestaurantIdDay($id, $day);
			if ($filter == "all") {
				$restaurantHour = $this->restaurantHour->getSpecificRestaurantHourByIdRestaurantId($id);
			}
			switch (count($restaurantHour)) {
				case 0: {
					throw new \Exception();
					break;
				}
				default:
					foreach ($restaurantHour as $hour) {
						/**
						 * Changing time to restaurant timezone
						 */
						$hour['opening_time'] = $this->changeToTimezoneOfBranch($hour['opening_time'], $restaurantBranch['timezone']);
						$hour['closing_time'] = $this->changeToTimezoneOfBranch($hour['closing_time'], $restaurantBranch['timezone']);
					}
					$this->logStoreHelper->storeLogInfo(array('Restaurant Hour', [
						"status" => "200",
						"data" => $restaurantHour
					]));
					return response()->json([
						"status" => "200",
						"data" => $restaurantHour
					], 200);
					break;
			}
		} catch (\Exception $ex) {
			$this->logStoreHelper->storeLogError(array('Restaurant Hour', [
				"status" => "404",
				"data" => "Requested restaurant hour not found"
			]));
			return response()->json([
				"status" => "404",
				"data" => "Requested restaurant hour not found"
			], 404);
		}
	}

	/**
	 * get specific restaurant hour based on passed day
	 * @param $branch_id
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function show($branch_id, $day)
	{
		/**
		 *validation for branch_id
		 */
		try {
			$restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($branch_id);
		} catch (\Exception $e) {
			return response()->json([
				'status' => '404',
				'message' => 'Requested Restaurant branch could not be found'], 404);
		}
		try {
			$day = ucfirst($day);
			$restaurantHour = $this->restaurantHour->getSpecificRestaurantHourByIdRestaurantIdDay($branch_id, $day);
			switch (count($restaurantHour)) {
				case 0: {
					throw new \Exception();
					break;
				}
				default:
					foreach ($restaurantHour as $hour) {
						/**
						 * Changing time to restaurant timezone
						 */
						$hour['opening_time'] = $this->changeToTimezoneOfBranch($hour['opening_time'], $restaurantBranch['timezone']);
						$hour['closing_time'] = $this->changeToTimezoneOfBranch($hour['closing_time'], $restaurantBranch['timezone']);
					}
					$this->logStoreHelper->storeLogInfo(array('Restaurant Hour', [
						"status" => "200",
						"data" => $restaurantHour
					]));
					return response()->json([
						"status" => "200",
						"data" => $restaurantHour
					], 200);
					break;
			}
		} catch (\Exception $ex) {
			$this->logStoreHelper->storeLogError(array('Restaurant Hour', [
				"status" => "404",
				"data" => "Requested restaurant hour not found"
			]));
			return response()->json([
				"status" => "404",
				"data" => "Requested restaurant hour not found"
			], 404);
		}
	}

	/**
	 * Display a listing of the restaurant hours from table named restaurant_hours
	 * if the url param type consists today then current day working hour is displayed
	 * otherwise all the working hour of specified branch is displayed
	 *
	 * @param $id
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function adminIndex($id)
	{
		/**
		 *validation for branch_id
		 */
		try {
			$restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($id);
		} catch (\Exception $e) {
			return response()->json([
				'status' => '404',
				'message' => 'Requested Restaurant branch could not be found'], 404);
		}

		try {
			$filter = Input::get("type");

			if ($filter == "today") {
				$day = date("l");
				$restaurantHour = $this->restaurantHour->getSpecificRestaurantHourByIdRestaurantIdDay($id, $day);
			} else {
				$restaurantHour = $this->restaurantHour->getSpecificRestaurantHourByIdRestaurantId($id);
			}

			switch (count($restaurantHour)) {
				case 0: {
					throw new \Exception();
					break;
				}
				default:
					foreach ($restaurantHour as $hour) {
						/**
						 * Changing time to restaurant timezone
						 */
						$hour['opening_time'] = $this->changeToTimezoneOfBranch($hour['opening_time'], $restaurantBranch['timezone']);
						$hour['closing_time'] = $this->changeToTimezoneOfBranch($hour['closing_time'], $restaurantBranch['timezone']);
					}
					$this->logStoreHelper->storeLogInfo(array('Restaurant Hour', [
						"status" => "200",
						"data" => $restaurantHour
					]));
					return \Datatables::of($restaurantHour)->make(true);
					break;
			}
		} catch (\Exception $ex) {
			$this->logStoreHelper->storeLogError(array('Restaurant Hour', [
				"status" => "404",
				"data" => "Requested restaurant hour not found"
			]));
			return response()->json([
				"status" => "404",
				"data" => "Requested restaurant hour not found"
			], 404);
		}
	}

	/**
	 * get specific restaurant hour based on passed day
	 * @param $branch_id
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function adminShow($branch_id)
	{
		/**
		 *validation for branch_id
		 */
		try {
			$restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($branch_id);
		} catch (\Exception $e) {
			return response()->json([
				'status' => '404',
				'message' => 'Requested Restaurant branch could not be found'], 404);
		}
		try {
			if (!$this->checkRolePotention($branch_id)) {
				return response()->json([
					"status" => "403",
					"message" => "You are not allowed to view  restaurant hours "
				], 403);
			}
			$restaurantHour = $this->restaurantHour->getSpecificRestaurantHourByIdRestaurantId($branch_id);
			switch (count($restaurantHour)) {
				case 0: {
					throw new \Exception();
					break;
				}
				default:
					foreach ($restaurantHour as $hour) {
						/**
						 * Changing time to restaurant timezone
						 */
						$hour['opening_time'] = $this->changeToTimezoneOfBranch($hour['opening_time'], $restaurantBranch['timezone']);
						$hour['closing_time'] = $this->changeToTimezoneOfBranch($hour['closing_time'], $restaurantBranch['timezone']);
					}
					$this->logStoreHelper->storeLogInfo(array('Restaurant Hour', [
						"status" => "200",
						"data" => $restaurantHour
					]));
					return response()->json([
						"status" => "200",
						"data" => $restaurantHour
					], 200);
					break;
			}
		} catch (\Exception $ex) {
			$this->logStoreHelper->storeLogError(array('Restaurant Hour', [
				"status" => "404",
				"data" => "Requested restaurant hour not found"
			]));
			return response()->json([
				"status" => "404",
				"data" => "Requested restaurant hour not found"
			], 404);
		}
	}

	/**
	 * get specific restaurant hour based on passed day
	 * @param $branch_id
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function adminShowV2($branch_id)
	{
		/**
		 *validation for branch_id
		 */
		try {
			$restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($branch_id);
		} catch (\Exception $e) {
			return response()->json([
				'status' => '404',
				'message' => 'Requested Restaurant branch could not be found'], 404);
		}
		if ($restaurantBranch['shift'] == 1) {
			$restaurantHourResults = $this->getSingleRestaurantHourList($restaurantBranch['id']);
		}else {
			$restaurantHourResults = $this->getMultipleRestaurantHourList($restaurantBranch['id']);
		}
		try {
			if (!$this->checkRolePotention($branch_id)) {
				return response()->json([
					"status" => "403",
					"message" => "You are not allowed to view  restaurant hours "
				], 403);
			}
			$restaurantHour = $this->restaurantHour->getSpecificRestaurantHourByIdRestaurantId($branch_id);
			switch (count($restaurantHour)) {
				case 0: {
					throw new \Exception();
					break;
				}
				default:
					foreach ($restaurantHour as $hour) {
						/**
						 * Changing time to restaurant timezone
						 */
						$hour['opening_time'] = $this->changeToTimezoneOfBranch($hour['opening_time'], $restaurantBranch['timezone']);
						$hour['closing_time'] = $this->changeToTimezoneOfBranch($hour['closing_time'], $restaurantBranch['timezone']);
					}
					$count = 0;
					foreach ($restaurantHourResults as $key => $restaurantHourResult){
						foreach ($restaurantHour as $hour){
							if($restaurantHourResult['day'] == $hour['day'] && $restaurantHourResult['shift'] == $hour['shift']){
								$restaurantHourResults[$key] = $hour;
								continue;
							}

						}
						$count++;
					}
					$this->logStoreHelper->storeLogInfo(array('Restaurant Hour', [
						"status" => "200",
						"data" => $restaurantHour
					]));
					return response()->json([
						"status" => "200",
						"data" => $restaurantHourResults
					], 200);
					break;
			}
		} catch (\Exception $ex) {
			$this->logStoreHelper->storeLogError(array('Restaurant Hour', [
				"status" => "404",
				"data" => "Requested restaurant hour not found"
			]));
			return response()->json([
				"status" => "404",
				"data" => "Requested restaurant hour not found"
			], 404);
		}
	}

	/**
	 * @param $branch_id
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store($branch_id, Request $request)
	{
		DB::beginTransaction();
		try {
			$this->validate($request, [
				'restaurant_hour' => "required|array",
			]);
			$restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($branch_id);
		} catch (ModelNotFoundException $ex) {
			DB::rollBack();
			$this->logStoreHelper->storeLogError(array('Restaurant Hour', [
				'status' => '404',
				"message" => "Restaurant branch could not be found"
			]));
			return response()->json([
				'status' => '404',
				"message" => "Restaurant branch could not be found"
			], 404);
		} catch (\Exception $e) {
			DB::rollBack();
			/*
			 *creates log for storing error
			 * */
			$this->logStoreHelper->storeLogError(array("Error in Validation", [
				"status" => "422",
				"message" => $e->response->original
			]));
			return response()->json([
				"status" => "422",
				"message" => $e->response->original
			], 422);
		}
		if ($this->checkRolePotention($branch_id)) {
			foreach ($request["restaurant_hour"] as $day => $restaurantHour) {
				$restaurantDay["day"] = $day;
				/**
				 * custom validation to check day
				 * @return bool
				 * */
				$rules = [
					"day" => "required|week_day_validation",
				];
//                custom validation message
				$messages = [
					"week_day_validation" => "The $day must be either Sunday,Monday,Tuesday,Wednesday,Friday or Saturday"
				];

				$validator = Validator::make($restaurantDay, $rules, $messages);
				if ($validator->fails()) {
					$error = $validator->errors();
					$this->logStoreHelper->storeLogError(array('Restaurant Hour', [
						'status' => '422',
						"message" => [$error]
					]));
					return response()->json([
						'status' => '422',
						"message" => [$error]
					], 422);
				}
//                $data = Settings::getSettings('restaurant-hour-limit');
//                if ($data["status"] != 200) {
//                    return response()->json(
//                        $data["message"]
//                        , $data["status"]);
//                }
//                $limit = \Redis::get('restaurant-hour-limit');
				if (!is_array($restaurantHour)) {
					DB::rollBack();
					$this->logStoreHelper->storeLogInfo(array('Restaurant Hour', [
						"status" => "404",
						"message" => "Hours in day must be in array"
					]));
					return response()->json([
						"status" => "404",
						"message" => "Hours in day must be in array"
					], 404);
				}
				if (count($restaurantHour) != $restaurantBranch->shift) {
					DB::rollBack();
					$this->logStoreHelper->storeLogInfo(array('Restaurant Hour', [
						"status" => "404",
						"message" => "Number of shift must be equal to shift of restaurant"
					]));
					return response()->json([
						"status" => "404",
						"message" => "Number of shift must be equal to shift of restaurant"
					], 404);
				} else {
					$restaurantHourCount = count($restaurantHour);
					foreach ($restaurantHour as $key => $hour) {
						$RestaurantData[$key] = $hour;
						$shiftInArray = Config::get('config.restaurant_hour_shift');
						if ( $restaurantHourCount > 1) {
							$shiftInString = join(' or ', array_filter(array_merge(array(join(', ', array_slice($shiftInArray, 0, -1))), array_slice($shiftInArray, -1)), 'strlen'));;
							$RestaurantData[$key]["branch_id"] = $branch_id;
							$RestaurantData[$key]["day"] = $day;
							$rules = [
								"$key.shift" => "required|shift_validation",
								"$key.opening_time" => 'required|date_format:H:i',
								"$key.closing_time" => "required|date_format:H:i",
								"$key.closed" => "required|integer|min:0|max:1"
							];
							$messages = [
								"shift_validation" => "The shift must be either $shiftInString"
							];

						} else {
							$shiftInString = $shiftInArray[0];
							$RestaurantData[$key]["branch_id"] = $branch_id;
							$RestaurantData[$key]["day"] = $day;
							$rules = [
								"$key.shift" => "required|day_shift_validation",
								"$key.opening_time" => 'required|date_format:H:i',
								"$key.closing_time" => "required|date_format:H:i",
								"$key.closed" => "required|integer|min:0|max:1"
							];
							$messages = [
								"day_shift_validation" => "The shift must be $shiftInString"
							];
						}

						$validator = Validator::make($RestaurantData, $rules, $messages);
						// dd( $rules );
						if ($validator->fails()) {
							$error = $validator->errors();
							$this->logStoreHelper->storeLogError(array('Restaurant Hour', [
								'status' => '422',
								"message" => [$day => $error]
							]));
							return response()->json([
								'status' => '422',
								"message" => [$day => $error]
							], 422);
						}
						if ($RestaurantData[$key]["opening_time"] > $RestaurantData[$key]["closing_time"]) {
							if ($RestaurantData[$key]["closing_time"] >= "03:00") {
								return response()->json([
									'status' => '422',
									"message" => [$day => [$key . ".closing_time" => ["Closing time must be greater than opening time"]]]
								], 422);
							}
						}
						$checkforDuplicate = $this->restaurantHour->getSpecificRestaurantHourByIdRestaurantIdDayShift($branch_id, $restaurantDay["day"], $RestaurantData[$key]["shift"]);
						if (count($checkforDuplicate) == 0) {
							/**
							 * Converting restaurant hour to utc
							 */
							$RestaurantData[$key]['opening_time'] = $this->changeToUTC($restaurantHour[$key]['opening_time'], $restaurantBranch['timezone']);
							$RestaurantData[$key]['closing_time'] = $this->changeToUTC($restaurantHour[$key]['closing_time'], $restaurantBranch['timezone']);
							$flag = "None";
							if ($RestaurantData[$key]['opening_time'] > $RestaurantData[$key]['closing_time']) {
								$flag = "Greater";
							}
							$RestaurantData[$key]['difference_checker'] = $flag;
							/** @noinspection PhpUnusedLocalVariableInspection */
							$restaurantHour = $this->restaurantHour->createRestaurantHour($RestaurantData[$key]);
						} else {
							DB::rollBack();
							$this->logStoreHelper->storeLogError(array('Restaurant Hour', [
								'status' => "409",
								"message" => "duplicate entry "
							]));
							return response()->json([
								'status' => "409",
								"message" => "duplicate entry "
							], 409);
						}
					}
				}
			}


			DB::commit();
			$this->logStoreHelper->storeLogInfo(array('Restaurant Hour', [
				"status" => "200",
				"message" => "Restaurant hours added successfully"
			]));
			return response()->json([
				"status" => "200",
				"message" => "Restaurant hours added successfully"
			], 200);
		} else {
			DB::rollBack();
			return response()->json([
				"status" => "403",
				"message" => "You are not allowed to create  restaurant hour "
			], 403);
		}
	}

	/**
	 * @param $branch_id
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function update($branch_id, Request $request)
	{
		DB::beginTransaction();
		try {
			$this->validate($request, [
				'restaurant_hour' => "required|array",
			]);
			$restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($branch_id);
		} catch (ModelNotFoundException $ex) {
			DB::rollBack();
			$this->logStoreHelper->storeLogError(array('Restaurant Hour', [
				'status' => '404',
				"message" => "Restaurant branch could not be found"
			]));
			return response()->json([
				'status' => '404',
				"message" => "Restaurant branch could not be found"
			], 404);
		} catch (\Exception $e) {
			DB::rollBack();
			/*
			 *creates log for storing error
			 * */
			$this->logStoreHelper->storeLogError(array("Error in Validation", [
				"status" => "422",
				"message" => $e->response->original
			]));
			return response()->json([
				"status" => "422",
				"message" => $e->response->original
			], 422);
		}
		if ($this->checkRolePotention($branch_id)) {
			try {
				$this->restaurantHour->deleteRestaurantHourByBranchId($restaurantBranch->id);
			} catch (ModelNotFoundException $ex) {
				DB::rollBack();
				$this->logStoreHelper->storeLogError(array('Restaurant Hour', [
					'status' => '404',
					"message" => "Restaurant hour could not be deleted"
				]));
				return response()->json([
					'status' => '404',
					"message" => "Restaurant hour could not be deleted"
				], 404);
			}

			foreach ($request["restaurant_hour"] as $day => $restaurantHour) {
				$restaurantDay["day"] = $day;
				/**
				 * custom validation to check day
				 * @return bool
				 * */
				$rules = [
					"day" => "required|week_day_validation",
				];
//                custom validation message
				$messages = [
					"week_day_validation" => "The $day must be either Sunday,Monday,Tuesday,Wednesday,Friday or Saturday"
				];

				$validator = Validator::make($restaurantDay, $rules, $messages);
				if ($validator->fails()) {
					$error = $validator->errors();
					$this->logStoreHelper->storeLogError(array('Restaurant Hour', [
						'status' => '422',
						"message" => [$error]
					]));
					return response()->json([
						'status' => '422',
						"message" => [$error]
					], 422);
				}
//                $data = Settings::getSettings('restaurant-hour-limit');
//                if ($data["status"] != 200) {
//                    return response()->json(
//                        $data["message"]
//                        , $data["status"]);
//                }
//                $limit = \Redis::get('restaurant-hour-limit');
				if (!is_array($restaurantHour)) {
					DB::rollBack();
					$this->logStoreHelper->storeLogInfo(array('Restaurant Hour', [
						"status" => "404",
						"message" => "Hours in day must be in array"
					]));
					return response()->json([
						"status" => "404",
						"message" => "Hours in day must be in array"
					], 404);
				}
				if (count($restaurantHour) != $restaurantBranch->shift) {
					DB::rollBack();
					$this->logStoreHelper->storeLogInfo(array('Restaurant Hour', [
						"status" => "404",
						"message" => "Number of shift must be equal to shift of restaurant"
					]));
					return response()->json([
						"status" => "404",
						"message" => "Number of shift must be equal to shift of restaurant"
					], 404);
				} else {
					$flags = 0;
					foreach ($restaurantHour as $key => $hour) {
						$RestaurantData[$key] = $hour;
						$shiftInArray = Config::get('config.restaurant_hour_shift');
						if (count($hour) > 1) {
							$shiftInString = join(' or ', array_filter(array_merge(array(join(', ', array_slice($shiftInArray, 0, -1))), array_slice($shiftInArray, -1)), 'strlen'));

							$RestaurantData[$key]["branch_id"] = $branch_id;
							$RestaurantData[$key]["day"] = $day;
							$rules = [
								"$key.shift" => "required|shift_validation",
								"$key.opening_time" => 'required|date_format:H:i',
								"$key.closing_time" => "required|date_format:H:i",
								"$key.closed" => "required|integer|min:0|max:1"
							];
							$messages = [
								"shift_validation" => "The shift must be either $shiftInString"
							];
						} else {
							$shiftInString = $shiftInArray[0];
							$RestaurantData[$key]["branch_id"] = $branch_id;
							$RestaurantData[$key]["day"] = $day;
							$rules = [
								"$key.shift" => "required|day_shift_validation",
								"$key.opening_time" => 'required|date_format:H:i',
								"$key.closing_time" => "required|date_format:H:i",
								"$key.closed" => "required|integer|min:0|max:1"
							];
							$messages = [
								"day_shift_validation" => "The shift must be $shiftInString"
							];
						}
//                        return $RestaurantData[$key];


						$validator = Validator::make($RestaurantData, $rules, $messages);
						if ($validator->fails()) {
							$error = $validator->errors();
							$this->logStoreHelper->storeLogError(array('Restaurant Hour', [
								'status' => '422',
								"message" => [$day => $error]
							]));
							return response()->json([
								'status' => '422',
								"message" => [$day => $error]
							], 422);
						}
						if ($RestaurantData[$key]["opening_time"] > $RestaurantData[$key]["closing_time"]) {
							if ($RestaurantData[$key]["closing_time"] >= "03:00") {
								return response()->json([
									'status' => '422',
									"message" => [$day => [$key . ".closing_time" => ["Closing time must be greater than opening time"]]]
								], 422);
							}
						}
						$checkforDuplicate = $this->restaurantHour->getSpecificRestaurantHourByIdRestaurantIdDayShift($branch_id, $restaurantDay["day"], $RestaurantData[$key]["shift"]);
						if (count($checkforDuplicate) == 0) {
							/**
							 * Converting restaurant hour to utc
							 */
//                            $flags++;
//                            if($flags == 2){
//                                return $hour;
//                            }
							$RestaurantData[$key]['opening_time'] = $this->changeToUTC($hour['opening_time'], $restaurantBranch['timezone']);
							$RestaurantData[$key]['closing_time'] = $this->changeToUTC($hour['closing_time'], $restaurantBranch['timezone']);
							$flag = "None";
							if ($RestaurantData[$key]['opening_time'] > $RestaurantData[$key]['closing_time']) {
								$flag = "Greater";
							}
							$RestaurantData[$key]['difference_checker'] = $flag;


							/** @noinspection PhpUnusedLocalVariableInspection */
							$restaurantHour = $this->restaurantHour->createRestaurantHour($RestaurantData[$key]);
						} else {
							DB::rollBack();
							$this->logStoreHelper->storeLogError(array('Restaurant Hour', [
								'status' => "409",
								"message" => "duplicate entry "
							]));
							return response()->json([
								'status' => "409",
								"message" => "duplicate entry "
							], 409);
						}
					}
				}
			}
			DB::commit();
			$this->logStoreHelper->storeLogInfo(array('Restaurant Hour', [
				"status" => "200",
				"message" => "Restaurant hours added successfully"
			]));
			return response()->json([
				"status" => "200",
				"message" => "Restaurant hours added successfully"
			]);
		} else {
			DB::rollBack();
			return response()->json([
				"status" => "403",
				"message" => "You are not allowed to update  restaurant hour "
			], 403);
		}
	}

	/**
	 * Delete record which id is $id
	 * admin or super-admin can delete ,
	 * restaurant-admin can delete only its associated branch_id restaurant hour
	 * branch-admin can delete only its associated restaurant hour
	 *
	 * @param $id
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function destroy($id)
	{
		try {
			$data = $this->restaurantHour->getSpecificRestaurantHour($id);
			$restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranchByStatus($data["branch_id"], 1);
			$branch_id = $data["branch_id"];
			if ($this->checkRolePotention($branch_id)) {
				$this->restaurantHour->deleteRestaurantHourById($id);
				$this->logStoreHelper->storeLogInfo(array('Restaurant Hour', [
					"status" => "200",
					"message" => "Specified Restaurant hour deleted successfully",
					"data" => ["id" => $id]
				]));
				return response()->json([
					"status" => "200",
					"message" => "Specified Restaurant hour deleted successfully"
				], 200);
			} else {
				return response()->json([
					"status" => "403",
					"message" => "You are not allowed to delete this restaurant hour "
				], 403);
			}
		} catch (ModelNotFoundException $ex) {
			$this->logStoreHelper->storeLogError(array('Restaurant Hour', [
				'status' => '404',
				"message" => "Restaurant hour id could not be found"
			]));
			return response()->json([
				'status' => '404',
				"message" => "Restaurant hour id could not be found"
			], 404);
		}
	}

	/*
	 * Verify current User Role
	 *
	 * super-admin and admin have access to all restaurant hour..
	 *
	 * if .....   user1 is restaurant-admin with id1
	 * id1 has 3 branches branch1, branch2, branch3
	 * then restaurant-admin can only access restaurant hour in branch1,branch2,branch3 not other branches like: branch4
	 *
	 * if ..... user1 is branch-admin with id1
	 * id1 has one branch branch1
	 * the branch-admin can only access drestaurant hour in branch1 not other
	 *
	 *
	 *
	 * @param $branch_id
	 * @return bool
	 *
	 * */
	private function checkRolePotention($branch_id)
	{

		// restaurant-admin has access to  his restaurant only
		if ((RoleChecker::hasRole('restaurant-admin') || (RoleChecker::get('parentRole') == 'restaurant_admin'))) {
			try {

				if (RoleChecker::get('is_restaurant_admin') != true) {
					throw new \Exception();
				}
				if (in_array($branch_id, RoleChecker::get('branch_id'))) {
					return true;
				} else {
					throw new \Exception();
				}

			} catch (\Exception $ex) {
				return false;
			}
		} elseif (RoleChecker::hasRole('branch-admin') || RoleChecker::hasRole('branch-operator') || (RoleChecker::get('parentRole') == 'branch_admin')) {
			try {
				if (RoleChecker::get('is_restaurant_admin') != false) {
					throw new \Exception();
				}
				if (RoleChecker::get('branch_id') == $branch_id) {
					return true;
				} else {
					throw new \Exception();
				}

			} catch (\Exception $ex) {
				return false;
			}
		}
		return true;
	}

	public function changeToTimezoneOfBranch($time, $timezone)
	{
		$parsed = date_parse($time);
		$now = Carbon::now($timezone);
		$diff = $now->offset;
		$time = $parsed['hour'] * 3600 + $parsed['minute'] * 60;
		$time = $time + $diff;
		$date = gmdate('H:i', $time);
		return $date;
		//return date('h:i:s a', strtotime($date));
	}

	public function changeToUTC($time, $timezone)
	{
		$parsed = date_parse($time);
		$now = Carbon::now($timezone);
		$diff = $now->offset;
		$time = $parsed['hour'] * 3600 + $parsed['minute'] * 60;
		$time = $time - $diff;
		$date = gmdate('H:i', $time);
		return $date;
	}

	public function getSingleRestaurantHourList($id)
	{
		$restaurantHourResults = [
			[
				"id" => 0,
				"branch_id" => $id,
				"closed" => 1,
				"day" => "Sunday",
				"shift" => "Day",
				"opening_time" => "00:00",
				"closing_time" => "00:00",
				"difference_checker" => "None"
			],
			[
				"id" => 0,
				"branch_id" => $id,
				"closed" => 1,
				"day" => "Monday",
				"shift" => "Day",
				"opening_time" => "00:00",
				"closing_time" => "00:00",
				"difference_checker" => "None"
			],
			[
				"id" => 0,
				"branch_id" => $id,
				"closed" => 1,
				"day" => "Tuesday",
				"shift" => "Day",
				"opening_time" => "00:00",
				"closing_time" => "00:00",
				"difference_checker" => "None"
			],
			[
				"id" => 0,
				"branch_id" => $id,
				"closed" => 1,
				"day" => "Wednesday",
				"shift" => "Day",
				"opening_time" => "00:00",
				"closing_time" => "00:00",
				"difference_checker" => "None"
			],
			[
				"id" => 0,
				"branch_id" => $id,
				"closed" => 1,
				"day" => "Thursday",
				"shift" => "Day",
				"opening_time" => "00:00",
				"closing_time" => "00:00",
				"difference_checker" => "None"
			],
			[
				"id" => 0,
				"branch_id" => $id,
				"closed" => 1,
				"day" => "Friday",
				"shift" => "Day",
				"opening_time" => "00:00",
				"closing_time" => "00:00",
				"difference_checker" => "None"
			],
			[
				"id" => 0,
				"branch_id" => $id,
				"closed" => 1,
				"day" => "Saturday",
				"shift" => "Day",
				"opening_time" => "00:00",
				"closing_time" => "00:00",
				"difference_checker" => "None"
			]
		];
		return $restaurantHourResults;
	}

	public function getMultipleRestaurantHourList($id)
	{
		$restaurantHourResults = [
			[
				"id" => 0,
				"branch_id" => $id,
				"closed" => 1,
				"day" => "Sunday",
				"shift" => "Day",
				"opening_time" => "00:00",
				"closing_time" => "00:00",
				"difference_checker" => "None"
			],
			[
				"id" => 0,
				"branch_id" => $id,
				"closed" => 1,
				"day" => "Sunday",
				"shift" => "Evening",
				"opening_time" => "00:00",
				"closing_time" => "00:00",
				"difference_checker" => "None"
			],
			[
				"id" => 0,
				"branch_id" => $id,
				"closed" => 1,
				"day" => "Monday",
				"shift" => "Day",
				"opening_time" => "00:00",
				"closing_time" => "00:00",
				"difference_checker" => "None"
			],
			[
				"id" => 0,
				"branch_id" => $id,
				"closed" => 1,
				"day" => "Monday",
				"shift" => "Evening",
				"opening_time" => "00:00",
				"closing_time" => "00:00",
				"difference_checker" => "None"
			],
			[
				"id" => 0,
				"branch_id" => $id,
				"closed" => 1,
				"day" => "Tuesday",
				"shift" => "Day",
				"opening_time" => "00:00",
				"closing_time" => "00:00",
				"difference_checker" => "None"
			],
			[
				"id" => 0,
				"branch_id" => $id,
				"closed" => 1,
				"day" => "Tuesday",
				"shift" => "Evening",
				"opening_time" => "00:00",
				"closing_time" => "00:00",
				"difference_checker" => "None"
			],
			[
				"id" => 0,
				"branch_id" => $id,
				"closed" => 1,
				"day" => "Wednesday",
				"shift" => "Day",
				"opening_time" => "00:00",
				"closing_time" => "00:00",
				"difference_checker" => "None"
			],
			[
				"id" => 0,
				"branch_id" => $id,
				"closed" => 1,
				"day" => "Wednesday",
				"shift" => "Evening",
				"opening_time" => "00:00",
				"closing_time" => "00:00",
				"difference_checker" => "None"
			],
			[
				"id" => 0,
				"branch_id" => $id,
				"closed" => 1,
				"day" => "Thursday",
				"shift" => "Day",
				"opening_time" => "00:00",
				"closing_time" => "00:00",
				"difference_checker" => "None"
			],
			[
				"id" => 0,
				"branch_id" => $id,
				"closed" => 1,
				"day" => "Thursday",
				"shift" => "Evening",
				"opening_time" => "00:00",
				"closing_time" => "00:00",
				"difference_checker" => "None"
			],
			[
				"id" => 0,
				"branch_id" => $id,
				"closed" => 1,
				"day" => "Friday",
				"shift" => "Day",
				"opening_time" => "00:00",
				"closing_time" => "00:00",
				"difference_checker" => "None"
			],
			[
				"id" => 0,
				"branch_id" => $id,
				"closed" => 1,
				"day" => "Friday",
				"shift" => "Evening",
				"opening_time" => "00:00",
				"closing_time" => "00:00",
				"difference_checker" => "None"
			],
			[
				"id" => 0,
				"branch_id" => $id,
				"closed" => 1,
				"day" => "Saturday",
				"shift" => "Day",
				"opening_time" => "00:00",
				"closing_time" => "00:00",
				"difference_checker" => "None"
			],
			[
				"id" => 0,
				"branch_id" => $id,
				"closed" => 1,
				"day" => "Saturday",
				"shift" => "Evening",
				"opening_time" => "00:00",
				"closing_time" => "00:00",
				"difference_checker" => "None"
			]
		];
		return $restaurantHourResults;
	}
}