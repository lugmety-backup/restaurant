<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/3/17
 * Time: 2:00 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repo\CategoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repo\CategoryTranslationInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;
use RoleChecker;
use LogStoreHelper;
use Illuminate\Support\Facades\Validator;
use AuthChecker;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Datatables;
use ImageUploader;
use LocationService;
use Bschmitt\Amqp\Facades\Amqp;

class CategoryController extends Controller
{
    protected $category;
    protected $categoryTranslation;
    protected $checker;
    protected $logStoreHelper;

    /**
     * CategoryController constructor.
     * @param CategoryInterface $category
     * @param RoleChecker $checker
     * @param CategoryTranslationInterface $categoryTranslation
     * @param LogStoreHelper $logStoreHelper
     */

    public function __construct(CategoryInterface $category, RoleChecker $checker,
                                CategoryTranslationInterface $categoryTranslation,
                                LogStoreHelper $logStoreHelper)
    {
        $this->category = $category;
        $this->categoryTranslation = $categoryTranslation;
        $this->checker = $checker;
        $this->logStoreHelper = $logStoreHelper;

    }

    /**
     * Display the list of categories
     * If 'lang' parameter is not supplied,then default language is set to 'en'.
     * If 'status' parameter is not supplied, Authorized personnel can view all the data i.e. of both status 0 and 1.
     * If 'status' parameter is supplied, Authorized personnel can view data having requested status. Others can view data having status 1 in any case.
     * 'limit' parameter is used to get no. of records, user want to view in per page.
     * If 'limit' parameter is not supplied, only 10 records are shown per page. Else, no. of records is equal to limit supplied.
     * If 'sort_by' parameter is not suppied, the data is displayed in descending order. Else, data is displayed in order supplied.
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        /*
         * get all food categories
         * */
        //return view('test');
        try {
            $status = Input::get('status');
            if (isset($status)) {
                $request['status'] = Input::get("status");

            }

            $request['lang'] = Input::get("lang", "en");
            $request['sort_by'] = Input::get("sort_by", "desc");
            $request['limit'] = Input::get("limit", 10);
            $request['country_id'] = Input::get("country_id", null);
            /*
             * Validate the status and limit
             * Status must be 0 or 1.
             * limit must be integer.
             *
             * */
            $rules = [
                'status' => 'sometimes|required|integer|min:0|max:1',
                'limit' => 'integer',
                "country_id" => "required|integer|min:0"//added by ashish country id validation
            ];
            $validator = Validator::make($request, $rules);
            if ($validator->fails()) {
                return response()->json([
                    "status" => '422',
                    "message" => $validator->errors()
                ], 422);
            }

            /**
             *
             * check the authority of the logged in user and set the status according to the output of check function of AuthChecker facade.
             * if output is "true", set status to all ie user can view all addon according to restaurant
             *if output  is "false", set status to 1 i.e. user can only view addon of status 1.
             * * if status and sort by is null, then data having status=1 and sort_by=desc
             *
             */

                $request['status'] = 1;

            /**
             * Fetch data according to the request provided.
             * If data is available, throw exception message
             */
            //dd($request['status']);
            $categories = $this->category->getAllCategory($request['status'], $request['sort_by'], $request['limit'], $request['country_id']);
            try {
                if (!$categories->first()) {
                    throw  new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Empty Record"
                ], 404);
            }
            foreach ($categories as $key => $category) {
                /**
                 * Get category Translation based on @param lang
                 * if empty record is obtained default en lang is returned
                 *If default en lang is not available, throw exception
                 * */
                $categoryTranslation = $this->categoryTranslation->getSpecificCategoryTransalationByLang($category->id, $request['lang']);
                try {
                    if ($categoryTranslation->count() == 0) {
                        if ($request['lang'] == 'en') {
                            throw new \Exception();
                        } else {
                            $categoryTranslation = $this->categoryTranslation->getSpecificCategoryTransalationByLang($category->id, 'en');
                            if ($categoryTranslation->count() == 0) {
                                throw new \Exception();
                            }
                        }

                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Default language english not found in database"
                    ], 404);
                }

                /*
                 * attach name and lang_code in $category variable
                 * */
                $category['name'] = $categoryTranslation[0]['name'];
                $category['lang'] = $categoryTranslation[0]['lang'];
            }

            return response()->json([
                'status' => "200",
                "data" => $categories
            ], 200);

        } catch (\Exception $ex) {

            return response()->json([
                "status" => "404",
                "message" => "something went wrong"
            ], 404);
        }
    }

    /**
     *
     * Store category
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */


    public function store(Request $request)
    {
        /**
         * Check if logged in user has authority to insert data.
         * If 'true', continue. Else abort with exception message.
         *
         * Validate the request
         * sort field is required and must be integer.
         * status field is required and must be integer. Only 0 or 1 is acceptable.
         * translation field is required and must be an array
         * If validation fails throw error message and abort.
         */
        if (AuthChecker::check()) {
            try {
                $this->validate($request, [//validating the request
                    "sort" => "required|integer",
                    "status" => "required|integer|min:0|max:1",
                    "translation" => "required|array",
                    "country_id" => "required|integer"
                ]);

            } catch (\Exception $e) {

                return response()->json([
                    "status" => "422",
                    "message" => $e->response->original
                ]);
            }


            try {
                /**
                 * Start DB transaction to ensure the operation is reversed in case not successfully committed.
                 *
                 */
                DB::beginTransaction();
                /**
                 * Check if request has image.
                 * If image is present save the image and return location of image.
                 */
                $countryValidate = LocationService::checkLanguageCode($request['country_id'], $request['translation']);

                if ($countryValidate['status'] != 200) {
                    return response()->json([
                        "status" => $countryValidate["status"],
                        "message" => $countryValidate["message"]
                    ], $countryValidate["status"]);

                }

                if ($request->has('image')) {

                    $imageUpload = $this->saveImage($request['image'], "image");
                    if ($imageUpload["status"] != 200) {
                        return response()->json(
                            $imageUpload["message"]
                            , $imageUpload["status"]);
                    }
                    $request['image'] = $imageUpload["message"]["data"];

                }
                /**
                 * Insert Data in category table.
                 */

                $categories = $this->category->createCategory($request->all());

                foreach ($request['translation'] as $key => $translation) {
                    $translation['category_id'] = $categories->id;
                    $translation['lang'] = $key;
                    /**
                     * Validate lang and name fields.
                     * lang field is required and must be alphabets.
                     * name field is required.
                     * If validation fails, abort with validation error message.
                     * Else insert data in category_translation table.
                     */
                    $rules = [
                        "lang" => 'required|alpha',
                        "name" => "required"
                    ];
                    $translation['lang'] = str_slug($translation['lang']);

                    $validator = Validator::make($translation, $rules);
                    if ($validator->fails()) {
                        $error = $validator->errors();
                        $this->logStoreHelper->storeLogError([
                            "Validation Error", [
                                'status' => '422',
                                "message" => [$key => $error]
                            ]
                        ]);
                        return response()->json([
                            'status' => '422',
                            "message" => [$key => $error]
                        ], 422);
                    }
                    /**
                     * Insert data into category_translation
                     */

                    $createTranslation = $this->categoryTranslation->createCategoryTranslation($translation);


                }
                $this->logStoreHelper->storeLogInfo([
                    "Category",
                    [
                        "status" => "200",
                        "message" => "Category Created Successfully"
                    ]
                ]);
                DB::commit();
                return response()->json([
                    "status" => "200",
                    "message" => "Category Created Successfully"
                ], 200);

            } catch (\Exception $e) {
                return response()->json([
                    "status" => "422",
                    "message" => "There was error in creating Category"
                ], 422);
            }
        } else {
            return response()->json([
                'status' => "401",
                "message" => "Only Admin or Super Admin can create Category "
            ], 401);
        }


    }

    /**
     *  Delete Category.
     * only Admin and super admin
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function destroy($id)
    {
        /**
         * Check if logged in user has authority to delete data.
         * If 'true', continue. Else abort with exception message.
         */

        if (AuthChecker::check()) {
            try {
                /*
                 * delete category along with its child rows
                 *
                 *
                 * */
                $category = $this->category->getSpecificCategory($id);

                $this->category->deleteCategory($id);
                /**
                 * if category has image, delete the image file from the location
                 */
//                if (!empty($category->image)) {
//                    $this->deleteFile($category->image);
//                }
                return response()->json([
                    "status" => "200",
                    "message" => "Requested Category Deleted Successfully"
                ], 200);

            } catch (ModelNotFoundException $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Requested Category could not be found"
                ], 404);

            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "422",
                    "message" => "There were problem deleting Category"
                ], 422);
            }
        } else {
            return response()->json([
                "status" => "401",
                "message" => "Only Admin or Super Admin can delete Category "
            ], 401);
        }


    }

    /**
     * Display category of given id.
     * If status of required data is 0, then only authorized personnel can view the record. Unauthorized can only view data having status 1.
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function show($id)
    {

        try {

            $request['lang'] = Input::get("lang", "en");

            try {

                $category = $this->category->getSpecificCategory($id);
                /**
                 * Check if category is active.
                 * If category is inactive, throw exception for unauthorized user.
                 * Display data for authorized users.
                 */
                if ($category['status'] == 0) {
                    if (!AuthChecker::check()) {
                        throw new \Exception();
                    }
                }

            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Requested Category could not be found"
                ], 404);
            }
            /*
             * Get category Translation based on @param lang
             * if empty record is obtained default en lang is returned
             *
             * */
            $categoryTranslation = $this->categoryTranslation->getSpecificCategoryTransalationByLang($category->id, $request['lang']);
            try {
                if ($categoryTranslation->count() == 0) {
                    if ($request['lang'] == 'en') {
                        throw new \Exception();
                    } else {
                        $categoryTranslation = $this->categoryTranslation->getSpecificCategoryTransalationByLang($category->id, 'en');
                        if ($categoryTranslation->count() == 0) {
                            throw new \Exception();
                        }
                    }

                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Default language english not found in database"
                ], 404);
            }


            /*
             * attach name and lang in $restaurant variable
             *
             * */
            $category['name'] = $categoryTranslation[0]['name'];
            $category['lang'] = $categoryTranslation[0]['lang'];
            return response()->json([
                "status" => "200",
                "data" => $category
            ], 200);

        } catch (ModelNotFoundHttpException $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Requested Category could not be found"
            ], 404);
        }
    }

    /**
     * Update category of given id. Only admin or super admin can update category
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function update(Request $request, $id)
    {
        /**
         * Check if logged in user has authority to insert data.
         * If 'true', continue. Else abort with exception message.
         *
         * Validate the request
         * sort field is required and must be integer.
         * status field is required and must be integer. Only 0 or 1 is acceptable.
         * translation field is required and must be an array
         * If validation fails throw error message and abort.
         */

        if (AuthChecker::check()) {
            try {
                $category = $this->category->getSpecificCategory($id);

                try {
                    $this->validate($request, [
                        "sort" => "required|integer",
                        "status" => "required|integer|min:0|max:1",
                        "translation" => "required|array",
                        "country_id" => "required"
                    ]);

                } catch (\Exception $e) {
                    return response()->json([
                        "status" => "422",
                        "message" => $e->response->original
                    ]);
                }

                $countryValidate = LocationService::checkLanguageCode($request['country_id'], $request['translation']);

                if ($countryValidate['status'] != 200) {
                    return response()->json([
                        "status" => $countryValidate["status"],
                        "message" => $countryValidate["message"]
                    ], $countryValidate["status"]);

                }


                DB::beginTransaction();
                /**
                 * If request has image, save the image file and get the location if it.
                 */
                if ($request->has('image')) {

                    $imageUpload = $this->saveImage($request['image'], "image");
                    if ($imageUpload["status"] != 200) {
                        return response()->json(
                            $imageUpload["message"]
                            , $imageUpload["status"]);
                    }
                    $request['image'] = $imageUpload["message"]["data"];

                }
                /**
                 * If request has image, delete the image file.
                 */
//                if (!empty($category->image)) {
//                    $this->deleteFile($category->image);
//                }
                /**
                 * update the data for given id
                 */

                $categories = $this->category->updateCategory($id, $request->all());
                /*
                * Delete related translations of given category and reenter data
                * */

                $this->categoryTranslation->deleteTranslationByCategory($id);
                /*
                * translation field validation
                * */
                foreach ($request['translation'] as $key => $translation) {
                    $translation['category_id'] = $id;
                    $translation['lang'] = $key;
                    /**
                     * Validate lang and name fields.
                     * lang field is required and must be alphabets.
                     * name field is required.
                     * If validation fails, abort with validation error message.
                     * Else insert data in category_translation table.
                     */
                    $rules = [
                        "lang" => 'required|alpha',
                        "name" => "required"
                    ];
                    $translation['lang'] = str_slug($translation['lang']);

                    $validator = Validator::make($translation, $rules);
                    if ($validator->fails()) {
                        $error = $validator->errors();
                        $this->logStoreHelper->storeLogError([
                            "Validation Error", [
                                "status" => '422',
                                "message" => [$key => $error]
                            ]

                        ]);
                        return response()->json([
                            "status" => '422',
                            "message" => [$key => $error]
                        ], 422);
                    }
                    /**
                     * re-enter data for translation related to given category id
                     */

                    $createTranslation = $this->categoryTranslation->createCategoryTranslation($translation);


                }
                $this->logStoreHelper->storeLogInfo([
                    "Category", [
                        "status" => "200",
                        "message" => "Category Updated Successfully",
                        "data" => $categories
                    ]
                ]);

                DB::commit();
                return response()->json([
                    "status" => "200",
                    "message" => "Category updated Successfully"
                ], 200);

            } catch (\Exception $e) {
                return response()->json([
                    "status" => "404",
                    "message" => "Requested Category could not be found"
                ], 404);
            }

        } else {
            return response()->json([
                "status" => "401",
                "message" => "Only Admin or Super Admin can update Category "
            ], 401);
        }

    }

    /**
     *Upload the image to folder of the given path and returns the path of the saved image
     * @param $image
     * @return string
     */

    protected function saveImage($image, $type)
    {
        $imaged['avatar'] = $image;
        $imaged["type"] = $type;
        $imageUpload = ImageUploader::upload($imaged);
        return $imageUpload;


    }

    /**
     * Delete image from the folder incase data related to that image is deleted.
     * @param $image
     * @return bool
     */

    protected function deleteFile($image)
    {
        $image = substr($image, 1);
        File::delete($image);
        return true;
    }

    /**
     * Get all the category data for datatable.
     * Category name is only available in english.
     * @param Request $request
     * @return mixed
     */

    public function getForDatatable()
    {
        //if ($request->ajax()) {
        $countryId = Input::get("country_id");
        if (isset($countryId)) {
            $request['country_id'] = $countryId;
            $rules = [
                'country_id' => 'sometimes|required|integer',
            ];
            $validator = Validator::make($request, $rules);
            if ($validator->fails()) {
                return response()->json([
                    "status" => '422',
                    "message" => $validator->errors()
                ]);
            }
        } else {
            $request['country_id'] = "all";
        }
        try {
            $categoryData = new Collection($this->category->getCategoryForDatatable($request['country_id']));
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage()
            ],500);
        }

        return Datatables::of($categoryData)
            ->addColumn('name', function ($category) {
                return $category->categoryTranslation->map(function ($translation) {
                    return $translation->name;
                })->implode('');
            })->make(true);
        //}
    }

    /**
     * Display all the translations for given category to display in the admin dashboard
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function adminShow($id)
    {
        if (AuthChecker::check()) {
            try {
                /**
                 * Get specific category from category table
                 */
                $category = $this->category->getSpecificCategory($id);

                $categoryTranslation = $category->categoryTranslation;

                /*
                 * attach name and lang in $restaurant variable
                 *
                 * */
                $category['translation'] = $categoryTranslation;
                //$category['lang'] = $categoryTranslation[0]['lang'];
                return response()->json([
                    "status" => "200",
                    "data" => $category
                ], 200);

            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Requested Category could not be found"
                ], 404);
            }
        } else {
            return response()->json([
                "status" => "401",
                "message" => "Unauthorized action"
            ], 401);
        }
    }

    public function activeCategoriesForAdmin($countryId)
    {
        /*
         * get all food categories
         * */
        if (AuthChecker::check()) {
            try {

                /**
                 * Fetch data according to the request provided.
                 * If data is available, throw exception message
                 */
                //dd($request['status']);
                $categories = $this->category->activeCategoriesForAdmin($countryId);
                try {
                    if (!$categories->first()) {
                        throw  new \Exception();
                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Empty Record"
                    ], 404);
                }
                foreach ($categories as $key => $category) {
                    /**
                     * Get category Translation based on @param lang
                     * if empty record is obtained default en lang is returned
                     *If default en lang is not available, throw exception
                     * */
                    $categoryTranslation = $category->categoryTranslation()->where('lang','en')->get();
                    try {
                        if ($categoryTranslation->count() == 0) {

                                throw new \Exception();

                            }


                    } catch (\Exception $ex) {
                        return response()->json([
                            "status" => "404",
                            "message" => "Default language english not found in database"
                        ], 404);
                    }

                    /*
                     * attach name and lang_code in $category variable
                     * */
                    $category['name'] = $categoryTranslation[0]['name'];
                    $category['lang'] = $categoryTranslation[0]['lang'];
                }

                return response()->json([
                    'status' => "200",
                    "data" => $categories
                ], 200);

            } catch (\Exception $ex) {

                return response()->json([
                    "status" => "404",
                    "message" => "something went wrong"
                ], 404);
            }
        }else {
            return response()->json([
                "status" => "401",
                "message" => "Only Admin or Super Admin can view Category "
            ], 401);
        }
    }



}