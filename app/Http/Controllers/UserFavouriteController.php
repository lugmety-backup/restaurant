<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 7/20/17
 * Time: 11:48 AM
 */

namespace App\Http\Controllers;

use App\Repo\UserFavouriteInterface;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Support\Facades\Config;
use LogStoreHelper;
use RoleChecker;
use Illuminate\Http\Request;
use App\Repo\FoodInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Input;

class UserFavouriteController extends Controller
{
    protected $favourite;
    protected $logStoreHelper;
    protected $restaurantBranch;
    protected $food;

    public function __construct(UserFavouriteInterface $favourite, LogStoreHelper $logStoreHelper, FoodInterface $food)
    {
        $this->favourite = $favourite;
        $this->logStoreHelper = $logStoreHelper;
        $this->food = $food;
    }

    public function index()
    {
        try {
            $request['limit'] = Input::get('limit', 10);
            $lang = Input::get('lang', "en");
            $userId = RoleChecker::getUser();
            $favData = $this->favourite->getFavOfUser($userId, $request['limit']);


            foreach ($favData as $key=>$fav) {
                $food = $fav->food;
                if(empty($food))
                {
                    unset($favData[$key]);
                    continue;
                }

                $foodTranslation = $food->foodTranslation()->where('lang', $lang)->get();
                try {
                    if ($foodTranslation->count() == 0) {
                        if ($lang == 'en') {
                            throw new \Exception();
                        } else {
                            $foodTranslation = $food->foodTranslation()->where('lang', 'en')->get();
                            if ($foodTranslation->count() == 0) {
                                throw new \Exception();
                            }
                        }

                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Default language english not found in database"
                    ], 404);
                }

                $restaurant = $food->restaurantBranch;

                $restaurantTranslation = $restaurant->branchTranslation()->where('lang_code', $lang)->get();
                try {
                    if ($restaurantTranslation->count() == 0) {
                        if ($lang == 'en') {
                            throw new \Exception();
                        } else {
                            $restaurantTranslation = $restaurant->branchTranslation()->where('lang_code', 'en')->get();
                            if ($restaurantTranslation->count() == 0) {
                                throw new \Exception();
                            }
                        }

                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Default language english not found in database"
                    ], 404);

                }
                if (!empty($food['featured_image'])) {
                    /**
                     * get position of last occurance of / in fetaured_image and added the postion by 1
                     */
                    $getpositionOfImageInUrl = strrpos($food->featured_image, "/") + 1;
                    /**
                     * add normal- prefix in $getpositionOfImageInUrl postition
                     */
                    $food["featured_image_resized"] = substr_replace($food->featured_image, "normal-", $getpositionOfImageInUrl, 0);
                } else {
                    $food["featured_image_resized"] = [];
                }

                $fav['food_id'] = $food['id'];
                $fav['price'] = $food['price'];

                $fav['featured_image'] = $food['featured_image'];
                $fav['featured_image_resized'] = $food["featured_image_resized"];
                $fav['food_name'] = $foodTranslation[0]['name'];
                $fav['food_description'] = $foodTranslation[0]['description'];
                $fav['restaurant_id'] = $food['restaurant_id'];
                $fav['restaurant_name'] = $restaurantTranslation[0]['name'];
                $fav['restaurant_address'] = $restaurantTranslation[0]['address'];
                $fav['restaurant_logo'] = $restaurant['logo'];
                $fav['restaurant_slug'] = $restaurant['slug'];
                $fav['country_id'] = $restaurant['country_id'];
                $country_id_list[] = $restaurant['country_id'];

            }

            $country_id_list = collect($country_id_list)->unique();
            foreach ($country_id_list as $d){
                $country_id_list_data['ids'][] = $d;
            }

            $url = Config::get("config.location_url");
            $country = \RemoteCall::store($url . "/country/list",$country_id_list_data);

            if ($country["status"] != "200") {
                return response()->json([
                    "status" => $country["status"],
                    "message" => $country["message"]
                ], $country["status"]);
            }
            $values = $country['message']['data'];
            foreach ($favData as $key=>$fav) {
                foreach ($values as $value){
                    if($fav['country_id'] == $value['id']){
                        $favData[$key]['currency_symbol'] = $value['currency_symbol'];
                    }
                }
            }



            $favData = $this->manualPagination($request['limit'], array_values($favData->toArray()), count($favData));
            return response()->json([
                "status" => "200",
                "data" => $favData->withPath("/public/user-favourite")->appends(["limit" => $request['limit'],"lang" => $lang])
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Empty Record"
            ], 404);
        }

    }

    public function indexShowingIdOnly()
    {
        try {
            $request['limit'] = Input::get('limit', 10);
            $lang = Input::get('lang', "en");
            $userId = RoleChecker::getUser();
            $favData = $this->favourite->getFavOfUser($userId, $request['limit']);
            $favDataReturn= [];
            foreach ($favData as $key=>$fav) {
                $food = $fav->food;
                if(empty($food))
                {
                    unset($favData[$key]);
                    continue;
                }

                $foodTranslation = $food->foodTranslation()->where('lang', $lang)->get();
                try {
                    if ($foodTranslation->count() == 0) {
                        if ($lang == 'en') {
                            throw new \Exception();
                        } else {
                            $foodTranslation = $food->foodTranslation()->where('lang', 'en')->get();
                            if ($foodTranslation->count() == 0) {
                                throw new \Exception();
                            }
                        }

                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Default language english not found in database"
                    ], 404);
                }

                $restaurant = $food->restaurantBranch;
                $restaurantTranslation = $restaurant->branchTranslation()->where('lang_code', $lang)->get();
                try {
                    if ($restaurantTranslation->count() == 0) {
                        if ($lang == 'en') {
                            throw new \Exception();
                        } else {
                            $restaurantTranslation = $restaurant->branchTranslation()->where('lang_code', 'en')->get();
                            if ($restaurantTranslation->count() == 0) {
                                throw new \Exception();
                            }
                        }

                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Default language english not found in database"
                    ], 404);

                }
                if (!empty($food['featured_image'])) {
                    /**
                     * get position of last occurance of / in fetaured_image and added the postion by 1
                     */
                    $getpositionOfImageInUrl = strrpos($food->featured_image, "/") + 1;
                    /**
                     * add normal- prefix in $getpositionOfImageInUrl postition
                     */
                    $food["featured_image_resized"] = substr_replace($food->featured_image, "normal-", $getpositionOfImageInUrl, 0);
                } else {
                    $food["featured_image_resized"] = [];
                }
                $favDataReturn[] = $fav['food_id'];
            }
            $favData = $this->manualPagination($request['limit'], $favDataReturn, count($favDataReturn));
            return response()->json([
                "status" => "200",
                "data" => $favData->withPath("/public/user-all-favourite")->appends(["limit" => $request['limit'],"lang" => $lang])
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Empty Record"
            ], 404);
        }

    }

    public function store(Request $request)
    {
        $lang = Input::get('lang','en');


        try {
            $this->validate($request, [
                "food_id" => "required|integer",
            ]);

        } catch (\Exception $e) {

            return response()->json([
                "status" => "422",
                "message" => $e->response->original
            ]);
        }

        try {
            $food = $this->food->getActiveSpecificFood($request['food_id']);

            $request['user_id'] = RoleChecker::getUser();

            /*********** if food is already added to favourite then unfavourite the food **********/
            $favourite = $this->favourite->getSpecificFavourite2($request['food_id'],$request['user_id']);
            if($favourite != null){
                $this->favourite->deleteFavourite($favourite->id,$favourite->user_id);

                try{
                    $data = \Settings::getErrorMessage('food-is-set-as-not-favourite');
                    $error_message = $data['message'][$lang];
                    return response()->json([
                        "status" => "200",
                        "message" => $error_message,
                        "flag" => "false"
                    ], 200);
                }catch (\Exception $ex){
                    return response()->json([
                        "status" => "200",
                        "message" => "Food is set as not favourite.",
                        "flag" => "false"
                    ], 200);
                }

            }
            /**************** unfavourite food ends **************/

            $create = $this->favourite->createFavourite($request->only('user_id', 'food_id'));

            $this->logStoreHelper->storeLogInfo([
                "userFavorite",
                [
                    "status" => "200",
                    "message" => "Favourite Created Successfully"
                ]
            ], 200);
            try{
                $data = \Settings::getErrorMessage('food-is-set-as-favourite');
                if ($data["status"] != 200) {
                    return response()->json(
                        $data["message"]
                        , $data["status"]);
                }
                $error_message = $data['message'][$lang];
                return response()->json([
                    "status" => "200",
                    "message" => $error_message,
                    "flag" => "true"
                ], 200);
            }catch (\Exception $ex){
                return response()->json([
                    'status' => "200",
                    "message" => "Food is set as favourite.",
                    "flag" => "true"
                ], 200);
            }


        } catch (ModelNotFoundException $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Empty Record for requested food"
            ], 404);
        } catch (\Exception $exception) {
            return response()->json([
                "status" => "409",
                "message" => "Food already added to favourite"
            ], 409);
        }

    }


    public function destroy($id)
    {

        $userId = RoleChecker::getUser();

        try {
            /**
             * delete Favourite along with its translation
             */

            $this->favourite->deleteFavourite($id, $userId);

            return response()->json([
                "status" => "200",
                "message" => "Requested Favourite Deleted Successfully"
            ], 200);

        } catch (ModelNotFoundException $notFoundException) {
            /**
             * If requested data is not found, return 404 error with message.
             */
            return response()->json([
                "status" => '404',
                "message" => "Requested Favourite not found"
            ], 404);

        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => "There were problem deleting Favourite"
            ], 422);
        }

    }

    private function manualPagination($limit, $data, $count)
    {
        $page = Input::get('page', 1);
        $paginate = $limit;
        $offSet = ($page-1) * $limit;
//        dd($data);
        $itemsForCurrentPage = array_slice($data, $offSet, $paginate, false);
        return new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, $count, $paginate, $page);

    }
}