<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 10/8/17
 * Time: 11:29 AM
 */

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class ExportController extends Controller
{
    private $db_ext;
    private $db_ext1;

    public function __construct()
    {
        $this->db_ext = DB::connection('mysql_external');
        $this->db_ext1 = DB::connection('mysql_external1');
    }


    public function exportRestaurant()
    {
        try {

            $oldRestaurant = $this->db_ext->table('restaurants')->get();
            //return $oldRestaurant;

            DB::beginTransaction();
            $i = 0;
            foreach ($oldRestaurant as $restaurant) {
                unset($insertRestaurant);
                unset($insertRestaurantTranslation1);
                unset($insertRestaurantTranslation2);
                unset($insertResMeta);
                unset($insertResAdmin);

                $city = $this->db_ext->table('districts')->join('cities', 'districts.city_id', '=', 'cities.id')->where('districts.id', $restaurant->district_id)->pluck('cities.name');

                if (count($city) > 0) {

                    $insertResAdmin['city'] = $city[0];
                    $insertResAdmin['address_line_1'] = !empty($restaurant->address) ? $restaurant->address : 'Something';

                }

                $insertResAdmin['first_name'] = $restaurant->name;
                $insertResAdmin['last_name'] = 'Administrator';
                $insertResAdmin['password'] = app("hash")->make('123456');
                $insertResAdmin['gender'] = 'male';
                $insertResAdmin['mobile'] = !empty($restaurant->mobile_no) ? $restaurant->mobile_no : 123456789;
                $insertResAdmin['status'] = $restaurant->status;
                $insertResAdmin['email'] = str_replace("-", ".", strtolower($restaurant->slug_profile)) . ".admin@lugmety.com";
                $insertResAdmin['country'] = 'Saudi Arabia';
                $insertResAdmin['country_code'] = '+966';


                $id = $this->db_ext1->table('users')->insertGetId($insertResAdmin);

                $insertResMeta['user_id'] = $id;
                $insertResMeta['is_restaurant_admin'] = 1;
                $insertResMeta['restaurant_id'] = $restaurant->id;

                $this->db_ext1->table('restaurant_user_meta')->insert($insertResMeta);

                $this->db_ext1->table('role_user')->insert([
                    'user_id' => $id,
                    'role_id' => 5

                ]);


                $insertRestaurant['id'] = $restaurant->id;
                $insertRestaurant['address'] = !empty($restaurant->address) ? $restaurant->address : 'Something';
                $insertRestaurant['phone_no'] = !empty($restaurant->phone_no) ? $restaurant->phone_no : 123456789;
                $insertRestaurant['mobile_no'] = !empty($restaurant->mobile_no) ? $restaurant->mobile_no : 123456789;
                $insertRestaurant['email_address'] = $insertResAdmin['email'];
                $insertRestaurant['website'] = 'http://www.asdfaf.com';
                $insertRestaurant['contact_person'] = $restaurant->name;
                $insertRestaurant['commission_rate'] = $restaurant->site_commission_rate;
                $insertRestaurant['status'] = $restaurant->status;
                $insertRestaurant['country_id'] = 1;

                $insertRestaurant['created_at'] = $restaurant->created_at;
                $insertRestaurant['updated_at'] = $restaurant->updated_at;
                $insertRestaurant['deleted_at'] = $restaurant->is_deleted == "N" ? null : $restaurant->updated_at;

                $insertRestaurantTranslation1['lang_code'] = 'en';
                $insertRestaurantTranslation1['name'] = $restaurant->name;
                $insertRestaurantTranslation1['restaurant_id'] = $restaurant->id;

                DB::table('restaurants')->insert($insertRestaurant);


                DB::table('restaurant_translation')->insert($insertRestaurantTranslation1);

                if (!empty($restaurant->ar_name)) {
                    $insertRestaurantTranslation2['lang_code'] = 'ar';
                    $insertRestaurantTranslation2['name'] = $restaurant->ar_name;
                    $insertRestaurantTranslation2['restaurant_id'] = $restaurant->id;

                    DB::table('restaurant_translation')->insert($insertRestaurantTranslation2);
                }

            }
            DB::commit();

            return response()->json([

                'message' => 'data successfully migrated',
                'status' => '200'

            ], 200);

        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'status' => '400'
            ], 400);
        }

    }


    public function exportBranch()
    {
        try {

            $oldRestaurant = $this->db_ext->table('restaurants')->get();
            //return $oldRestaurant;

            DB::beginTransaction();

            foreach ($oldRestaurant as $restaurant) {
                unset($insertRestaurant);
                unset($insertRestaurantTranslation1);
                unset($insertRestaurantTranslation2);
                unset($insertResMeta);
                unset($insertResAdmin);
                unset($insertHour);

                $city = $this->db_ext->table('districts')->join('cities', 'districts.city_id', '=', 'cities.id')->where('districts.id', $restaurant->district_id)->select('cities.name', 'cities.id')->get();

                if (count($city) > 0) {

                    $insertResAdmin['city'] = $city[0]->name;
                    $insertResAdmin['address_line_1'] = !empty($restaurant->address) ? $restaurant->address : 'Something';

                    $insertRestaurant['city_id'] = $city[0]->id;

                }
                $user = $this->db_ext->table('user_restaurant_owners')->join('users', 'user_restaurant_owners.user_id', '=', 'users.id')->where('user_restaurant_owners.id', $restaurant->user_restaurant_owner_id)->select('users.*')->get()->toArray();

                if (!empty($restaurant->google_map_coordinate)) {
                    $coordinates = explode(",", $restaurant->google_map_coordinate);
                }
                if (!empty($restaurant->working_hours)) {
                    $hours = unserialize($restaurant->working_hours);
                }


                $insertRestaurant['id'] = $restaurant->id;
                $insertRestaurant['country_id'] = 1;
                $insertRestaurant['district_id'] = $restaurant->district_id;
                $insertRestaurant['logo'] = !empty($restaurant->profile_image) ? "/" . $restaurant->profile_image : '/images/34716633959d9fecedae91.png';
                $insertRestaurant['cover_image'] = '';
                $insertRestaurant['phone_no'] = !empty($restaurant->phone_no) ? $restaurant->phone_no : 123456789;
                $insertRestaurant['mobile_no'] = !empty($restaurant->mobile_no) ? $restaurant->mobile_no : 123456789;
                $insertRestaurant['email_address'] = $user[0]->email;
                $insertRestaurant['slug'] = $restaurant->slug_profile;
                $insertRestaurant['min_purchase_amt'] = 0;
                $insertRestaurant['max_purchase_amt'] = 0;
                $insertRestaurant['no_of_seats'] = !empty($restaurant->no_of_seats) ? $restaurant->no_of_seats : 0;
                $insertRestaurant['available_seats'] = !empty($restaurant->available_seats) ? $restaurant->available_seats : 0;
                $insertRestaurant['commission_rate'] = $restaurant->site_commission_rate;
                $insertRestaurant['status'] = $restaurant->status;
                $insertRestaurant['is_featured'] = $restaurant->is_featured == "Y" ? 1 : 0;
                $insertRestaurant['pre_order'] = 0;
                $insertRestaurant['latitude'] = isset($coordinates[0]) ? ($coordinates[0]) : null;
                $insertRestaurant['longitude'] = isset($coordinates[1]) ? trim($coordinates[01]) : null;
                $insertRestaurant['machine_ref_key'] = $restaurant->machine_reference_key;
                $insertRestaurant['parent_id'] = $restaurant->id;
                $insertRestaurant['approximate_delivery_time'] = !empty($restaurant->max_estimated_delivery_time) ? $restaurant->max_estimated_delivery_time : 0;
                $insertRestaurant['timezone'] = 'Asia/Riyadh';
                $insertRestaurant['freelance_drive_supported'] = $restaurant->freelance;
                $insertRestaurant['shift'] = isset($hours) ? count($hours) : 0;
                $insertRestaurant['created_at'] = $restaurant->created_at;
                $insertRestaurant['updated_at'] = $restaurant->updated_at;
                $insertRestaurant['deleted_at'] = $restaurant->is_deleted == "N" ? null : $restaurant->updated_at;
                $insertRestaurant['shift'] = 1;

                $oldDescription = $this->db_ext->table('restaurant_metas')->where('restaurant_id', $restaurant->id)->get();

                if (count($oldDescription) > 0) {

                    $insertRestaurantTranslation1['description'] = $oldDescription[0]->content;
                    $insertRestaurantTranslation2['description'] = $oldDescription[0]->ar_content;

                }

                $insertRestaurantTranslation1['lang_code'] = 'en';
                $insertRestaurantTranslation1['name'] = $restaurant->name;
//                $insertRestaurantTranslation1['description'] = "";
                $insertRestaurantTranslation1['address'] = empty($restaurant->address) ? "Saudi Arabia" : $restaurant->address;
                $insertRestaurantTranslation1['branch_id'] = $restaurant->id;


                DB::table('restaurant_branches')->insert($insertRestaurant);

                $shift = 1;
                foreach ($hours as $key => $days) {

                    unset($insertHour);


                    if ($key == 'day_shift') {
                        $insertHour['branch_id'] = $restaurant->id;
                        $insertHour['shift'] = 'Day';
                    } else {
                        $insertHour['shift'] = 'Evening';
                        $insertHour['branch_id'] = $restaurant->id;
                    }
                    foreach ($days as $day => $time) {

                       if($day == "sat")
                       {
                           $day = "Saturday";
                       }elseif ($day == "wed")
                       {
                           $day = "Wednesday";
                       }else{
                           $day = ucfirst($day) . "day";
                       }
                        $insertHour['day'] = $day;

                        $insertHour['opening_time'] = $this->changeToUTC($time['open_hr'], 'Asia/Riyadh');
                        $insertHour['closing_time'] = $this->changeToUTC($time['close_hr'], 'Asia/Riyadh');
                        $flag = "None";
                        if ($insertHour['opening_time'] > $insertHour['closing_time']) {
                            $flag = "Greater";
                        } elseif ($insertHour['opening_time'] == $insertHour['closing_time']) {
                            continue;

                        }
                        if($insertHour['shift'] == 'Evening')
                        {
                            $shift = 2;
                        }

                        $RestaurantData[$key]['difference_checker'] = $flag;
                        DB::table('restaurant_hours')->insert($insertHour);

                    }


                }
                $insertRestaurant['shift'] = $shift;
                DB::table('restaurant_branches')->where('id', $restaurant->id)->update(array_except($insertRestaurant, ['id']));


                DB::table('branch_translation')->insert($insertRestaurantTranslation1);

                if (!empty($restaurant->ar_name)) {
                    $insertRestaurantTranslation2['lang_code'] = 'ar';
                    $insertRestaurantTranslation2['name'] = empty($restaurant->ar_name) ? $restaurant->name : $restaurant->ar_name;
//                    $insertRestaurantTranslation2['description'] = "";
                    $insertRestaurantTranslation2['address'] = empty($restaurant->ar_address) ? "Saudi Arabia" : $restaurant->ar_address;
                    $insertRestaurantTranslation2['branch_id'] = $restaurant->id;

                    DB::table('branch_translation')->insert($insertRestaurantTranslation2);
                }

                $deliveryOption = unserialize($restaurant->delivery_option);

                foreach ($deliveryOption as $option) {
                    $insertShipping = [];
                    if ($option == "Home Delivery") {
                        $insertShipping['slug'] = "home-delivery";
                    } else {
                        $insertShipping['slug'] = "pickup";
                    }
                    $insertShipping['restaurant_branch_id'] = $restaurant->id;

                    DB::table('shipping_method')->insert($insertShipping);


                }
                $deliveryZone = unserialize($restaurant->delivery_zone);

                foreach ($deliveryZone as $zone) {
                    $insertZone = [];

                    $insertZone['district_id'] = $zone;

                    $insertZone['branch_id'] = $restaurant->id;

                    DB::table('delivery_zones')->insert($insertZone);


                }


//                if (isset($hours)) {
//                    if (!$this->insertBranchHours($hours, $restaurant->id)) {
//                        return response()->json([
//                            'message' => 'Something went wrong while inserting restaurant hours',
//                            'status' => '400'
//                        ], 400);
//                    }
//                }


                $insertResAdmin['first_name'] = $user[0]->first_name;
                $insertResAdmin['last_name'] = $user[0]->last_name;
                $insertResAdmin['password'] = $user[0]->password;
                $insertResAdmin['gender'] = !empty($user[0]->gender) ? $user[0]->gender : 'male';
                $insertResAdmin['mobile'] = !empty($user[0]->mobile) ? $user[0]->mobile : 123456789;
                $insertResAdmin['status'] = $user[0]->status;
                $insertResAdmin['email'] = $user[0]->email;
                $insertResAdmin['country'] = 'Saudi Arabia';
                $insertResAdmin['country_code'] = '+966';
                $insertResAdmin['facebook_id'] = empty($user[0]->facebook_id) ? null : $user[0]->facebook_id;
                $insertResAdmin['google_id'] = empty($user[0]->google_id) ? null : $user[0]->google_id;
                $insert['birthday'] = $user[0]->birthdate;

                $id = $this->db_ext1->table('users')->insertGetId($insertResAdmin);

                $insertResMeta['user_id'] = $id;
                $insertResMeta['is_restaurant_admin'] = 0;
                $insertResMeta['restaurant_id'] = $restaurant->id;
                $insertResMeta['branch_id'] = $restaurant->id;

                $this->db_ext1->table('restaurant_user_meta')->insert($insertResMeta);

                $this->db_ext1->table('role_user')->insert([
                    'user_id' => $id,
                    'role_id' => 7

                ]);



            }
            DB::commit();

            return response()->json([

                'message' => 'data successfully migrated',
                'status' => '200'

            ], 200);

        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'status' => '400'
            ], 400);
        }
    }

    public function exportFoodCategories()
    {
        try {


            $oldCategories = $this->db_ext->table('food_categories')->get();
            DB::beginTransaction();
            foreach ($oldCategories as $category) {
                {
                    unset($insertCategory);
                    unset($insertTranslation1);
                    unset($insertTranslation2);

                    $insertCategory['id'] = $category->id;
                    $insertCategory['country_id'] = 1;
                    $insertCategory['sort'] = $category->sort_position;
                    $insertCategory['image'] = !empty($category->image) ? '/' . $category->image : '/images/34716633959d9fecedae91.png';
                    $insertCategory['status'] = $category->status;
                    $insertCategory['created_at'] = $category->created_at;
                    $insertCategory['updated_at'] = $category->updated_at;

                    DB::table('category')->insert($insertCategory);

                    $insertTranslation1['category_id'] = $category->id;
                    $insertTranslation1['lang'] = "en";
                    $insertTranslation1['name'] = $category->name;
                    DB::table('category_translation')->insert($insertTranslation1);

                    if (!empty($category->ar_name)) {
                        $insertTranslation2['category_id'] = $category->id;
                        $insertTranslation2['lang'] = "ar";
                        $insertTranslation2['name'] = $category->ar_name;
                        DB::table('category_translation')->insert($insertTranslation2);
                    }
                }
            }

            DB::commit();

            return response()->json([

                'message' => 'data successfully migrated',
                'status' => '200'

            ], 200);
        } catch (\Exception $ex) {
            return response()->json([

                'message' => $ex->getMessage(),
                'status' => '400'

            ], 400);
        }
    }

    public function insertBranchHours($hours, $branchId)
    {

        foreach ($hours as $key => $days) {

            unset($insertHour);

            if ($key == 'day_shift') {
                $insertHour['branch_id'] = $branchId;
                $insertHour['shift'] = 'Morning';
            } else {
                $insertHour['shift'] = 'Evening';
                $insertHour['branch_id'] = $branchId;
            }
            foreach ($days as $day => $time) {


                $insertHour['day'] = $day == "sat" ? "Saturday" : ucfirst($day) . "day";

                $insertHour['opening_time'] = $this->changeToUTC($time['open_hr'],'Asia/Riyadh');
                $insertHour['closing_time'] = $this->changeToUTC($time['close_hr'],'Asia/Riyadh');
                $flag = "None";
                if($insertHour['opening_time']>$insertHour['closing_time']){
                    $flag = "Greater";
                }
                elseif ($insertHour['opening_time'] == $insertHour['closing_time'])
                {
                    continue;
                }

                $RestaurantData[$key]['difference_checker']=$flag;
                if (DB::table('restaurant_hours')->insert($insertHour)) {
                    return true;
                } else {
                    return false;
                }
            }

        }
        return true;

    }

    public function exportFoodandAddons()
    {
        try {


            $oldFood = $this->db_ext->table('foods')->get();
            DB::beginTransaction();
            foreach ($oldFood as $food) {
                if ($food->food_or_addon == "F") {
                    $insertFood = [];
                    $insertFood['id'] = $food->id;
                    $insertFood['restaurant_id'] = $food->restaurant_id;
                    $insertFood['veg'] = $food->veg_or_non_veg == "V" ? 1 : 0;
                    $insertFood['price'] = $food->price;
                    $insertFood['status'] = $food->status;
                    $insertFood['created_at'] = $food->created_at;
                    $insertFood['updated_at'] = $food->updated_at;
                    $insertFood['gallery'] = null;

                    $images = $this->db_ext->table('food_images')->where('food_id', $food->id)->pluck('image_path');
                    if (count($images) > 0) {
                        foreach ($images as $image) {
                            $insertFood['gallery'][] = "/" . $image;

                        }

                    }

                    if (!empty($food->featured_image)) {
                        $insertFood['featured_image'] = "/" . $food->featured_image;
                        $insertFood['gallery'][] = "/" . $food->featured_image;

                    } else {
                        $insertFood['featured_image'] = null;
                        $insertFood['gallery'] = null;
                    }
                    if (!empty($insertFood['gallery'])) {
                        $insertFood['gallery'] = serialize($insertFood['gallery']);
                    };

                    $insertFoodTranslation1 = [];
                    $insertFoodTranslation1['lang'] = "en";
                    $insertFoodTranslation1['name'] = $food->name;
                    $insertFoodTranslation1['ingredients'] = $food->ingredients;
                    $insertFoodTranslation1['description'] = $food->desc;
                    $insertFoodTranslation1['food_id'] = $food->id;


                    DB::table('food')->insert($insertFood);

                    DB::table('food_translation')->insert($insertFoodTranslation1);

                    if (!empty($food->ar_name)) {
                        $insertFoodTranslation2 = [];
                        $insertFoodTranslation2['lang'] = "ar";
                        $insertFoodTranslation2['name'] = $food->ar_name;
                        $insertFoodTranslation2['ingredients'] = $food->ar_ingredients;
                        $insertFoodTranslation2['description'] = $food->ar_desc;
                        $insertFoodTranslation2['food_id'] = $food->id;
                        DB::table('food_translation')->insert($insertFoodTranslation2);
                    }

                    if(!empty($food->extra_info))
                    {
                        $insertFoodSize = [];
                        $insertFoodSize['food_id'] = $food->id;
                        $insertFoodSize['price'] = $food->price;
                        $insertFoodSize['size'] = str_slug($food->extra_info);
                        $insertFoodSize['is_default'] = "1";

                        $size_id = DB::table('food_size')->insertGetId($insertFoodSize);

                        $insertSizeTranslation1 = [];
                        $insertSizeTranslation1['lang'] = "en";
                        $insertSizeTranslation1['title'] = ucfirst($food->extra_info);
                        $insertSizeTranslation1['food_size_id'] = $size_id;

                        DB::table('food_size_translation')->insert($insertSizeTranslation1);

                        if(!empty($food->ar_extra_info))
                        {
                            $insertSizeTranslation1 = [];
                            $insertSizeTranslation1['lang'] = "ar";
                            $insertSizeTranslation1['title'] = $food->ar_extra_info;
                            $insertSizeTranslation1['food_size_id'] = $size_id;

                            DB::table('food_size_translation')->insert($insertSizeTranslation1);
                        }
                    }
                    $foodSizes = $this->db_ext->table('food_extra_infos')->where('food_id', $food->id)->get();

                    if (count($foodSizes) > 0) {
                        $i = 0;
                        foreach ($foodSizes as $size) {
                            $insertFoodSize = [];
                            $insertFoodSize['food_id'] = $food->id;
                            $insertFoodSize['price'] = (float)$size->key_value;
                            $insertFoodSize['size'] = str_slug($size->key_name);
                            $insertFoodSize['is_default'] = (empty($food->extra_info) && $i==0) ? "1" : "0";

                            $size_id = DB::table('food_size')->insertGetId($insertFoodSize);

                            $insertSizeTranslation1 = [];
                            $insertSizeTranslation1['lang'] = "en";
                            $insertSizeTranslation1['title'] = $size->key_name;
                            $insertSizeTranslation1['food_size_id'] = $size_id;

                            DB::table('food_size_translation')->insert($insertSizeTranslation1);

                            if (!empty($food->ar_name)) {
                                $insertSizeTranslation2 = [];
                                $insertSizeTranslation2['lang'] = "ar";
                                $insertSizeTranslation2['title'] = $size->ar_key_name;
                                $insertSizeTranslation2['food_size_id'] = $size_id;

                                DB::table('food_size_translation')->insert($insertSizeTranslation2);
                            }
                            $i++;
                        }
                    }

                } else {
                    $insertAddon = [];
                    $insertAddon['id'] = $food->id;
                    $insertAddon['restaurant_id'] = $food->restaurant_id;
                    $insertAddon['price'] = $food->price;
                    $insertAddon['status'] = $food->status;
                    $insertAddon['created_at'] = $food->created_at;
                    $insertAddon['updated_at'] = $food->updated_at;


                    $insertAddonTranslation1 = [];
                    $insertAddonTranslation1['lang'] = "en";
                    $insertAddonTranslation1['name'] = $food->name;
                    $insertAddonTranslation1['description'] = $food->desc;
                    $insertAddonTranslation1['addon_id'] = $food->id;

                    DB::table('addon')->insert($insertAddon);

                    DB::table('addon_translation')->insert($insertAddonTranslation1);

                    if (!empty($food->ar_name)) {
                        $insertAddonTranslation2 = [];
                        $insertAddonTranslation2['lang'] = "ar";
                        $insertAddonTranslation2['name'] = $food->ar_name;
                        $insertAddonTranslation2['description'] = $food->ar_desc;
                        $insertAddonTranslation2['addon_id'] = $food->id;
                        DB::table('addon_translation')->insert($insertAddonTranslation2);
                    }
                }

            }

            DB::commit();
            return response()->json([

                'message' => 'data successfully migrated',
                'status' => '200'

            ], 200);
        } catch (\Exception $ex) {
            return response()->json([

                'message' => $ex->getMessage(),
                'status' => '400'

            ], 400);
        }
    }

    public function exportCuisine()
    {
        try {
            DB::beginTransaction();
            $oldCusine = $this->db_ext->table('restaurant_categories')->get();
            foreach ($oldCusine as $cuisine) {
                $insertCuisine = [];
                $insertCuisine['id'] = $cuisine->id;
                $insertCuisine['status'] = $cuisine->status;
                $insertCuisine['country_id'] = 1;
                $insertCuisine['created_at'] = $cuisine->created_at;
                $insertCuisine['updated_at'] = $cuisine->updated_at;


                $insertTranslation1 = [];
                $insertTranslation1['lang_code'] = "en";
                $insertTranslation1['name'] = $cuisine->name;
                $insertTranslation1['cuisine_id'] = $cuisine->id;


                DB::table('cuisine')->insert($insertCuisine);

                DB::table('cuisine_translation')->insert($insertTranslation1);

                if (!empty($cuisine->ar_name)) {
                    $insertTranslation2 = [];
                    $insertTranslation2['lang_code'] = "ar";
                    $insertTranslation2['name'] = $cuisine->ar_name;
                    $insertTranslation2['cuisine_id'] = $cuisine->id;
                    DB::table('cuisine_translation')->insert($insertTranslation2);
                }
            }
            DB::commit();
            return response()->json([

                'message' => 'data successfully migrated',
                'status' => '200'

            ], 200);

        } catch (\Exception $ex) {
            return response()->json([

                'message' => $ex->getMessage(),
                'status' => '400'

            ], 400);
        }


    }

    public function exportAddonCategories()
    {
        try {
            DB::beginTransaction();
            $oldCatrgories = $this->db_ext->table('addonscategories')->get();
            foreach ($oldCatrgories as $category) {
                $insertCategory = [];
                $insertCategory['id'] = $category->id;
                $insertCategory['status'] = $category->status;
                $insertCategory['restaurant_id'] = $category->restaurant_id;
                $insertCategory['min_addon'] = $category->min;
                $insertCategory['max_addon'] = $category->max;
                $insertCategory['created_at'] = $category->created_at;
                $insertCategory['updated_at'] = $category->updated_at;


                $insertTranslation1 = [];
                $insertTranslation1['lang'] = "en";
                $insertTranslation1['name'] = $category->name;
                $insertTranslation1['category_id'] = $category->id;


                DB::table('addoncategory')->insert($insertCategory);

                DB::table('addoncategory_translation')->insert($insertTranslation1);

                if (!empty($category->ar_name)) {
                    $insertTranslation2 = [];
                    $insertTranslation2['lang'] = "ar";
                    $insertTranslation2['name'] = $category->name;
                    $insertTranslation2['category_id'] = $category->id;
                    DB::table('addoncategory_translation')->insert($insertTranslation2);
                }
            }
            DB::commit();
            return response()->json([

                'message' => 'data successfully migrated',
                'status' => '200'

            ], 200);

        } catch (\Exception $ex) {
            return response()->json([

                'message' => $ex->getMessage(),
                'status' => '400'

            ], 400);
        }
    }

    public function exportCategoryFoodPivot()
    {
        try {


            DB::beginTransaction();
            /**
             * Insert into category_food_pivot table
             */
            $pivotData1 = $this->db_ext->table('category_food_pivot')->get();

            foreach ($pivotData1 as $data) {

                $restaurant = $this->db_ext->table('foods')->where('id', $data->food_id)->pluck('restaurant_id');
                // return $restaurant[0];
                $insertPivot['category_id'] = $data->food_category_id;
                $insertPivot['food_id'] = $data->food_id;

                DB::table('food_category_pivot')->insert($insertPivot);
                /**
                 * Insert into restaurant_food_category_pivot table
                 */
                try {
                    DB::table('restaurant_food_category_pivot')->insert(
                        ['restaurant_id' => $restaurant[0],
                            'category_id' => $data->food_category_id,
                        ]
                    );
                } catch (\Exception $e) {
                    continue;
                }


            }

            /**
             * Insert into category_restaurant_pivot table
             */
            $pivotData2 = $this->db_ext->table('category_restaurant_pivot')->get();
            foreach ($pivotData2 as $data) {
                $insertPivot2['cuisine_id'] = $data->restaurant_category_id;
                $insertPivot2['restaurant_id'] = $data->restaurant_id;

                DB::table('cuisine_restaurant_pivot')->insert($insertPivot2);
            }

            DB::commit();
            return response()->json([

                'message' => 'data successfully migrated',
                'status' => '200'

            ], 200);
        } catch (\Exception $ex) {
            return response()->json([

                'message' => $ex->getMessage(),
                'status' => '400'

            ], 400);
        }


    }

    public function exportFoodAddonCategoryPivot()
    {
        try {
            /**
             *Fetch addoncategory using addon id and insert into food_addoncategory_pivot table
             */
            DB::beginTransaction();
            $pivotData = $this->db_ext->table('food_addons')->get();
            foreach ($pivotData as $data) {
                $addonCategory = $this->db_ext->table('addons_addonscategories')->where('addon_id', $data->addon_id)->pluck('addonscategories_id');
                if (count($addonCategory) > 0) {


                    foreach ($addonCategory as $category)
                        try {
                            DB::table('food_addoncategory_pivot')->insert(
                                [
                                    'food_id' => $data->food_id,
                                    'category_id' => $category,
                                ]
                            );
                        } catch (\Exception $e) {
                            continue;
                        }


                }

            }
            /**
             *
             */
            $pivotData1 = $this->db_ext->table('food_addonscategories')->get();
            foreach ($pivotData1 as $data) {
                try {
                    DB::table('food_addoncategory_pivot')->insert(
                        [
                            'food_id' => $data->food_id,
                            'category_id' => $data->addonscategories_id,
                        ]
                    );
                } catch (\Exception $e) {
                    continue;
                }
            }
            /**
             * Insert into adddon_addoncategory_pivot table
             */
            $pivotData2 = $this->db_ext->table('addons_addonscategories')->get();
            foreach ($pivotData2 as $data) {
                try {
                    DB::table('addon_addoncategory_pivot')->insert(
                        [
                            'addon_id' => $data->addon_id,
                            'category_id' => $data->addonscategories_id,
                        ]
                    );
                } catch (\Exception $e) {
                    continue;
                }
            }
            DB::commit();
            return response()->json([

                'message' => 'data successfully migrated',
                'status' => '200'

            ], 200);

        } catch (\Exception $ex) {
            return response()->json([

                'message' => $ex->getMessage(),
                'status' => '400'

            ], 400);
        }

    }

    public function exportFavourites()
    {
        try {
            DB::beginTransaction();
            $pivotData = $this->db_ext->table('favorites')->get();
            foreach ($pivotData as $data) {
                try {
                    DB::table('user_favourite')->insert(
                        [
                            'id' => $data->id,
                            'food_id' => $data->food_id,
                            'user_id' => $data->user_id,
                            'created_at' => $data->created_at,
                            'updated_at' => $data->updated_at
                        ]
                    );
                } catch (\Exception $e) {
                    continue;
                }
            }
            DB::commit();
            return response()->json([

                'message' => 'data successfully migrated',
                'status' => '200'

            ], 200);

        } catch (\Exception $ex) {
            return response()->json([

                'message' => $ex->getMessage(),
                'status' => '400'

            ], 400);
        }

    }

    public function exportReviews()
    {
        try {
            DB::beginTransaction();
            $pivotData = $this->db_ext->table('reviews')->get();
            foreach ($pivotData as $data) {
                try {
                    DB::table('reviews')->insert(
                        [
                            'id' => $data->id,
                            'restaurant_id' => $data->restaurant_id,
                            'user_id' => $data->user_id,
                            'review' => $data->comments,
                            'rating' => $data->rating,
                            'created_at' => $data->created_at,
                            'updated_at' => $data->updated_at
                        ]
                    );
                } catch (\Exception $ex) {
                    return $ex->getMessage();
                }
            }
            DB::commit();
            return response()->json([

                'message' => 'data successfully migrated',
                'status' => '200'

            ], 200);

        } catch (\Exception $ex) {
            return response()->json([

                'message' => $ex->getMessage(),
                'status' => '400'

            ], 400);
        }
    }

    public function exportMethods()
    {
        try {
            DB::beginTransaction();
            $paymentMethods = $this->db_ext->table('payment_types')->get();

            foreach ($paymentMethods as $payment) {

                if ($payment->name == 'Credit Card') {
                    $method[$payment->id] = 'creditcard';
                } elseif ($payment->name == 'Cash on Pick Up') {
                    $method[$payment->id] = 'cash-on-pickup';
                } else {
                    $method[$payment->id] = str_slug($payment->name);
                }

            }


            $paymentPivot = $this->db_ext->table('payment_restaurant_pivot')->get();

            foreach ($paymentPivot as $pivot) {
                $insertPivot = [];
                $insertPivot['restaurant_branch_id'] = $pivot->restaurant_id;
                $insertPivot['slug'] = $method[$pivot->payment_type_id];
                DB::table('payment_method')->insert($insertPivot);
            }


            DB::commit();

            return response()->json([

                'message' => 'data successfully migrated',
                'status' => '200'

            ], 200);
        } catch (\Exception $ex) {
            return response()->json([

                'message' => $ex->getMessage(),
                'status' => '400'

            ], 400);
        }


    }

    public function changeToUTC($time, $timezone){
        $parsed = date_parse($time);
        $now = Carbon::now($timezone);
        $diff = $now->offset;
        $time = $parsed['hour']*3600+$parsed['minute']*60;
        $time= $time - $diff;
        $date = gmdate('H:i', $time);
        return $date;
    }


}