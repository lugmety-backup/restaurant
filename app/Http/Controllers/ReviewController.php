<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/7/2017
 * Time: 9:14 PM
 */

namespace App\Http\Controllers;
use App\BranchTranslation;
use App\Repo\RestaurantBranchInterface;
use App\Repo\RestaurantBranchTranslationInterface;
use App\Repo\ReviewInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use LogStoreHelper;
use RemoteCall;
use Illuminate\Support\Facades\Config;
use RoleChecker;
use Datatables;
class ReviewController extends Controller
{
    /**
     * @var ReviewInterface
     */
    protected $review;
    /**
     * @var RestaurantBranchInterface
     */
    protected $restaurant;
    /**
     * @var LogStoreHelper
     */
    protected $log;

    protected $branchTranslation;


    /**
     * ReviewController constructor.
     * @param ReviewInterface $review
     * @param RestaurantBranchInterface $restaurant
     * @param LogStoreHelper $log
     */
    public function __construct(ReviewInterface $review, RestaurantBranchInterface $restaurant, LogStoreHelper $log,RestaurantBranchTranslationInterface $branchTranslation)
    {
        $this->review = $review;
        $this->restaurant = $restaurant;
        $this->log = $log;
        $this->branchTranslation = $branchTranslation;
    }

    /**
     * returns the list of reviews for logged in user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllReviewsOfUser(Request $request){
        try{
            $this->validate($request,[
                "limit" => "required|integer|min:1"
            ]);
        }
        catch (\Exception $ex){
            $request->limit = 10;
        }
        $userId = RoleChecker::getUser();
        $allReview = $this->review->getAllReviewOfUser($userId, $request->limit);

        if(count($allReview) ==0){
            return response()->json([
                "status" => "404",
                "message" => "You don't have reviews"
            ],404);
        }
        return response()->json([
            "status" => "200",
            "data" => $allReview
        ]);
    }

    /**
     * get specific review for logged in user
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSpecificReview($id){
        try{
            $userId = RoleChecker::getUser();
            $checkReveiw = $this->review->getSpecificReviewByUser($id,$userId);

            $this->log->storeLogInfo([
                "Review viewed successfully",[
                    "status" => "200",
                    "data" => $checkReveiw
                ]
            ]);
            return response()->json([
                "status" => "200",
                "data" => $checkReveiw
            ]);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "Review not found"
            ],404);
        }
    }

    /**
     * get specific review for user who have permission
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSpecificReviewForAdmin($id){
        try{
            $checkReveiw = $this->review->getSpecificReview($id);
            $restaurantName = DB::table("branch_translation")->where([
                ["branch_id",$checkReveiw->restaurant_id],
                ["lang_code","en"]
            ])->first();
            if(!$restaurantName){
                return response()->json([
                    "status" => "404",
                    "message" => "Restaurant Name could not be found."
                ],404);
            }
            $checkReveiw["name"] = $restaurantName->name;
            if (RoleChecker::hasRole('admin') || RoleChecker::hasRole('super-admin')) {
                return response()->json([
                    "status" => '200',
                    "data" => $checkReveiw
                ]);
            }
            elseif (RoleChecker::hasRole("restaurant-admin") || (RoleChecker::get('parentRole') == "restaurant-admin")) {
                $restaurantInfo = RoleChecker::getRestaurantInfo();
                if (!is_null($restaurantInfo) && ($restaurantInfo["is_restaurant_admin"] == true)) {
                    if (!is_null($restaurantInfo["branch_id"]) && in_array($checkReveiw->restaurant_id,$restaurantInfo["branch_id"])) {
                        return response()->json([
                            "status" => '200',
                            "data" => $checkReveiw
                        ]);
                    }
                } else {
                    /**
                     * this means there is some modification done in oauth restaurant_meta_table .
                     * and  is_restaurant_admin is false
                     * */
                    return response()->json([
                        "status" => "403",
                        "message" => "You can view your restaurant review only."
                    ], 403);
                }
            }
            elseif (RoleChecker::hasRole("branch-admin") || (RoleChecker::get('parentRole') == "branch-admin")) {
                $restaurantInfo = RoleChecker::getRestaurantInfo();
                if (!is_null($restaurantInfo) && !is_null($restaurantInfo["branch_id"]) && ($restaurantInfo["is_restaurant_admin"] == false) && ($checkReveiw->restaurant_id == $restaurantInfo["branch_id"])) {
                    return response()->json([
                        "status" => '200',
                        "data" => $checkReveiw
                    ]);
                }
                else {
                    /**
                     * this means there is some modification done in oauth restaurant_meta_table .
                     * and  is_restaurant_admin is true if you are branch-admin
                     * */
                    return response()->json([
                        "status" => "403",
                        "message" => "You can view your restaurant review only."
                    ], 403);
                }
            }
            else{
                return response()->json([
                    "status" => "403",
                    "message" => "You are not allowed to view review"
                ],403);
            }



            $this->log->storeLogInfo([
                "Review viewed successfully",[
                    "status" => "200",
                    "data" => $checkReveiw
                ]
            ]);
            return response()->json([
                "status" => "200",
                "data" => $checkReveiw
            ]);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "Review not found"
            ],404);
        }
    }

    /**
     * stores user review on order
     * @param Request $request
     */
    public function storeReview(Request $request){
        try {
            $this->validate($request, [
                "order_id" => "required|integer|min:1|unique:reviews,order_id",
                "rating" => "required|numeric|min:1|max:5",
                "review" => "sometimes|string",
                'driver_review' => "sometimes|string",
                'lugmety_review' => "sometimes|string"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        $lang = Input::get('lang','en');
        if(!RoleChecker::hasRole("customer")){
            return \Settings::getErrorMessageOrDefaultMessage('restaurant-only-customer-are-allowed-to-post-review',
                "Only customer are allowed to post review.", 403, $lang);
        }
        $user_id = RoleChecker::getUser();
        /**
         * calling order service to check order_id exist and get restaurant id in return
         */
        $orderFullUrl = Config::get("config.order_base_url")."/order/$request->order_id/check/$user_id";
        $checkOrder = RemoteCall::getSpecificWithoutOauth($orderFullUrl);
        if($checkOrder["status"] !==200){
            return response()->json($checkOrder["message"],$checkOrder["status"]);
        }
        $request["rating"] = $request->get("rating",0);
        $request->merge([
            "review_status" => "pending"
        ]);
        $request = $request->all();
        $request["user_id"] = $user_id;
        $request["restaurant_id"] = $checkOrder["message"]["data"]["restaurant_id"];
        try {
            $createReview = $this->review->createReview($request);
            $this->log->storeLogInfo(array("Review Created successfully",[
                "status" => "200",
                "data" => $createReview
            ]));
            return \Settings::getErrorMessageOrDefaultMessageWithPayload('restaurant-review-posted-successfully',
                "Review posted successfully.", 200, $lang,[
                    'order_id' => $request['order_id']
                ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "500",
                "message" => "Error creating review."
            ],500);
        }

    }

    /**
     * updates reviews for the user who commented that review
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateReview($id,Request $request){

        try{
            $this->validate($request,[
                "rating" => "required|numeric|min:1|max:5",
                "review" => "sometimes|string",
                'driver_review' => "sometimes|string",
                'lugmety_review' => "sometimes|string"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        $lang = Input::get('lang','en');
        try{
            $checkReveiw = $this->review->getSpecificReview($id);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "Review not found"
            ],404);
        }
        $userId = RoleChecker::getUser();
        if($checkReveiw->user_id !== $userId){
            return response()->json([
                "status" => "403",
                "message" => "You can only update your review only"
            ],403);
        }
        try {
            $request->merge([
                "review_status" => "pending"
            ]);
            $updateReview = $this->review->updateReview($checkReveiw, $request->except('order_id'));
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "500",
                "message" => "Error updating review"
            ],500);
        }
        $this->log->storeLogInfo(array("Review updated successfully",[
            "status" => "200",
            "data" => ["id"=>$id]
        ]));
        return \Settings::getErrorMessageOrDefaultMessage('restaurant-review-updated-successfully',
            "Review updated successfully.", 200, $lang);
//        return response()->json([
//            "status" => "200",
//            "message" => "Review updated successfully"
//        ]);

    }

    public function updateReviewByAdminOnly($id,Request $request){
        try{
            $message = [
                "review_status.in" => "The review_status value should be either approved,rejected or pending."
            ];
            $this->validate($request,[
                "review_status" => "required|in:approved,rejected,pending",
            ],$message);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ]);
        }
        $lang = Input::get('lang','en');
        try{
            $checkReveiw = $this->review->getSpecificReview($id);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "Review not found"
            ],404);
        }

        try {
            $updateReview = $this->review->updateReview($checkReveiw, $request->only("review_status"));
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "500",
                "message" => "Error updating review"
            ],500);
        }
        $this->log->storeLogInfo(array("Review updated successfully",[
            "status" => "200",
            "data" => ["id"=>$id]
        ]));
        return \Settings::getErrorMessageOrDefaultMessage('restaurant-review-updated-successfully',
            "Review updated successfully.", 200, $lang);
    }

    public function getReviewListBasedOnOrderId(Request $request){
        try{
            $this->validate($request,[
                "id" =>"required|array"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        try{
            $reviewList = $this->review->getAllReviewListByOrderId($request->id);
            return response()->json([
                "status" => "200",
                "data" => $reviewList
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "500",
                "message" => "Error getting review"
            ],500);
        }
    }

    /**
     * get all restaurant review by restaurant id for public
     * @param $restaurantId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllReviewsForRestaurant($restaurantId, Request $request){
        try{
            $this->validate($request,[
                "limit" => "required|integer|min:1"
            ]);
        }
        catch (\Exception $ex){
            $request->limit = 10;
        }

        /**
         * checks if the restaurant is available and status is 1
         */
         $checkRestaurant = $this->restaurant->getSpecificRestaurantBranchForPublic($restaurantId);
        if(count($checkRestaurant) == 0){
            return response()->json([
                "status" => "404",
                "message" => "Restaurant Not found"
            ]);
        }
        try{
            /**
             * gets all latest restaurant reviews based on @param $restaurantId
             */
            $getAllReviews = $this->review->getAllReviewByRestaurantIdAndLimit($restaurantId,  $request->limit);

            if(count($getAllReviews) == 0){
                throw new \Exception();
            }
            $userDatas = [];
            foreach ($getAllReviews as $getReview){
                $userData[] = $getReview->user_id;
            }
            $userDatas["users"] = array_unique($userData);
            $oauthUrl = Config::get('config.oauth_base_url')."/review/users/list";
            $getUserDetailsForReview =  RemoteCall::storeWithoutOauth($oauthUrl,$userDatas);
            if($getUserDetailsForReview["status"] != "200"){
                throw new \Exception();
            }
            $userDatasFromOauth = $getUserDetailsForReview["message"]["data"];
            if(empty($userDatasFromOauth)){
                throw new \Exception();
            }
            foreach ($getAllReviews as $key =>$allReview){
               foreach ($userDatasFromOauth as $userData){
                    if($allReview->user_id !== $userData["id"]) continue;
                    $date = $allReview->updated_at;
                    unset($allReview["updated_at"]);
                    $allReview->updated_at = Carbon::parse($date)->tz($checkRestaurant[0]->timezone);
                    $allReview["user_name"] = $userData["first_name"];
//                    if(!empty($userData["middle_name"])) $allReview["user_name"] .= " ".$userData["middle_name"];
//                    $allReview["user_name"].= " ".$userData["last_name"];
                }
                if(!isset($allReview["user_name"])) {
                    collect($allReview)->forget($key);
//                   unset($getAllReviews[$key]);
                }
            }
//            $getAllReviews = collect($getAllReviews)->values()->all();
            return response()->json([
                "status" => "200",
                "data" => $getAllReviews
            ],200);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "404",
                "message" => "Empty Restaurant Review"
            ],404);
        }
    }


    /**
     * get all restaurant review by restaurant id for admin
     * @param $restaurantId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllReviewsForAdmin($restaurantId){
        try{
            /**
             * checks if the restaurant is available and status is 1
             */
            $checkRestaurant = $this->restaurant->getSpecificRestaurantBranch($restaurantId);
            if(count($checkRestaurant) == 0){
                return response()->json([
                    "status" => "404",
                    "message" => "Restaurant Not found"
                ]);
            }
            /**
             * gets all latest restaurant reviews based on @param $restaurantId
             */
            $getAllReviews = $this->review->getAllReviewByRestaurantId($restaurantId);
            $getAllReviews->makeHidden(["restaurant_id"]);
            if(count($getAllReviews) == 0){
                throw new \Exception();
            }
            return Datatables::of($getAllReviews)->make(true);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "404",
                "message" => "Empty Restaurant Review"
            ],404);
        }
    }

    public function countPendingReview(){
        try{
            if (RoleChecker::hasRole('admin') || RoleChecker::hasRole('super-admin')) {
                return response()->json([
                    "status" => "200",
                    "count" => $this->review->getAllReviewForAdmin(true)
                ]);
            }
            else{
                return response()->json([
                    "status" => "200",
                    "count" => "Only admin and super admin can view review count."
                ]);
            }
        }
        catch (\Exception $ex){
            $this->log->storeLogError(["Error displaying review count",[
                "message" => $ex->getMessage()
            ]]);
            return response()->json([
                "status" => "500",
                "message" => "Error displaying review count."
            ],500);
        }
    }

    public function getAllReviewsForAdmins(){
        try{
            if (RoleChecker::hasRole('admin') || RoleChecker::hasRole('super-admin')) {
                $getAllReview = $this->review->getAllReviewForAdmin();
                return Datatables::of($getAllReview)
                    ->make(true);
            }
            elseif (RoleChecker::hasRole("restaurant-admin") || (RoleChecker::get('parentRole') == "restaurant-admin")) {
                $restaurantInfo = RoleChecker::getRestaurantInfo();
                if (!is_null($restaurantInfo) && ($restaurantInfo["is_restaurant_admin"] == true)) {
                    if (!is_null($restaurantInfo["branch_id"])) {

                        $getReviewForRestaurantAdmin = $this->review->getReviewForRestaurantAdmin($restaurantInfo["branch_id"]);
                        return Datatables::of($getReviewForRestaurantAdmin)
                            ->make(true);
                    }
                } else {
                    /**
                     * this means there is some modification done in oauth restaurant_meta_table .
                     * and  is_restaurant_admin is false
                     * */
                    return response()->json([
                        "status" => "403",
                        "message" => "Forbidden"
                    ], 403);
                }
            }
            elseif (RoleChecker::hasRole("branch-admin") || (RoleChecker::get('parentRole') == "branch-admin")) {
                $restaurantInfo = RoleChecker::getRestaurantInfo();
                if (!is_null($restaurantInfo) && !is_null($restaurantInfo["branch_id"]) && ($restaurantInfo["is_restaurant_admin"] == false)) {
                    $getReviewForRestaurantAdmin = $this->review->getReviewForRestaurantAdmin([$restaurantInfo["branch_id"]]);
                    return Datatables::of($getReviewForRestaurantAdmin)
                        ->make(true);
                }
                else {
                    /**
                     * this means there is some modification done in oauth restaurant_meta_table .
                     * and  is_restaurant_admin is true if you are branch-admin
                     * */
                    return response()->json([
                        "status" => "403",
                        "message" => "Forbidden."
                    ], 403);
                }
            }
            else{
                return response()->json([
                    "status" => "403",
                    "message" => "You are not allowed to view review"
                ],403);
            }
        }

        catch (\Exception $ex){
            $this->log->storeLogError(["error listing reviwe",[
                "message" => $ex->getMessage()
            ]]);
            return response()->json([
                "status" => "500",
                "message" => "Error listing reviews."
            ],500);
        }
    }


    /**
     * delete specified review only user with permission can delete the review
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteReview($id){
        try{
            $checkReveiw = $this->review->getSpecificReview($id);

            $checkReveiw->delete();//deletes the review
            $this->log->storeLogInfo([
                "Review deleted successfully",[
                    "status" => "200",
                    "data" => $checkReveiw
                ]
            ]);
            return response()->json([
                "status" => "200",
                "message" => "Review deleted successfully"
            ]);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "Review not found"
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "500",
                "message" => "Error deleting review"
            ]);
        }

    }


}