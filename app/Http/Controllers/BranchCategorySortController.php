<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 3/19/2018
 * Time: 12:55 PM
 */

namespace App\Http\Controllers;


use App\Models\BranchCategorySorting;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use LogStoreHelper;
use App\Repo\RestaurantBranchInterface;
use RoleChecker;
class BranchCategorySortController extends Controller
{
    protected $restaurantBranch;
    protected $log;
    public function __construct(RestaurantBranchInterface $restaurantBranch, LogStoreHelper $log)
    {
        $this->restaurantBranch = $restaurantBranch;
        $this->log = $log;
    }

    /**
     * create food category sorting .This will be helpful for displaying the food category in top or button based on the sort value
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createCategorySorting(Request $request){
        DB::beginTransaction();
        $dnd = false;
        try{
            if($request->has("dnd") && $request->dnd){
                $dnd = true;
                $this->validate($request,[
                    "branch_id" => "required|integer|min:1",
                    "category_id" => "required|integer|min:1",
                    "new_position" => "required|integer|min:1",
                    "previous_position" => "required|integer|min:1"
                ]);
            }
            else{
                $this->validate($request,[
                    "branch_id" => "required|integer|min:1",
                    "category_info" => "required|array",
                    "category_info.*.id" => "required|integer|min:1",
                    "category_info.*.sort" => "required|integer|min:1"
                ]);
            }
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        try{
            if(!$this->checkUserAuthority($request->branch_id)){
                return response()->json([
                    "status" => "403",
                    "message" => "Permission denied"
                ],403);
            }
            $getRestaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($request->branch_id);
            if($dnd){
                $positionList = [$request->new_position,$request->previous_position];
                $getCategoriesBasedOnThePosition = BranchCategorySorting::where("branch_id",$request->branch_id)->whereIn("sort",$positionList)->get();
                if(count($getCategoriesBasedOnThePosition) != count($positionList)){
                    return response()->json([
                        "status" => "403",
                        "message" => "One or more position could be found in database"
                    ],403);
                }
                else{

                    foreach ($getCategoriesBasedOnThePosition as $category){
                        if($category["category_id"] == $request->category_id){
                            $count++;
                            $category->touch([
                                "sort" => $request->new_position,
                                "updated_at" =>  $currentdate
                            ]);
                        }
                        else{
                            $category->update([
                                "sort" => $request->previous_position
                            ]);
                        }
                    }

                }
            }
            else{
                foreach ($request->category_info as $key => $category){
                    $attachCategoryToRestaurantBranch = $getRestaurantBranch->branchCategory()->syncWithoutDetaching([$category["category_id"] =>["sort" => $key+1,"updated_at" =>Carbon::now("utc")]]);
                }
            }
            $this->log->storeLogInfo(["creating category sorting",[
                "data" => $request->all()
            ]]);
            DB::commit();


            return response()->json([
                "status" => "200",
                "message" => "Category sorting added successfully."
            ]);
        }
        catch (ModelNotFoundException $ex){
            $this->log->storeLogError(["Restaurant branch could not be found",[
                "data" => $request->all(),
                "message" => $ex->getMessage()
            ]]);
            return response()->json([
                "status" => "404",
                "message" => "Could not found requested restaurant branch."
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError(["creating category sorting error",[
                "data" => $request->all(),
                "message" => $ex->getMessage()
            ]]);
            return response()->json([
                "status" => "500",
                "message" => "Error attaching categories sorting."
            ],500);
        }
    }

    /**
     * listing of specific category by admin,superadmin ,restaurant admin and branch admim
     * @param $branchId
     * @return \Illuminate\Http\JsonResponse
     */
    public function viewCategorySorting($branchId){
        DB::beginTransaction();

        if(!$this->checkUserAuthority($branchId)){
            return response()->json([
                "status" => "403",
                "message" => "Permission denied"
            ],403);
        }
        try{
            $getRestaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($branchId);
            $getMainCategoryData = $getRestaurantBranch->category()->get();
            $getMainCategoryDataIds = collect($getMainCategoryData)->pluck("id")->values()->all();
            $getCategoryBySorting = $getRestaurantBranch->branchCategorySorting()->get();
            $getCategorySortingIds = collect($getCategoryBySorting)->pluck("category_id")->values()->all();
            $differenceInCategories = array_values(array_diff($getMainCategoryDataIds,$getCategorySortingIds));
            if(count($differenceInCategories) != 0) {
                foreach ($getMainCategoryData as $mainCategory) {
                    foreach ($differenceInCategories as $diffIncatgory) {
                        if ($diffIncatgory == $mainCategory["id"]) {
                            $attachCategoryToRestaurantBranch = $getRestaurantBranch->branchCategory()->syncWithoutDetaching([$mainCategory["id"] => ["sort" => $mainCategory["sort"],"updated_at" => Carbon::now('utc')]]);
                        }
                    }
                }
                $getCategoryBySorting = $getRestaurantBranch->branchCategorySorting()->get();
            }
            $getCategoryBySorting = collect($getCategoryBySorting)->sortByDesc("updated_at")->sortBy("sort")->values()->all();
            foreach ($getCategoryBySorting as $category){
                $categoryList = $category->restaurantBranchCategory()->first();
                $categoryTranslation = $categoryList->categoryTranslation()->where("lang","en")->first();
                if($categoryTranslation){
                    $category["name"] = $categoryTranslation->name;
                }
                else{
                    $category["name"] = "";
                }
            }
            DB::commit();
            return response()->json([
                "status" => "200",
                "data" => $getCategoryBySorting
            ]);
        }
        catch (ModelNotFoundException $ex){
            $this->log->storeLogError(["Restaurant branch could not be found",[
                "data" => ["branch_id" =>$branchId ],
                "message" => $ex->getMessage()
            ]]);
            return response()->json([
                "status" => "404",
                "message" => "Could not found requested restaurant branch."
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError(["creating category sorting error",[
                "data" => ["branch_id" =>$branchId ],
                "message" => $ex->getMessage()
            ]]);
            return response()->json([
                "status" => "500",
                "message" => "Error viewing categories sorting."
            ],500);
        }
    }

    /**
     * detach category form the branch menu sorting
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeCategorySorting(Request $request){
        DB::beginTransaction();
        try{
            $this->validate($request,[
                "branch_id" => "required|integer|min:1",
                "category_info" => "required|array",
                "category_info.*.id" => "required|integer|min:1"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        try{
            if(!$this->checkUserAuthority($request->branch_id)){
                return response()->json([
                    "status" => "403",
                    "message" => "Permission denied"
                ],403);
            }
            $getRestaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($request->branch_id);
            foreach ($request->category_info as $category){
                $detachCategoryToRestaurantBranch = $getRestaurantBranch->branchCategory()->detach([$category["id"]]);
            }
            $this->log->storeLogInfo(["removing category sorting",[
                "data" => $request->all()
            ]]);
            DB::commit();

            return response()->json([
                "status" => "200",
                "message" => "Category sorting removed successfully."
            ]);
        }
        catch (ModelNotFoundException $ex){
            $this->log->storeLogError(["Restaurant branch could not be found",[
                "data" => $request->all(),
                "message" => $ex->getMessage()
            ]]);
            return response()->json([
                "status" => "404",
                "message" => "Could not found requested restaurant branch."
            ],404);
        }

        catch (\Exception $ex){
            $this->log->storeLogError(["creating category sorting error",[
                "data" => $request->all(),
                "message" => $ex->getMessage()
            ]]);
            return response()->json([
                "status" => "500",
                "message" => "Error attaching categories sorting."
            ],500);
        }
    }

    /**
     * check the authority of the user.It will check weather the user is admin,suoer admin,branch admin or restaurant admin
     * @param $restaurant_id
     * @return bool
     */
    private function checkUserAuthority($restaurant_id){

        if (RoleChecker::hasRole('admin') || RoleChecker::hasRole('super-admin')) {
            return true;


        } /**
         * if user have role restaurant-admin or parent role restaurant-admin, then verification is done.
         * oauth response consists restaurant information.if user is restaurant-admin and is_restaurant_admin is false then user cant be able to view
         */
        elseif (RoleChecker::hasRole("restaurant-admin") || (RoleChecker::get('parentRole') == "restaurant-admin")) {
            $restaurantInfo = RoleChecker::getRestaurantInfo();
            $flag = false;
            if (!is_null($restaurantInfo) && ($restaurantInfo["is_restaurant_admin"] == true) && !is_null($restaurantInfo["branch_id"])) {
                if (!in_array($restaurant_id, $restaurantInfo["branch_id"])) {
                    return false;
                }

                return true;

            } else {
                return true;
            }
        }
        elseif (RoleChecker::hasRole("branch-admin")) {
            $restaurantInfo = RoleChecker::getRestaurantInfo();
            if (!is_null($restaurantInfo) && !is_null($restaurantInfo["branch_id"]) && ($restaurantInfo["is_restaurant_admin"] == false)) {
                if (($restaurantInfo["branch_id"] != $restaurant_id)) {

                    return false;
                }
                return true;

            } else {
                return false;
            }

        } else {
            return false;
        }
    }



}