<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/28/2017
 * Time: 12:38 PM
 */

namespace App\Http\Controllers;

use App\Http\RoleCheckerFacade;
use App\Repo\RestaurantBranchInterface;
use App\Repo\RestaurantInterface;
use App\RestaurantBranch;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Repo\RestaurantUsersInterface;
use Illuminate\Support\Facades\Input;
use RoleChecker;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use LogStoreHelper;
use Validator;
use RemoteCall;
use ImageUploader;
/**
 * Class RestaurantUserController
 * @package App\Http\Controllers
 */
class RestaurantUserController extends Controller
{
    protected $restaurantUsers;
    protected $restaurant;
    protected $checker;
    protected $logStoreHelper;
    protected $restaurantBranch;
    protected $oauthBaseUrl;

    /**
     * RestaurantController constructor.
     * @param RestaurantUsersInterface $restaurantUsers
     * @param RoleChecker $checker
     * @param LogStoreHelper $logStoreHelper
     * @param $restaurant
     * @internal param $restaurant_translation
     */
    public function __construct(RestaurantUsersInterface $restaurantUsers,
                                RoleChecker $checker,
                                RestaurantInterface $restaurant,
                                LogStoreHelper $logStoreHelper)
    {
        $this->checker=$checker;
        $this->logStoreHelper=$logStoreHelper;
        $this->restaurant=$restaurant;
        $this->oauthBaseUrl=Config::get('config.oauth_base_url');
    }


    public function restaurantUserViewAll($restaurant_id){
        try{
            $this->restaurant->getSpecificRestaurant($restaurant_id,$status=1);
        }catch (\Exception $e)
        {
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Restaurant  could not be found or status is disabled'], 404);
        }
        if(!$this->checkRolePotential($restaurant_id)){
            $this->logStoreHelper->storeLogError(array("No access", [
                "status" => "403",
                "message" => "You have no access to view Restaurant User"
            ]));
            return response()->json([
                "status" => "403",
                "message" => "You have no access to view Restaurant User"
            ],403);
        }
         $user=RemoteCall::getSpecific("$this->oauthBaseUrl/user/restaurant/$restaurant_id");
        if($user['status']!=200) {
            return response()->json(
                $user['message'], $user['status']);
        }
        return \Datatables::of(collect($user['message']["data"]))->make(true);

    }

    /**
     * Create a new user for restaurant based on the role passed.
     * User can be created only for restaurant whose status is enabled i.e 1
     * Only admin or super admin can create user and authorized restaurant admin can create user as well
     * @param $restaurant_id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function restaurantUserCreate($restaurant_id, Request $request){
        try{
            $this->restaurant->getSpecificRestaurant($restaurant_id,$status=1);
        }catch (\Exception $e)
        {
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Restaurant  could not be found or status is disabled'], 404);
        }
        try {
            $message =[
                "domain_validation" => "The mail server for domain does not exist."
            ];
            $this->validate($request, [
                'status'=>'integer|min:0|max:1',
                'first_name' => 'required|max:50',
                'last_name' => 'required|max:50',
                'email' => 'required|email|domain_validation|max:255',
                'password' => 'required|max:255',
                'gender'=>'required|in:male,female',
                'mobile'=>'required|min:7',
                "role"=>"required",
                "birthday"=>"date_format:Y-m-d",
            ],$message);
        }catch (\Exception $ex) {
            $this->logStoreHelper->storeLogError(array("Error in Validation in user index", [
                "status" => "422",
                "message" => $ex->response->original
            ]));
            return response()->json([
                "status" => "422",
                "message" =>$ex->response->original
            ],422);
        }
        /**
         * if user has no access
         * */
        if(!$this->checkRolePotential($restaurant_id)){
            $this->logStoreHelper->storeLogError(array("No access", [
                "status" => "403",
                "message" => "You have no access to create Restaurant User"
            ]));
            return response()->json([
                "status" => "403",
                "message" => "You have no access to create Restaurant User"
            ],403);
        }
        $request['role']=$request["role"];
        $request['meta']=[
            'is_restaurant_admin' => true,
            'restaurant_id' => $restaurant_id
        ];
        $user=RemoteCall::store("$this->oauthBaseUrl/restaurant/user/create",$request->all());
        if($user['status']!=200) {
            return response()->json(
                $user['message'], $user['status']);
        }
        return response()->json([
            "status"=>"200",
            "message"=>"Restaurant User created successfully"
        ],200);
    }

    public function restaurantUserDelete($restaurant_id,$id){
        try{
            $this->restaurant->getSpecificRestaurant($restaurant_id,$status=1);
        }catch (\Exception $e)
        {
            return response()->json([
                'status'=>'404',
                'message'=>'Requested Restaurant  could not be found or status is disabled'], 404);
        }
        /**
         * if user has no access
         * */
        if(!$this->checkRolePotential($restaurant_id)){
            $this->logStoreHelper->storeLogError(array("No access", [
                "status" => "403",
                "message" => "You have no access to delete Restaurant User"
            ]));
            return response()->json([
                "status" => "403",
                "message" => "You have no access to delete Restaurant User"
            ],403);
        }
        $user=RemoteCall::deleteSpecific("$this->oauthBaseUrl/restaurant/user/$id");
        if($user['status']!=200) {
            $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser",[
                $user['message']
            ]));
            return response()->json(
                $user['message'], $user['status']);
        }
        $this->logStoreHelper->storeLogInfo(array("Restaurant User",[
            'status'=>'200',
            'message'=>'Restaurant User deleted successfully'
        ]));
        return response()->json([
            'status'=>'200',
            'message'=>"Restaurant User deleted successfully"
        ]);

    }

    /**
     * check the user role level.
     *
     * @param $restaurant_id
     * @return bool
     */
    private function checkRolePotential($restaurant_id){
        // restaurant-admin has access to  his restaurant only
        if ((RoleChecker::hasRole('restaurant-admin') || (RoleChecker::get('parentRole') == 'restaurant-admin'))) {
            try {
                if(RoleChecker::get('is_restaurant_admin') != true){
                    throw new \Exception();
                }
                if ($restaurant_id == RoleChecker::get('restaurant_id')) {
                    return true;
                } else {
                    throw new \Exception();
                }

            } catch (\Exception $ex) {
                return false;
            }
        }
        /**
         * Branch admin has no access to this
         * */
        elseif (RoleChecker::hasRole('branch-admin') || (RoleChecker::get('parentRole') == 'branch-admin') ) {
            return false;
        }
        return true;
    }
}

