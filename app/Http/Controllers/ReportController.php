<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 12/18/17
 * Time: 12:51 PM
 */

namespace App\Http\Controllers;


use Carbon\Carbon;
use Illuminate\Http\Request;


use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{


    public function getBranchesOfRestaurant($id)
    {
        try {
            $restaurant = DB::table('restaurants')
                ->join("restaurant_translation", "restaurant_translation.restaurant_id", "=", "restaurants.id")
                ->join("restaurant_branches", "restaurants.id", "=", "restaurant_branches.parent_id")
                ->join("branch_translation", "restaurant_branches.id", "=", "branch_translation.branch_id")
                ->where('restaurants.id', $id)
                ->select('restaurant_translation.name as parent_name', 'restaurant_branches.id as branch_id', 'branch_translation.name', 'restaurants.id as parent_id')->where([
                    ["restaurant_translation.lang_code", "en"],
                    ["branch_translation.lang_code", "en"]
                ])
                ->get();
            if (empty($restaurant)) {
                throw new \Exception();
            }
            foreach ($restaurant as $res) {
                $data['branch_list'][] = $res->branch_id;
                $data['branch_name'][$res->branch_id] = $res->name;

            }
            $data['parent_name'] = $restaurant[0]->parent_name;


            return response()->json([
                "status" => "200",
                "data" => $data
            ], 200);

        } catch (\Exception $ex) {

            return response()->json([
                "status" => "404",
                "message" => "Restaurant not found"
            ], 404);
        }
    }

    public function getBranchesOfRestaurants(Request $request)
    {
        try {
            $data = [];
            //return $request->restaurant_id;
            foreach ($request->restaurant_id as $id) {
                $restaurant = DB::table('restaurants')
                    ->join("restaurant_translation", "restaurant_translation.restaurant_id", "=", "restaurants.id")
                    ->join("restaurant_branches", "restaurants.id", "=", "restaurant_branches.parent_id")
                    ->join("branch_translation", "restaurant_branches.id", "=", "branch_translation.branch_id")
                    ->where('restaurants.id', $id)
                    ->select('restaurant_translation.name as parent_name', 'restaurant_branches.id as branch_id', 'branch_translation.name', 'restaurants.id as parent_id')->where([
                        ["restaurant_translation.lang_code", "en"],
                        ["branch_translation.lang_code", "en"]
                    ])
                    ->get();


                if ($restaurant->count() <= 0) {
                    continue;
                }
                unset($check);
                foreach ($restaurant as $res) {

                    $check['branch_list'][] = $res->branch_id;
                    $check['branch_name'][$res->branch_id] = $res->name;

                }
                $check['parent_name'] = $restaurant[0]->parent_name;
                $data[$res->parent_id] = $check;

            }

            return response()->json([
                "status" => "200",
                "data" => $data
            ], 200);


        } catch (\Exception $ex) {

            return response()->json([
                "status" => "404",
                "message" => $ex->getMessage()
            ], 404);
        }
    }

    public function getRestaurantAndBranches()
    {
        try{

            $restaurant_count =   DB::table('restaurants')->select(DB::raw('count(*) as restaurant_count, country_id'))
                ->groupBy('country_id')->get();

            $branch_count = DB::table('restaurant_branches')->select(DB::raw('count(*) as branch_count, country_id'))
                ->groupBy('country_id')->get();




            return response()->json([
                "status" => "200",
                "data" => [
                    'restaurant_count'=> $restaurant_count,
                    'branch_count' => $branch_count
                ]
            ], 200);


        } catch (\Exception $ex) {

            return response()->json([
                "status" => "404",
                "message" => $ex->getMessage()
            ], 404);
        }


    }

}