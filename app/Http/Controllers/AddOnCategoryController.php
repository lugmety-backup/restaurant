<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/8/17
 * Time: 3:23 PM
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Repo\AddOnCategoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repo\AddOnCategoryTranslationInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;
use AuthChecker;
use RestaurantChecker;
use LogStoreHelper;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Datatables;
use LocationService;
use App\Repo\RestaurantBranchInterface;



class AddOnCategoryController extends Controller
{
    protected $addonCategory;
    protected $addonCategoryTranslation;
    protected $checker;
    protected $logStoreHelper;
    protected $restaurantBranch;


    /**
     * AddOnCategoryController constructor.
     * @param AddOnCategoryInterface $addonCategory
     * @param AddOnCategoryTranslationInterface $addonCategoryTranslation
     * @param LogStoreHelper $logStoreHelper
     */
    public function __construct(AddOnCategoryInterface $addonCategory,
                                AddOnCategoryTranslationInterface $addonCategoryTranslation,
                                LogStoreHelper $logStoreHelper, RestaurantBranchInterface $restaurantBranch)
    {
        $this->addonCategory = $addonCategory;
        $this->addonCategoryTranslation = $addonCategoryTranslation;
        $this->logStoreHelper = $logStoreHelper;
        $this->restaurantBranch = $restaurantBranch;

    }

    /**
     * Display the list of addon category related to specific restaurant
     * If 'lang' parameter is not supplied,then default language is set to 'en'.
     * If 'status' parameter is not supplied, Authorized personnel can view all the data i.e. of both status 0 and 1.
     * If 'status' parameter is supplied, Authorized personnel can view data having requested status. Others can view data having status 1 in any case.
     * 'limit' parameter is used to get no. of records, user want to view in per page.
     * 'page' parameter is used to get page no. of current record display
     * If 'limit' parameter is not supplied, only 10 records are shown per page. Else, no. of records is equal to limit supplied.
     *
     * @param $restaurantId
     * @return \Illuminate\Http\JsonResponse
     */

    public function index($restaurantId)
    {
        try {
//            if (!is_null(Input::get('status'))) {
//                $request['status'] = Input::get("status");
//
//            };
//            $request['lang'] = Input::get("lang", "en");
//            $request['limit'] = Input::get("limit", 10);
//            /*
//             * Validate the status and limit
//             * Status must be 0 or 1.
//             * limit must be integer.
//             *
//             * */
//            $rules = [
//                'status' => 'sometimes|required|integer|min:0|max:1',
//                'limit' => 'required|integer'
//            ];
//            $validator = Validator::make($request, $rules);
//            if ($validator->fails()) {
//                return response()->json([
//                    "status" => "422",
//                    "message" => $validator->errors()
//                ], 422);
//            }
//            /**
//             *
//             * check the authority of the logged in user and set the status according to the output of check function of AuthChecker facade.
//             * if output is "true", set status to all ie user can view all addon categories according to restaurant
//             *if output  is "false", set status to 1 i.e. user can only view addon categories of status 1.
//             */
//
//            if (AuthChecker::check($restaurantId)) {
//                /**
//                 * If status is not given or empty, then status is set to "all".
//                 * It means all the available data is to fetched.
//                 * If status is supplied, filter data accordingly
//                 */
//                if (!isset($request['status'])) {
//                    $request['status'] = "all";
//
//                }
//
//                /**
//                 * If the user is not a authorized personnel, check the status of restaurant.
//                 * If restaurant is active, fetch active addon categories of that restaurant.
//                 * If restaurant is inactive, throw exception
//                 */
//            } else {
//                if (RestaurantChecker::check($restaurantId)) {
//                    $request['status'] = 1;
//                } else {
//                    return response()->json([
//
//                        "status" => "404",
//                        "message" => "Restaurant you entered is not found"
//
//                    ], 404);
//                }
//
//            }
//
//            $categories = $this->addonCategory->getAllAddOnCategory($restaurantId, $request['status'], $request['limit']);
//            try {
//                if (!$categories->first()) {
//                    throw  new \Exception();
//                }
//            } catch (\Exception $ex) {
//                return response()->json([
//                    "status" => "404",
//                    "message" => "Empty Record"
//                ], 404);
//            }
//            foreach ($categories as $key => $category) {
//                /**
//                 * Get category Translation based on @param lang
//                 * if empty record is obtained, default en lang is returned
//                 * If default en lang is not available, throw exception
//                 * */
//                $categoryTranslation = $this->addonCategoryTranslation->getSpecificAddOnCatTransalationByLang($category->id, $request['lang']);
//                try {
//                    if ($categoryTranslation->count() == 0) {
//                        if ($request['lang'] == 'en') {
//                            throw new \Exception();
//                        } else {
//                            $categoryTranslation = $this->addonCategoryTranslation->getSpecificAddOnCatTransalationByLang($category->id, 'en');
//                            if ($categoryTranslation->count() == 0) {
//                                throw new \Exception();
//                            }
//                        }
//
//                    }
//                } catch (\Exception $ex) {
//                    return response()->json([
//                        "status" => "404",
//                        "message" => "Default language english not found in database"
//                    ], 404);
//                }
//
//                /*
//                 * attach name and lang_code in $category variable
//                 * */
//                $category['name'] = $categoryTranslation[0]['name'];
//                $category['lang'] = $categoryTranslation[0]['lang'];
//            }
//
//            return response()->json([
//                'status' => "200",
//                "data" => $categories
//            ], 200);



            $categories = $this->addonCategory->getAllActiveAddonCategories($restaurantId);
            //return $categories;
            try {
                if (!$categories->first()) {
                    throw  new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Empty Record"
                ], 404);
            }
            foreach ($categories as $key => $category) {
                /**
                 * Get category Translation based on @param lang
                 * if empty record is obtained, default en lang is returned
                 * If default en lang is not available, throw exception
                 * */
                $categoryTranslation = $this->addonCategoryTranslation->getSpecificAddOnCatTransalationByLang($category->id, "en");
                try {
                    if ($categoryTranslation->count() == 0) {
                         {
                            throw new \Exception();
                        }


                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Default language english not found in database"
                    ], 404);
                }

                /*
                 * attach name and lang_code in $category variable
                 * */
                $category['name'] = $categoryTranslation[0]['name'];
                $category['lang'] = $categoryTranslation[0]['lang'];
            }

            return response()->json([
                'status' => "200",
                "data" => $categories
            ], 200);

        } catch (\Exception $ex) {

            return response()->json([
                "status" => "404",
                "message" => "something went wrong"
            ], 404);
        }
    }

    /**
     * Store addon category related to specific restaurant
     * @param $restaurantId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function store($restaurantId, Request $request)
    {
        /**
         * Check if logged in user has authority to insert data.
         * If 'true', continue. Else abort with exception message.
         *
         * Validate the request
         * min_addon field is required and must be integer.
         * max_addon field is required and must be integer.
         * status field is required and must be integer. Only 0 or 1 is acceptable.
         * translation field is required and must be an array
         * If validation fails throw error message and abort.
         */
        $restaurant = $this->restaurantBranch->getSpecificRestaurantBranchForPublic($restaurantId);

        if (count($restaurant) > 0) {
            if (AuthChecker::check($restaurantId) == "true") {
                try {
                    $this->validate($request, [
                        "min_addon" => "required|integer",
                        "max_addon" => "required|integer|min:" . $request['min_addon'],
                        "status" => "required|integer|min:0|max:1",
                        "translation" => "required|array"
                    ]);

                } catch (\Exception $e) {

                    return response()->json([
                        "status" => "422",
                        "message" => $e->response->original
                    ], 422);
                }

                try {
                    /**
                     * Use remote location service to verify the translation request.
                     * If the translation lang codes are not same as country's lang codes, error message is returned.
                     */
                    $countryValidate = LocationService::checkLanguageCode($restaurant[0]['country_id'], $request['translation']);

                    if ($countryValidate['status'] != 200) {
                        return response()->json([
                            "status" => $countryValidate["status"],
                            "message" => $countryValidate["message"]
                        ], $countryValidate["status"]);

                    }
                    /**
                     * Start DB transaction to ensure the operation is reversed in case not successfully committed.
                     *
                     */
                    DB::beginTransaction();
                    $request['restaurant_id'] = $restaurantId;

                    /**
                     * Insert request to addoncategory table
                     */

                    $category = $this->addonCategory->createAddOnCategory($request->only('restaurant_id', 'min_addon', 'max_addon', 'status'));

                    /**
                     * Validate each item of an array.
                     * For category_id, use id of recently created category
                     */

                    foreach ($request['translation'] as $key => $translation) {
                        $translation['category_id'] = $category->id;
                        $translation['lang'] = $key;
                        /**
                         * Validate lang and name fields.
                         * lang field is required and must be alphabets.
                         * name field is required.
                         * If validation fails, abort with validation error message.
                         * Else insert data in addoncategory_translation table.
                         */
                        $rules = [
                            "lang" => 'required|alpha',
                            "name" => "required",
                        ];
                        $translation['lang'] = str_slug($translation['lang']);

                        $validator = Validator::make($translation, $rules);
                        if ($validator->fails()) {
                            $error = $validator->errors();
                            $this->logStoreHelper->storeLogError([
                                "Error in Validation", [
                                    "status" => "422",
                                    "message" => [$key => $error]
                                ]
                            ]);
                            return response()->json([
                                "status" => "422",
                                "message" => [$key => $error]
                            ], 422);
                        }

                        /**
                         * Insert $translation into addoncategory_translation table
                         */
                        $createTranslation = $this->addonCategoryTranslation->createAddOnCatTranslation($translation);

                    }
                    /**
                     * If operation is successfully executed, commit it, else revert whole operation.
                     */

                    DB::commit();

                    $this->logStoreHelper->storeLogInfo([
                        "Addon Category", [
                            "status" => "200",
                            "message" => "Addon Category Created Successfully"
                        ]
                    ]);
                    return response()->json([
                        "status" => "200",
                        "message" => "Addon Category Created Successfully"
                    ], 200);
                } catch (\Exception $e) {
                    return response()->json([
                        "status" => "422",
                        "message" => "There were some problem in creating addon category"
                    ], 422);
                }
            } else {
                return response()->json([
                    "status" => "401",
                    "message" => "Only Authorized personnel can create addon category"
                ], 401);
            }
        } else {
            return response()->json([

                "status" => "404",
                "message" => "Restaurant you entered is not found"

            ], 404);
        }


    }

    /**
     * Delete addon category
     * @param $restaurantId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function destroy($restaurantId, $id)
    {
        /**
         * Check if logged in user has authority to delete data.
         * If 'true', continue. Else abort with exception message.
         */


        if (AuthChecker::check($restaurantId)) {
            try {

                /*
                 * delete category along with its child rows
                 *
                 *
                 * */
                $this->addonCategory->getSpecificAddonCategoryByRestaurant($id, $restaurantId);

                $this->addonCategory->deleteAddOnCategory($id);
                return response()->json([
                    "status" => "200",
                    "message" => "Requested Category Deleted Successfully"
                ], 200);


            } catch (ModelNotFoundException $notFoundException) {
                /**
                 * If requested data is not found, return 404 error with message.
                 */
                return response()->json([
                    'status' => '404',
                    'message' => "Requested category not found"
                ], 404);
            }
        } else {
            return response()->json([
                'status' => "401",
                "message" => "Only Authorized personnel can delete Category"
            ], 401);
        }


    }

    /**
     *
     * Display addon category of given id.
     * If status of required data is 0, then only authorized personnel can view the record. Unauthorized can only view data having status 1.
     * @param $restaurantId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function show($restaurantId, $id)
    {
        try {

            $request['lang'] = Input::get("lang", "en");
            /**
             *
             * Check if restaurant is active, for unauthorized user.
             * if restaurant is not active, throw exception message.
             */
            if (!AuthChecker::check($restaurantId)) {
                if (!RestaurantChecker::check($restaurantId)) {
                    return response()->json([

                        "status" => "404",
                        "message" => "Restaurant you entered is not found"

                    ], 404);
                }
            }

            /*
             *
             * Fetch data of category having specific id of any restaurant.
            */
            try {

                $category = $this->addonCategory->getSpecificAddonCategoryByRestaurant($id, $restaurantId);

                /**
                 * Check if category is active.
                 * If category is inactive, throw exception for unauthorized user.
                 * Display data for authorized users.
                 */

                if ($category['status'] == 0) {

                    if (!AuthChecker::check($restaurantId)) {
                        throw new \Exception();
                    }
                }


            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Empty Record for requested category"
                ], 404);
            }
            /*
             * Get category Translation based on @param lang
             * if empty record is obtained default en lang is returned
             * If default en lang is not available, throw exception
             *
             * */
            $categoryTranslation = $this->addonCategoryTranslation->getSpecificAddOnCatTransalationByLang($category->id, $request['lang']);
            try {
                if ($categoryTranslation->count() == 0) {
                    if ($request['lang'] == 'en') {
                        throw new \Exception();
                    } else {
                        $categoryTranslation = $this->addonCategoryTranslation->getSpecificAddOnCatTransalationByLang($category->id, 'en');
                        if ($categoryTranslation->count() == 0) {
                            throw new \Exception();
                        }
                    }

                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Default language english not found in database"
                ], 404);
            }


            /*
             * attach name and lang in $category variable
             * */
            $category['name'] = $categoryTranslation[0]['name'];
            $category['lang'] = $categoryTranslation[0]['lang'];
            return response()->json([
                'status' => "200",
                "data" => $category
            ], 200);

        } catch (ModelNotFoundException $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Empty Record for requested category"
            ], 404);
        }
    }

    /**
     *Update addon category of given id
     * @param $restaurantId
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function update($restaurantId, Request $request, $id)
    {
        /**
         * Check if logged in user has authority to update data.
         * If 'true', continue. Else abort with exception message.
         *
         * Validate the request
         * min_addon field is required and must be integer.
         * max_addon field is required and must be integer.
         * status field is required and must be integer. Only 0 or 1 is acceptable.
         * translation field is required and must be an array
         * If validation fails throw error message and abort.
         */
        $restaurant = $this->restaurantBranch->getSpecificRestaurantBranchForPublic($restaurantId);

        if (count($restaurant) > 0) {
            if (AuthChecker::check($restaurantId) == "true") {
                try {
                    $this->validate($request, [//validating the request
                        "min_addon" => "required|integer",
                        "max_addon" => "required|integer|min:" . $request['min_addon'],
                        "status" => "required|integer|min:0|max:1",
                        "translation" => "required|array"
                    ]);

                } catch (\Exception $e) {
                    /*
                     *creates log for storing error
                     * */

                    return response()->json([
                        "status" => "422",
                        "message" => $e->response->original
                    ], 422);
                }

                try {
                    /**
                     * Use remote location service to verify the translation request.
                     * If the translation lang codes are not same as country's lang codes, error message is returned.
                     */
                    $countryValidate = LocationService::checkLanguageCode($restaurant[0]['country_id'], $request['translation']);

                    if ($countryValidate['status'] != 200) {
                        return response()->json([
                            "status" => $countryValidate["status"],
                            "message" => $countryValidate["message"]
                        ], $countryValidate["status"]);

                    }
                    /**
                     * Start DB transaction to ensure the operation is reversed in case not successfully committed.
                     *
                     */
                    DB::beginTransaction();

                    $category = $this->addonCategory->getSpecificAddonCategoryByRestaurant($id, $restaurantId);

                    /*
                   * Delete related translations of given category using relationships and re-enter data
                   * */

                    $category->categoryTranslation()->delete();
                    /**
                     * Validate lang and name fields.
                     * lang field is required and must be alphabets.
                     * name field is required.
                     * If validation fails, abort with validation error message.
                     * Else update data in addoncategory_translation table.
                     */
                    foreach ($request['translation'] as $key => $translation) {
                        $translation['category_id'] = $category->id;
                        $translation['lang'] = $key;
                        $rules = [
                            "lang" => 'required|alpha',
                            "name" => "required",
                        ];
                        $translation['lang'] = str_slug($translation['lang']);

                        $validator = Validator::make($translation, $rules);
                        if ($validator->fails()) {
                            $error = $validator->errors();
                            $this->logStoreHelper->storeLogError([
                                "Error In validation", [
                                    "status" => "422",
                                    "message" => [$key => $error]
                                ]
                            ]);
                            return response()->json([
                                "status" => "422",
                                "message" => [$key => $error]
                            ], 422);
                        }

                        $createTranslation = $this->addonCategoryTranslation->createAddOnCatTranslation($translation);
                    }

                    /**
                     * update data of the given category id
                     */
                    $category = $this->addonCategory->updateAddOnCategory($id, $request->only('min_addon', 'max_addon', 'status'));

                    $this->logStoreHelper->storeLogInfo([
                        "AddonCategory", [
                            "status" => "200",
                            "message" => "Category Updated Successfully",
                            "data" => $category
                        ]
                    ]);
                    /**
                     * If operation is successfully executed, commit it, else revert whole operation.
                     */

                    DB::commit();
                    return response()->json([
                        "status" => "200",
                        "message" => "Category updated Successfully"
                    ], 200);

                } catch (ModelNotFoundException $notFoundException) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Requested category not found"
                    ], 404);
                } catch (\Exception $e) {
                    return response()->json([
                        "status" => "422",
                        "message" => "There were some problem in updating category "
                    ], 422);
                }
            } else {
                return response()->json([
                    "status" => "401",
                    "message" => "Only Authorized personnel can update category "
                ], 401);
            }
        } else {
            return response()->json([

                "status" => "404",
                "message" => "Restaurant you entered is not found"

            ], 404);
        }

    }

    /**
     * Get all the addon data for datatable.
     * Addon name is only available in english.
     * @param Request $request
     * @return mixed
     */

    public function getForDatatable($restaurantId, Request $request)
    {
        try {
            $addonCategoryData = new Collection($this->addonCategory->getAddonCategoryForDatatable($restaurantId));
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage()
            ],500);
        }
        return Datatables::of($addonCategoryData)
            ->addColumn('name', function ($addonCategory) {
                return $addonCategory->categoryTranslation->map(function ($translation) {
                    return $translation->name;
                })->implode('');
            })->make(true);
        //}
    }

    /**
     *
     * Display addon category of given id with all related translations for admin.
     * @param $restaurantId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function adminShow($restaurantId, $id)
    {
        if (AuthChecker::check($restaurantId)) {

            /*
             *
             * Fetch data of category having specific id of any restaurant.
            */
            try {

                $category = $this->addonCategory->getSpecificAddonCategoryByRestaurant($id, $restaurantId);


                /*
                 * attach name and lang in $category variable
                 * */
                $category['translation'] = $category->categoryTranslation;

                return response()->json([
                    'status' => "200",
                    "data" => $category
                ], 200);

            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Empty Record for requested category"
                ], 404);
            }
        } else {
            return response()->json([
                "status" => "401",
                "message" => "Unauthorized action"
            ], 401);
        }


    }
}