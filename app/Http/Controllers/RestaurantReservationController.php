<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/7/2017
 * Time: 9:14 PM
 */

namespace App\Http\Controllers;

use App\Events\EmailEvent;
use App\Http\Facades\RoleCheckerFacade;
use App\Repo\RestaurantBranchInterface;
use App\Repo\RestaurantHourInterface;
use App\Repo\RestaurantReservationInterface;
use App\RestaurantReservation;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use LogStoreHelper;
use RemoteCall;
use Illuminate\Support\Facades\Config;
use RoleChecker;
use Datatables;
use App\Repo\RestaurantBranchTranslationInterface;

class RestaurantReservationController extends Controller
{
    /**
     * @var RestaurantReservationInterface
     */
    protected $restaurantReservation;
    protected $restaurantReservationModel;
    /**
     * @var RestaurantBranchInterface
     */
    protected $restaurantBranch;
    /**
     * @var LogStoreHelper
     */
    protected $log;

    protected $logStoreHelper;
    protected $restaurantHour;

    protected $branchTranslation;

    /**
     * RestaurantReservationController constructor.
     * @param RestaurantReservationInterface $RestaurantReservation
     * @param RestaurantBranchInterface $restaurant
     * @param LogStoreHelper $log
     */
    public function __construct(RestaurantReservation $restaurantReservationModel, RestaurantReservationInterface $restaurantReservation, RestaurantBranchInterface $restaurantBranch, LogStoreHelper $logStoreHelper, RestaurantHourInterface $restaurantHour, RestaurantBranchTranslationInterface $branchTranslation)
    {
        $this->restaurantReservation = $restaurantReservation;
        $this->restaurantBranch = $restaurantBranch;
        $this->logStoreHelper = $logStoreHelper;
        $this->restaurantHour = $restaurantHour;
        $this->restaurantReservationModel = $restaurantReservationModel;
        $this->branchTranslation = $branchTranslation;
    }

    public function adminIndex($branch_id)
    {
        try {
            $restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($branch_id);
        } catch (ModelNotFoundException $ex) {
            $this->logStoreHelper->storeLogError(array('Restaurant Reservation', [
                'status' => '404',
                "message" => "Restaurant branch could not be found"
            ]));
            return response()->json([
                'status' => '404',
                "message" => "Restaurant branch could not be found"
            ], 404);
        }

        if (!$this->checkRolePotention($branch_id)) {
            return response()->json([
                'status' => '403',
                "message" => "Unauthorized"
            ], 403);
        }

        try {
            $restaurantReservation = $this->restaurantReservation->getAllRestaurantReservationOfBranch($branch_id, null);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Restaurant Reservation could not be found"
            ], 404);
        }
        $this->logStoreHelper->storeLogInfo(array("Restaurant Reservation", [
            'status' => '200',
            'data' => $restaurantReservation
        ]));
        return \Datatables::of($restaurantReservation)->make(true);

    }

    public function selfIndex()
    {
        $user_id = RoleCheckerFacade::getUser();
        try {
            $restaurantReservation = $this->restaurantReservation->getAllRestaurantReservationOfUser($user_id);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Restaurant Reservation could not be found"
            ], 404);
        }
        $this->logStoreHelper->storeLogInfo(array("Restaurant Reservation", [
            'status' => '200',
            'data' => $restaurantReservation
        ]));
        return response()->json([
            "status" => "200",
            "data" => $restaurantReservation
        ], 200);

    }

    public function adminShow($id, $branch_id, Request $request)
    {
        try {
            $restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($branch_id);
        } catch (ModelNotFoundException $ex) {
            $this->logStoreHelper->storeLogError(array('Restaurant Reservation', [
                'status' => '404',
                "message" => "Restaurant branch could not be found"
            ]));
            return response()->json([
                'status' => '404',
                "message" => "Restaurant branch could not be found"
            ], 404);
        }

        try {
            $restaurantReservation = $this->restaurantReservation->getSpecificRestaurantReservation($id);
        } catch (ModelNotFoundException $ex) {
            $this->logStoreHelper->storeLogError(array('Restaurant Reservation', [
                'status' => '404',
                "message" => "Restaurant reservation could not be found"
            ]));
            return response()->json([
                'status' => '404',
                "message" => "Restaurant reservation could not be found"
            ], 404);
        }

        return response()->json([
            'status' => '200',
            "data" => $restaurantReservation
        ], 200);

    }

    public function create($branch_id, Request $request)
    {
        $lang = Input::get('lang','en');
        try {
            $restaurantBranch = ($this->restaurantBranch->getSpecificRestaurantBranch($branch_id));
        } catch (ModelNotFoundException $ex) {
            $this->logStoreHelper->storeLogError(array('Restaurant Reservation', [
                'status' => '404',
                "message" => "Restaurant branch could not be found"
            ]));
            return \Settings::getErrorMessageOrDefaultMessage('restaurant-restaurant-branch-could-not-be-found',
                "Restaurant branch could not be found.", 404, $lang);
        }
        try {
            $this->validate($request, [
                'full_name' => "required",
                "email" => "required|email",
                "phone" => "required|min:4|max:19",
                "booking_date" => "required|date_format:Y-m-d",
                "booking_time" => "required",
                "special_instruction" => "sometimes",
                "seats_count" => "required|integer|min:1",
                "token" => "sometimes"
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            /*
             *creates log for storing error
             * */
            $this->logStoreHelper->storeLogError(array("Error in Validation", [
                "status" => "422",
                "message" => $e->response->original
            ]));
            return response()->json([
                "status" => "422",
                "message" => $e->response->original
            ], 422);
        }
        $booking_date = Carbon::createFromFormat('Y-m-d', $request['booking_date']);
        $booking_day = $booking_date->format('l');
        try {
            $restaurantHours = $this->restaurantHour->getSpecificRestaurantHourByIdRestaurantIdDay($branch_id, $booking_day);
            foreach ($restaurantHours as $restaurantHour) {
                $restaurantHour['opening_time'] = $this->changeToTimezoneOfBranch($restaurantHour['opening_time'], $restaurantBranch['timezone']);

                $restaurantHour['closing_time'] = $this->changeToTimezoneOfBranch($restaurantHour['closing_time'], $restaurantBranch['timezone']);
            }
        } catch (ModelNotFoundException $ex) {
            $this->logStoreHelper->storeLogError(array('Restaurant Reservation', [
                'status' => '404',
                "message" => "Restaurant hour could not be found"
            ]));
            return response()->json([
                'status' => '404',
                "message" => "Restaurant hour could not be found"
            ], 404);
        }

        $notification['booking_date'] = $request['booking_date'];
        $notification['booking_time'] = $request['booking_time'];

        $request['booking_time'] = Carbon::createFromFormat('Y-m-d H:i A', '1975-05-21 ' . $request['booking_time'])->format("H:i:s");
        $booking_time = strtotime($request['booking_time']);
        $check = 0; //false
        foreach ($restaurantHours as $hour) {
            $opening_time = strtotime($hour['opening_time']);

            $closing_time = strtotime($this->decreaseTimeByAnHour($restaurantHour['closing_time']));

            if ($opening_time < $booking_time && $booking_time <= $closing_time) {
                $check = 1;
            }
        }
        if ($check == 0) {
            return \Settings::getErrorMessageOrDefaultMessage('restaurant-restaurant-reservation-not-available-at-this-time',
                "Restaurant reservation not available at this time.", 404, $lang);
        }
        $max_booking = $this->changeToUTC($request['booking_time'], $restaurantBranch['timezone']);
        $min_booking = $this->decreaseTimeByAnHour($max_booking);
        $min_booking = $this->decreaseTimeByAnHour($min_booking);
//        return $max_booking;
        $seatsReserved = $this->restaurantReservationModel->where([
            ['restaurant_id', $branch_id],
            ['booking_date', $request['booking_date']],
            ['booking_time', '>', $min_booking],
            ['booking_time', '<=', $max_booking]
        ])->where(function ($query) {
            $query->where('status', 'new')
                ->orWhere('status', 'accept');
        })
            ->selectRaw('sum(seats_count) as seat_sum')->first()->seat_sum;
        //return $seatsReserved;
        if ($restaurantBranch['no_of_seats'] < $seatsReserved + $request['seats_count']) {
            $seatsAvailable = $restaurantBranch['no_of_seats'] - $seatsReserved;
            try{
                $error_data = \Settings::getErrorMessage('restaurant-requested-number-of-seats-not-available');
                $error_message = $error_data['message'][$lang];
                return response()->json([
                    'status' => '404',
                    "message" => "Requested number of seats not available! Seats available at this time is " . $seatsAvailable
                ], 404);
            }catch (\Exception $ex){
                return response()->json([
                    'status' => '404',
                    "message" => "Requested number of seats not available! Seats available at this time is " . $seatsAvailable
                ], 404);
            }
        }

        $request['user_id'] = 0;
        if ($request['token'] != null) {
            $url = Config::get("config.oauth_base_url");
            $response = RemoteCall::getSpecificByToken($url . "/user/details", $request['token']);
            $response = json_decode(json_encode($response), true);
            if ($response['message']['status'] == 200) {
                $request['user_id'] = $response['message']['data']['id'];
            }

        }
        try {
            $request['booking_time'] = $this->changeToUTC($request['booking_time'], $restaurantBranch['timezone']);
            $request['restaurant_id'] = $branch_id;
            $request['status'] = "new";
            //            return $request->all();
            $restaurantReservations = $this->restaurantReservation->createRestaurantReservation($request->all());
        } catch (ModelNotFoundException $ex) {
            $this->logStoreHelper->storeLogError(array('Restaurant Reservation', [
                'status' => '404',
                "message" => "Restaurant reservation could not be created"
            ]));
            return \Settings::getErrorMessageOrDefaultMessage('restaurant-restaurant-reservation-could-not-be-created',
                "Restaurant reservation could not be created.", 404, $lang);
        }

        $restaurant = $restaurantBranch->branchTranslation()->where('lang_code', 'en')->first();
        $restaurantBranch = $restaurantBranch->toArray();
        $restaurantBranch['name'] = $restaurant['name'];

        $notification['id'] = $restaurantReservations->id;
        $notification['email'] = $request['email'];
        $notification['name'] = ucwords($request['full_name']);
        $notification['phone'] = $request['phone'];
        $notification['seats_count'] = $request['seats_count'];
        $notification['special_instruction'] = $request['special_instruction'];


        $message = [
            "service" => "restaurant service",
            "message" => "restaurant reservation created",
            "data" => [
                "user_details" => [],
                'reserver' => $notification,
                "restaurant" => $restaurantBranch,
                "restaurant_id" => $restaurantBranch['id'],
                "parent_id" => $restaurantBranch['parent_id']
            ]
        ];
        //return $message;

        event(new EmailEvent($message));

        $this->logStoreHelper->storeLogError(array('Restaurant Reservation', [
            'status' => '200',
            "message" => "Restaurant Reservation created successfully"
        ]));
        return \Settings::getErrorMessageOrDefaultMessage('restaurant-reservation-has-been-created-you-will-be-notified',
            "Reservation has been created. You will be notified after reservation is accepted.", 200, $lang);
//        return response()->json([
//            'status' => '200',
//            "message" => "Reservation has been created. You will be notified after reservation is accepted."
//        ], 200);

    }

    public function verify($id, $branch_id, Request $request)
    {
        try {
            $restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($branch_id);
        } catch (ModelNotFoundException $ex) {
            $this->logStoreHelper->storeLogError(array('Restaurant Reservation', [
                'status' => '404',
                "message" => "Restaurant branch could not be found"
            ]));
            return response()->json([
                'status' => '404',
                "message" => "Restaurant branch could not be found"
            ], 404);
        }

        if (!$this->checkRolePotention($branch_id)) {
            return response()->json([
                'status' => '403',
                "message" => "Unauthorized"
            ], 403);
        }

        try {
            $restaurantReservation = $this->restaurantReservation->getSpecificRestaurantReservation($id);
        } catch (ModelNotFoundException $ex) {
            $this->logStoreHelper->storeLogError(array('Restaurant Reservation', [
                'status' => '404',
                "message" => "Restaurant reservation could not be found"
            ]));
            return response()->json([
                'status' => '404',
                "message" => "Restaurant reservation could not be found"
            ], 404);
        }

        try {
            $messages = [
                "in" => "The shift must be either accept or reject"
            ];
            $this->validate($request, [
                'status' => "required|in:accept,reject",
            ], $messages);
        } catch (\Exception $e) {
            DB::rollBack();
            /*
             *creates log for storing error
             * */
            $this->logStoreHelper->storeLogError(array("Error in Validation", [
                "status" => "422",
                "message" => $e->response->original
            ]));
            return response()->json([
                "status" => "422",
                "message" => $e->response->original
            ], 422);
        }

        try {
            $req['status'] = $request['status'];
            //            return $request->all();
            $restaurantReservations = $this->restaurantReservation->updateRestaurantReservation($id, $req);
        } catch (ModelNotFoundException $ex) {
            $this->logStoreHelper->storeLogError(array('Restaurant Reservation', [
                'status' => '404',
                "message" => "Restaurant reservation could not be updated."
            ]));
            return response()->json([
                'status' => '404',
                "message" => "Restaurant reservation could not be updated."
            ], 404);
        }
        $restaurant = $restaurantBranch->branchTranslation()->where('lang_code', 'en')->first();
        $restaurantBranch = $restaurantBranch->toArray();
        $restaurantBranch['name'] = $restaurant['name'];

        $notification['id'] = $restaurantReservation->id;
        $notification['email'] = $restaurantReservation->email;
        $notification['name'] = ucwords($restaurantReservation->full_name);
        $notification['phone'] = $restaurantReservation->phone;
        $notification['booking_date'] = $restaurantReservation->booking_date;
        $notification['booking_time'] = $this->changeToTimezoneOfBranch($restaurantReservation->booking_time, $restaurantBranch['timezone']);
        $notification['seats_count'] = $restaurantReservation->seats_count;
        $notification['special_instruction'] = $restaurantReservation->special_instruction;
        $notification['status'] = $request['status'];
        $notification['updated_at'] = $this->changeToTimezoneOfBranch($restaurantReservation->updated_at, $restaurantBranch['timezone']);


        $message = [
            "service" => "restaurant service",
            "message" => "restaurant reservation verified",
            "data" => [
                "user_details" => [],
                'reserver' => $notification,
                "restaurant" => $restaurantBranch,
                "restaurant_id" => $restaurantBranch['id'],
                "parent_id" => $restaurantBranch['parent_id']
            ]
        ];

        event(new EmailEvent($message));

        $this->logStoreHelper->storeLogError(array('Restaurant Reservation', [
            'status' => '200',
            "message" => "Restaurant reservation updated."
        ]));
        return response()->json([
            'status' => '200',
            "message" => "Restaurant reservation updated."
        ], 200);

    }

    public function timeListOfRestaurantHours($branch_id)
    {
        try {
            $restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($branch_id);
        } catch (ModelNotFoundException $ex) {
            $this->logStoreHelper->storeLogError(array('Restaurant Reservation', [
                'status' => '404',
                "message" => "Restaurant branch could not be found"
            ]));
            return response()->json([
                'status' => '404',
                "message" => "Restaurant branch could not be found"
            ], 404);
        }
        $today_date = Carbon::now($restaurantBranch['timezone']);
        $today = $today_date->format('l');
        $now_time = $today_date->format("H:i:s");
        $today_date = $today_date->format('Y-m-d');

        $tomorrow_date = Carbon::tomorrow($restaurantBranch['timezone']);
        $tomorrow = $tomorrow_date->format('l');
        $tomorrow_date = $tomorrow_date->format('Y-m-d');
        try {
            $restaurantHoursToday = $this->restaurantHour->getSpecificRestaurantHourByIdRestaurantIdDay($branch_id, $today);
            foreach ($restaurantHoursToday as $restaurantHourToday) {
                $restaurantHourToday['opening_time'] = $this->changeToTimezoneOfBranch($restaurantHourToday['opening_time'], $restaurantBranch['timezone']);
                $restaurantHourToday['closing_time'] = $this->changeToTimezoneOfBranch($restaurantHourToday['closing_time'], $restaurantBranch['timezone']);
                $restaurantHourToday['opening_time'] = $this->convertToNearestThirtyMinuteTime($restaurantHourToday['opening_time']);
                $restaurantHourToday['closing_time'] = $this->decreaseTimeByAnHour($restaurantHourToday['closing_time']);
            }
            $restaurantHoursTomorrow = $this->restaurantHour->getSpecificRestaurantHourByIdRestaurantIdDay($branch_id, $tomorrow);
            foreach ($restaurantHoursTomorrow as $restaurantHourTomorrow) {
                $restaurantHourTomorrow['opening_time'] = $this->changeToTimezoneOfBranch($restaurantHourTomorrow['opening_time'], $restaurantBranch['timezone']);
                $restaurantHourTomorrow['closing_time'] = $this->changeToTimezoneOfBranch($restaurantHourTomorrow['closing_time'], $restaurantBranch['timezone']);
                $restaurantHourTomorrow['opening_time'] = $this->convertToNearestThirtyMinuteTime($restaurantHourTomorrow['opening_time']);
                $restaurantHourTomorrow['closing_time'] = $this->decreaseTimeByAnHour($restaurantHourTomorrow['closing_time']);
            }
        } catch (ModelNotFoundException $ex) {
            $this->logStoreHelper->storeLogError(array('Restaurant Reservation', [
                'status' => '404',
                "message" => "Restaurant hour could not be found"
            ]));
            return response()->json([
                'status' => '404',
                "message" => "Restaurant hour could not be found"
            ], 404);
        }

        $time_list = [];
        $time_list['today']['time'] = [];
        $time_list['today']['label'] = "Today";
        $time_list['today']['date'] = $today_date;
        $time_list['tomorrow']['time'] = [];
        $time_list['tomorrow']['label'] = "Tomorrow";
        $time_list['tomorrow']['date'] = $tomorrow_date;
//        foreach ($restaurantHoursToday as $restaurantHour) {
//            if ($restaurantHour['shift'] == "Day" && $restaurantHour["closed"] == 0) {
//                $day_time = $this->decreaseTimeByAnHour($restaurantHour['closing_time']);
//            }
//        }
//        foreach ($restaurantHoursToday as $restaurantHour) {
//            $time = $restaurantHour['opening_time'];
//            if ($restaurantHour['shift'] == "Day" && $restaurantHour["closed"] == 0) {
//                while ($restaurantHour['closing_time'] >= $time) {
//                    if ($now_time < $time) {
//                        $time_list['today']['time'][] = $this->changeTimeFormat($time);
//                    }
//                    $time = $this->increaseTimeByFifteenMinute($time);
//                    $time = $this->increaseTimeByFifteenMinute($time);
//                }
//            } elseif ($restaurantHour['shift'] == "Evening" && $restaurantHour["closed"] == 0) {
//                while ($restaurantHour['closing_time'] >= $time) {
//                    if ($now_time < $time && $time > $day_time) {
//                        $time_list['today']['time'][] = $this->changeTimeFormat($time);
//                    }
//
//                    $time = $this->increaseTimeByFifteenMinute($time);
//                    $time = $this->increaseTimeByFifteenMinute($time);
//                }
//            }
//
//        }

        $today_day_time = [];
        $today_evening_time = [];
        foreach ($restaurantHoursToday as $restaurantHour) {
            $time = $restaurantHour['opening_time'];
            if ($restaurantHour['shift'] == "Day" && $restaurantHour["closed"] == 0) {
                if($restaurantHour['opening_time'] < $now_time && $now_time < $restaurantHour['closing_time']) $open_flag = true;
                while ($restaurantHour['closing_time'] >= $time) {
                    if ($now_time < $time) {
                        $today_day_time[] = $time;
                    }
                    $time = $this->increaseTimeByFifteenMinute($time);
                    $time = $this->increaseTimeByFifteenMinute($time);
                }
            } elseif ($restaurantHour['shift'] == "Evening" && $restaurantHour["closed"] == 0) {
                if($restaurantHour['opening_time'] < $now_time && $now_time < $restaurantHour['closing_time']) $open_flag = true;
                while ($restaurantHour['closing_time'] >= $time) {
                    if ($now_time < $time) {
                        $today_evening_time[] = $time;
                    }
                    $time = $this->increaseTimeByFifteenMinute($time);
                    $time = $this->increaseTimeByFifteenMinute($time);
                }
            }

        }

        $today_time = collect($today_day_time)->merge(collect($today_evening_time))->values();
        $today_time = collect($today_time)->sort()->unique()->values();
        if (count($today_time) > 0) {
            foreach ($today_time as $data) {
                $time_list['today']['time'][] = $this->changeTimeFormat($data);
            }
        }






//        foreach ($restaurantHoursTomorrow as $restaurantHour) {
//            if ($restaurantHour['shift'] == "Day" && $restaurantHour["closed"] == 0) {
//                $day_time_closing = $this->decreaseTimeByAnHour($restaurantHour['closing_time']);
//                $day_time_opening = $restaurantHour['opening_time'];
//            }
//        }
//        foreach ($restaurantHoursTomorrow as $restaurantHour) {
//            $time = $restaurantHour['opening_time'];
//            if ($restaurantHour['shift'] == "Day" && $restaurantHour["closed"] == 0) {
//                while ($restaurantHour['closing_time'] >= $time) {
//                    $time_list['tomorrow']['time'][] = $this->changeTimeFormat($time);
//                    $time = $this->increaseTimeByFifteenMinute($time);
//                    $time = $this->increaseTimeByFifteenMinute($time);
//                }
//            } elseif ($restaurantHour['shift'] == "Evening" && $restaurantHour["closed"] == 0) {
//                while ($restaurantHour['closing_time'] >= $time) {
//                    if ($time < $day_time_opening || $time > $day_time_closing) {
//                        $time_list['tomorrow']['time'][] = $this->changeTimeFormat($time);
//                    }
//                    $time = $this->increaseTimeByFifteenMinute($time);
//                    $time = $this->increaseTimeByFifteenMinute($time);
//                }
//            }
//        }

        $tomorrow_time = [];
        foreach ($restaurantHoursTomorrow as $restaurantHour) {
            $time = $restaurantHour['opening_time'];
            if ($restaurantHour['shift'] == "Day" && $restaurantHour["closed"] == 0) {
                while ($restaurantHour['closing_time'] >= $time) {
                    $tomorrow_time[] = $time;
                    $time = $this->increaseTimeByFifteenMinute($time);
                    $time = $this->increaseTimeByFifteenMinute($time);
                }
            } elseif ($restaurantHour['shift'] == "Evening" && $restaurantHour["closed"] == 0) {
                while ($restaurantHour['closing_time'] >= $time) {
                    $tomorrow_time[] = $time;
                    $time = $this->increaseTimeByFifteenMinute($time);
                    $time = $this->increaseTimeByFifteenMinute($time);
                }
            }
        }
        $datas = collect($tomorrow_time)->sort()->unique()->values();
        foreach ($datas as $data){
            $time_list['tomorrow']['time'][] = $this->changeTimeFormat($data);
        }




        $time_list_return = [];
        if (count($time_list['today']['time']) > 0) {
            $time_list_return[0] = $time_list['today'];
            if (count($time_list['tomorrow']['time']) > 0) {
                $time_list_return[1] = $time_list['tomorrow'];
            }
        } else {
            if (count($time_list['tomorrow']['time']) > 0) {
                $time_list_return[0] = $time_list['tomorrow'];
            }
        }
        return response()->json([
            'status' => '200',
            "data" => $time_list_return
        ], 200);

    }



    /**
     * delete specified Restaurant Reservation only user with permission can delete the RestaurantReservation
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id, $branch_id)
    {
        try {
            $restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($branch_id);
        } catch (ModelNotFoundException $ex) {
            $this->logStoreHelper->storeLogError(array('Restaurant Reservation', [
                'status' => '404',
                "message" => "Restaurant branch could not be found"
            ]));
            return response()->json([
                'status' => '404',
                "message" => "Restaurant branch could not be found"
            ], 404);
        }

        if (!$this->checkRolePotention($branch_id)) {
            return response()->json([
                'status' => '403',
                "message" => "Unauthorized"
            ], 403);
        }

        try {
            $this->restaurantReservation->deleteRestaurantReservation($id);
            $this->logStoreHelper->storeLogInfo([
                "RestaurantReservation deleted successfully", [
                    "status" => "200"
                ]
            ]);
            return response()->json([
                "status" => "200",
                "message" => "Restaurant Reservation deleted successfully"
            ]);
        } catch (ModelNotFoundException $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Restaurant Reservation not found"
            ], 404);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "500",
                "message" => "Error deleting RestaurantReservation"
            ]);
        }

    }

    /*
     * Verify current User Role
     *
     * super-admin and admin have access to all restaurant hour..
     *
     * if .....   user1 is restaurant-admin with id1
     * id1 has 3 branches branch1, branch2, branch3
     * then restaurant-admin can only access restaurant hour in branch1,branch2,branch3 not other branches like: branch4
     *
     * if ..... user1 is branch-admin with id1
     * id1 has one branch branch1
     * the branch-admin can only access drestaurant hour in branch1 not other
     *
     *
     *
     * @param $branch_id
     * @return bool
     *
     * */
    private function checkRolePotention($branch_id)
    {

        // restaurant-admin has access to  his restaurant only
        if ((RoleChecker::hasRole('restaurant-admin') || (RoleChecker::get('parentRole') == 'restaurant_admin'))) {
            try {

                if (RoleChecker::get('is_restaurant_admin') != true) {
                    throw new \Exception();
                }
                if (in_array($branch_id, RoleChecker::get('branch_id'))) {
                    return true;
                } else {
                    throw new \Exception();
                }

            } catch (\Exception $ex) {
                return false;
            }
        } elseif (RoleChecker::hasRole('branch-admin') || (RoleChecker::get('parentRole') == 'branch_admin')) {
            try {
                if (RoleChecker::get('is_restaurant_admin') != false) {
                    throw new \Exception();
                }
                if (RoleChecker::get('branch_id') == $branch_id) {
                    return true;
                } else {
                    throw new \Exception();
                }

            } catch (\Exception $ex) {
                return false;
            }
        }
        return true;
    }


    public function changeToTimezoneOfBranch($time, $timezone)
    {
        $parsed = date_parse($time);
        $now = Carbon::now($timezone);
        $diff = $now->offset;
        $time = $parsed['hour'] * 3600 + $parsed['minute'] * 60;
        $time = $time + $diff;
        $date = gmdate('H:i:s', $time);
        return $date;
        //return date('h:i:s a', strtotime($date));
    }

    public function changeToUTC($time, $timezone)
    {
        $parsed = date_parse($time);
        $now = Carbon::now($timezone);
        $diff = $now->offset;
        $time = $parsed['hour'] * 3600 + $parsed['minute'] * 60;
        $time = $time - $diff;
        $date = gmdate('H:i:s', $time);
        return $date;
    }

    public function decreaseTimeByAnHour($time)
    {
        $parsed = date_parse($time);
        $time = $parsed['hour'] * 3600 + $parsed['minute'] * 60;
        $time = $time - 3600;
        $date = gmdate('H:i:s', $time);
        return $date;
    }

    public function convertToNearestThirtyMinuteTime($time)
    {
        $parsed = date_parse($time);
        if ($parsed['minute'] > 0 && $parsed['minute'] < 30) $parsed['minute'] = 30;
        if ($parsed['minute'] > 30 && $parsed['minute'] < 60) {
            $parsed['hour']++;
            $parsed['minute'] = 0;
        }
        $time = $parsed['hour'] * 3600 + $parsed['minute'] * 60;
        $time = $time - 3600;
        $date = gmdate('H:i:s', $time);
        return $date;
    }

    public function increaseTimeByFifteenMinute($time)
    {
        $parsed = date_parse($time);
        $time = $parsed['hour'] * 3600 + $parsed['minute'] * 60;
        $time = $time + 900;
        $date = gmdate('H:i:s', $time);
        return $date;
    }

    public function changeTimeFormat($time)
    {
        $parsed = date_parse($time);
        $time = $parsed['hour'] * 3600 + $parsed['minute'] * 60;
        $date = gmdate('h:i A', $time);
        return $date;
    }


}