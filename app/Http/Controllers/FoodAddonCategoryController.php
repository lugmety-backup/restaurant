<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/10/17
 * Time: 2:45 PM
 */

namespace App\Http\Controllers;

use App\Repo\FoodInterface;
use App\Repo\AddOnCategoryInterface;
use Illuminate\Http\Request;
use App\Repo\FoodAddonCategoryInterface;
use Illuminate\Support\Facades\Input;
use AuthChecker;
use LogStoreHelper;
use Validator;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;


class FoodAddonCategoryController extends Controller
{
    /**
     * @var FoodAddonCategoryInterface
     */

    private $foodAddonCategory;
    private $food;
    private $category;
    private $logStoreHelper;

    /**
     * FoodAddonCategoryController constructor.
     * @param FoodAddonCategoryInterface $foodAddonCategory
     * @param AddOnCategoryInterface $category
     * @param FoodInterface $food
     */

    public function __construct(FoodAddonCategoryInterface $foodAddonCategory,AddonCategoryInterface $category, FoodInterface $food, LogStoreHelper $logStoreHelper)
    {
        $this->foodAddonCategory = $foodAddonCategory;
        $this->category = $category;
        $this->food = $food;
        $this->logStoreHelper = $logStoreHelper ;
    }

    /**
     * Display addon categories related to food of a specific restaurant.
     * If 'lang' parameter is not supplied,then default language is set to 'en'.
     * @param $restaurantId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($restaurantId, $id)
    {
        $lang = Input::get("lang","en");

        try{
            $food=$this->food->getSpecificFoodByRestaurant($id,$restaurantId);
            /**
             * Check count of result. If result count is 0, throw exception. Else check if the status of result is 0.
             * If user is not logged in or unauthorized, throw exception if status is zero.
             * If the user is logged in and authorized personnel, continue.
             */

            if (count($food) == 0) {
                throw new \Exception();
            } elseif ($food['status'] == 0) {
                if (!AuthChecker::check($restaurantId))
                    throw new \Exception();
            }

            try{
                /**
                 * If user is logged in and authorized personnel, get all the addon categories related to specific food.
                 * S/He can view categories of both status i.e. 1 or 0.
                 * S/He can view addon categories of only 1 or 0 using filter.
                 * If user is not logged in or unauthorized personnel, S/He can only view active categories (status = 1)
                 */
                if (AuthChecker::check($restaurantId)) {
                    if (!is_null(Input::get('status'))) {
                        $foodCategory = $food->addonCategory()->where('restaurant_id',$restaurantId)->where('status', Input::get('status'))->get();
                    } else {
                        $foodCategory = $food->addonCategory()->where('restaurant_id',$restaurantId)->get();
                    }

                } else {
                    $foodCategory= $food->addonCategory()->where('restaurant_id',$restaurantId)->where('status', 1)->get();
                }

                /**
                 * Get translation of each addon category according to the language desired through 'lang' parameter.
                 * Store translation in $category array along with other deatils.
                 * If translation is not available in requested language, default language english is used.
                 * If translation is not avaliable in default language, throw exception and abort.
                 */
                foreach ($foodCategory as $category)
                {
                    $translation=$category->categoryTranslation()->where('lang',$lang)->get();
                    try {
                        if ($translation->count() == 0) {
                            if ($lang == 'en') {
                                throw new \Exception();
                            } else {
                                $translation = $category->categoryTranslation()->where('lang','en')->get();
                                if ($translation->count() == 0) {
                                    throw new \Exception();
                                }
                            }

                        }
                    } catch (\Exception $ex) {
                        return response()->json([
                            "status" => "404",
                            "message" => "Default language english not found in database"
                        ], 404);
                    }

                    $category['lang'] = $translation[0]['lang'];
                    $category['name'] = $translation[0]['name'];

                }


                if(!$foodCategory->first()){
                    throw new \Exception();
                }
            }
            catch (\Exception $ex){
                return response()->json([
                    'status'=>'404',
                    "message"=>'Empty Record'
                ],404);
            }
            return response()->json([
                'status'=>'200',
                'data'=>$foodCategory
            ],200);
        }
        catch (\Exception $e){
            return response()->json([
                'status'=>'404',
                'message'=>'Food Could not found'
            ],404);
        }
    }

    /**
     * Attach aadon categories to food of a specific restaurant
     * Only Authorized user can attach category to food.
     * @param $restaurantId
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function store($restaurantId, $id,Request $request)
    {

        /**
         * Check if logged in user has authority to attach data.
         * If 'true', continue. Else abort with exception message.
         */

        if(AuthChecker::check($restaurantId)) {
            try {
                $food = $this->food->getSpecificFoodByRestaurant($id, $restaurantId);
                /**
                 * Check if food is present and active.
                 * If food is not present or inactive, throw exception, else continue.
                 */
                if (count($food) == 0 || $food['status'] == 0) {
                    throw new \Exception();
                }

            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    'message' => "Food of id: " . $id . " may be removed or disabled"
                ], 404);
            }
            /**
             * Validate the request.
             * Request for field 'category_id' is required and should be an array
             */
            try {
                $this->validate($request, [
                    "category_id" => "required|array",

                ]);

            } catch (\Exception $e) {

                return response()->json([
                    "status" => "422",
                    "message" => $e->response->original
                ], 422);


            }
            /**
             * Start DB transaction to ensure the operation is reversed in case not successfully committed.
             *
             */
            DB::beginTransaction();


            foreach ($request['category_id'] as $categoryId) {
                try {
                    /**
                     * Check if addon category of given id is present and active.
                     * If addon category is not present or inactive, throw exception, else continue.
                     */
                    $category = $this->category->getSpecificAddonCategoryByRestaurant($categoryId, $restaurantId);
                    if (count($category) == 0 || $category['status'] == 0) {
                        throw new \Exception();
                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => '404',
                        "message" => "Addon Category of id:" . $categoryId . " may be removed or disabled."
                    ], 404);
                }

                $request = $categoryId;
                /**
                 * Attach addon category to addon.
                 * If specific addon category and addon are already attached, throw exception.
                 */

                try {
                    $foodCategory = $this->foodAddonCategory->attachCategoryToFood($id, $request);
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => '409',
                        "message" => "Duplicate Entry for food_id " . $id . " and category_id " . $categoryId
                    ], 409);

                }

            }
            DB::commit();
            $this->logStoreHelper->storeLogInfo([
                "Food-AddonCategory Relation",[
                    "status" => "200",
                    "message" => "Addon Categories are attached to food id: " . $id
                ]
            ]);

            return response()->json([
                "status" => "200",
                "message" => "Category attached successfully to food"
            ], 200);
        }else {
            return response()->json([
                "status" => "401",
                "message" => "Only Authorized personnel can attach category to food"
            ], 401);
        }

    }

    /**
     * Detach addon categories from food of a specific restaurant
     * Only authorized user can detach category from food.
     * @param $restaurantId
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function destroy($restaurantId,$id, Request $request)
    {
        /**
         * Check if logged in user has authority to detach data.
         * If 'true', continue. Else abort with exception message.
         */

        if(AuthChecker::check($restaurantId)) {

            try {

                $food = $this->food->getSpecificFoodByRestaurant($id, $restaurantId);
                /**
                 * Check if food is present and active.
                 * If food is not present or inactive, throw exception, else continue.
                 */
                if (count($food) == 0 || $food['status'] == 0) {
                    throw new \Exception();
                }

            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Food of id: " . $id . " may be removed or disabled"
                ], 404);
            }

            /**
             * Validate the request.
             * Request for field 'category_id' is required and should be an array
             */

            try {
                $this->validate($request, [
                    "category_id" => "required|array",

                ]);

            } catch (\Exception $e) {

                return response()->json([
                    "status" => "422",
                    "message" => $e->response->original
                ], 422);
            }
            /**
             * Start DB transaction to ensure the operation is reversed in case not successfully committed.
             *
             */
            DB::beginTransaction();

            foreach ($request['category_id'] as $categoryId) {
                try {
                    /**
                     * Check if addon category of given id is present and active.
                     * If addon category is not present or inactive, throw exception, else continue.
                     */
                    $category = $this->category->getSpecificAddonCategoryByRestaurant($categoryId, $restaurantId);
                    if (count($category) == 0 || $category['status'] == 0) {
                        throw new \Exception();
                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Category of id:" . $categoryId . " could not be found"
                    ], 404);
                }
                $request = $categoryId;
                /**
                 * Detach addon category from food.
                 *
                 */

                $foodCategory = $this->foodAddonCategory->detachCategoryFromFood($id, $request);


            }
            /**
             * If operation is successfully executed, commit it, else revert whole operation.
             */

            DB::commit();

            $this->logStoreHelper->storeLogInfo([
                "Food-AddonCategory Relation" , [
                    "status" => "200",
                    "message" => "Addon Categories are detached from food id: " . $id
                ]
            ]);

            return response()->json([
                "status" => "200",
                "message" => "Category detached successfully from food"
            ], 200);
        }else {
            return response()->json([
                "status" => "401",
                "message" => "Only Authorized personnel can detach category from food"
            ], 401);
        }
    }

    /**
     * Display food related to given addon category of a specific restaurant.
     * If 'lang' parameter is not supplied,then default language is set to 'en'.
     * @param $restaurantId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function getFoodByCategory($restaurantId,$id)
    {
        $lang = Input::get("lang","en");

        try{

            $category=$this->category->getSpecificAddonCategoryByRestaurant($id, $restaurantId);
            /**
             * Check count of result. If result count is 0, throw exception. Else check if the status of result is 0.
             * If user is not logged in or unauthorized, throw exception if status is 0.
             * If the user is logged in and authorized personnel, continue.
             */
            if (count($category) == 0) {
                throw new \Exception();
            } elseif ($category['status'] == 0) {
                if (!AuthChecker::check($restaurantId))
                    throw new \Exception();
            }

            try{

                /**
                 * If user is logged in and authorized personnel, get all the food related to specific category.
                 * S/He can veiw food of both status i.e. 1 or 0.
                 * S/He can view food of only 1 or 0 using filter.
                 * If user is not logged in or unauthoried personnel, S/He can only view active food (status = 1)
                 */

                if (AuthChecker::check($restaurantId)) {
                    if (!is_null(Input::get('status'))) {
                        $foodCategory = $category->food()->where('restaurant_id', $restaurantId)->where('status', Input::get('status'))->get();
                    } else {
                        $foodCategory = $category->food()->where('restaurant_id', $restaurantId)->get();
                    }

                } else {
                    $foodCategory = $category->food()->where('restaurant_id', $restaurantId)->where('status', 1)->get();
                }

                /**
                 * Get translation of each food according to the language desired through 'lang' parameter.
                 * Store translation in $food array along with other deatils.
                 * If translation is not available in requested language, default language english is used.
                 * If translation is not avaliable in default language, throw exception and abort.
                 */
                foreach ($foodCategory as $food)
                {
                    $translation=$food->foodTranslation()->where('lang',$lang)->get();
                    try {
                        if ($translation->count() == 0) {
                            if ($lang == 'en') {
                                throw new \Exception();
                            } else {
                                $translation = $food->foodTranslation()->where('lang','en')->get();
                                if ($translation->count() == 0) {
                                    throw new \Exception();
                                }
                            }

                        }
                    } catch (\Exception $ex) {
                        return response()->json([
                            "status" => "404",
                            "message" => "Default language english not found in database"
                        ], 404);
                    }

                    $food['lang'] = $translation[0]['lang'];
                    $food['name'] = $translation[0]['name'];
                    $food['description'] = $translation[0]['description'];
                    $food['ingredients'] = $translation[0]['ingredients'];
                    if(!empty($food['gallery']))
                    {
                        $food['gallery'] = unserialize($food['gallery']);
                    }

                }

                if(!$foodCategory->first()){
                    throw new \Exception();
                }
            }
            catch (\Exception $ex){
                return response()->json([
                    "status" => '404',
                    "message" => 'Empty Record'
                ],404);
            }
            return response()->json([
                "status" => "200",
                "data" => $foodCategory
            ],200);
        }
        catch (\Exception $e){
            return response()->json([
                "status" => "404",
                "message" => "Category Could not found"
            ],404);
        }

    }


}