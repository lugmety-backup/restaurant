<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/10/17
 * Time: 2:45 PM
 */

namespace App\Http\Controllers;

use App\Repo\AddOnInterface;
use App\Repo\AddOnCategoryInterface;
use Illuminate\Http\Request;
use App\Repo\AddonAddonCategoryInterface;
use Illuminate\Support\Facades\Input;
use AuthChecker;
use LogStoreHelper;
use Validator;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;


class AddonAddonCategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $addonCategory;
    private $addon;
    private $category;
    private $logStoreHelper;

    public function __construct(AddonAddonCategoryInterface $addonCategory, AddOnCategoryInterface $category, AddOnInterface $addon, LogStoreHelper $logStoreHelper)
    {
        $this->addonCategory = $addonCategory;
        $this->category = $category;
        $this->logStoreHelper = $logStoreHelper;
        $this->addon = $addon;
    }

    /**
     * Display Addon Categories related to addon of a specific restaurant.
     * If 'lang' parameter is not supplied,then default language is set to 'en'.
     * If 'status' parameter is not supplied, then default status is 1 for unauthorized personnel.
     * For authorized personnel, all addon categories is displayed i.e of status 0 and 1.
     * @param $restaurantId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function index($restaurantId, $id)
    {
        $lang = Input::get("lang", "en");

        try {
            $addon = $this->addon->getSpecificAddonByRestaurant($id, $restaurantId);
            /**
             * Check count of result. If result count is 0, throw exception. Else check if the status of result is 0.
             * If user is not logged in or unauthorized, throw exception if status is zero.
             * If the user is logged in and authorized personnel, continue.
             */
            if (count($addon) == 0) {
                throw new \Exception();
            } elseif ($addon['status'] == 0) {
                if (!AuthChecker::check($restaurantId))
                    throw new \Exception();
            }


            try {
                /**
                 * If user is logged in and authorized personnel, get all the categories related to specific addon.
                 * S/He can view categories of both status i.e. 1 or 0.
                 * S/He can view addon categories of only 1 or 0 using filter.
                 * If user is not logged in or unauthorized personnel, S/He can only view active categories (status = 1)
                 */

                if (AuthChecker::check($restaurantId)) {
                    if (!is_null(Input::get('status'))) {
                        $addonCategory = $addon->addonCategory()->where('restaurant_id', $restaurantId)->where('status', Input::get('status'))->get();
                    } else {
                        $addonCategory = $addon->addonCategory()->where('restaurant_id', $restaurantId)->get();
                    }

                } else {
                    $addonCategory = $addon->addonCategory()->where('restaurant_id', $restaurantId)->where('status', 1)->get();
                }
                /**
                 * Get translation of each addon category according to the language desired through 'lang' parameter.
                 * Store translation in $category array along with other deatils.
                 * If translation is not available in requested language, default language english is used.
                 * If translation is not avaliable in default language, throw exception and abort.
                 */

                foreach ($addonCategory as $category) {
                    $translation = $category->categoryTranslation()->where('lang', $lang)->get();
                    try {
                        if ($translation->count() == 0) {
                            if ($lang == 'en') {
                                throw new \Exception();
                            } else {
                                $translation = $category->categoryTranslation()->where('lang', 'en')->get();
                                if ($translation->count() == 0) {
                                    throw new \Exception();
                                }
                            }

                        }
                    } catch (\Exception $ex) {
                        return response()->json([
                            "status" => "404",
                            "message" => "Default language english not found in database"
                        ], 404);
                    }

                    $category['lang'] = $translation[0]['lang'];
                    $category['name'] = $translation[0]['name'];

                }

                if (!$addonCategory->first()) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    "message" => 'Empty Record'
                ], 404);
            }
            return response()->json([
                'status' => '200',
                'data' => $addonCategory->all()
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '404',
                'message' => 'Addon Could not found'
            ], 404);
        }
    }

    /**
     * Attach Categories to addon of a specific restaurant
     * Only authoried personnel can attach addon categories to addon
     * @param $restaurantId
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store($restaurantId, $id, Request $request)
    {
        if (AuthChecker::check($restaurantId)) {

            try {
                /**
                 * Check if addon is present and active.
                 * If addon is not present or inactive, throw exception, else continue.
                 */

                $addon = $this->addon->getSpecificAddonByRestaurant($id,$restaurantId);

                if (count($addon) == 0 || $addon['status'] == 0) {
                    throw new \Exception();
                }

            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    'message' => "Addon of id: " . $id . " may be removed or disabled"
                ], 404);
            }

            /**
             * Validate the request.
             * Request for field 'category_id' is required and should be an array
             */

            try {
                $this->validate($request, [
                    "category_id" => "required|array",

                ]);

            } catch (\Exception $e) {

                return response()->json([
                    "status" => "422",
                    "message" => $e->response->original
                ], 422);

            }
            /**
             * Start DB transaction to ensure the operation is reversed in case not successfully committed.
             *
             */
            DB::beginTransaction();

            foreach ($request['category_id'] as $categoryId) {
                try {
                    /**
                     * Check if addon category of given id is present and active.
                     * If addon category is not present or inactive, throw exception, else continue.
                     */
                    $category = $this->category->getSpecificAddonCategoryByRestaurant($categoryId, $restaurantId);

                    if (count($category) == 0 || $category['status'] == 0) {
                        throw new \Exception();
                    }

                } catch (\Exception $ex) {
                    return response()->json([
                        'status' => '404',
                        'message' => "Addon Category of id:" . $categoryId . " may be removed or disabled."
                    ], 404);
                }
                /**
                 * Attach addon category to addon.
                 * If specific addon category and addon are already attached, throw exception.
                 */
                $request = $categoryId;
                try {
                    $addonCategory = $this->addonCategory->attachCategoryToAddon($id, $request);
                } catch (\Exception $ex) {
                    return response()->json([
                        'status' => '409',
                        'message' => "Duplicate Entry for addon_id " . $id . " and category_id " . $categoryId
                    ], 409);

                }

            }
            /**
             * If operation is successfully executed, commit it, else revert whole operation.
             */
            DB::commit();

            $this->logStoreHelper->storeLogInfo([
                "Addon-AddonCategory Relation", [
                    "status" => "200",
                    "message" => "Categories of are attached to addon id: " . $id
                ]
            ]);

            return response()->json([
                'status' => '200',
                'message' => 'Category attached successfully to addon'
            ], 200);
        } else {
            return response()->json([
                'status' => "401",
                "message" => "Only Authorized personnel can attach category to addon"
            ], 401);
        }


    }

    /**
     * Detach Categories from addon of a specific restaurant
     * @param $restaurantId
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function destroy($restaurantId, $id, Request $request)
    {
        if (AuthChecker::check($restaurantId)) {
            /**
             * Check if addon is present and active.
             * If addon is not present or inactive, throw exception, else continue.
             */

            try {
                $addon = $this->addon->getSpecificAddonByRestaurant($id, $restaurantId);
                if (count($addon) == 0 || $addon['status'] == 0) {
                    throw new \Exception();
                }

            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    'message' => "Addon of id: " . $id . " may be removed or disabled."
                ], 404);
            }

            /**
             * Validate the request.
             * Request for field 'category_id' is required and should be an array
             */

            try {
                $this->validate($request, [//validating the request
                    "category_id" => "required|array",

                ]);

            } catch (\Exception $e) {
                /*
                 *creates log for storing error
                 * */

                return response()->json([
                    "status" => "422",
                    "message" => $e->response->original
                ], 422);


            }
            /**
             * Start DB transaction to ensure the operation is reversed in case not successfully committed.
             *
             */
            DB::beginTransaction();

            foreach ($request['category_id'] as $categoryId) {
                try {
                    /**
                     * Check if addon category of given id is present.
                     * If addon category is not present, throw exception, else continue.
                     */
                    $category = $this->category->getSpecificAddonCategoryByRestaurant($categoryId, $restaurantId);

                } catch (\Exception $ex) {
                    return response()->json([
                        'status' => '404',
                        'message' => "Category of id:" . $categoryId . " could not be found"
                    ], 404);
                }

                /**
                 * Detach addon category from addon.
                 *
                 */
                $request = $categoryId;
                $addonCategory = $this->addonCategory->detachCategoryFromAddon($id, $request);


            }
            /**
             * If operation is successfully executed, commit it, else revert whole operation.
             */
            DB::commit();
            $this->logStoreHelper->storeLogInfo([
                "Addon-AddonCategory Relation" ,[
                    "status" => "200",
                    "message" => "Categories are detached from addon id: " . $id
                ]
            ]);

            return response()->json([
                'status' => '200',
                'message' => 'Category detached successfully from Addon'
            ], 200);
        } else {
            return response()->json([
                'status' => "401",
                "message" => "Only Authorized personnel can detach category from addon"
            ], 401);
        }

    }

    /**
     * Display addons related to given category of a specific restaurant.
     * If 'lang' parameter is not supplied,then default language is set to 'en'.
     * If 'status' parameter is not supplied, then default status is 1 for unauthorized personnel.
     * For authorized personnel, all addon categories is displayed i.e of status 0 and 1.
     * @param $restaurantId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function getAddonByCategory($restaurantId, $id)
    {
        $lang = Input::get("lang", 'en');

        try {

            $category = $this->category->getSpecificAddonCategoryByRestaurant($id, $restaurantId);

            /**
             * Check count of result. If result count is 0, throw exception. Else check if the status of result is 0.
             * If user is not logged in or unauthorized, throw exception if status is 0.
             * If the user is logged in and authorized personnel, continue.
             */

            if (count($category) == 0) {
                throw new \Exception();
            } elseif ($category['status'] == 0) {
                if (!AuthChecker::check($restaurantId))
                    throw new \Exception();
            }

            /**
             * If user is logged in and authorized personnel, get all the addons related to specific category.
             * S/He can veiw addons of both status i.e. 1 or 0.
             * S/He can view addons of only 1 or 0 using filter.
             * If user is not logged in or unauthorized personnel, S/He can only view active addons (status = 1)
             */

            try {

                if (AuthChecker::check($restaurantId)) {
                    if (!is_null(Input::get('status'))) {
                        $addonCategory = $category->addon()->where('restaurant_id', $restaurantId)->where('status', Input::get('status'))->get();
                    } else {
                        $addonCategory = $category->addon()->where('restaurant_id', $restaurantId)->get();
                    }

                } else {
                    $addonCategory = $category->addon()->where('restaurant_id', $restaurantId)->where('status', 1)->get();
                }

                /**
                 * Get translation of each addon according to the language desired through 'lang' parameter.
                 * Store translation in $category array along with other deatils.
                 * If translation is not available in requested language, default language english is used.
                 * If translation is not avaliable in default language, throw exception and abort.
                 */

                foreach ($addonCategory as $addon) {
                    $translation = $addon->addonTranslation()->where('lang', $lang)->get();
                    try {
                        if ($translation->count() == 0) {
                            if ($lang == 'en') {
                                throw new \Exception();
                            } else {
                                $translation = $addon->addonTranslation()->where('lang', 'en')->get();
                                if ($translation->count() == 0) {
                                    throw new \Exception();
                                }
                            }

                        }
                    } catch (\Exception $ex) {
                        return response()->json([
                            "status" => "404",
                            "message" => "Default language english not found in database"
                        ], 404);
                    }

                    $addon['lang'] = $translation[0]['lang'];
                    $addon['name'] = $translation[0]['name'];
                    $addon['description'] = $translation[0]['description'];


                }

                if (!$addonCategory->first()) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    "message" => 'Empty Record'
                ], 404);
            }
            return response()->json([
                'status' => '200',
                'data' => $addonCategory
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => '404',
                'message' => 'Category Could not found'
            ], 404);
        }

    }


}