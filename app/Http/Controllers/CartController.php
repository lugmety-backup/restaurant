<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 6/14/17
 * Time: 10:51 AM
 */

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Repo\FoodInterface;
use App\Repo\AddOnInterface;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use LogStoreHelper;
use RestaurantChecker;
use RemoteCall;


class CartController extends Controller
{
    protected $food;
    protected $addon;
    protected $logStoreHelper;

    public function __construct(FoodInterface $food, AddOnInterface $addon, LogStoreHelper $logStoreHelper)
    {
        $this->addon = $addon;
        $this->food = $food;
        $this->logStoreHelper = $logStoreHelper;
    }

    /**
     * Returns data for cart service.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
//    public function reorderCart(Request $request)
//    {
//
//        //$request['lang'] = Input::get("lang", "en");
//        /**
//         * Validate request.
//         * restaurant_id  should be integer and is required.
//         * items is required and should be in array
//         */
//
//        try {
//            $this->validate($request, [
//                "restaurant_id" => "required|integer",
//                "items" => "required|array",
//            ]);
//
//        } catch (\Exception $e) {
//
//            return response()->json([
//                "status" => "422",
//                "message" => $e->response->original
//            ], 422);
//        }
//
//        $restaurantResponse = RestaurantChecker::checkOpenRestaurantAndPreOrder($request['restaurant_id']);
//
//
//        if (!$restaurantResponse) {
//
//            return response()->json([
//                "status" => "403",
//                "message" => "Restaurant is not available."
//            ], 403);
//
//        }
//
//        else {
//            try {
//                $country = RemoteCall::getSpecific(Config::get("config.country_url")."/".$restaurantResponse['country_id']);
//                if ($country['status'] != 200) {
//                    throw new \Exception("Currency code not found");
//                }
//                /**
//                 * Process each data of items.
//                 * Validate each data of items.
//                 * Validate food of given id is presnt and active. Check if the given addon is active and under addoncategory related to given food.
//                 * If everything goes fine, return cart data, end display respective errors.
//                 */
//
//                foreach ($request['items'] as $item) {
//
//                    $rules = [
//                        "quantity" => 'required|integer|min:1',
//                        "addons" => 'array',
//                    ];
//
//                    $validator = Validator::make($item, $rules);
//                    if ($validator->fails()) {
//                        $error = $validator->errors();
//                        $this->logStoreHelper->storeLogError([
//                            "Validation Error",
//                            [
//                                'status' => '422',
//                                "message" => $error
//                            ]
//
//                         ]);
//                         return response()->json([
//                             'status' => '422',
//                             "message" => $error
//                         ],422);
//                     }
//                     /**
//                      * Initialize $addonTotal with value 0.
//                      */
//                     $addonTotal = 0;
//                     /**
//                      * Get food details of given restaurant.
//                      * If food details is not available, display error message.
//                      */
//                     $foodData = $this->food->getSpecificFoodByRestaurant($item['id'], $request['restaurant_id']);
//
//                     if ($foodData['status'] != 1) {
//                         throw new \Exception("Food of id:" . $item['id'] . " not found");
//                     }
//
//                    if(isset($item['size'])) {
//
//                        $foodSize = $foodData->foodSize()->where('size', $item['size'])->first();
//                        if (count($foodSize) == 0) {
//                            throw new \Exception('Food size not found');
//
//                        } else {
//                            $sizeTranslation = $foodSize->FoodSizeTranslation;
//                            //return $sizeTranslation;
//                            if ($sizeTranslation->count() == 0) {
//
//                                throw new \Exception("Size translation not found");
//                            }
//
//                            foreach ($sizeTranslation as $trans) {
//                                $foodSizeTranslation[$trans['lang']] = $trans['title'];
//                            }
//                            $foodData['price'] = $foodSize['price'];
//
//                        }
//                    }else{
//                        $foodSize['size'] = "";
//                        $foodSizeTranslation = new \stdClass();
//                    }
//                     /**
//                      * Get food translation for given food id.
//                      * If requested translation is available, pass default translation.
//                      * If default translation is not available send error message.
//                      */
//                     $translation = $foodData->foodTranslation;
//
//                     if ($translation->count() == 0) {
//
//                         throw new \Exception("Food translation not found");
//                     }
//
//                     foreach ($translation as $trans)
//                     {
//                         $foodName[$trans['lang']] = $trans['name'];
//                     }
//
//                     /**
//                      * if $item['addons'] is not empty, get all related active addon categories of the given food.
//                      * Then, get all the active related addons for each addon categories.
//                      * check if requested addons match with feteched addon data.
//                      * If matched, proceed, else throw error message.
//                      */
//
//                     if (isset($item['addons']) && !empty($item['addons'])) {
//                         /**
//                          * Get active related addon categories of the given food
//                          */
//
//                         $category = $foodData->addonCategory()->where('restaurant_id', $request['restaurant_id'])->where('status', 1)->get();
//                         /**
//                          * Get active related addons of addon category
//                          */
//                         unset($addonId);
//                         foreach ($category as $cat) {
//
//
//                             $addon = $cat->addon()->select('addon.id')->where('restaurant_id', $request['restaurant_id'])->where('status', 1)->get();
//
//                             foreach ($addon as $add) {
//
//                                 $addonId[] = $add['id'];
//                             }
//
//                         }
//
//                         if (!isset($addonId)) {
//                             throw new \Exception("No addons are available");
//                         }
//                         /**
//                          * check if the addon array lies in the list of active related addons of the addon categories.
//                          * If true, proceed else throw error message.
//                          */
//                         if (count(array_diff($item['addons'], $addonId)) != 0) {
//
//                             throw new \Exception("Given addons are not available");
//
//                         } else {
//                             /**
//                              * Unset addonData to remove data present previously.
//                              */
//                             unset($addonData);
//                             /**
//                              * Sort addon list
//                              */
//                             sort($item['addons']);
//                             /**
//                              * Fetch data of addons of given id.
//                              */
//                             foreach ($item['addons'] as $id) {
//
//                                 $addon = $this->addon->getSpecificAddonByRestaurant($id, $request['restaurant_id']);
//
//                                 /**
//                                  * Get addon translation for given food id.
//                                  * If requested translation is available, pass default translation.
//                                  * If default translation is not available send error message.
//                                  */
//                                 $addonTrans = $addon->addonTranslation;
//
//                                 if ($addonTrans->count() == 0) {
//
//                                     throw new \Exception("Addon name not found");
//                                 }
//
//                                 foreach ($addonTrans as $trans)
//                                 {
//                                     $addonName[$trans['lang']] = $trans['name'];
//                                 }
//
//                                 $addonData[] = [
//                                     'id' => (integer)$id,
//                                     'name' => $addonName,
//                                     'price' => (float)$addon['price'],
//                                 ];
//                                 $addonTotal += (float)$addon['price'];
//
//                             }
//                         }
//
//
//                     } else {
//                         $addonData = [
//
//                         ];
//                     }
//                     /**
//                      * Set all the values and kepp in array cartData.
//                      */
//                     $unitPrice = $foodData['price'] + $addonTotal;
//
//                     if (!empty($foodData['featured_image'])) {
//                         /**
//                          * get position of last occurance of / in fetaured_image and added the postion by 1
//                          */
//                         $getpositionOfImageInUrl = strrpos($foodData['featured_image'], "/") + 1;
//                         /**
//                          * add normal- prefix in $getpositionOfImageInUrl postition
//                          */
//                         $image_resized = substr_replace($foodData['featured_image'], "normal-", $getpositionOfImageInUrl, 0);
//                     } else {
//                         $image_resized = [];
//                     }
//
//                     $cartDetail[] = [
//                         'id' => (integer)$item['id'],
//                         'name' => $foodName,
//                         'price' => (float)$foodData['price'],
//                         'total_unit_price' => (float)$unitPrice,
//                         'sub_total' => (float)($item['quantity'] * $unitPrice),
//                         'quantity' => (integer)$item['quantity'],
//                         'addons' => $addonData,
//                         'image_url' => $image_resized,
//                         'special_instruction' => isset($item['special_instruction']) ? $item['special_instruction']: '',
//                     ];
//
//                     $cartData = [
//                         'cartData' => $cartDetail,
//                         'restaurant' => $restaurantResponse['resName'],
//                         'restaurant_slug' => $restaurantResponse['slug'],
//                         'currency' => $country["message"]["data"]['currency_symbol'],
//                     ];
//
//
//                 }
//                 $this->logStoreHelper->storeLogInfo([
//                     'Cart Data Validation',
//                     [
//                         "status" => "200",
//                         "message" => "Cart data Validated",
//                         "data" => $cartData
//                     ]
//
//                 ]);
//
//                 return response()->json([
//                     "status" => "200",
//                     "message" => "Data Validated",
//                     "data" => $cartData
//                 ], 200);
//
//
//             } catch (ModelNotFoundException $ex) {
//                 return response()->json([
//                     "status" => "404",
//                     "message" => "Entered data is not available."
//                 ], 404);
//             } catch (\Exception $e) {
//
//                 return response()->json([
//                     "status" => "404",
//                     "message" => $e->getMessage()
//                 ], 404);
//             }
//         }
//    }

    public function cart(Request $request)
    {

        //$request['lang'] = Input::get("lang", "en");
        /**
         * Validate request.
         * restaurant_id  should be integer and is required.
         * items is required and should be in array
         */

        try {
            $this->validate($request, [
                "restaurant_id" => "required|integer",
                "items" => "required|array",
            ]);

        } catch (\Exception $e) {

            return response()->json([
                "status" => "422",
                "message" => $e->response->original
            ], 422);
        }
        $restaurantResponse = RestaurantChecker::checkOpenRestaurantAndPreOrder($request['restaurant_id']);

        if (!$restaurantResponse) {

            return response()->json([
                "status" => "403",
                "message" => "Restaurant is not available."
            ], 403);

        } else {
            try {

                /**
                 * Process each data of items.
                 * Validate each data of items.
                 * Validate food of given id is presnt and active. Check if the given addon is active and under addoncategory related to given food.
                 * If everything goes fine, return cart data, end display respective errors.
                 */
                $country = RemoteCall::getSpecific(Config::get("config.country_url")."/".$restaurantResponse['country_id']);
                if ($country['status'] != 200) {
                    throw new \Exception("Currency code not found");
                    }

                foreach ($request['items'] as $item) {

                    $rules = [
                        "food_id" => 'required|integer',
                        "quantity" => 'required|integer|min:1',
                        "addon_categories" => 'array',
                    ];

                    $validator = Validator::make($item, $rules);
                    if ($validator->fails()) {
                        $error = $validator->errors();
                        $this->logStoreHelper->storeLogError([
                            "Validation Error",
                            [
                                'status' => '422',
                                "message" => $error
                            ]

                        ]);
                        return response()->json([
                            'status' => '422',
                            "message" => $error
                        ], 422);
                    }
                    $foodSizeTranslation = [];
                    /**
                     * Initialize $addonTotal with value 0.
                     */
                    $addonTotal = 0;
                    /**
                     * Get food details of given restaurant.
                     * If food details is not available, display error message.
                     */
                    $foodData = $this->food->getSpecificFoodByRestaurant($item['food_id'], $request['restaurant_id']);

                    if ($foodData['status'] != 1 ) {
                        throw new \Exception("Food of id:" . $item['food_id'] . " not found");
                    }elseif ($foodData['out_of_stock'] == "1")
                    {
                        throw new \Exception("Food of id:" . $item['food_id'] . " is out of stock");
                    }


                    $foodSizeList = $foodData->foodSize;
                    if(count($foodSizeList) > 0)
                    {
                        if(isset($item['size']) && !empty($item['size'])) {

                            $foodSize = $foodData->foodSize()->where('size', $item['size'])->first();

                        }else{
                            $foodSize = $foodData->foodSize()->where('is_default', "1")->first();

                        }
                        if (count($foodSize) == 0) {
                            throw new \Exception('Food size not found');

                        } else {
                            $sizeTranslation = $foodSize->FoodSizeTranslation;

                            if ($sizeTranslation->count() == 0) {

                                throw new \Exception("Size translation not found");
                            }

                            foreach ($sizeTranslation as $trans) {

                                $foodSizeTranslation[$trans['lang']] = $trans['title'];
                            }
                            $foodData['price'] = $foodSize['price'];
                        }
                    }else{
                        $foodSize['size'] = '';
                        $foodSizeTranslation = new \stdClass();

                    }


                    /**
                     * Get food translation for given food id.
                     * If requested translation is available, pass default translation.
                     * If default translation is not available send error message.
                     */
                    $translation = $foodData->foodTranslation;

                    if ($translation->count() == 0) {

                        throw new \Exception("Food translation not found");
                    }

                    foreach ($translation as $trans)
                    {
                        $foodName[$trans['lang']] = $trans['name'];
                    }




                    $addonData = [];
                    /**
                     * Check if $item['addon_categories'] is present and not empty.
                     */
                    if (isset($item['addon_categories']) && !empty($item['addon_categories'])) {
                        /**
                         * Get all the related addon categories of food.
                         */
                        $relatedCategories = $foodData->addonCategory()->where('restaurant_id', $request['restaurant_id'])->where('status', 1)->get();
                        if(count($relatedCategories) <= 0){
                            throw new \Exception("No related addon categories are available");
                        }
                        unset($catId);
                        /**
                         * Set a new array  $categoryData[] which has primary key of addon category as index(key) to hold each category details.
                         * Get all the ids of addon category and assign in  an array $catId.
                         *
                         */

                        foreach ($relatedCategories as $cat) {

                            $categoryData[$cat['id']] = $cat;

                            $catId[] = $cat['id'];

                            /**
                             * If $catId is not set, then throw error message.
                             */
                            if (!isset($catId)) {
                                throw new \Exception("No related addon categories are available");
                            }
                        }
                        /**
                         * For every addon category Check if the given category id are present in $catId.
                         * If present, proceed. Else throw error message.
                         */
                        foreach ($item['addon_categories'] as $addonCategory) {

                            /**
                             * Check if given addon category is present in array $catId.
                             * If present, proceed.
                             * Else, throw error message.
                             */

                            if (in_array($addonCategory['id'], $catId)) {
                            $categoryTrans = $categoryData[$addonCategory['id']]->categoryTranslation;


                            if ($categoryTrans->count() == 0) {

                                throw new \Exception("Addon category name not found");
                            }

                            foreach ($categoryTrans as $trans)
                            {
                                $categoryName[$trans['lang']] = $trans['name'];
                            }


                                /**
                                 * Check if $addonCategory has "addons" index and not empty, proceed.
                                 * Else throw error message.
                                 */
                                if(!isset($addonCategory['addons']) || count($addonCategory['addons']) <= 0)
                                {
                                    if (($categoryData[$addonCategory['id']]['min_addon'] == 0)){
                                        continue;
                                    }
                                }
                                if (isset($addonCategory['addons']) && count($addonCategory['addons']) >0) {
                                    /**
                                     * Validate the no of addons sent.
                                     * If validation fails, send error message.
                                     * Else, proceed.
                                     */

                                    if (($categoryData[$addonCategory['id']]['min_addon'] <= count($addonCategory['addons'])) && (count($addonCategory['addons']) <= $categoryData[$addonCategory['id']]['max_addon'])) {
                                        /**
                                         * Fetch all the addons related to addon category.
                                         * Put all the addon ids in $addonId array.
                                         */
                                        $addon = $categoryData[$addonCategory['id']]->addon()->select('addon.id')->where('restaurant_id', $request['restaurant_id'])->where('status', 1)->get();
                                        unset($addonId);

                                        foreach ($addon as $add) {

                                            $addonId[] = $add['id'];
                                        }

                                        /**
                                         * If $addonId is not set, throw error message
                                         */
                                        if (!isset($addonId)) {
                                            throw new \Exception("No addons are available");
                                        }
                                        /**
                                         * If input addon ids does not match with array of $addonId, throw error message.
                                         * ELse Proceed to calculation.
                                         */
                                        if (count(array_diff($addonCategory['addons'], $addonId)) != 0) {

                                            throw new \Exception("Given addons are not available");

                                        }else{
                                            /**
                                             * Unset addonData to remove data present previously.
                                             */

                                            /**
                                             * Sort addon list
                                             */
                                            sort($addonCategory['addons']);
                                            /**
                                             * Fetch data of addons of given id.
                                             */
                                            foreach ($addonCategory['addons'] as $id) {
                                                $addon = $this->addon->getSpecificAddonByRestaurant($id, $request['restaurant_id']);

                                                /**
                                                 * Get addon translation for given food id.
                                                 * If requested translation is available, pass default translation.
                                                 * If default translation is not available send error message.
                                                 */
                                                $addonTrans = $addon->addonTranslation;

                                                if ($addonTrans->count() == 0) {

                                                    throw new \Exception("Addon name not found");
                                                }

                                                foreach ($addonTrans as $trans)
                                                {
                                                    $addonName[$trans['lang']] = $trans['name'];
                                                }

                                                if($restaurantResponse['tax_type'] == "inclusive")
                                                {
                                                    if(!isset($tax_rate)) {
                                                        $taxData = $this->calculateTax($restaurantResponse['city_id'], $addon['price']);
                                                        if ($taxData['status'] != 200) {
                                                            return $taxData;
                                                        }
                                                        $tax_rate = $taxData['data']['tax_percentage'];
                                                    }
                                                    $addon['price'] = $addon['price']/(1+$tax_rate*0.01);


                                                }

                                                $addonData[] = [
                                                    'id' => (integer)$id,
                                                    'name' => $addonName,
                                                    'price' => (float)$addon['price'],
                                                    'category_id' => (integer) $addonCategory['id'],
                                                    'category_name' => $categoryName

                                                ];
                                                $addonTotal += (float)$addon['price'];

                                            }
                                        }

                                    } else {
                                        return response()->json([
                                            "status" => "422",
                                            "message" => "You have to choose at least " . $categoryData[$addonCategory['id']]['min_addon'] . " and at most " . $categoryData[$addonCategory['id']]['max_addon'] . " addons"
                                        ], 422);
                                    }




                                }
                                else{
                                    return response()->json([
                                        "status" => "422",
                                        "message" => "Addons not selected"
                                    ], 422);
                                }
                            }else{
                                return response()->json([
                                    "status" => "404",
                                    "message" => "Addon Category not found"
                                ], 404);
                            }
                        }


                    } else {
                        $addonData = [

                        ];
                    }

                    if($restaurantResponse['tax_type'] == "inclusive")
                    {
                        if(!isset($tax_rate)) {
                            $taxData = $this->calculateTax($restaurantResponse['city_id'], $foodData['price']);
                            if ($taxData['status'] != 200) {
                                return $taxData;
                            }
                            $tax_rate = $taxData['data']['tax_percentage'];


                        }
                        $foodData['price'] = $foodData['price']/(1+$tax_rate*0.01);



                    }
                    /**
                     * Set all the values and kepp in array cartData.
                     */
                    $unitPrice = $foodData['price'] + $addonTotal;

                    if(!empty($foodData['featured_image'])) {
                        /**
                         * get position of last occurance of / in fetaured_image and added the postion by 1
                         */
                        $getpositionOfImageInUrl = strrpos($foodData['featured_image'],"/")+1;
                        /**
                         * add normal- prefix in $getpositionOfImageInUrl postition
                         */
                        $image_resized = substr_replace($foodData['featured_image'], "normal-", $getpositionOfImageInUrl, 0);
                    }else
                    {
                        $image_resized =[];
                    }




                    $cartDetail[] = [
                        'id' => (integer)$item['food_id'],
                        'name' => $foodName,
                        'price' => (float)$foodData['price'],
                        'total_unit_price' => (float)$unitPrice,
                        'sub_total' => (float)($item['quantity'] * $unitPrice),
                        'quantity' => (integer)$item['quantity'],
                        'addons' => $addonData,
                        'image_url' => $image_resized,
                        'size_slug' => $foodSize['size'],
                        'size_title' => $foodSizeTranslation,
                        'special_instruction' => isset($item['special_instruction']) ?  preg_replace('/\s+/', ' ', trim(filter_var($item['special_instruction'], FILTER_SANITIZE_STRING))): '',
                    ];


                }

                $cartData = [
                    'cartData' => $cartDetail,
                    'restaurant' => $restaurantResponse['resName'],
                    'restaurant_slug' => $restaurantResponse['slug'],
                    'currency' => trim($country["message"]["data"]['currency_symbol']),
                    'restaurant_city_id' => $restaurantResponse['city_id'],
                    'restaurant_tax_type' => $restaurantResponse['tax_type'],
                    'tax_rate' => isset($tax_rate)? $tax_rate: 0
                ];

                $this->logStoreHelper->storeLogInfo([
                    'Cart Data Validation',
                    [
                        "status" => "200",
                        "message" => "Cart data Validated",
                        "data" => $cartData
                    ]

                ]);

                return response()->json([
                    "status" => "200",
                    "message" => "Data Validated",
                    "data" => $cartData
                ],200);

            }
            catch
            (ModelNotFoundException $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Entered Food is not available."
                ], 404);
            } catch (\Exception $e) {

                return response()->json([
                    "status" => "404",
                    "message" => $e->getMessage()
                ], 404);
            }


        }
    }

    private function calculateTax($cityId, $totalAmount)
    {
        $request['city_id'] = (integer)$cityId;
        $request['amount'] = $totalAmount;

        $amount = RemoteCall::store(Config::get("config.location_url")."/calculate/tax", $request);
        if($amount['status'] == 503)
        {
            $amount['message'] =[
                'status' => 503,
                'message' => "Error Connecting to Service"
            ];
        }
        return ($amount['message']);

    }


}