<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/7/2017
 * Time: 9:14 PM
 */

namespace App\Http\Controllers;

use App\BranchTranslation;
use App\Repo\RestaurantBranchInterface;
use App\Repo\RestaurantBranchTranslationInterface;
use App\Repo\CustomerRestaurantInterface;
use App\RestaurantBranch;
use Carbon\Carbon;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use LogStoreHelper;
use RemoteCall;
use Illuminate\Support\Facades\Config;

class CustomerRestaurantController extends Controller
{
    /**
     * @var customerRestaurantInterface
     */
    protected $customerRestaurant;
    /**
     * @var RestaurantBranchInterface
     */
    protected $restaurant;
    /**
     * @var LogStoreHelper
     */
    protected $log;


    /**
     * customerRestaurantController constructor.
     * @param customerRestaurantInterface $review
     * @param RestaurantBranchInterface $restaurant
     * @param LogStoreHelper $log
     */
    public function __construct(CustomerRestaurantInterface $customerRestaurnant, RestaurantBranchInterface $restaurant, LogStoreHelper $log)
    {
        $this->customerRestaurant = $customerRestaurnant;
        $this->restaurant = $restaurant;
        $this->log = $log;
    }

    public function getRestaurantsOfCustomer($user_id, Request $request)
    {
        try {
            $this->validate($request, [
                "search" => "sometimes|string",
                "sort_column" => "sometimes",
                "sort_by" => "sometimes",
                "status" => "sometimes|integer|min:0|max:1"
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }

        $filter = [
            "limit" => ($request->get("limit",10)),
            "sort_column" => ($request->has("sort_column")) ? $request->sort_column : null,
            "search" => ($request->has("search")) ? $request->search : null,
            "sort_by" => $request->get("sort_by", "desc"),
            "status" => $request->get("status", "null")
        ];
        try {
            $restaurants = $this->customerRestaurant->getAllRestaurantByUserId($user_id);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "404",
                "message" => "Restaurant of customer could not be found."
            ], 404);
        }
        $restaurants = $restaurants->pluck("restaurant_id")->values()->all();
        $restaurants = $this->restaurant->getAllRestaurantByIds($restaurants, $filter);
        return response()->json([
            "status" => "200",
            "data" => $restaurants
        ], 200);
    }

    public function attachRestaurantToCustomer($user_id, Request $request){
        try {
            $this->validate($request, [
                'restaurant_id' => 'required|integer',
            ]);
        } catch (\Exception $e) {
            $this->log->storeLogInfo(array("RestaurantBranch", [
                "status" => '422',
                "message" => $e->response->original
            ]));
            return response()->json([
                'status' => '422',
                'message' => $e->response->original
            ], 422);
        }
        try {
            $restaurants = $this->customerRestaurant->createCustomerRestaurant(["user_id"=>$user_id, "restaurant_id" => $request['restaurant_id']]);
        } catch (QueryException $e){
            return response()->json([
                "status" => "409",
                "message" => "Restaurant with id ".$request['restaurant_id']." already exists."
            ], 409);
        }
        catch (\Exception $e) {
            return response()->json([
                "status" => "404",
                "message" => "Restaurant of customer could not be created."
            ], 404);
        }
        return response()->json([
            "status" => "200",
            "message" => "Restaurant of customer created successfully."
        ], 200);
    }

    public function detachRestaurantToCustomer($user_id, Request $request){
        try {
            $this->validate($request, [
                'restaurant_id' => 'required|integer',
            ]);
        } catch (\Exception $e) {
            $this->log->storeLogInfo(array("RestaurantBranch", [
                "status" => '422',
                "message" => $e->response->original
            ]));
            return response()->json([
                'status' => '422',
                'message' => $e->response->original
            ], 422);
        }
        try {
            $restaurants = $this->customerRestaurant->deleteRestaurantByUserId($user_id, $request['restaurant_id']);
        }
        catch (\Exception $e) {
            return response()->json([
                "status" => "404",
                "message" => "Restaurant of customer could not be created."
            ], 404);
        }
        return response()->json([
            "status" => "200",
            "message" => "Restaurant of customer deleted successfully."
        ], 200);
    }

    public function updateRestaurantsOfCustomer($user_id, Request $request)
    {
        try {
            $this->validate($request, [
                'restaurants' => 'required|array',
                ]);
        } catch (\Exception $e) {
            $this->log->storeLogInfo(array("RestaurantBranch", [
                "status" => '422',
                "message" => $e->response->original
            ]));
            return response()->json([
                'status' => '422',
                'message' => $e->response->original
            ], 422);
        }

        try {
            $restaurants = $this->restaurant->getAllRestaurantByIds($request['restaurants'])->pluck("id")->values()->all();
            $data = [];
            foreach ($restaurants as $key => $restaurant) {
                $data[$key]['user_id'] = $user_id;
                $data[$key]['restaurant_id'] = $restaurant;
            }
        } catch (\Exception $e) {
            $this->log->storeLogInfo(array("RestaurantBranch", [
                "status" => '422',
                "message" => $e->response->original
            ]));
            return response()->json([
                'status' => '422',
                'message' => $e->response->original
            ], 422);
        }

        try {
//            $userData = $request->except('restaurant');
//            $userData['role'] = 'branch-admin';
//            $user = RemoteCall::store(Config::get('config.oauth_base_url') . "/user/create", $userData);
//            if ($user['status'] != 200) {
//                $this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser", [
//                    $user['message']
//                ]));
//                return response()->json(
//                    $user['message'], $user['status']);
//            }
            $restaurants = $this->customerRestaurant->deleteAndCreateMultipleCustomerRestaurant($user_id, $data);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "404",
                "message" => "Restaurant of customer could not be created."
            ], 404);
        }
        return response()->json([
            "status" => "200",
            "message" => "Restaurant of customer created successfully."
        ], 200);
    }


}