<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/26/17
 * Time: 11:22 AM
 */

namespace App\Http\Controllers;

use App\Repo\FoodAddonCategoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Repo\FoodInterface;
use App\Repo\FoodTranslationInterface;
use App\Repo\AddOnInterface;
use App\Repo\AddOnTranslationInterface;
use App\Repo\AddOnCategoryInterface;
use App\Repo\AddOnCategoryTranslationInterface;
use App\Repo\AddonAddonCategoryInterface;
use App\Repo\FoodCategoryInterface;
use Illuminate\Support\Facades\Config;
use LogStoreHelper;
use Illuminate\Support\Facades\DB;
use RestaurantChecker;
use App\Repo\RestaurantCategoryInterface;

class CloneController extends Controller
{
    protected $food;
    protected $foodTranslation;
    protected $addon;
    protected $addonTranslation;
    protected $category;
    protected $categoryTranslation;
    protected $foodAddonCategory;
    protected $addonCategory;
    protected $foodCategory;
    protected $restaurantCategory;

    /**
     * CloneController constructor.
     * @param FoodInterface $food
     * @param FoodTranslationInterface $foodTranslation
     * @param AddOnInterface $addon
     * @param AddOnTranslationInterface $addonTranslation
     * @param AddOnCategoryInterface $category
     * @param AddOnCategoryTranslationInterface $categoryTranslation
     * @param LogStoreHelper $logStoreHelper
     * @param FoodAddonCategoryInterface $foodAddonCategory
     * @param AddonAddonCategoryInterface $addonCategory
     * @param FoodCategoryInterface $foodCategory
     */

    public function __construct(FoodInterface $food, FoodTranslationInterface $foodTranslation, AddOnInterface $addon, AddOnTranslationInterface $addonTranslation, AddOnCategoryInterface $category, AddOnCategoryTranslationInterface $categoryTranslation, LogStoreHelper $logStoreHelper, FoodAddonCategoryInterface $foodAddonCategory, AddonAddonCategoryInterface $addonCategory, FoodCategoryInterface $foodCategory, RestaurantCategoryInterface $restaurantCategory)
    {
        $this->food = $food;
        $this->foodTranslation = $foodTranslation;
        $this->addon = $addon;
        $this->addonTranslation = $addonTranslation;
        $this->category = $category;
        $this->categoryTranslation = $categoryTranslation;
        $this->foodAddonCategory = $foodAddonCategory;
        $this->addonCategory = $addonCategory;
        $this->foodCategory = $foodCategory;
        $this->logStoreHelper = $logStoreHelper;
        $this->restaurantCategory = $restaurantCategory;

    }

    /**
     * Clone the menu of one restaurant branch to another branch.
     * This cloning includes food, addon and addon categories and their relationships with each other.
     *
     * @param $fromId
     * @param $toId
     */
    /**
     * @param $fromId
     * @param $toId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function cloneMenu($fromId, $toId, Request $request)
    {
        /**
         * Check if the restaurants are active. If restaurant are not active, throw 404 status code for bad request.
         */
        if ((!RestaurantChecker::check($fromId)) || (!RestaurantChecker::check($toId))) {
            return response()->json([

                "status" => "404",
                "message" => "Please enter active source and destination restaurants"

            ], 404);

        }
        /**
         * Validate the request.
         * Request for field 'food_id' is required and should be an array
         */

        try {
            $this->validate($request, [//validating the request
                "food_id" => "required|array",
            ]);

        } catch (\Exception $e) {

            return response()->json([
                "status" => "422",
                "message" => $e->response->original
            ], 422);


        }

        /**
         * Start DB transaction to ensure the operation is reversed in case not successfully committed.
         *
         */

        DB::beginTransaction();

        foreach ($request['food_id'] as $foodId) {
            try {
                $food = $this->food->getSpecificFoodByRestaurant($foodId, $fromId);

                $translation = $food->foodTranslation;
//                if ($food['status'] == 0) {
//
//                    throw new \Exception("Food of id:" . $foodId . " is not active");
//
                //}
                 if ($translation->count() == 0) {

                    throw new \Exception("Default translation of food id " . $food['id'] . " is not found.");
                }
                /**
                 * Insert new food row with the data same as food of source restaurant except for restaurant_id.
                 * restaurant_id is assigned with id of destination restaurant.
                 * Pass data of food and destination restaurant id to  protected function createFoodRequest() to get request for insertion
                 */

                $foodCreate = $this->food->createFood($this->createFoodRequest($food, $toId));

                /**
                 * Fetch an array of food translation request.
                 * Get all translations related to specific food.
                 * Pass $translation and id of newly created food to function createFoodTransRequest() to get array of food translation request
                 *
                 */
                $foodTransRequest = $this->createFoodTransRequest($translation, $foodCreate['id']);
                /**
                 * Insert each of food translation request into food_translation table
                 */
                foreach ($foodTransRequest as $request) {
                    $this->foodTranslation->createFoodTranslation($request);


                }

            } catch (ModelNotFoundException $exception) {
                return response()->json([
                    'status' => '404',
                    'message' => "Food of id:" . $foodId . " not found"
                ], 404);
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    'message' => $ex->getMessage()
                ], 404);
            }
            try {
            /**
             * Fetch categories and their translation related to food of source restaurant.
             * Food must have at least 1 related category.
             * If food does not have any related category, throw exception and revert the cloning process.
             */


                /**
                 * Fetch active categories related to food.
                 * Check if the categories are present. If categories are not present, throw 404 status code.
                 */
                $foodCategory = $food->category()->get();


                if (count($foodCategory) == 0) {
                    throw new \Exception("Category for food id: ".$foodId . " not found");
                }
                /**
                 * Attach each food related categories to newly created food.
                 *
                 */

                foreach ($foodCategory as $category) {
                    $this->foodCategory->attachCategoryToFood($foodCreate['id'], $category['id']);

                    if (count($this->restaurantCategory->getSpecificRestaurantCategory($toId, $category['id'])) <= 0)
                    {
                       $this->restaurantCategory->attachCategoryToRestaurant($toId, $category['id']);

                    }
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'status' => '404',
                    'message' => $ex->getMessage()
                ], 404);

            }

            /**
             * Fetch all addon category and their translation related to food of same restaurant, here source restaurant.
             * Insert new add on categories and their translation with same data except restaurant_id.
             * Id of destination restaurant id is used for restaurant_id.
             */
            try {

                $addonCategory = $food->addonCategory()->where('restaurant_id', $fromId)->get();
                /**
                 * Fetch active categories related to food.
                 * Check if the categories are present. If categories are not present, continue for next food.
                 */

                if (count($addonCategory) == 0) {
                    throw new \Exception();
                }

                foreach ($addonCategory as $category) {
                    try {
                        $catTranslation = $category->categoryTranslation()->get();
                        if (count($catTranslation) == 0) {
                            throw new \Exception();
                        }

                    } catch (\Exception $ex) {
                        return response()->json([
                            'status' => "404",
                            "message" => "Default translation of addon Category id " . $category['id'] . " is not found."
                        ], 404);

                    }

                    try {
                        /**
                         * Fetch all the active addons related to specific addon category.
                         * Check if the addons are present. If
                         */

                        $addonList = $category->addon()->where('restaurant_id', $fromId)->get();

                        if (count($addonList) == 0) {
                            throw new \Exception();
                        }
                    } catch (\Exception $ex) {
                        return response()->json([
                            'status' => "404",
                            "message" => "Empty record for addons related to category of id:" . $category['id']
                        ], 404);

                    }


                    /**
                     * Insert new addon category row with the data same as food related addon category, except for restaurant_id.
                     * restaurant_id is assigned with id of destination restaurant.
                     * Pass data of addon category and destination restaurant id to  protected function createCategoryRequest() to get request for insertion
                     */

                    $categoryCreate = $this->category->createAddOnCategory($this->createCategoryRequest($category, $toId));

                    /**
                     * Fetch an array of addon category translation request.
                     * Get all translations related to specific addon category.
                     * Pass $catTranslation and id of newly created addon category to function createCategoryTransRequest() to get array of addon category translation request
                     *
                     */

                    $categoryTransRequest = $this->createCategoryTransRequest($catTranslation, $categoryCreate['id']);

                    /**
                     * Insert each of food translation request into addoncategory_translation table
                     */
                    foreach ($categoryTransRequest as $request) {
                        $this->categoryTranslation->createAddOnCatTranslation($request);
                    }

                    /**
                     * attach newly created addon category to newly created food
                     */

                    $this->foodAddonCategory->attachCategoryToFood($foodCreate['id'], $categoryCreate['id']);

                    try {
                        foreach ($addonList as $addon) {

                            try {
                                $addonTranslation = $addon->addonTranslation;
                                if (count($addonTranslation) == 0) {
                                    throw new \Exception();
                                }

                            } catch (\Exception $ex) {
                                return response()->json([
                                    'status' => "404",
                                    "message" => "Default translation of addon id " . $addon['id'] . " is not found."
                                ], 404);

                            }


                            $addonCreate = $this->addon->createAddOn($this->createAddonRequest($addon, $toId));

                            $addonTransRequest = $this->createAddonTransRequest($addonTranslation, $addonCreate['id']);
                            foreach ($addonTransRequest as $request) {
                                $this->addonTranslation->createAddOnTranslation($request);
                            }

                            /**
                             * attach newly created addon to newly created to addon category
                             */

                            $this->addonCategory->attachCategoryToAddon($addonCreate['id'], $categoryCreate['id']);
                        }

                    } catch (\Exception $ex) {
                        continue;

                    }


                }

            } catch (\Exception $ex) {
                continue;
            }


        }
        DB::commit();
        $this->logStoreHelper->storeLogInfo([
            'Clone Menu', [
                'status' => '200',
                "message" => 'Food menu cloned successfully'
            ]

        ]);

        return response()->json([
            'status' => "200",
            "message" => "Food menu Cloned Successfully"
        ], 200);


    }

    /**
     * @param $food
     * @param $restaurantId
     * @return array
     */

    protected function createFoodRequest($food, $restaurantId)
    {

        $foodRequest = [
            'restaurant_id' => $restaurantId,
            'price' => $food['price'],
            'veg' => $food['veg'],
            'featured_image' => !empty($food['featured_image']) ? trim(str_replace(Config::get("config.image_service_base_url_cdn"),"",$food['featured_image'])) :null,
            'gallery' => $food['gallery'],
            'status' => $food['status']
        ];

        return $foodRequest;

    }

    /**
     * Creates request for food translation
     * @param $translation
     * @param $foodId
     * @return array
     */

    protected function createFoodTransRequest($translation, $foodId)
    {
        foreach ($translation as $trans) {
            $translationRequest[] = [
                'food_id' => $foodId,
                'lang' => $trans['lang'],
                'name' => $trans['name'],
                'description' => $trans['description'],
                'ingredients' => $trans['ingredients']
            ];

        }


        return $translationRequest;
    }

    /**
     * Creates request for addon category
     * @param $category
     * @param $restaurantId
     * @return array
     */

    protected function createCategoryRequest($category, $restaurantId)
    {
        $categoryRequest = [
            'restaurant_id' => $restaurantId,
            'min_addon' => $category['min_addon'],
            'max_addon' => $category['max_addon'],
            'status' => $category['status']
        ];


        return $categoryRequest;
    }

    /**
     * Creates request for addon category transalation
     * @param $translation
     * @param $categoryId
     * @return array
     */

    protected function createCategoryTransRequest($translation, $categoryId)
    {
        foreach ($translation as $trans) {
            $translationRequest[] = [
                'category_id' => $categoryId,
                'lang' => $trans['lang'],
                'name' => $trans['name']
            ];

        }
        return $translationRequest;
    }

    /**
     * Creates request for addon
     * @param $addon
     * @param $restaurantId
     * @return array
     */

    protected function createAddonRequest($addon, $restaurantId)
    {
        $addonRequest = [
            'restaurant_id' => $restaurantId,
            'price' => $addon['price'],
            'status' => $addon['status']
        ];

        return $addonRequest;
    }

    /**
     * creates request for addon translation
     * @param $translation
     * @param $addonId
     * @return array
     */

    protected function createAddonTransRequest($translation, $addonId)
    {
        foreach ($translation as $trans) {
            $translationRequest[] = [
                'addon_id' => $addonId,
                'lang' => $trans['lang'],
                'name' => $trans['name'],
                'description' => $trans['description']
            ];
        }
        return $translationRequest;
    }


}