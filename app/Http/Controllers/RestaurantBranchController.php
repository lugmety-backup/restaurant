<?php

namespace App\Http\Controllers;

use App\Events\EmailEvent;
use App\Models\CuisineBranchSorting;
use App\Repo\BranchSortingInterface;
use App\Repo\CategoryInterface;
use App\Repo\CuisineInterface;
use App\Repo\CustomerRestaurantInterface;
use App\Repo\DeliveryZoneInterface;
use App\Repo\FeaturedRestaurantSortingInterface;
use App\Repo\FoodCategoryInterface;
use App\Repo\PaymentMethodInterface;
use App\Repo\RestaurantHourInterface;
use App\Repo\RestaurantInterface;
use App\Repo\RestaurantTranslationInterface;
use App\Repo\RestaurantUsersInterface;
use App\Repo\ShippingMethodInterface;
use App\RestaurantBranch;
use App\RestaurantUser;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Cache;
use Mockery\Exception;
use Redis;
use RoleChecker;
use App\Repo\RestaurantBranchInterface;
use App\Repo\RestaurantBranchTranslationInterface;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Log;
use LogStoreHelper;
use Cocur\Slugify\Slugify;
use RemoteCall;
use Settings;
use ImageUploader;
use App\Http\RestaurantCheckerFacade;
use App\Repo\RestaurantCategoryInterface;
use App\Repo\PivotInterface;


/**
 * Class RestaurantBranchController
 * @package App\Http\Controllers
 */
class RestaurantBranchController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	protected $restaurantBranch;
	/**
	 * @var RestaurantBranchTranslationInterface
	 */
	protected $restaurantBranchTranslation;
	/**
	 * @var LogStoreHelper
	 */
	protected $logStoreHelper;
	/**
	 * @var Slugify
	 */
	protected $slugify;
	protected $restaurant;
	protected $restaurantTranslation;
	protected $restaurantHour;
	protected $oauthBaseUrl;
	protected $deliveryZone;
	protected $paymentMethod;
	protected $shippingMethod;
	protected $restaurantCheckerFacade;
	protected $cuisine;
	protected $category;
	protected $featureRestaurantSorting;
	protected $foodCategory;
	private $restaurantCategory;
	private $pivot;
	private $customerRestaurant;
	private $branch_sorting;

	/**
	 * RestaurantBranchController constructor.
	 * @param RestaurantBranchInterface $restaurantBranch
	 * @param RestaurantInterface $restaurant
	 * @param LogStoreHelper $logStoreHelper
	 * @param Slugify $slugify
	 * @param RestaurantBranchTranslationInterface $restaurantBranchTranslation
	 */
	public function __construct(RestaurantBranchInterface $restaurantBranch,
		RestaurantInterface $restaurant,
		LogStoreHelper $logStoreHelper,
		Slugify $slugify,
		PaymentMethodInterface $paymentMethod,
		ShippingMethodInterface $shippingMethod,
		RestaurantBranchTranslationInterface $restaurantBranchTranslation,
		RestaurantTranslationInterface $restaurantTranslation,
		RestaurantHourInterface $restaurantHour,
		DeliveryZoneInterface $deliveryZone,
		RestaurantCheckerFacade $restaurantCheckerFacade,
		CuisineInterface $cuisine,
		CategoryInterface $category,
		FoodCategoryInterface $foodCategory, PivotInterface $pivot,
		BranchSortingInterface $branch_sorting,
		RestaurantCategoryInterface $restaurantCategory, CustomerRestaurantInterface $customerRestaurant,
		FeaturedRestaurantSortingInterface $featureRestaurantSorting
	)
	{
		$this->restaurantBranch = $restaurantBranch;
		$this->restaurant = $restaurant;
		$this->restaurantBranchTranslation = $restaurantBranchTranslation;
		$this->logStoreHelper = $logStoreHelper;
		$this->slugify = $slugify;
		$this->restaurantTranslation = $restaurantTranslation;
		$this->restaurantHour = $restaurantHour;
		$this->oauthBaseUrl = Config::get('config.oauth_base_url');
		$this->deliveryZone = $deliveryZone;
		$this->paymentMethod = $paymentMethod;
		$this->shippingMethod = $shippingMethod;
		$this->restaurantCheckerFacade = $restaurantCheckerFacade;
		$this->cuisine = $cuisine;
		$this->category = $category;
		$this->foodCategory = $foodCategory;
		$this->restaurantCategory = $restaurantCategory;
		$this->pivot = $pivot;
		$this->customerRestaurant = $customerRestaurant;
		$this->featureRestaurantSorting = $featureRestaurantSorting;
		$this->branch_sorting = $branch_sorting;
	}


	/**
	 * Display all the restaurant branches of Restaurant with respect to role. If the logged-in user is admin, super-admin, restaurant-admin or parent-role restaurant-admin, the user will get all the branches of user's restaurant. Else if the logged-in user is branch-admin or parent-role branch-admin, the user get only his/her's restaurant branch.
	 *
	 * @param $id
	 * @param Request $request
	 * @return \Illuminate\Http\Response
	 */

	public function adminIndex($restaurant_id, Request $request)
	{
		//RoleChecker::hasRole("Admin");
		$request['status'] = Input::get('status');
		$request['lang'] = Input::get('lang', 'en');
		if (!RoleChecker::hasRole('super-admin') && !RoleChecker::hasRole('admin') &&
		    !RoleChecker::hasRole('restaurant-admin') &&
		    (RoleChecker::get('parentRole') != 'restaurant-admin')
		    &&
		    !RoleChecker::hasRole('branch-admin') &&
		    (RoleChecker::get('parentRole') != 'branch-admin')
		) {
			return response()->json([
				'status' => '403',
				'message' => 'You have no access'], 403);
		}

		/**
		 *validation for restaurant_id
		 */
		try {
			$restaurant = $this->restaurant->getSpecificRestaurant($restaurant_id, null);
		} catch (\Exception $e) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Requested Restaurant branch could not be found'
			]));
			return response()->json([
				'status' => '404',
				'message' => 'Requested Restaurant branch could not be found'], 404);
		}

		try {
			$restaurantBranchs = $this->restaurantBranch->getAllRestaurantBranchByStatusForDataTable($restaurant_id);
			if (count($restaurantBranchs) == 0 || is_null($restaurantBranchs)) {
				throw new \Exception();
			}
		} catch (\Exception $ex) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Empty Record'
			]));
			return response()->json([
				'status' => '404',
				"message" => 'Empty Record'
			], 404);
		}
		try {
			$branchList = [];
			$branchList["ids"] = array_values(array_unique($restaurantBranchs->pluck("id")->all()));
			$orderBaseUrl = Config::get("config.order_base_url");
			$fullUrl = $orderBaseUrl . '/machine/status';
			$getAllStatus = RemoteCall::storeWithoutOauth($fullUrl, $branchList);
			if ($getAllStatus["status"] != "200") {
				return response()->json([
					"status" => $getAllStatus["status"],
					"message" => $getAllStatus["message"]
				], $getAllStatus["status"]);
			}
			$isEmptyStatus = $getAllStatus["message"]["data"]["allEmpty"];
			$allMachineStatus = $getAllStatus["message"]["data"]["data"];
			foreach ($restaurantBranchs as $restaurantBranch) {
				$restaurantBranch["machine_status"] = "Inactive";
				if ($isEmptyStatus === false) {
					foreach ($allMachineStatus as $machineStatus) {
						if ($machineStatus["restaurant_id"] == $restaurantBranch["id"]) {
							if ($machineStatus["status"]) {
								$restaurantBranch["machine_status"] = "Active";
							}
							break;
						}
					}
				}
				$reviews = $restaurantBranch->restaurantReviews();
				$restaurantBranch['average_rating'] = round($reviews->avg("rating"), 1, PHP_ROUND_HALF_DOWN);
				if ($restaurantBranch["average_rating"] == null) {
					$restaurantBranch["average_rating"] = 0;
				}
				$restaurantBranch['review_count'] = $reviews->count();

				$restaurantBranch['shipping_methods'] = $restaurantBranch->shippingMethod()->get();
				$restaurantBranch['payment_methods'] = $restaurantBranch->paymentMethod()->get();

				$translation = $this->restaurantBranchTranslation->getAllRestaurantBranchTranslationByLangCode($restaurantBranch->id, $request['lang']);
				if (count($translation) == 0) {
					$translation = $this->restaurantBranchTranslation->getAllRestaurantBranchTranslationByLangCode($restaurantBranch->id, "en");
				}
				if (count($translation) != 0) {
					$restaurantBranch['lang_code'] = $translation[0]->lang_code;
					$restaurantBranch['name'] = $translation[0]->name;
					$restaurantBranch["address"] = $translation[0]->address;
				}

				try {
					$restaurantBranch['restaurant_open'] = $this->restaurantCheckerFacade->checkOpenRestaurant($restaurantBranch->id);
				} catch (\Exception $ex) {
					$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
						'status' => '404',
						'message' => 'Restaurant Branch could not be found'
					]));
					return response()->json([
						'status' => '404',
						"message" => 'Restaurant Branch could not be found'
					], 404);
				}
			}
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '200',
				'message' => $restaurantBranchs
			]));
			return \Datatables::of($restaurantBranchs)->make(true);

		} catch (\Exception $e) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Requested Restaurant Branch could not be found'
			]));
			return response()->json([
				'status' => '404',
				'message' => 'Requested Restaurant Branch could not be found'
			], 404);
		}
	}

	public function index($restaurant_id, Request $request)
	{
		//RoleChecker::hasRole("Admin");
		$request['status'] = Input::get('status');
		$request['lang'] = Input::get('lang', 'en');
		if (!RoleChecker::hasRole('super-admin') && !RoleChecker::hasRole('admin')) {
			return response()->json([
				'status' => '403',
				'message' => 'You have no access'], 403);
		}

		/**
		 *validation for restaurant_id
		 */
		try {
			$restaurant = $this->restaurant->getSpecificRestaurant($restaurant_id, null);
		} catch (\Exception $e) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Requested Restaurant branch could not be found'
			]));
			return response()->json([
				'status' => '404',
				'message' => 'Requested Restaurant branch could not be found'], 404);
		}
		if ((RoleChecker::hasRole('restaurant-admin') || (RoleChecker::get('parentRole') == 'restaurant_admin'))) {
			try {
				if (RoleChecker::get('is_restaurant_admin') != true) {
					throw new \Exception();
				}
				if ($restaurant_id == RoleChecker::get('restaurant_id')) {
				} else {
					throw new \Exception();
				}

			} catch (\Exception $ex) {
				return response()->json([
					'status' => '403',
					'message' => 'You have no access'], 403);
			}
		}

		try {
			$restaurantBranchs = $this->restaurantBranch->getAllRestaurantBranchByStatusForDataTable($restaurant_id);
			if (count($restaurantBranchs) == 0 || is_null($restaurantBranchs)) {
				throw new \Exception();
			}
		} catch (\Exception $ex) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Empty Record'
			]));
			return response()->json([
				'status' => '404',
				"message" => 'Empty Record'
			], 404);
		}

		$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
			'status' => '200',
			'message' => $restaurantBranchs
		]));
		return response()->json([
			'status' => '200',
			"data" => $restaurantBranchs
		], 200);


	}

	/**
	 * Display specific restaurant branches of Restaurant with respect to role. If the logged-in user is admin, super-admin, restaurant-admin or parent-role restaurant-admin, the user will get all the branch of user's restaurant. Else if the logged-in user is branch-admin or parent-role branch-admin, the user get only his/her's restaurant branch. And The resource owner requires permission ***view-restaurant-branch*** .
	 *
	 * @param $restaurant_id
	 * @param $branch_id
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function show($restaurant_id, $branch_id, Request $request)
	{
		/**
		 *validation for restaurant_id
		 */
		try {
			$restaurant = $this->restaurant->getSpecificRestaurant($restaurant_id, null);
		} catch (\Exception $e) {
			return response()->json([
				'status' => '404',
				'message' => 'Requested Restaurant branch could not be found'], 404);
		}
		/**
		 *validation for branch_id
		 */
		try {
			if (ctype_digit($branch_id)) {
				$restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($branch_id);
			} else {
				$restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranchBySlug($branch_id);
			}

		} catch (\Exception $ex) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Empty Record'
			]));
			return response()->json([
				'status' => '404',
				"message" => 'Empty Record'
			], 404);
		} catch (ModelNotFoundException $ex) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Restaurant Branch could not be found'
			]));
			return response()->json([
				'status' => '404',
				"message" => 'Restaurant Branch could not be found'
			], 404);
		}
		try {
			if ($restaurantBranch['parent_id'] != $restaurant_id) {
				throw new \Exception();
			}
		} catch (\Exception $e) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Requested Branch is not found in requested restaurant'
			]));
			return response()->json([
				'status' => '404',
				'message' => 'Requested Branch is not found in requested restaurant'], 404);
		}

		try {

			$restaurantBranch->hideTinAndPrefix = false;
			$restaurantBranch->makeVisible(["logistics_fee","delivery_fee"]);
			$reviews = $restaurantBranch->restaurantReviews();
			$restaurantBranch['average_rating'] = round($reviews->avg("rating"), 1, PHP_ROUND_HALF_DOWN);
			if ($restaurantBranch["average_rating"] == null) {
				$restaurantBranch["average_rating"] = 0;
			}
			$restaurantBranch['review_count'] = $reviews->count();

			$restaurantBranch['shipping_methods'] = $restaurantBranch->shippingMethod()->get();
			$restaurantBranch['payment_methods'] = $restaurantBranch->paymentMethod()->get();
			$restaurantBranch['cuisines'] = $restaurantBranch->cuisine()->get();
			$restaurantBranch['delivery_zones'] = $restaurantBranch->deliveryZone()->get();
			$restaurantBranch['food_categories'] = $restaurantBranch->category()->get();
//            foreach ($restaurantBranch['food_categories'] as $food){
//                $food['translation'] = $food->categoryTranslation()->get();
//            }

			$url = Config::get("config.location_url");
			$country = RemoteCall::getSpecificWithoutOauth($url . "/location/names?country_id=" . $restaurantBranch['country_id'] . "&city_id=" . $restaurantBranch['city_id'] . "&district_id=" . $restaurantBranch['district_id']);
			if ($country["status"] != "200") {
				return response()->json([
					"status" => $country["status"],
					"message" => $country["message"]
				], $country["status"]);
			}
			$restaurantBranch['country'] = $country['message']['data']['country'];
			$restaurantBranch['city'] = $country['message']['data']['city'];
			$restaurantBranch['district'] = $country['message']['data']['district'];
			$restaurantBranch["currency"] = $country['message']['data']["currency"];
			$translation = $this->restaurantBranchTranslation->getAllRestaurantBranchTranslation($restaurantBranch->id);
			$restaurantBranch['translation'] = $translation;
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '200',
				'message' => $restaurantBranch
			]));
			return response()->json([
				'status' => '200',
				'data' => $restaurantBranch
			], 200);
		} catch (QueryException $e) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Requested Restaurant Branch could not be found'
			]));
			return response()->json([
				'status' => '404',
				'message' => 'Requested Restaurant Branch could not be found'
			], 404);
		} catch (\Exception $e) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Requested Restaurant Branch could not be found'
			]));
			return response()->json([
				'status' => '404',
				'message' => 'Requested Restaurant Branch could not be found'
			], 404);
		}
	}

	/**
	 * return list of restaurant branches based on input array list of ids
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getAllRestaurantForDrivers(Request $request)
	{
		try {
			$this->validate($request, [
				"restaurant_ids" => "required|array",
				'lang' => "required"
			]);
		} catch (\Exception $ex) {
			return response()->json([
				"status" => "422",
				"message" => $ex->response->original
			], 422);
		}
		try {
			//gets restaurant details based on arrays of ids, ignores id which is not avialable and displays the remaining restaurant branches details
			$restaurants = $this->restaurantBranch->getRestaurantListForDrivers($request->restaurant_ids);
			//checks if the restaurants list is empty or not if empty then throw exception
			if (!$restaurants->first()) {
				throw new Exception();
			}
		} catch (\Exception $ex) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Nearby Restaurants Orders not found'
			]));
			return response()->json([
				'status' => '404',
				"message" => 'Nearby Restaurants Orders not found'
			], 404);
		}
		foreach ($restaurants as $restaurantBranch) {
			$translation = $this->restaurantBranchTranslation->getAllRestaurantBranchTranslationByLangCode($restaurantBranch->id, $request['lang']);
			if (count($translation) == 0) {
				$translation = $this->restaurantBranchTranslation->getAllRestaurantBranchTranslationByLangCode($restaurantBranch->id, "en");
			}
			if (count($translation) != 0) {
				$restaurantBranch['name'] = $translation[0]->name;
				$restaurantBranch["address"] = $translation[0]->address;
			}
			try {
				$restaurantBranch['restaurant_open'] = $this->restaurantCheckerFacade->checkOpenRestaurant($restaurantBranch->id);
				/**
				 * If restaurant is closed the previously driver is not allowed to accept order now driver can accept the order though restaurant is closed. based on task
				 * https://basecamp.com/3732022/projects/15697415/todos/371425628
				 */
				if($restaurantBranch['restaurant_open'] == false){
					$restaurantBranch['restaurant_open'] = true;
				}
			} catch (\Exception $ex) {
				$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
					'status' => '404',
					'message' => 'Restaurant Branch could not be found'
				]));
				return response()->json([
					'status' => '404',
					"message" => 'Restaurant Branch could not be found'
				], 404);
			}

		}
		return response()->json([
			"status" => "200",
			"data" => $restaurants->makeHidden(['country_id', 'city_id', 'district_id', 'parent_id', 'mobile_no', 'email_address', 'commission_rate', 'slug', 'min_purchase_amt',
				'max_purchase_amt', 'no_of_seats', 'available_seats', 'machine_ref_key', 'freelance_drive_supported',
				'is_featured', 'pre_order', 'methods', 'status', 'timezone', 'shift',
				"is_approved", 'approximate_delivery_time', "free_delivery"])
		]);

	}

	/**
	 * Display all the restaurant branches which is publicly available.
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function publicIndex(Request $request)
	{
		/**
		 * RoleChecker::hasRole("Admin");
		 */
		$request['lang'] = Input::get('lang', 'en');
		$request['limit'] = Input::get("limit", 10);
		$request['country_id'] = Input::get("country_id");
		$request["district_id"] = Input::get("district_id");
		$request["is_featured"] = Input::get("is_featured");
		//added by ashish
		$request['city_id'] = Input::get("city_id");
		/**
		 * if limit param is other than integer and
		 */
		if (!is_null($request['limit'])) {
			$rules = [
				"limit" => "numeric|min:1"];
			$validator = Validator::make($request->all(), $rules);

			if ($validator->fails()) {
				$request['limit'] = 10;
			}
		}
		/**
		 * validation for country_id
		 */
//        if (!is_null($request['country_id'])) {
		$rules = [
			"country_id" => "required|integer",
			"city_id" => "nullable|integer|min:1",
			"district_id" => "nullable|integer|min:1",
			"is_featured" => "nullable|integer|min:0|max:1"
		];
		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '422',
				'message' => $validator->errors()
			]));
			return response()->json([
				'status' => '422',
				"message" => $validator->errors()
			]);
		}
//        } else {
//            $this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
//                'status' => '422',
//                'message' => ["country_id" => ['The country_id parameter is required']]
//            ]));
//            return response()->json([
//                'status' => '422',
//                "message" => ["country_id" => ['The country_id parameter is required']]
//            ], 422);
//        }


//        else {
//            $this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
//                'status' => '422',
//                'message' => ["city_id" => ['The city_id parameter is required']]
//            ]));
//            return response()->json([
//                'status' => '422',
//                "message" => ["city_id" => ['The city_id parameter is required']]
//            ], 422);
//        }


		try {
			$returnData = $this->restaurantBranch->getAllRestaurantBranchForPublic($request);
			$restaurantBranchs = $returnData['data'];
			$parameter = $returnData['parameter'];

		} catch (\Exception $ex) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Restaurant Branch could not be found'
			]));
			return response()->json([
				'status' => '404',
				"message" => 'Restaurant Branch could not be found'
			], 404);
		}

		try {
			foreach ($restaurantBranchs as $restaurantBranch) {
				$restaurantBranch->makeHidden(["commission_rate", "email_address", "machine_ref_key", "is_approved"]);

				$reviews = $restaurantBranch->restaurantReviews();
				$restaurantBranch['average_rating'] = round($reviews->avg("rating"), 1, PHP_ROUND_HALF_DOWN);
				if ($restaurantBranch["average_rating"] == null) {
					$restaurantBranch["average_rating"] = 0;
				}
				$restaurantBranch['review_count'] = $reviews->count();

				$restaurantBranch['shipping_methods'] = $restaurantBranch->shippingMethod()->get();
				$restaurantBranch['payment_methods'] = $restaurantBranch->paymentMethod()->get();

				$translation = $this->restaurantBranchTranslation->getAllRestaurantBranchTranslationByLangCode($restaurantBranch->id, $request['lang']);
				if (count($translation) == 0) {
					$translation = $this->restaurantBranchTranslation->getAllRestaurantBranchTranslationByLangCode($restaurantBranch->id, "en");
				}
				if (count($translation) != 0) {
					$restaurantBranch['lang_code'] = $translation[0]->lang_code;
					$restaurantBranch['name'] = $translation[0]->name;
					$restaurantBranch["address"] = $translation[0]->address;
				}
				try {
					$restaurantBranch['restaurant_open'] = $this->restaurantCheckerFacade->checkOpenRestaurant($restaurantBranch->id);
				} catch (\Exception $ex) {
					$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
						'status' => '404',
						'message' => 'Restaurant Branch could not be found'
					]));
					return response()->json([
						'status' => '404',
						"message" => 'Restaurant Branch could not be found'
					], 404);
				}
				if ($restaurantBranch['cover_image'] == "") {
					$data = \Settings::getSettings('cover-image');
					if ($data["status"] != 200) {
						return response()->json(
							$data["message"]
							, $data["status"]);
					}
					$coverImage = \Redis::get('cover-image');
					$restaurantBranch['cover_image'] = $coverImage;
				}

			}
			$restaurantBranchs = $this->manualPagination($request["limit"], $restaurantBranchs->toArray(), count($restaurantBranchs));
			$restaurantBranchs->withPath($parameter);
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '200',
				'message' => $restaurantBranchs
			]));
			return response()->json([
				'status' => '200',
				'data' => $restaurantBranchs
			], 200);

		} catch (\Exception $e) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Requested Restaurant Branch could not be found'
			]));

			return response()->json([
				'status' => '404',
				'message' => 'Requested Restaurant Branch could not be found'
			], 404);
		}


	}

	public function publicShow($branch_id, Request $request)
	{
		//RoleChecker::hasRole("Admin");
		$lang = $request['lang'] = Input::get('lang', 'en');
		$version = Input::get('version');
		try {
			if (ctype_digit($branch_id)) {
				$restaurantBranchs = $this->restaurantBranch->getSpecificRestaurantBranchForPublic($branch_id);
			} else {
				$restaurantBranchs = $this->restaurantBranch->getSpecificRestaurantBranchBySlugForPublic($branch_id);
			}
			if (count($restaurantBranchs) == 0) {
				throw new \Exception();
			}
		} catch (\Exception $ex) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Restaurant Branch not found'
			]));
			return response()->json([
				'status' => '404',
				"message" => 'Restaurant Branch not found'
			], 404);
		}
		try {
			foreach ($restaurantBranchs as $restaurantBranch) {
				$mainRestaurant = $restaurantBranch->restaurant()->get();
				if (count($mainRestaurant) == 0) {
					$restaurantBranch["is_parent_active"] = false;

				} else {
					$restaurantBranch["is_parent_active"] = true;
				}
				$restaurantBranch->makeHidden(["commission_rate", "email_address", "machine_ref_key", "is_approved"]);
				$reviews = $restaurantBranch->restaurantReviews();
				$restaurantBranch['average_rating'] = round($reviews->avg("rating"), 1, PHP_ROUND_HALF_DOWN);
				if ($restaurantBranch["average_rating"] == null) {
					$restaurantBranch["average_rating"] = 0;
				}
				$restaurantBranch['review_count'] = $reviews->count();
				$restaurantBranch['shipping_methods'] = $restaurantBranch->shippingMethod()->get();
				$restaurantBranch['payment_methods'] = $restaurantBranch->paymentMethod()->get();
				if($version !== 'v2'){
					foreach ($restaurantBranch['payment_methods'] as $key => $payment_method){
						if(in_array($payment_method['slug'],['mada','payfort'])){
							unset($restaurantBranch['payment_methods'][$key]);
						}
					}
				}
				else{
					foreach ($restaurantBranch['payment_methods'] as $key => $payment_method){
						if(in_array($payment_method['slug'],['creditcard'])){
							unset($restaurantBranch['payment_methods'][$key]);
						}
					}
				}
				$restaurantBranch['payment_methods'] = collect($restaurantBranch['payment_methods'])->values()->all();
//                $restaurantBranch['average_rating'] = $restaurantBranch->averageReviews()->first()['average'];

				foreach ($restaurantBranch['payment_methods'] as $payment) {
					$array["slugs"]['payment'][] = $payment->slug;
				}
				foreach ($restaurantBranch['shipping_methods'] as $shipping) {
					$array["slugs"]['shipping'][] = $shipping->slug;
				}
				$array["lang"] = $request['lang'];

				/**
				 * calling order service to get payment method details
				 */
				$orderBaseUrl = Config::get('config.order_base_url');
				$orderDetail = RemoteCall::storeWithoutOauth($orderBaseUrl . "/payment-shipping-method", $array);
				if ($orderDetail["status"] != 200) {
					$restaurantBranch['payment_methods'] = [];
					$restaurantBranch['shipping_methods'] = [];
				} else {
					$restaurantBranch['payment_methods'] = $orderDetail["message"]["data"]["payment"];
					$restaurantBranch['shipping_methods'] = $orderDetail["message"]["data"]["shipping"];
				}


				/**
				 * get all delivery zone of branch
				 */
				$deliveryZone = $this->deliveryZone->getAllDeliveryZone($restaurantBranch->id);
				$districts = [];
				$results = [];
				if (count($deliveryZone) != 0) {
					$districtFlag = true;
					foreach ($deliveryZone as $zone) {
						$districts['districts'][] = $zone['district_id'];
					}
				} else {
					$districtFlag = false;
					$districts['districts'] = ["5"];
				}
				$districts['lang'] = $request['lang'];
				$districts['country_id'] = $restaurantBranch['country_id'];
				$url = Config::get("config.location_url");
				$districtNames = RemoteCall::store($url . "/public/district/getnames", $districts);
//                $districtNames = RemoteCall::store("localhost:9000"."/public/district/getnames" , $districts);

				if (isset($districtNames['message']['data']['country'])) {
					$restaurantBranch['currency_code'] = $districtNames['message']['data']['country']['currency_code'];
					$restaurantBranch['currency_symbol'] = $districtNames['message']['data']['country']['currency_symbol'];
					$restaurantBranch['min_purchase_amt_currency'] = $restaurantBranch['currency_symbol'] . $restaurantBranch['min_purchase_amt'];
					$restaurantBranch['max_purchase_amt_currency'] = $restaurantBranch['currency_symbol'] . $restaurantBranch['max_purchase_amt'];
				}


				if ($districtNames["status"] == "200") {
					if ($districtFlag) $results = $districtNames['message']['data']['districts'];
					else $results = [];
					foreach ($results as $key => $result) {
						foreach ($deliveryZone as $zone) {
							if ($zone['district_id'] == $result['district_id']) {
								$results[$key]['id'] = $zone['id'];
							}
						}
					}
				}

//                }


				$restaurantBranch['delivery_zones'] = $results;


				/**
				 * restaurant hour
				 */

				$restaurantHour = $this->restaurantHour->getSpecificRestaurantHourByIdRestaurantId($restaurantBranch->id);
				$hours = [];
				$hours["sunday"] = [];
				$hours["monday"] = [];
				$hours["tuesday"] = [];
				$hours["wednesday"] = [];
				$hours["thursday"] = [];
				$hours["friday"] = [];
				$hours["saturday"] = [];
				if (count($restaurantHour) != 0) {
					foreach ($restaurantHour as $hour) {
						$hour["opening_time"] = $this->changeToTimezoneOfBranch($hour['opening_time'], $restaurantBranch['timezone']);
						$hour["closing_time"] = $this->changeToTimezoneOfBranch($hour['closing_time'], $restaurantBranch['timezone']);
						$hour["opening_time"] = Carbon::parse($hour["opening_time"])->format("h:i A");
						$hour["closing_time"] = Carbon::parse($hour["closing_time"])->format("h:i A");
						if ($hour->day == "Sunday") {
							$hours["sunday"][] = $hour;
						} else if ($hour->day == "Monday") {
							$hours["monday"][] = $hour;
						} else if ($hour->day == "Tuesday") {
							$hours["tuesday"][] = $hour;
						} else if ($hour->day == "Wednesday") {
							$hours["wednesday"][] = $hour;
						} else if ($hour->day == "Thursday") {
							$hours["thursday"][] = $hour;
						} else if ($hour->day == "Friday") {
							$hours["friday"][] = $hour;
						} else if ($hour->day == "Saturday") {
							$hours["saturday"][] = $hour;
						}

					}
				}
//                $final_hours = [];
//                foreach ($hours as $key => $hour){
//                    $final_hours[][$key] = $hour;
//                }
				$restaurantBranch['restaurant_hour'] = $hours;


//                $translation = $this->restaurantBranchTranslation->getAllRestaurantBranchTranslationByLangCode($restaurantBranch->id, $request['lang']);
//                if (count($translation) == 0) {
//                    $translation = $this->restaurantBranchTranslation->getAllRestaurantBranchTranslationByLangCode($restaurantBranch->id, "en");
//                }
//                if (count($translation) != 0) {
//                    $restaurantBranch['lang_code'] = $translation[0]->lang_code;
//                    $restaurantBranch['name'] = $translation[0]->name;
//                    $restaurantBranch['address'] = $translation[0]->address;
//                }

				try {
					$cuisines = $restaurantBranch->cuisine()->get();
					if ($cuisines->count() == 0) {
						throw new \Exception();
					}
					foreach ($cuisines as $cuisine) {
						$translation = $cuisine->cuisineTranslations()->where('lang_code', $lang)->get();

						try {
							if ($translation->count() == 0) {
								if ($lang == 'en') {
									throw new \Exception();
								} else {
									$translation = $cuisine->cuisineTranslations()->where('lang_code', 'en')->get();
									if ($translation->count() == 0) {
										throw new \Exception();
									}
								}
							}
							$cuisine["name"] = $translation[0]["name"];
							$cuisine["lang_code"] = $translation[0]["lang_code"];
						} catch (\Exception $ex) {
							return response()->json([
								"status" => "404",
								"message" => "Default language english not found for cuisine translation"
							], 404);
						}
					}
				} catch (\Exception $ex) {
					$cuisines = [];
				}
				$restaurantBranch["cuisines"] = $cuisines;
				try {
					$restaurantBranch['restaurant_open'] = $this->restaurantCheckerFacade->checkOpenRestaurant($restaurantBranch->id);
				} catch (\Exception $ex) {
					$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
						'status' => '404',
						'message' => 'Restaurant Branch could not be found'
					]));
					return response()->json([
						'status' => '404',
						"message" => 'Restaurant Branch could not be found'
					], 404);
				}
				try {
					/**
					 * Get specific restaurant branch of given id.
					 * If restaurant is not active or available, throw 404 error message.
					 */
//                    $restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($id);
					$id = $branch_id;
					$lang = $request["lang"];
					if (count($restaurantBranch) == 0 || $restaurantBranch['status'] == 0) {
						throw new \Exception();
					}
					/**
					 * Get translation of each restaurant branch according to the language desired through 'lang' parameter.
					 * Store translation in $restaurantBranch array along with other details.
					 * If translation is not available in requested language, default language english is used.
					 * If translation is not available in default language, throw exception and abort.
					 */
					$translation = $restaurantBranch->branchTranslation()->where('lang_code', $lang)->get();

					try {
						if ($translation->count() == 0) {
							if ($lang == 'en') {
								throw new \Exception();
							} else {
								$translation = $restaurantBranch->branchTranslation()->where('lang_code', 'en')->get();
								if ($translation->count() == 0) {
									throw new \Exception();
								}
							}

						}
					} catch (\Exception $ex) {
						return response()->json([
							"status" => "404",
							"message" => "Default language english not found in database"
						], 404);
					}

					$restaurantBranch['lang'] = $translation[0]['lang_code'];
					$restaurantBranch['name'] = $translation[0]['name'];
					$restaurantBranch['description'] = $translation[0]['description'];
					$restaurantBranch["address"] = $translation[0]['address'];
					/**
					 * Check count of result. If result count is 0, throw exception. Else check if the status of result is 0.
					 * If user is not logged in or unauthorized, throw exception if status is zero.
					 * If the user is logged in and authorized personnel, continue.
					 */

					try {
						/**
						 * Get categories related to restaurant branch.
						 * If categories is not active or available, throw 404 error message.
						 */
						$categorySortingOfRestaurant = $restaurantBranch->branchCategory()->orderBy("branch_category_sorting.sort", "asc")->orderBy("branch_category_sorting.updated_at", "desc")->pluck("category_id")->values()->all();
						if (count($categorySortingOfRestaurant) == 0) {
							$restaurantCategory = $restaurantBranch->category()->where("category.status", 1)->get();
						} else {
							$filteredRestaurantCategory = $restaurantBranch->category()->whereIn("category.id", $categorySortingOfRestaurant)->where("category.status", 1)->get();
							$restaurantCategory = collect($filteredRestaurantCategory)->sortBy(function ($model) use ($categorySortingOfRestaurant) {
								return array_search($model->getKey(), $categorySortingOfRestaurant);
							});
						}

						/**
						 * Get translation of each category according to the language desired through 'lang' parameter.
						 * Store translation in $category array along with other details.
						 * If translation is not available in requested language, default language english is used.
						 * If translation is not available in default language, throw exception and abort.
						 */

						foreach ($restaurantCategory as $key => $category) {
							$translation = $category->categoryTranslation()->where('lang', $lang)->get();
							try {
								if ($translation->count() == 0) {
									if ($lang == 'en') {
										throw new \Exception();
									} else {
										$translation = $category->categoryTranslation()->where('lang', 'en')->get();
										if ($translation->count() == 0) {
											throw new \Exception();
										}
									}

								}
							} catch (\Exception $ex) {
								return response()->json([
									"status" => "404",
									"message" => "Default language english not found in database"
								], 404);
							}

							$category['lang'] = $translation[0]['lang'];
							$category['name'] = $translation[0]['name'];

							$foodData = $category->food()->where('status', 1)->where('restaurant_id', $restaurantBranch->id)->get();
							if (count($foodData) == 0) {
								unset($restaurantCategory[$key]);
								continue;
							}
							/**
							 * Get Food related to  category and restaurant branch.
							 * If food is not active or available, throw 404 error message.
							 */
							foreach ($foodData as $food) {
								/**
								 * Get translation of each food according to the language desired through 'lang' parameter.
								 * Store translation in $food array along with other details.
								 * If translation is not available in requested language, default language english is used.
								 * If translation is not available in default language, throw exception and abort.
								 */
								$translation = $food->foodTranslation()->where('lang', $lang)->get();
								try {
									if ($translation->count() == 0) {
										if ($lang == 'en') {
											throw new \Exception();
										} else {
											$translation = $food->foodTranslation()->where('lang', 'en')->get();
											if ($translation->count() == 0) {
												throw new \Exception();
											}
										}

									}
								} catch (\Exception $ex) {
									return response()->json([
										"status" => "404",
										"message" => "Default language english not found in database"
									], 404);
								}
								$foodSize = $food->foodSize()->get();

								if ($foodSize->count() == 0) {

									$food['size'] = [];

								} else {

									foreach ($foodSize as $key => $size) {

										if ($size['is_default'] == "1") {
											$food['price'] = $size['price'];

										}
										$sizeTranslation = $size->FoodSizeTranslation()->where('lang', $request['lang'])->first();
										if (count($sizeTranslation) == 0) {
											if ($request['lang'] == 'en') {
												unset($foodSize[$key]);
											} else {
												$sizeTranslation = $size->FoodSizeTranslation()->where('lang', 'en')->first();
												if (count($sizeTranslation) == 0) {
													unset($foodSize[$key]);
												}
											}

										} else {
											$size['title'] = $sizeTranslation['title'];
										}
										$size['is_default'] = (integer)$size['is_default'];
									}

									$food['size'] = $foodSize;
								}

								if (!empty($food->gallery)) {
									unset($foos, $foodNormal);
									$food["gallery"] = unserialize($food->gallery);
									foreach ($food["gallery"] as $key => $foo) {
										/**
										 * get position of last occurance of / in fetaured_image and added the postion by 1
										 */
										$getpositionOfImageInUrl = strrpos($foo, "/") + 1;
										//todo add from setting
										$normalImage = Config::get("config.image_service_base_url_cdn") . substr_replace($foo, "normal-", $getpositionOfImageInUrl, 0);
										$foos[] = Config::get("config.image_service_base_url_cdn") . $foo;
										$foodNormal[] = $normalImage;
									}
									$food["gallery"] = $foos;
									$food["gallery_resized"] = $foodNormal;
								} else {
									$food["gallery"] = [];
									$food["gallery_resized"] = [];
								}
								//todo add from setting
								if (!empty($food->featured_image)) {
									/**
									 * get position of last occurance of / in fetaured_image and added the postion by 1
									 */
									$getpositionOfImageInUrl = strrpos($food->featured_image, "/") + 1;
									/**
									 * add normal- prefix in $getpositionOfImageInUrl postition
									 */
									$food["featured_image_resized"] = substr_replace($food->featured_image, "normal-", $getpositionOfImageInUrl, 0);
								} else {
									$food["featured_image_resized"] = [];
								}
								$food['lang'] = $translation[0]['lang'];
								$food['name'] = $translation[0]['name'];
								$food['description'] = $translation[0]['description'];
								$food['ingredients'] = $translation[0]['ingredients'];
								$food['has_addon'] = false;

								/**
								 * Get addon category related to food and restaurant branch.
								 * If addon category is not active or available, throw 404 error message.
								 */
								$addonCategory = $food->addonCategory()->where('status', 1)->where('status', 1)->where('restaurant_id', $restaurantBranch->id)->get();


								if (count($addonCategory) > 0) {
									foreach ($addonCategory as $addcategory) {
										/**
										 * Get addon related to addon category and restaurant branch.
										 * If addon is not active or available, throw 404 error message.
										 */
										$addon = $addcategory->addon()->where('status', 1)->where('restaurant_id', $restaurantBranch->id)->count();

										if ($addon > 0) {

											$food['has_addon'] = true;
										}
									}
								}


							}
							$category['foodData'] = $foodData;
						}
						$restaurantBranch['categories'] = $restaurantCategory->values();

						if (!$restaurantCategory->first()) {
							throw new \Exception();
						}

					} catch (\Exception $ex) {
						$restaurantBranch['categories'] = [];
					}
				} catch (\Exception $e) {
					return response()->json([
						'status' => '404',
						'message' => 'Restaurant Could not found'
					], 404);
				}

				if ($restaurantBranch['cover_image'] == "") {
					$data = \Settings::getSettings('cover-image');
					if ($data["status"] != 200) {
						return response()->json(
							$data["message"]
							, $data["status"]);
					}
					$coverImage = \Redis::get('cover-image');
					$restaurantBranch['cover_image'] = $coverImage;
				}

			}
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '200',
				'message' => $restaurantBranchs[0]
			]));
			return response()->json([
				'status' => '200',
				'data' => $restaurantBranchs[0]
			], 200);


		} catch (\Exception $e) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Requested Restaurant Branch could not be found'
			]));
			return response()->json([
				'status' => '404',
				'message' => 'Requested Restaurant Branch could not be found'
			], 404);
		}


	}

	public function publicShowWithAllDetails($branch_id, Request $request)
	{
		//RoleChecker::hasRole("Admin");
		$checkPreOrder = false;
		$request['lang'] = Input::get('lang', 'en');
		$request["delivery_time"] = Input::get("delivery_time");
		$request["pre_order"] = Input::get("pre_order");
		try {
			$restaurantBranchs = $this->restaurantBranch->getSpecificRestaurantBranchForPublic($branch_id);
			if (count($restaurantBranchs) == 0) {
				throw new \Exception();
			}

			//checking if the restaurant customer belongs to the passed branch id
			if($request->has('is_restaurant_customer') && $request->is_restaurant_customer){
				//if count is 0 then it means the user is not assigned to current restaurant branch
				if(count($this->customerRestaurant->checkUserBelongsToRestaurant($request["user_id"],$branch_id)) == 0 ){
					return \Settings::getErrorMessageOrDefaultMessage('error-you-are-not-authorized-to-place-order-please-contact-customer-support-for-more-detail',
						"Error! You are not authorized to place order. Please contact customer support for more detail.", 403, $request['lang']);
				}
			}
		} catch (\Exception $ex) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Requested Restaurant Branch could not be found'
			]));
			return response()->json([
				'status' => '404',
				"message" => 'Requested Restaurant Branch could not be found'
			], 404);
		}
		if (!is_null($request["delivery_time"]) && !is_null($request["pre_order"])) {
			if (($restaurantBranchs[0]->pre_order != 1) || ($restaurantBranchs[0]->pre_order != "1")) {
				return response()->json([
					"status" => "403",
					"message" => "Pre Order feature is not available"
				], 403);
			} else {
				//convert the datetime into utc format based on restaurant timezone
				if ($request["delivery_time"] == 'asap') {
					$checkPreOrder = $this->restaurantCheckerFacade->checkPreOrderForRestaurantAsap($restaurantBranchs[0]->id);
				} else {
					$dateInfo = $this->changeToUTC($request["delivery_time"], $restaurantBranchs[0]->timezone);
					//checks if the restaurant is open for passed @param $dateInfo
					$checkPreOrder = $this->restaurantCheckerFacade->checkPreOrderForRestaurant($restaurantBranchs[0]->id, $dateInfo);
					if (!$checkPreOrder) {
						return response()->json([
							"status" => "403",
							"message" => "Restaurant is not available for Pre Order in requested delivery time"
						], 403);
					}
				}
			}
		}

		try {
			$restaurantBranch = $restaurantBranchs[0];
			$restaurantBranch->hideTinAndPrefix = false;
			$restaurantBranch->makeVisible(["logistics_fee","delivery_fee"]);
			$restaurantBranch['shipping_methods'] = $restaurantBranch->shippingMethod()->get();
			$restaurantBranch['payment_methods'] = $restaurantBranch->paymentMethod()->get();

			$reviews = $restaurantBranch->restaurantReviews();
			$restaurantBranch['average_rating'] = round($reviews->avg("rating"), 1, PHP_ROUND_HALF_DOWN);
			if ($restaurantBranch["average_rating"] == null) {
				$restaurantBranch["average_rating"] = 0;
			}
			$restaurantBranch['review_count'] = $reviews->count();

			/**
			 * Translation of branch
			 */
			$translation = $this->restaurantBranchTranslation->getAllRestaurantBranchTranslationByLangCode($restaurantBranch->id, $request['lang']);
			if (count($translation) == 0) {
				$translation = $this->restaurantBranchTranslation->getAllRestaurantBranchTranslationByLangCode($restaurantBranch->id, "en");
			}
			if (count($translation) != 0) {
				$restaurantBranch['lang_code'] = $translation[0]->lang_code;
				$restaurantBranch['name'] = $translation[0]->name;
				$restaurantBranch["address"] = $translation[0]->address;
			}
			/**
			 * Delivery Zone of branch
			 */
			$deliveryZone = $this->deliveryZone->getAllDeliveryZone($restaurantBranch->id, null);
			if (count($deliveryZone) != 0) {
				$restaurantBranch['delivery_zone'] = $deliveryZone->makeHidden(['id', 'branch_id']);
			}
			$restaurantBranch['restaurant_open'] = false;
			/**
			 * Restaurant Hour of branch
			 */
			try {
				/**
				 * checks if the restaurant is opended or not
				 */
				$restaurantBranch['restaurant_open'] = $this->restaurantCheckerFacade->checkOpenRestaurant($restaurantBranch->id);
				/**
				 * if restaurant is closed then then show error message
				 */
//                if($restaurantBranch['restaurant_open'] == false){
//                    $this->logStoreHelper->storeLogError(array("RestaurantBranch", [
//                        'status' => '403',
//                        'message' => 'Restaurant is closed at the moment. Please try again later.'
//                    ]));
//                    return response()->json([
//                        'status' => '403',
//                        'message' => 'Restaurant is closed at the moment. Please try again later.'
//                    ], 403);
//                }
				if ($request["delivery_time"] == 'asap') {
					if ($checkPreOrder) $restaurantBranch["delivery_time"] = Carbon::parse($checkPreOrder)->timezone($restaurantBranch["timezone"])->toDateTimeString();
					else {
						return response()->json([
							"status" => "404",
							"message" => "Nearby delivery time could not be found"
						], 404);
					}

				} else {
					$restaurantBranch["delivery_time"] = $request["delivery_time"];
				}

			} catch (\Exception $ex) {
				$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
					'status' => '404',
					'message' => 'Restaurant Branch could not be found'
				]));
				return response()->json([
					'status' => '404',
					"message" => 'Restaurant Branch could not be found'
				], 404);
			}
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '200',
				'message' => $restaurantBranch
			]));

			/**
			 * Check if restaurant is open or closed
			 */
//todo timezone conversion
//            $nepal = Carbon::now('Asia/Kathmandu','H:i');
//            printf("nepal time ".$nepal. "<br>");
//            $utc = $nepal->setTimeZone(new \DateTimeZone('UTC'));
//            echo $utc;


//            $now = Carbon::now();
//            $parsed = date_parse($now);
//            $time = $parsed['hour']*3600+$parsed['minute']*60+$parsed['second'];
//            dd($parsed);
//            $now = Carbon::now('Asia/Kathmandu');
//            $diff = $now->offset;
//            $time = $parsed['hour']*3600+$parsed['minute']*60;
//            $time= $time - $diff;
//            $current = gmdate('H:i', $time);
//            echo  $current;
//
//dd("test");

			return response()->json([
				'status' => '200',
				'data' => $restaurantBranch
			], 200);

		} catch (\Exception $e) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Requested Restaurant Branch could not be found'
			]));
			return response()->json([
				'status' => '404',
				'message' => 'Requested Restaurant Branch could not be found'
			], 404);
		}


	}

	/**
	 * this endpoint is used to get branch info for notification contact us by branch operator
	 * @param Request $request
	 * @param $branch_id
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getSpecificBranchForNotification(Request $request, $branch_id){
		try{
			/**
			 * checks if the request has hash field or the passed hash is valid or not
			 */
			if(!$request->has('hash') || !password_verify('Alice123$',$request->hash)){
				return response()->json([
					'status' => '403',
					'message' => 'Forbidden.'
				],403);
			}
			$branchDetail = $this->restaurantBranch->getSpecificRestaurantBranch($branch_id);
			$branchDetail["additional_detail"] =  $branchDetail->branchTranslation()->where('lang_code','en')->select('name','address')->first();
			return response()->json([
				'status' => '200',
				'data' => $branchDetail
			]);
		}
		catch (ModelNotFoundException $ex){
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Requested Restaurant Branch could not be found',
				'data' => [$request->all(),$branch_id]
			]));
			return response()->json([
				'status' => '404',
				'message' => 'Requested Restaurant Branch could not be found'
			], 404);
		}
		catch (\Exception $ex){
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '500',
				'message' =>$ex,
				'data' =>   [$request->all(),$branch_id]
			]));
			return response()->json([
				'status' => '500',
				'message' => 'Error fetching branch detail'
			], 500);
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @param $restaurant_id
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store($restaurant_id, Request $request)
	{
//        try {
		try {
			$restaurant = $this->restaurant->getSpecificRestaurantCheckForBranch($restaurant_id, 1);
//            $restaurantTranslation = $this->restaurantTranslation->getSpecificRestaurantTranslationByCodeAndId('en', $restaurant_id);
		} catch (\Exception $e) {
			return response()->json([
				'status' => '404',
				'message' => 'Requested Restaurant could not be found or Restaurant status is disabled'], 404);
		}
		/**
		 * checking roles potential
		 */
		if (!($this->checkRolePotentialForCreateOrDelete($restaurant_id))) {
			return response()->json([
				'status' => '401',
				'message' => 'Unauthorized'], 401);
		}
		/**
		 * validation for restaurantBranch
		 */
		try {
			$message = [
				"domain_validation" => "The mail server for domain does not exist.",
				"check_slug" => "The slug has already been taken.",
				"tax_type.in" => "The tax type field value should either be exclusive,inclusive or none."
			];
			$this->validate($request, [
				'country_id' => 'required|integer',
				'city_id' => 'required|integer',
				'district_id' => 'required|integer',
				'logo' => 'required',
				'cover_image' => 'required',
				'phone_no' => 'required|min:7',
				'mobile_no' => 'required|min:7',
				'email_address' => 'required|email|unique:restaurant_branches',
				'commission_rate' => 'required|',
				'slug' => 'required|check_slug',
				'min_purchase_amt' => 'required|integer',
				'max_purchase_amt' => 'required|integer',
				'no_of_seats' => 'required|integer',
				'machine_ref_key' => 'required',
				'freelance_drive_supported' => 'integer|min:0|max:1',
				'is_featured' => 'integer|min:0|max:1',
				'pre_order' => 'integer|min:0|max:1',
				'translation' => 'required|array',
				'shipping_methods' => 'required|array',
				'payment_methods' => 'required|array',
				'status' => 'integer|min:0|max:1',
				'logistics_fee' =>'required|numeric|min:0',
				'delivery_fee' =>'required|numeric|min:0',
				'latitude' => 'required|numeric',
				'longitude' => 'required|numeric',
				"cuisines" => "array|required",
				"food_categories" => "required|array",
				"shift" => "sometimes|integer|min:1|max:2",
				"free_delivery" => "sometimes|integer|min:0|max:1",
				'delivery_zones' => 'required|array',
				"tax_type" => "required|in:exclusive,inclusive,none",
				"tin" => "sometimes|string",
				"cc_commision_rate" => "numeric|min:0|max:100",
				"user" => "required|array",
				'user.status' => 'integer|min:0|max:1',
				'user.first_name' => 'required|max:50',
				'user.last_name' => 'required|max:50',
				'user.email' => 'required|email|domain_validation|max:255',
				'user.password' => 'required|max:255',
				'user.gender' => 'required',
				'user.mobile' => 'required|min:4|max:19',
				"user.birthday" => "date_format:Y-m-d",
				"additional_contact" => "array",
				"additional_contact" => "sometimes|array",
				"additional_contact.*" => "present|min:7"
			], $message);
		} catch (\Exception $e) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				"status" => '422',
				"message" => $e->response->original
			]));
			return response()->json([
				'status' => '422',
				'message' => $e->response->original
			], 422);
		}
		$request["freelance_drive_supported"] = $request->get("freelance_drive_supported", 0);
		$request["is_featured"] = $request->get("is_featured", 0);
		$request["pre_order"] = $request->get("pre_order", 0);
		/**
		 * Modification of slug accourding to restaurant name
		 */
		$request['slug'] = $this->slugify->slugify($request['slug'], ['regexp' => '/([^A-Za-z]|-)+/']);
		if (empty($request['slug'])) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				"status" => '422',
				"message" => "The slug may not only contain numbers,special characters."
			]));
			return response()->json([
				"status" => "422",
				"message" => ["slug" => "The slug may not only contain numbers,special characters."]
			], 422);
		}
		//validation for additional_contact
		$additional_contacts = [];
		if ($request->has('additional_contact')) {
			foreach ($request['additional_contact'] as $contact) {
				if (is_numeric($contact)) $additional_contacts[] = $contact;
			}
		}
		$request['additional_contact'] = $additional_contacts;

//        $request['slug'] = $restaurantTranslation[0]['name'] . '-' . $request['slug'];
		try {
			$dataValidation = $this->restaurantCategory->getDataForCategoryValidation($request['country_id']);
			$this->validate($request, [
//                'slug' => 'required|unique:restaurant_branches',
				"food_categories" => "required|array|in:" . implode($dataValidation['categories'], ","),
			]);
		} catch (\Exception $e) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				"status" => '422',
				"message" => $e->response->original
			]));
			return response()->json([
				'status' => '422',
				'message' => $e->response->original
			], 422);
		}

//        /**
//         * Converting shipping methods and payment methods to methods
//         */
//        $methods['shipping_methods'] = $request['shipping_methods'];
//        $methods['payment_methods'] = $request['payment_methods'];
		try {
			$request['status'] = Input::get('status', 1);//1 means active
			/**
			 * validation of translation field
			 */

			foreach ($request['translation'] as $key => $req) {
				$validators = Validator::make($req, [
					'name' => 'required',
					"address" => "required"
				]);
				if ($validators->fails()) {
					$this->logStoreHelper->storeLogError(array('Error', [
						'status' => '422',
						'message' => ["translation" => ["$key" => $validators->errors()]]
					]));
					return response()->json([
						'status' => '422',
						'message' => ["translation" => ["$key" => $validators->errors()]]
					], 422);
				}
			}

			$request['parent_id'] = $restaurant_id;
			$request["country_id"] = $restaurant->country_id;
			/**
			 * validating logo image
			 */
			if ($request->has('logo')) {
				$image['avatar'] = $request["logo"];
				$image["type"] = "logo";
				$imageUpload = ImageUploader::upload($image);
				if ($imageUpload["status"] != 200) {
					return response()->json(
						$imageUpload["message"]
						, $imageUpload["status"]);
				}
				$request['logo'] = $imageUpload["message"]["data"];
			}
			/**
			 * validating cover_image
			 */
			if ($request->has('cover_image')) {
				$imaged["avatar"] = $request["cover_image"];
				$imaged["type"] = "cover_image";
				$imageUpload = ImageUploader::upload($imaged);
				if ($imageUpload["status"] != 200) {
					return response()->json(
						$imageUpload["message"]
						, $imageUpload["status"]);
				}
				$request['cover_image'] = $imageUpload["message"]["data"];
			}

			/**
			 * Validating Country
			 */
			$url = Config::get("config.country_url");
			$country = RemoteCall::getSpecific($url . $request['country_id']);
			if ($country["status"] != "200") {
				return response()->json([
					"status" => $country["status"],
					"message" => $country["message"]
				], $country["status"]);
			}
			if ($request['timezone'] == null) {
				$request['timezone'] = $country['message']['data']['timezone'];
			}

			/**
			 * Validating Language translation
			 */
			$languageCheck = $this->checkLanguageCode($country['message']['data']['language'], $request["translation"]);
			switch ($languageCheck) {

				case null:
					{
						$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
							"status" => '422',
							"message" => ["translation" => "Input Translation does not match with specified country language code"]
						]));
						return response()->json([
							"status" => "422",
							"message" => ["translation" => "Input Translation does not match with specified country language code"]
						], 422);
						break;

					}
				case is_array($languageCheck) :
					{

						return response()->json([
							"status" => $languageCheck["status"],
							"message" => $languageCheck["message"]
						], $languageCheck["status"]);
						break;
					}
			}

			$districtCheck = $this->checkDistrict($request['city_id'], $request['delivery_zones']);
			switch ($districtCheck) {

				case false:
					{
						return response()->json([
							"status" => "422",
							"message" => "Input Districts does not match with Available districts of City"
						], 422);
						break;

					}
				case is_array($districtCheck) :
					{
						return response()->json([
							"status" => $districtCheck["status"],
							"message" => $districtCheck["message"]
						], $districtCheck["status"]);
						break;
					}

				case true:
					{
						$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
							"status" => "200",
							"message" => "Valid Translation"
						]));
						break;
					}
			}

			$request = $request->all();
			//return response()->json($request);

			/**
			 * creating restaurant branch
			 */

			DB::beginTransaction();
			/**
			 * check if shipping method array contains duplicate values
			 */
			$checkShippingMethod = $this->array_has_dupes($request["shipping_methods"]);
			if (is_null($checkShippingMethod)) {
				$this->logStoreHelper->storeLogError(array("RestaurantBranch", [
					"status" => "422",
					"message" => ["shipping_methods" => ["shipping methods array contains empty string"]]
				]));
				return response()->json([
					"status" => "422",
					"message" => ["shipping_methods" => ["shipping methods array contains empty string"]]
				], 422);
			} else if ($checkShippingMethod) {
				$this->logStoreHelper->storeLogError(array("RestaurantBranch", [
					"status" => "422",
					"message" => ["shipping_methods" => ["shipping methods array contains duplicate value"]]
				]));
				return response()->json([
					"status" => "422",
					"message" => ["shipping_methods" => ["shipping methods array contains duplicate value"]]
				], 422);
			}

			/**
			 * check if payment method array contains duplicate values
			 */
			$checkPaymentMethod = $this->array_has_dupes($request["payment_methods"]);
			if (is_null($checkPaymentMethod)) {
				$this->logStoreHelper->storeLogError(array("RestaurantBranch", [
					"status" => "422",
					"message" => ["payment_methods" => ["payment methods value contains empty string"]]
				]));
				return response()->json([
					"status" => "422",
					"message" => ["payment_methods" => ["payment methods value contains empty string"]]
				], 422);
			} else if ($checkPaymentMethod) {
				$this->logStoreHelper->storeLogError(array("RestaurantBranch", [
					"status" => "422",
					"message" => ["payment_methods" => ["payment methods array contains duplicate value"]]
				]));
				return response()->json([
					"status" => "422",
					"message" => ["payment_methods" => ["payment methods array contains duplicate value"]]
				], 422);
			}

			$restaurantBranch = $this->restaurantBranch->createRestaurantBranch($request);
//            if(RoleChecker::hasRole("admin")||RoleChecker::hasRole("super-admin")||RoleChecker::get('parentRole') == "admin"||RoleChecker::get('parentRole') == "super-admin"){
//                $restaurantBranch = $this->restaurantBranch->createRestaurantBranch($request);
//            }
//            else{
//                $request["status"] = 0;
//                $restaurantBranch = $this->restaurantBranch->createRestaurantBranch($request);
//            }

			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				"status" => '200',
				"message" => "Restaurant Branch Created Successfully",
				"data" => $restaurantBranch
			]));
			/******************* restaurant branch translation post starts ************/
			foreach ($request['translation'] as $key => $req) {
				$req['branch_id'] = $restaurantBranch['id'];
				$req['lang_code'] = $key;
				$req['lang_code'] = $this->slugify->slugify($req['lang_code'], ['regexp' => '/([^A-Za-z]|-)+/']);
				if (empty($req['lang_code'])) {
					$this->logStoreHelper->storeLogInfo(array("RestaurantBranchTranslation", [
						"status" => '422',
						"message" => "The slug may not only contain numbers,special characters.",
					]));
					return response()->json([
						"status" => "422",
						"message" => ["name" => "The slug may not only contain numbers,special characters."]
					], 422);
				}
				$rules = [
					"lang_code" => 'required|alpha',
					"name" => "required"
				];
				$validator = Validator::make($req, $rules);
				if ($validator->fails()) {
					$error = $validator->errors();
					$this->logStoreHelper->storeLogError(array('Restaurant Translation', [
						'status' => '422',
						"message" => [$key => $error]
					]));
					return response()->json([
						'status' => '422',
						"message" => [$key => $error]
					], 422);
				}
				$restaurantBranchTranslation = $this->restaurantBranchTranslation->createRestaurantBranchTranslation($req);
				$this->logStoreHelper->storeLogInfo(array("RestaurantBranchTranslation", [
					'status' => '200',
					'message' => 'Restaurant Branch Translation created successfully',
					'data' => $restaurantBranchTranslation
				]));
			}
			/***************** restaurant branch translation ends ***************/

			/**************** food category attach *****************************/
			$this->pivot->deletePivotForRestaurant($restaurantBranch['id']);
			$restaurantCategory = $this->restaurantCategory->attachCategoryToRestaurant($restaurantBranch['id'], $request['food_categories']);

			/**************** food category attach ends ***************************/

			/*************** delivery zone addition starts ******************************/
			/**
			 * deletion of all delivery zones
			 */
			try {
				$deliveryZone = $this->deliveryZone->getAllDeliveryZone($restaurantBranch['id'], null);
				if (count($deliveryZone) != 0) {
					foreach ($deliveryZone as $zone) {
						$deliveryZone = $this->deliveryZone->deleteDeliveryZone($zone->id);
					}
				}
			} catch (\Exception $exception) {
				DB::rollBack();
				return response()->json([
					'status' => '403',
					'message' => 'Delivery Zone could not be updated'
				], 403);
			}
			/**
			 *  createing delivery zones
			 */
			foreach ($request['delivery_zones'] as $req1['district_id']) {
				$req1['branch_id'] = $restaurantBranch['id'];
				try {
					$deliveryZone = $this->deliveryZone->createDeliveryZone($req1);
				} catch (\Exception $e) {
					DB::rollBack();
					return response()->json([
						'status' => '403',
						'message' => 'Requested Delivery Zone could not be created'
					], 403);
				}
			}

			/*************** delivery zone addition ends *******************************/

			/***************** restaurant cuisine create starts *************************/
			foreach ($request["cuisines"] as $cuisine) {
				try {
					$getCuisine = $this->cuisine->viewSpecificCuisineBasedOnStatus($cuisine);
				} catch (ModelNotFoundException $ex) {
					DB::rollBack();
					return response()->json([
						"status" => "404",
						"message" => "Cuisine with id $cuisine could not be found or status is disabled"
					], 404);
				}
				if ($restaurantBranch->country_id != $getCuisine->country_id) {
					DB::rollBack();
					return response()->json([
						"status" => "403",
						"message" => "Cuisine and Restaurant country is different"
					], 403);
				}
			}
			try {
				$restaurantBranch->cuisine()->attach($request["cuisines"]);
			} catch (\Exception $ex) {
				DB::rollBack();
				return response()->json([
					"status" => "409",
					"message" => "Cuisine could not be attached to Restaurant Branch due to duplicate entry"
				], 409);
			}
			/**************** restaurant cuisine create ends ***************************/

			/**********************************restaurant branch payment method starts ****************/
			foreach ($request['payment_methods'] as $paymentMethod) {
				$paymentMethodToCreate["restaurant_branch_id"] = $restaurantBranch['id'];
				$paymentMethodToCreate["slug"] = $paymentMethod;
				$createPaymentMethod = $this->paymentMethod->createPaymentMethod($paymentMethodToCreate);

				$this->logStoreHelper->storeLogInfo(array("Payment method created"), [
					"message" => "Payment Method Created",
					"data" => $createPaymentMethod
				]);
			}
			/***************** restaurant branch payment method ends ***************/

			/**
			 * start of shipping method part
			 */

			foreach ($request['shipping_methods'] as $shippingMethod) {
				$shippingMethodToCreate["restaurant_branch_id"] = $restaurantBranch['id'];
				$shippingMethodToCreate["slug"] = $shippingMethod;
				$createShippingMethod = $this->shippingMethod->createShippingMethod($shippingMethodToCreate);

				$this->logStoreHelper->storeLogInfo(array("Payment method created"), [
					"message" => "Shippping Method Created",
					"data" => $createShippingMethod
				]);
			}

			/**************************** branch user create starts ***************/
			$request['user']['meta']['is_restaurant_admin'] = false;
			$request['user']['meta']['restaurant_id'] = $restaurant_id;
			$request['user']['meta']['branch_id'] = $restaurantBranch['id'];
			$request['user']['role'] = 'branch-admin';
			$user = RemoteCall::store(Config::get('config.oauth_base_url') . "/user/create", $request['user']);
			if ($user['status'] != 200) {
				$this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser", [
					$user['message']
				]));
				return response()->json(
					$user['message'], $user['status']);
			}
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser", [
				'status' => '200',
				'message' => 'User Created Successfully'
			]));


			$notification['country'] = $country["message"]["data"]["name"];
			$notification['id'] = $restaurantBranch['id'];
			$notification['email_address'] = $request['email_address'];
			$notification['phone_no'] = $request['phone_no'];
			$notification['mobile_no'] = $request['mobile_no'];
			$notification['translation'] = $request['translation'];
			$notification['country_id'] = $request['country_id'];
			$notification['parent_id'] = $restaurant_id;


//            $message = ["service" => "restaurant service", "message" => "restaurant branch created", "data" => $notification];
//            event(new EmailEvent($message));

//            $message1 = ["service" => "oauth service", "message" => "user create", "data" => $request['user']];
//            event(new EmailEvent($message1));


			/************************** branch user create ends *******************/
			DB::commit();

			$message = [
				"service" => "restaurant service",
				"message" => "restaurant branch created",
				"data" => [
					"user_details" => [],
					"restaurant" => $notification
				]
			];
			event(new EmailEvent($message));

			return response()->json([
				'status' => '200',
				'message' => 'Restaurant Branch created successfully'
			], 200);



		} catch (QueryException $e) {
			DB::rollBack();
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '403',
				'message' => 'Restaurant Branch could not be updated! Query Build is incorrect',
				// 'data' => $restaurantBranch
			]));
			return response()->json([
				'status' => '403',
				'message' => 'Restaurant Branch could not be updated! Query Build is incorrect'
			], 403);
		} catch (\Exception $e) {
			DB::rollBack();
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Restaurant Branch could not be created'
			]));
			return response()->json([
				'status' => '403',
				'message' => 'Restaurant Branch could not be created'
			], 403);
		}

	}


	/**
	 * @param Request $request
	 * @param $id
	 * @param $branch_id
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function update(Request $request, $restaurant_id, $branch_id)
	{
		try {
			/**
			 *validation for restaurantBranch
			 */
			$message = [
				"check_slug_update" => "The slug has already been taken.",
				"tax_type.in" => "The tax type field value should either be exclusive,inclusive or none."
			];
			$this->validate($request, [
				'country_id' => 'required|integer',
				'city_id' => 'required|integer',
				'district_id' => 'required|integer',
				"tax_type" => "required|in:exclusive,inclusive,none",
				'logo' => 'sometimes',
				'cover_image' => 'sometimes',
				'phone_no' => 'required|min:7',
				'mobile_no' => 'required|min:7',
				'email_address' => 'required|email|unique:restaurant_branches,email_address,' . $branch_id,
				'commission_rate' => 'required|numeric|between:0,99.99',
				'slug' => 'required|check_slug_update:' . $branch_id,
				'min_purchase_amt' => 'required|integer',
				'max_purchase_amt' => 'required|integer',
				'no_of_seats' => 'required|integer',
				'machine_ref_key' => 'required',
				'freelance_drive_supported' => 'integer|min:0|max:1',
				'is_featured' => 'integer|min:0|max:1',
				'pre_order' => 'integer|min:0|max:1',
				'translation' => 'required|array',
				'shipping_methods' => 'required|array',
				'payment_methods' => 'required|array',
				'logistics_fee' =>'required|numeric|min:0',
				'delivery_fee' =>'required|numeric|min:0',
				'latitude' => 'required|numeric',
				'longitude' => 'required|numeric',
				"tin" => "sometimes|string",
				"cc_commision_rate" => "numeric|min:0|max:100",
				'status' => 'integer|min:0|max:1',
				"food_categories" => "required|array",
				'delivery_zones' => 'required|array',
				"shift" => "sometimes|integer|min:1|max:2",
				"free_delivery" => "sometimes|integer|min:0|max:1",
				"additional_contact" => "sometimes|array",
				"additional_contact.*" => "present|min:7"
			], $message);
		} catch (\Exception $e) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				"status" => '422',
				"message" => $e->response->original
			]));
			return response()->json([
				'status' => '422',
				'message' => $e->response->original
			], 422);
		}
		/**
		 *validation for restaurant_id
		 */
		try {
			$restaurant = $this->restaurant->getSpecificRestaurantCheckForBranch($restaurant_id, 1);
//            $restaurantTranslation = $this->restaurantTranslation->getSpecificRestaurantTranslationByCodeAndId('en', $restaurant_id);
		} catch (\Exception $e) {
			return response()->json([
				'status' => '404',
				'message' => 'Requested Restaurant branch could not be found or Restaurant status is disabled'], 404);
		}
		/**
		 *validation for branch_id
		 */
		try {
			$restaurantBranch = $getSpecificRestaurantDetail = $this->restaurantBranch->getSpecificRestaurantBranch($branch_id);
		} catch (\Exception $e) {
			return response()->json([
				'status' => '404',
				'message' => 'Requested Restaurant branch could not be found'], 404);
		}
		try {
			if ($restaurantBranch['parent_id'] != $restaurant_id) {
				throw new \Exception();
			}
		} catch (\Exception $e) {
			return response()->json([
				'status' => '404',
				'message' => 'Requested Branch is not found in requested restaurant'], 404);
		}
		/**
		 * checking roles potential
		 */
		if (!($this->checkRolePotentialForUpdate($restaurant_id, $branch_id))) {
			return response()->json([
				'status' => '401',
				'message' => 'Unauthorized'], 401);
		}

		/**
		 * Modification of slug accourding to restaurant name
		 */
		$request['slug'] = $this->slugify->slugify($request['slug'], ['regexp' => '/([^A-Za-z]|-)+/']);
		if (empty($request['slug'])) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				"status" => '422',
				"message" => "The slug may not only contain numbers,special characters."
			]));
			return response()->json([
				"status" => "422",
				"message" => ["name" => "The slug may not only contain numbers,special characters."]
			], 422);
		}
//        $request['slug'] = $restaurantTranslation[0]['name'] . '-' . $request['slug'];
		/**
		 * validation for additional_contact
		 */
		$additional_contacts = [];
		if ($request->has('additional_contact')) {
			foreach ($request['additional_contact'] as $contact) {
				if (is_numeric($contact)) $additional_contacts[] = $contact;
			}
		}
		$request['additional_contact'] = $additional_contacts;


		try {
			$dataValidation = $this->restaurantCategory->getDataForCategoryValidation($request['country_id']);
			$this->validate($request, [
				"food_categories" => "required|array|in:" . implode($dataValidation['categories'], ","),
//                'slug' => 'required|unique:restaurant_branches,slug,' . $branch_id
			]);
		} catch (\Exception $e) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				"status" => '422',
				"message" => $e->response->original
			]));
			return response()->json([
				'status' => '422',
				'message' => $e->response->original
			], 422);
		}

		$request['status'] = Input::get('status', 1);//1 means active
		/**
		 * validation of translation field
		 */
		foreach ($request['translation'] as $key => $req) {
			$validators = Validator::make($req, [
				'name' => 'required',
				"address" => "required"
			]);
			if ($validators->fails()) {
				$this->logStoreHelper->storeLogError(array('Error', [
					'status' => '422',
					'message' => ["translation" => ["$key" => $validators->errors()]]
				]));
				return response()->json([
					'status' => '422',
					'message' => ["translation" => ["$key" => $validators->errors()]]
				], 422);
			}
		}
		$request->merge(["parent_id" => $restaurant_id]);
		$request->merge(["country_id" => $restaurant->country_id]);
		/**
		 * validating logo image
		 */

		if ($request->has('logo') && (strpos($request->logo, "data:image/") !== false)) {
			$image['avatar'] = $request["logo"];
			$image["type"] = "logo";
			$imageUpload = ImageUploader::upload($image);
			if ($imageUpload["status"] != 200) {
				return response()->json(
					$imageUpload["message"]
					, $imageUpload["status"]);
			}
			$request->merge(["logo" => $imageUpload["message"]["data"]]);
			$logo = true;
		} else {
			$logo = false;
		}

		/**
		 * validating cover_image
		 */
		if ($request->has('cover_image') && (strpos($request->cover_image, "data:image/") !== false)) {
			$image['avatar'] = $request["cover_image"];
			$image["type"] = "cover_image";
			$imageUpload = ImageUploader::upload($image);
			if ($imageUpload["status"] != 200) {
				return response()->json(
					$imageUpload["message"]
					, $imageUpload["status"]);
			}
			$request->merge(["cover_image" => $imageUpload["message"]["data"]]);
			$coverImage = true;
//            $request['cover_image'] = ;
		} else {
			$coverImage = false;
		}


		/**
		 * Validating Language translation
		 */
		$url = config::get("config.country_url");
		$country = RemoteCall::getSpecific($url . $request['country_id']);
		if ($country["status"] != "200") {
			return response()->json([
				"status" => $country["status"],
				"message" => $country["message"]
			], $country["status"]);
		}
		$languageCheck = $this->checkLanguageCode($country['message']['data']['language'], $request["translation"]);
		switch ($languageCheck) {

			case null:
				return response()->json([
					"status" => "422",
					"message" => "Input Translation does not match with specified country language code"
				], 422);
				break;
			case is_array($languageCheck) :

				return response()->json([
					"status" => $languageCheck["status"],
					"message" => $languageCheck["message"]
				], $languageCheck["status"]);
				break;
			default:
				$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
					"status" => "200",
					"message" => "Valid Translation"
				]));
				break;

		}

		$districtCheck = $this->checkDistrict($request['city_id'], $request['delivery_zones']);
		switch ($districtCheck) {

			case false:
				{
					return response()->json([
						"status" => "422",
						"message" => "Input Districts does not match with Available districts of City"
					], 422);
					break;

				}
			case is_array($districtCheck) :
				{
					return response()->json([
						"status" => $districtCheck["status"],
						"message" => $districtCheck["message"]
					], $districtCheck["status"]);
					break;
				}

			case true:
				{
					$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
						"status" => "200",
						"message" => "Valid Translation"
					]));
					break;
				}
		}
		if ($logo == false && $coverImage == false) {
			$request = $request->except(["cover_image", "logo"]);
		} elseif ($logo == false && $coverImage) {
			$request = $request->except("logo");
		} elseif ($logo && $coverImage == false) {
			$request = $request->except("cover_image");
		} else {
			$request = $request->all();
		}

		/**
		 * Updating Restaurant Branch
		 */
		DB::beginTransaction();
		try {
			/**
			 * check if shipping method array contains duplicate values
			 */
			$checkShippingMethod = $this->array_has_dupes($request["shipping_methods"]);
			if (is_null($checkShippingMethod)) {
				$this->logStoreHelper->storeLogError(array("RestaurantBranch", [
					"status" => "422",
					"message" => ["shipping_methods" => ["shipping methods array contains empty string"]]
				]));
				return response()->json([
					"status" => "422",
					"message" => ["shipping_methods" => ["shipping methods array contains empty string"]]
				], 422);
			} else if ($checkShippingMethod) {
				$this->logStoreHelper->storeLogError(array("RestaurantBranch", [
					"status" => "422",
					"message" => ["shipping_methods" => ["shipping methods array contains duplicate value"]]
				]));
				return response()->json([
					"status" => "422",
					"message" => ["shipping_methods" => ["shipping methods array contains duplicate value"]]
				], 422);
			}

			/**
			 * check if payment method array contains duplicate values
			 */
			$checkPaymentMethod = $this->array_has_dupes($request["payment_methods"]);
			if (is_null($checkPaymentMethod)) {
				$this->logStoreHelper->storeLogError(array("RestaurantBranch", [
					"status" => "422",
					"message" => ["payment_methods" => ["payment methods value contains empty string"]]
				]));
				return response()->json([
					"status" => "422",
					"message" => ["payment_methods" => ["payment methods value contains empty string"]]
				], 422);
			} else if ($checkPaymentMethod) {
				$this->logStoreHelper->storeLogError(array("RestaurantBranch", [
					"status" => "422",
					"message" => ["payment_methods" => ["payment methods array contains duplicate value"]]
				]));
				return response()->json([
					"status" => "422",
					"message" => ["payment_methods" => ["payment methods array contains duplicate value"]]
				], 422);
			}
			$restaurantBranch = $this->restaurantBranch->updateRestaurantBranch($branch_id, $request);

			/******************* restaurant branch translation update starts ************/
			/******************* deletion of branch translation *****************/
			$branchTranslations = $this->restaurantBranchTranslation->getAllRestaurantBranchTranslation($branch_id);

			foreach ($branchTranslations as $branchTranslation) {
				$branchTranslation = $this->restaurantBranchTranslation->deleteRestaurantBranchTranslation($branchTranslation->id);
			}
			/******************* creating branch translation ***********************/
			foreach ($request['translation'] as $key => $req) {
				$req['branch_id'] = $restaurantBranch['id'];
				$req['lang_code'] = $key;
				$req['lang_code'] = $this->slugify->slugify($req['lang_code'], ['regexp' => '/([^A-Za-z]|-)+/']);
				if (empty($req['lang_code'])) {
					$this->logStoreHelper->storeLogInfo(array("RestaurantBranchTranslation", [
						"status" => '422',
						"message" => "The slug may not only contain numbers,special characters.",
					]));
					return response()->json([
						"status" => "422",
						"message" => ["name" => "The slug may not only contain numbers,special characters."]
					], 422);
				}
				$restaurantBranchTranslation = $this->restaurantBranchTranslation->createRestaurantBranchTranslation($req);
				$this->logStoreHelper->storeLogInfo(array("RestaurantBranchTranslation", [
					'status' => '200',
					'message' => 'Restaruant Branch Translation updated successfully',
					'data' => $restaurantBranchTranslation
				]));
			}
			/***************** restaurant branch translation update ends ***************/

			/**************** food category attach *****************************/
			$this->pivot->deletePivotForRestaurant($restaurantBranch['id']);
			$restaurantCategory = $this->restaurantCategory->attachCategoryToRestaurant($restaurantBranch['id'], $request['food_categories']);
			/**************** food category attach ends ***************************/

			/*************** delivery zone addition starts ******************************/
			/**
			 * deletion of all delivery zones
			 */
			try {
				$deliveryZone = $this->deliveryZone->getAllDeliveryZone($restaurantBranch['id'], null);
				if (count($deliveryZone) != 0) {
					foreach ($deliveryZone as $zone) {
						$deliveryZone = $this->deliveryZone->deleteDeliveryZone($zone->id);
					}
				}
			} catch (\Exception $exception) {
				DB::rollBack();
				return response()->json([
					'status' => '403',
					'message' => 'Delivery Zone could not be updated'
				], 403);
			}
			/**
			 *  createing delivery zones
			 */
			foreach ($request['delivery_zones'] as $req1['district_id']) {
				$req1['branch_id'] = $restaurantBranch['id'];
				try {
					$deliveryZone = $this->deliveryZone->createDeliveryZone($req1);
				} catch (\Exception $e) {
					DB::rollBack();
					return response()->json([
						'status' => '403',
						'message' => 'Requested Delivery Zone could not be created'
					], 403);
				}
			}

			/*************** delivery zone addition ends *******************************/


			/**********************************restaurant branch payment method starts ****************/
			$this->paymentMethod->deletePaymentMethodByrestaurantBranchId($branch_id);
			foreach ($request['payment_methods'] as $paymentMethod) {
				$paymentMethodToCreate["restaurant_branch_id"] = $restaurantBranch['id'];
				$paymentMethodToCreate["slug"] = $paymentMethod;
				$createPaymentMethod = $this->paymentMethod->createPaymentMethod($paymentMethodToCreate);

				$this->logStoreHelper->storeLogInfo(array("Payment method created"), [
					"message" => "Payment Method Created",
					"data" => $createPaymentMethod
				]);
			}
			/***************** restaurant branch payment method ends ***************/

			/**
			 * start of shipping method part
			 */
			$this->shippingMethod->deleteShippingMethodByBranchId($branch_id);//deletes the shipping method based on branch id
			foreach ($request['shipping_methods'] as $shippingMethod) {
				$shippingMethodToCreate["restaurant_branch_id"] = $restaurantBranch['id'];
				$shippingMethodToCreate["slug"] = $shippingMethod;
				$createShippingMethod = $this->shippingMethod->createShippingMethod($shippingMethodToCreate);

				$this->logStoreHelper->storeLogInfo(array("Payment method created"), [
					"message" => "Shippping Method Created",
					"data" => $createShippingMethod
				]);
			}
			/***************** restaurant branch shipping method ends ***************/

			/***************** restaurant cuisine create starts *************************/
			$cuisines = $restaurantBranch->cuisine()->get();
			foreach ($cuisines as $cuisine) {
				$restaurantBranch->cuisine()->detach($cuisine['id']);
			}

			foreach ($request["cuisines"] as $cuisine) {
				try {
					$getCuisine = $this->cuisine->viewSpecificCuisineBasedOnStatus($cuisine);
				} catch (ModelNotFoundException $ex) {
					DB::rollBack();
					return response()->json([
						"status" => "404",
						"message" => "Cuisine with id $cuisine could not be found or status is disabled"
					], 404);
				}
				if ($restaurantBranch->country_id != $getCuisine->country_id) {
					DB::rollBack();
					return response()->json([
						"status" => "403",
						"message" => "Cuisine and Restaurant country is different"
					], 403);
				}
			}
			try {
				$restaurantBranch->cuisine()->attach($request["cuisines"]);
				CuisineBranchSorting::whereNotIn('cuisine_id',$request['cuisines'])->where('branch_id',$branch_id)->delete();
//                return $restaurantBranch->cuisine()->get();

			} catch (\Exception $ex) {
				DB::rollBack();
				return response()->json([
					"status" => "409",
					"message" => "Cuisine could not be attached to Restaurant Branch due to duplicate entry"
				], 409);
			}
			/**************** restaurant cuisine create ends ***************************/
			if ($getSpecificRestaurantDetail->status != $request['status']) {
				$notification['country'] = $country["message"]["data"]["name"];
				$notification['id'] = $restaurantBranch['id'];
				$notification['email_address'] = $request['email_address'];
				$notification['phone_no'] = $request['phone_no'];
				$notification['mobile_no'] = $request['mobile_no'];
				$notification['translation'] = $request['translation'];
				$notification['country_id'] = $request['country_id'];
				$notification['status'] = $request['status'];

//            $message = ["service" => "restaurant service", "message" => "restaurant branch updated", "data" => $notification];
//            event(new EmailEvent($message));


				$message = [
					"service" => "restaurant service",
					"message" => "restaurant branch updated",
					"data" => [
						"user_details" => [],
						"restaurant" => $notification
					]
				];
				event(new EmailEvent($message));
			}
			DB::commit();
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				"status" => '200',
				"message" => "Restaurant Branch updated Successfully",
				"data" => $restaurantBranch,
				'request' => $request
			]));
			return response()->json([
				'status' => '200',
				'message' => 'Restaurant Branch updated successfully'
			], 200);
		} catch (QueryException $e) {
			DB::rollBack();
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '403',
				'message' => 'Restaurant Branch could not be updated! Query Build is incorrect',
				'data' => $restaurantBranch
			]));
			return response()->json([
				'status' => '403',
				'message' => 'Restaurant Branch could not be updated! Query Build is incorrect'
			], 403);
		} catch (\Exception $e) {
			DB::rollBack();
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '403',
				'message' => 'Restaurant Branch could not be updated',
				'data' => $restaurantBranch
			]));
			return response()->json([
				'status' => '403',
				'message' => 'Restaurant Branch could not be updated'
			], 403);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @param $branch_id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($restaurant_id, $branch_id)
	{
		/**
		 *validation for restaurant_id
		 */
		try {
			$restaurant = $this->restaurant->getSpecificRestaurant($restaurant_id, null);
		} catch (\Exception $e) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Requested Restaurant branch could not be found',
			]));
			return response()->json([
				'status' => '404',
				'message' => 'Requested Restaurant branch could not be found'], 404);
		}
		/**
		 *validation for branch_id
		 */
		try {
			$restaurantBranch = $this->restaurantBranch->getSpecificSoftDeletedOrNotSoftDeletedRestaurantBranch($branch_id, null);
			$translation = $restaurantBranch->branchTranslation()->where('lang_code', 'en')->first();


		} catch (\Exception $e) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Requested Restaurant branch could not be found',
			]));
			return response()->json([
				'status' => '404',
				'message' => 'Requested Restaurant branch could not be found'], 404);
		}

		try {
			if ($restaurantBranch['parent_id'] != $restaurant_id) {
				throw new \Exception();
			}
		} catch (\Exception $e) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Requested Branch is not found in requested restaurant',
			]));
			return response()->json([
				'status' => '404',
				'message' => 'Requested Branch is not found in requested restaurant'], 404);
		}
		/**
		 * checking roles potential
		 */
		if (!($this->checkRolePotentialForCreateOrDelete($restaurant_id))) {
			$this->logStoreHelper->storeLogError(array("unauthorized", [
				'status' => '401',
				'message' => 'Unauthorized'
			]));
			return response()->json([
				'status' => '401',
				'message' => 'Unauthorized'], 401);
		}
		DB::beginTransaction();

		if (is_null($restaurantBranch->deleted_at)) {
			/**
			 * this deleteALL method will use soft delete all the child and parent row if the soft delete features
			 * is used and set all the restaurant_user to status 0 based on the specified restaurant id
			 */
			try {
				$branch = $this->restaurantBranch->deleteRestaurantBranch($branch_id);


			} catch (\Exception $ex) {
				DB::rollBack();
				$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
					'status' => '404',
					'message' => 'Requested Restaurant Branch could not be deleted',
				]));
				return response()->json([
					'status' => '404',
					'message' => "Requested Restaurant Branch could not be deleted"
				], 404);
			}

			$request["method"] = "soft";
		} else {
			/**
			 * hard delete
			 * this will delete all its child rows as well its parent row
			 */
			try {
				$restaurantBranch->forceDelete();
			} catch (\Exception $ex) {
				DB::rollBack();
				$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
					'status' => '404',
					'message' => 'Requested Restaurant Branch could not be deleted',
				]));
				return response()->json([
					'status' => '404',
					'message' => "Requested Restaurant Branch could not be deleted"
				], 404);
			}

			$request["method"] = "hard";

		}
		$request['is_restaurant_admin'] = false;
		$request['restaurant_id'] = $restaurant_id;
		$request['branch_id'] = $branch_id;
		$user = RemoteCall::delete("$this->oauthBaseUrl/user/delete", $request);
		if ($user['status'] != 200) {
			DB::rollBack();
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", $user['message']));
			return response()->json(
				$user['message'], $user['status']);
		}
		$restaurant = $restaurantBranch->toArray();
		$restaurant['translation']['en'] = $translation->toArray();

		DB::commit();


		//$restaurant['translation']['en'] = $translation[0];


		$message = [
			"service" => "restaurant service",
			"message" => "restaurant branch deleted",
			"data" => [
				"user_details" => [],
				"restaurant" => $restaurant
			]
		];
		event(new EmailEvent($message));
		$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
			'status' => '200',
			'message' => 'Restaurant Branch deleted successfully',
		]));
		return response()->json([
			'status' => '200',
			'message' => "Restaurant Branch deleted successfully"
		], 200);


	}

	public function checkBranchDistrictCityId(Request $request)
	{
		try {
			$this->validate($request, [
				"district_id" => "sometimes|integer",
				"city_id" => "sometimes|integer"
			]);
		} catch (\Exception $ex) {
			return response()->json([
				"status" => "422",
				"message" => $ex->response->original
			], 403);
		}
		try {
			if ($request->has("city_id")) {
				if ($this->restaurantBranch->getCity($request["city_id"])) {
					return response()->json([
						"status" => "200",
						"data" => true
					]);
				} else {
					return response()->json([
						"status" => "200",
						"data" => false
					]);
				}

			} else if ($request->has("district_id")) {
				if ($this->restaurantBranch->getDistrict($request["district_id"])) {
					return response()->json([
						"status" => "200",
						"data" => true
					]);
				} else {
					return response()->json([
						"status" => "200",
						"data" => false
					]);
				}
			} else {
				return response()->json([
					"status" => "422",
					"message" => "city_id or district_id is required"
				], 422);
			}
		} catch (\Exception $ex) {
			return response()->json([
				"status" => "500",
				"message" => "Error in query"
			], 500);
		}
	}

	/**
	 * @param $country_id
	 * @param $languageTranslations
	 * @return bool|void
	 */
	public function checkLanguageCode($inputLanguages, $languageTranslations)
	{

		//return response()->json($locationResponseFromServiceCommunication["message"]["data"]["language"]);
		foreach ($inputLanguages as $language) {
			$languages[] = $language['code'];
		}
		$countLanguage = count($languages);
		foreach ($languageTranslations as $key => $translation) {
			$langlist[] = $key;
		}
		$countIntersect = count($intersectArray = array_intersect($languages, $langlist));
		if ($countLanguage !== $countIntersect) {
			return;
		}
		return true;
	}

	/**
	 * @param $branch_id
	 * @param $user_id
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function checkAuthority($branch_id, $user_id)
	{
		try {
			$restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($branch_id);
		} catch (\Exception $e) {
			return response()->json([
				'status' => '404',
				'message' => 'Requested Restaurant branch could not be found'], 404);
		}
		/*
         * super-admin and admin have access to all the restaurant delivery Zones..
         *
         * if .....   user1 is restaurant-admin with id1
         * id1 has 3 branches branch1, branch2, branch3
         * then restaurant-admin can only access delivery zones in branch1,branch2,branch3 not other branches like: branch4
         *
         * if ..... user1 is branch-admin with id1
         * id1 has one branch branch1
         * the branch-admin can only access delivery zones in branch1 not other
         *
         * other donot have access
         * */
		//admin and super-admin can access for store all the delivery zones..............
		if (RoleChecker::hasRole('admin') || RoleChecker::hasRole('super-admin')) {
			return response()->json([
				'status' => '200',
				'message' => 'true'], 200);
		} // restaurant-admin can view delivery zones under his restaurant branch
		elseif (RoleChecker::hasRole('restaurant-admin')) {

			try {
				$restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranchByStatus($branch_id, 1);
				$restaurant = $this->restaurant->getSpecificRestaurant($restaurantBranch[0]['parent_id'], 1);
				if ($restaurant['user_id'] == $user_id) {
					return response()->json([
						'status' => '200',
						'message' => 'true'], 200);
				}
			} catch (\Exception $ex) {
				return response()->json([
					'status' => '200',
					'message' => 'false'], 200);
			}
		} // branch-admin can access for store delivery zone of his/her branch only.........
		elseif (RoleChecker::hasRole('branch-admin')) {
			try {
				$user = $this->restaurantUser->getSpecificRestaurantUserByUserId($user_id, 1);
				if ($user['branch_id'] == $branch_id) {
					return response()->json([
						'status' => '200',
						'message' => 'true'], 200);
				}
			} catch (\Exception $ex) {
				return response()->json([
					'status' => '200',
					'message' => 'false'], 200);
			}
		}
		// others can't access for store deliveryzone..
		return response()->json([
			'status' => '200',
			'message' => 'false'], 200);
	}

	//kabina suwal [11:08 AM]

	/**
	 * this function is called by food service internally to check the user
	 * if restaurant_admin user id is equal to specified retaurant user_id
	 * or branch_admin user id is equal to specified branch user
	 *
	 * @param $branch_id
	 * @param $user_id
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function checkAuthorityForFood($branch_id, $user_id)
	{
		try {
			$restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($branch_id);
		} catch (\Exception $e) {
			return response()->json([
				'status' => '404',
				'message' => 'Requested Restaurant branch could not be found'], 404);
		}
		/*
         * super-admin and admin have access to all the restaurant delivery Zones..
         *
         * if .....   user1 is restaurant-admin with id1
         * id1 has 3 branches branch1, branch2, branch3
         * then restaurant-admin can only access delivery zones in branch1,branch2,branch3 not other branches like: branch4
         *
         * if ..... user1 is branch-admin with id1
         * id1 has one branch branch1
         * the branch-admin can only access delivery zones in branch1 not other
         *
         * other donot have access
         * */

		try {
			$restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranchByStatus($branch_id, 1);
			$restaurant = $this->restaurant->getSpecificRestaurant($restaurantBranch[0]['parent_id'], 1);
			if ($restaurant['user_id'] == $user_id) {
				return response()->json([
					'status' => '200',
					'message' => 'true'], 200);
			} else {
				$user = $this->restaurantUser->getSpecificRestaurantUserByUserId($user_id, 1);
				if ($user['branch_id'] == $branch_id) {
					return response()->json([
						'status' => '200',
						'message' => 'true'], 200);
				}
			}
		} catch (\Exception $ex) {
			return response()->json([
				'status' => '200',
				'message' => 'false'], 200);
		}


		// others can't access for store deliveryzone..
		return response()->json([
			'status' => '200',
			'message' => 'false'], 200);
	}

	/**
	 * @param $id
	 * @return bool
	 *
	 *  Check Role Authority
	 *
	 * super-admin and admin have access to all the restaurant branch.
	 *
	 * restaurant admin Or User with Parent_role restaurant admin can only create or delete branches of his own branch.
	 *
	 * other donot have access
	 *
	 */
	private function checkRolePotentialForCreateOrDelete($id)
	{
		// restaurant-admin has access to  his restaurant only
		if (RoleChecker::hasRole('restaurant-admin') || (RoleChecker::get('parentRole') == 'restaurant_admin')) {
			try {
				if (RoleChecker::get('is_restaurant_admin') != true) {
					throw new \Exception();
				}
				$restaurant_id = RoleChecker::get('restaurant_id');
				if ($restaurant_id == $id) {
					return true;
				} else {
					throw new \Exception();
				}

			} catch (\Exception $ex) {
				return false;
			}
		} elseif (RoleChecker::hasRole('branch-admin') || (RoleChecker::get('parentRole') == 'branch_admin')) {
			return false;
		}

		// others can access for store deliveryzone..
		return true;
	}

	private function checkRolePotentialForUpdate($restaurant_id, $branch_id)
	{
		// restaurant-admin has access to  his restaurant only
		if ((RoleChecker::hasRole('restaurant-admin') || (RoleChecker::get('parentRole') == 'restaurant_admin'))) {
			try {
				if (RoleChecker::get('is_restaurant_admin') != true) {
					throw new \Exception();
				}
				if ($restaurant_id == RoleChecker::get('restaurant_id')) {
					return true;
				} else {
					throw new \Exception();
				}

			} catch (\Exception $ex) {
				return false;
			}
		} elseif (RoleChecker::hasRole('branch-admin') || (RoleChecker::get('parentRole') == 'branch_admin')) {
			try {
				if (RoleChecker::get('is_restaurant_admin') != false) {
					throw new \Exception();
				}
				if (RoleChecker::get('branch_id') == $branch_id) {
					return true;
				} else {
					throw new \Exception();
				}

			} catch (\Exception $ex) {
				return false;
			}
		}
		return true;
	}


	/**
	 * checks restaurant slug based on country id and slug
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function checkSlug(Request $request)
	{
		try {
			$this->validate($request, [
				"country_id" => "required|integer|min:1",
				"slug" => "required|string"
			]);
		} catch (\Exception $ex) {
			return response()->json([
				"status" => "422",
				"message" => $ex->response->original
			], 422);
		}
		$checkSlug = $this->restaurantBranch->checkRestaurantSlug($request->country_id, $request->slug);
		if ($checkSlug) {
			return response()->json([
				"status" => "200",
				"data" => true
			]);
		} else {
			return response()->json([
				"status" => "200",
				"data" => false
			]);
		}
	}

	/**
	 * checks if the given array contains duplicate value and contains empty string
	 * @param $array
	 * @return bool
	 */
	private function array_has_dupes($array)
	{
		if (in_array("", $array)) {
			return;
		} else {
			return count($array) !== count(array_unique($array));
		}

	}

	public function search(Request $request)
	{
		try {
			$this->validate($request, [
				'city_id' => 'sometimes|integer|min:0',
				'country_id' => 'required|integer|min:1',
				'district_id' => 'sometimes|integer|min:0',
				'shipping' => 'sometimes|alpha-dash',
				'payment' => 'sometimes|alpha-dash',
				'cuisine' => 'sometimes|integer|min:0',
				'limit' => 'sometimes| integer|min:1',
				'lang' => 'sometimes|alpha',
				'sort' => 'sometimes|alpha',
				'is_featured' => 'sometimes|integer|min:0|max:1',
				'token' => 'sometimes|string',
				'latitude' => 'sometimes|numeric',
				'longitude' => 'sometimes|numeric'
			]);
		} catch (\Exception $e) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				"status" => '422',
				"message" => $e->response->original
			]));
			return response()->json([
				'status' => '422',
				'message' => $e->response->original
			], 422);
		}
		$request['restaurants'] = null;
		//check if logged in user is normal customer or restaurant customer . if user is restaurant customer then load only assigned restaurant to that restaurant customer
		if ($request->has('token')) {
			$user = RemoteCall::getSpecificByToken(Config::get('config.oauth_base_url') . "/user/details", $request['token']);
			if ($user['status'] != 200) {
				$this->logStoreHelper->storeLogInfo(array("RestaurantBranchUser", [
					$user['message']
				]));
				return response()->json(
					$user['message'], $user['status']);
			}
			if ($user['message']['data']['roles'][0]['name'] == "restaurant-customer") {
				$restaurants = $this->customerRestaurant->getAllRestaurantByUserId($user['message']['data']['id']);
				$request['restaurants'] = $restaurants->pluck("restaurant_id")->values()->all();
//                dd($request['restaurants']);
			}
		}

		try {
			$request['limit'] = Input::get("limit", 150);
			$request['lang'] = Input::get("lang", 'en');
			$request['city_id'] = Input::get("city_id", 0);
			$request['district_id'] = Input::get("district_id", 0);
			$request['cuisine'] = Input::get("cuisine", 0);
			$request['filter_by'] = Input::get('filter_by', 'all');
			$request['sort'] = Input::get('sort', 'rating');


//        return $request->all();
			$restaurantBranchRepo = $this->restaurantBranch->searchBranch($request->all());
//            return $restaurantBranchRepo;
			$restaurantBranchs = $restaurantBranchRepo['data'];
//            return $restaurantBranchs;
//            return $restaurantBranchRepo;
//            return $restaurantBranchs;
		} catch (\Exception $ex) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Restaurant Branch could not be found'
			]));
			return response()->json([
				'status' => '404',
				"message" => 'Restaurant Branch could not be found'
			], 404);
		}

		/**
		 * Location Service is called for currency
		 */
		if (count($restaurantBranchs) > 0) {
			$results = [];
			$districts['districts'] = ["5"];
			$districts['lang'] = "en";
			$districts['country_id'] = $restaurantBranchs[0]['country_id'];
			$url = Config::get("config.location_url");
//            $url = "https://api.lugmety.com/location/v1";
			$districtNames = RemoteCall::store($url . "/public/district/getnames", $districts);
		}

		$restaurant_lant_lang = '';
		$count = 0;
		foreach ($restaurantBranchs as $restaurantBranch) {

			$restaurantBranchTranslation = $restaurantBranch->branchTranslation()->where('lang_code', $request['lang'])->first();
			if ($restaurantBranchTranslation == null) {
				$restaurantBranchTranslation = $restaurantBranch->branchTranslation()->where('lang_code', "en")->first();
			}
			if ($restaurantBranchTranslation != null) {
				$restaurantBranch['lang_code'] = $restaurantBranchTranslation['lang_code'];
				$restaurantBranch['name'] = $restaurantBranchTranslation['name'];
			}

			$reviews = $restaurantBranch->restaurantReviews()->where("review_status",'approved')->get();
			$restaurantBranch['average_rating'] = round($reviews->avg("rating"), 1, PHP_ROUND_HALF_DOWN);
			if ($restaurantBranch["average_rating"] == null) {
				$restaurantBranch["average_rating"] = 0;
			}
			$restaurantBranch['review_count'] = $reviews->count();

//            $reviews = $restaurantBranch->restaurantReviews();
//            if ($restaurantBranch["average_rating"] == null) {
//                $restaurantBranch["average_rating"] = 0;
//            }
//            $restaurantBranch['average_rating'] = round($restaurantBranch['average_rating'],1,PHP_ROUND_HALF_DOWN);
//
//            $restaurantBranch['review_count'] = $reviews->count();
			try {
				$restaurantBranch->makeHidden(["commission_rate", "email_address", "machine_ref_key", "is_approved"]);
				$restaurantBranch['restaurant_open'] = $this->restaurantCheckerFacade->checkOpenRestaurant($restaurantBranch->id);
			} catch (\Exception $ex) {
				$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
					'status' => '404',
					'message' => 'Restaurant Branch could not be found'
				]));
				return response()->json([
					'status' => '404',
					"message" => 'Restaurant Branch could not be found'
				], 404);
			}
			if (isset($districtNames['message']['data']['country'])) {
				$restaurantBranch['currency_code'] = $districtNames['message']['data']['country']['currency_code'];
				$restaurantBranch['currency_symbol'] = $districtNames['message']['data']['country']['currency_symbol'];
				$restaurantBranch['min_purchase_amt_currency'] = $restaurantBranch['currency_symbol'] . $restaurantBranch['min_purchase_amt'];
				$restaurantBranch['max_purchase_amt_currency'] = $restaurantBranch['currency_symbol'] . $restaurantBranch['max_purchase_amt'];
			}

			try {
				$cuisines = $restaurantBranch->cuisine()->get();
				if ($cuisines->count() == 0) {
					throw new \Exception();
				}
				$cusinesinArray = [];
				foreach ($cuisines as $cuisine) {
					$translation = $cuisine->cuisineTranslations()->where('lang_code', $request['lang'])->get();

					try {
						if ($translation->count() == 0) {
							if ($request['lang'] == 'en') {
								throw new \Exception();
							} else {
								$translation = $cuisine->cuisineTranslations()->where('lang_code', 'en')->get();
								if ($translation->count() == 0) {
									throw new \Exception();
								}
							}
						}
						$cusinesinArray[] = $translation[0]["name"];
//                        $cuisine["lang_code"] = $translation[0]["lang_code"];
					} catch (\Exception $ex) {
						return response()->json([
							"status" => "404",
							"message" => "Default language english not found for cuisine translation"
						], 404);
					}
				}
			} catch (\Exception $ex) {
				$cusinesinArray = [];
			}
			$restaurantBranch["cuisines"] = $cusinesinArray;
			$restaurantBranch['shipping_methods'] = $restaurantBranch->shippingMethod()->get();
			$restaurantBranch['payment_methods'] = $restaurantBranch->paymentMethod()->get();

			if ($restaurantBranch['cover_image'] == "") {
				$data = \Settings::getSettings('cover-image');
				if ($data["status"] != 200) {
					return response()->json(
						$data["message"]
						, $data["status"]);
				}
				$coverImage = \Redis::get('cover-image');
				$restaurantBranch['cover_image'] = $coverImage;
			}


		}




//        $country['country_id'] = $request['country_id'];
//
//        $url = Config::get("config.location_url");
//        //$currencys = RemoteCall::store("localhost:9000" . "/public/country/currency/list", $country);
//        $currencys = RemoteCall::store($url . "/public/country/currency/list", $country);
//        //return $currencys;
//        if (isset($currencys['message']['data']['country'])) {
//            $currency = $currencys['message']['data']['country'];
//            foreach ($restaurantBranchs as $key => $restaurantBranch) {
//                $restaurantBranch['min_purchase_amt_currency'] = $currency['currency_symbol'] . $restaurantBranch['min_purchase_amt'];
//                $restaurantBranch['max_purchase_amt_currency'] = $currency['currency_symbol'] . $restaurantBranch['max_purchase_amt'];
//                $restaurantBranch['currency_symbol'] = $currency['currency_symbol'];
//                $restaurantBranch['currency_code'] = $currency['currency_code'];
//            }
//        }
//        $restaurantBranchs = collect($restaurantBranchs);
//        if ($request['filter_by'] == "pre_order") {
//            $restaurantBranchs = $restaurantBranchs->where("pre_order", "=","1");
//        }
//
////        return $restaurantBranchs;
		if($request->has("sort") && ($request["sort"]) =='rating'){
			$restaurantBranchs = collect($restaurantBranchs)->sortByDesc("average_rating")->values()->all();
		}
		if(\Redis::exists("restaurant-branch-sorting-country-$request->country_id-city-$request->city_id")){
			$order = unserialize(Redis::get("restaurant-branch-sorting-country-$request->country_id-city-$request->city_id"));
			if(empty($order)){
				$fetch_sorted_restaurant_sort = $this->branch_sorting->getSortedBranch($request->country_id,$request->city_id);
				$order = $branches_ids = collect($fetch_sorted_restaurant_sort)->pluck('branch_id')->values()->all();
				if(\Redis::exists("restaurant-branch-sorting-country-$request->country_id-city-$request->city_id")){
					\Redis::del("restaurant-branch-sorting-country-$request->country_id-city-$request->city_id");
					\Redis::set("restaurant-branch-sorting-country-$request->country_id-city-$request->city_id",serialize($branches_ids));

				}
				else{
					\Redis::set("restaurant-branch-sorting-country-$request->country_id-city-$request->city_id",serialize($branches_ids));
				}
			}
			$restaurantBranchs = collect($restaurantBranchs)->where("lang_code", $request['lang'])->values()->all();
			$restaurantBranchs = collect($restaurantBranchs)->sortBy(function($model) use ($order){
				return  $search = array_search($model->getKey(), $order);
//                $index = ($search)?$search:1000;
//                return $index;
			})->values()->all();
		}else{

			$fetch_sorted_restaurant_sort = $this->branch_sorting->getSortedBranch($request->country_id,$request->city_id);
			$order =  $branches_ids = collect($fetch_sorted_restaurant_sort)->pluck('branch_id')->values()->all();
			if(\Redis::exists("restaurant-branch-sorting-country-$request->country_id-city-$request->city_id")){
				\Redis::del("restaurant-branch-sorting-country-$request->country_id-city-$request->city_id");
				\Redis::set("restaurant-branch-sorting-country-$request->country_id-city-$request->city_id",serialize($branches_ids));

			}
			else{
				\Redis::set("restaurant-branch-sorting-country-$request->country_id-city-$request->city_id",serialize($branches_ids));
			}
			$restaurantBranchs = collect($restaurantBranchs)->where("lang_code", $request['lang'])->values()->all();
			$restaurantBranchs = collect($restaurantBranchs)->sortBy(function($model) use ($order){
				return  $search = array_search($model->getKey(), $order);
//                $index = ($search)?$search:1000;
//                return $index;
			})->values()->all();
		}

		$open = collect($restaurantBranchs)->where("restaurant_open", "=", true)->values()->all();
		$close = collect($restaurantBranchs)->where("restaurant_open", "=", false)->values()->all();
		if($request->has('cuisine') && $request->cuisine != 0){
//            return "cuisine-branch-sorting-'$request->cuisine";
			if(Redis::exists("cuisine-branch-sorting-$request->cuisine")){
				$branch_ids = unserialize(Redis::get("cuisine-branch-sorting-$request->cuisine"));
				if(empty($branch_ids)){
					$getCuisine = $this->cuisine->getSpecificCuisineForAdmin($request->cuisine);


					$getBranchBySorting = $getCuisine->cuisineBranchSorting()->get();

					$branch_ids = $getBranchBySortingBranchId = collect($getBranchBySorting)->sortBy('sort')->pluck('branch_id')->values()->all();
					if(\Redis::exists('cuisine-branch-sorting-'.$request->cuisine)){
						\Redis::del('cuisine-branch-sorting-'.$request->cuisine);
						\Redis::set('cuisine-branch-sorting-'.$request->cuisine,serialize($getBranchBySortingBranchId));

					}
					else{
						\Redis::set('cuisine-branch-sorting-'.$request->cuisine,serialize($getBranchBySortingBranchId));
					}
				}
				$cuisine_restaurants = collect($open)->whereIn('id',$branch_ids)->sort(function($model) use ($branch_ids){
					return $search = array_search($model->getKey(), $branch_ids);
					$index = ($search)?$search:1000;
					Log::info('sort_order',[
						'id' => $model->getKey(),
						'sort' => $search
					]);
					return $index;
				})->take(3)->values()->all();
				$cuisine_restaurants = collect($cuisine_restaurants)->map(function ($cuisine_rest) {
					$cuisine_rest['is_featured_restaurant'] = true;
					return $cuisine_rest;
				});
				$cuisine_rest_ids = collect($cuisine_restaurants)->pluck('id')->values()->all();
				$cuisine_remaining_rest = collect($open)->whereNotIn('id',$cuisine_rest_ids)->values()->all();
				$open = collect($cuisine_restaurants)->merge(collect($cuisine_remaining_rest))->values()->all();
			}
			else{
				$getCuisine = $this->cuisine->getSpecificCuisineForAdmin($request->cuisine);


				$getBranchBySorting = $getCuisine->cuisineBranchSorting()->get();

				$branch_ids = $getBranchBySortingBranchId = collect($getBranchBySorting)->sortBy('sort')->pluck('branch_id')->values()->all();
				if(\Redis::exists('cuisine-branch-sorting-'.$request->cuisine)){
					\Redis::del('cuisine-branch-sorting-'.$request->cuisine);
					\Redis::set('cuisine-branch-sorting-'.$request->cuisine,serialize($getBranchBySortingBranchId));

				}
				else{
					\Redis::set('cuisine-branch-sorting-'.$request->cuisine,serialize($getBranchBySortingBranchId));
				}
				$cuisine_restaurants = collect($open)->whereIn('id',$branch_ids)->sort(function($model) use ($branch_ids){
					return $search = array_search($model->getKey(), $branch_ids);
					$index = ($search)?$search:1000;
					Log::info('sort_order',[
						'id' => $model->getKey(),
						'sort' => $search
					]);
					return $index;
				})->take(3)->values()->all();
				$cuisine_restaurants = collect($cuisine_restaurants)->map(function ($cuisine_rest) {
					$cuisine_rest['is_featured_restaurant'] = true;
					return $cuisine_rest;
				});
				$cuisine_rest_ids = collect($cuisine_restaurants)->pluck('id')->values()->all();
				$cuisine_remaining_rest = collect($open)->whereNotIn('id',$cuisine_rest_ids)->values()->all();
				$open = collect($cuisine_restaurants)->merge(collect($cuisine_remaining_rest))->values()->all();
			}
		}
		else {
			$branch_ids = $this->featureRestaurantSorting->getSortedFeaturedRestaurantBranch($request->all());
			$featured_restaurants = collect($open)->whereIn('id', $branch_ids)->sort(function ($model) use ($branch_ids) {
				return $search = array_search($model->getKey(), $branch_ids);
				$index = ($search) ? $search : 1000;
				Log::info('sort_order', [
					'id' => $model->getKey(),
					'sort' => $search
				]);
				return $index;
			})->values()->all();
			$featured_restaurants = collect($featured_restaurants)->map(function ($featured_rest) {
				$featured_rest['is_featured_restaurant'] = true;
				return $featured_rest;
			});
			$featured_rest_ids = collect($featured_restaurants)->pluck('id')->values()->all();
			$featured_remaining_rest = collect($open)->whereNotIn('id', $featured_rest_ids)->values()->all();
			$open = collect($featured_restaurants)->merge(collect($featured_remaining_rest))->values()->all();
		}

		$close_preorder = collect($close)->where("pre_order", "=", "1")->values()->all();
		$close_non_preorder = collect($close)->where("pre_order", "=", "0")->values()->all();
		$restaurantBranchs = collect($open)->merge(collect($close_preorder))->values()->all();
		$restaurantBranchs = collect($restaurantBranchs)->merge(collect($close_non_preorder))->values()->all();
		if ($request["is_featured"] == 1) {
			$data = \Settings::getSettings('featured-restaurant');
			if ($data["status"] != 200) {
				return response()->json(
					$data["message"]
					, $data["status"]);
			}
			if (!is_array($data['message']['data']['value'])) {
				$value = [$data['message']['data']['value']];
			} else {
				$value = $data['message']['data']['value'];
			}
			$restaurantBranchsFeaturedPriority = collect($restaurantBranchs)->whereIn("id", $value);
			$restaurantBranchsFeaturedNonPriority = collect($restaurantBranchs)->whereNotIn("id", $value);
			$restaurantBranchs = $restaurantBranchsFeaturedPriority->merge($restaurantBranchsFeaturedNonPriority)->all();
//
		}
		if (!array($restaurantBranchs)) {
			$restaurantBranchs = $restaurantBranchs->toArray();
		}

		$restaurantBranchs = $this->manualPagination($request["limit"], $restaurantBranchs, count($restaurantBranchs));
		$google_matrix_response = [];
		$destination = [];
		foreach ($restaurantBranchs as $branch){
			$destination[ $branch['id'] ] = $branch['latitude'] .','. $branch['longitude'];
		}

		if($request->has('latitude') && $request->has('longitude')) {
			$data = \Settings::getSettings('google-coordinate-to-detail-api-key');
			if ($data["status"] != 200) {
				return response()->json(
					$data["message"]
					, $data["status"]);
			}
			$data = \Settings::getSettings('threshold-delivery-time');
			if ($data["status"] != 200) {
				return response()->json(
					$data["message"]
					, $data["status"]);
			}
			$threshold_delivery_time = (int) \Redis::get('threshold-delivery-time');
			$api_key = \Redis::get('google-coordinate-to-detail-api-key');
			$origins = $request->latitude.','.$request->longitude;
			if( count( $destination ) ){
				$destinations = array_chunk( $destination, 25, true );
				foreach( $destinations as $_destination ){

					$_destination_payload = implode( '|', $_destination );
					Log::info('restaurant_search_lat_lang',[
						'destination_payload' => "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=$_destination_payload&destinations=$origins&mode=driving&key=$api_key"
					]);
//return "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=$_destination_payload&destinations=$origins&mode=driving&key=$api_key";
					$_destination_response = $this->googleDistanceMatrix("https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=$_destination_payload&destinations=$origins&mode=driving&key=$api_key");


					$_destination_key_array = [];
					foreach( $_destination as $_dk => $_dv ) {
						$_destination_key_array[ $_dk ] = $_dk;
					}

					$_destination_response = array_combine( $_destination_key_array, $_destination_response );
					$google_matrix_response = array_replace_recursive( $google_matrix_response, $_destination_response );

				}
			}

			$current_time = Carbon::now('utc');
			if(!Redis::exists('restaurants_avg_delivery_time_value') ){
				$restaurantFullUrl = Config::get('config.order_base_url')."/fetch/avg/delivery/time";
				$restaurant_avg_delivery = RemoteCall::getSpecific($restaurantFullUrl);
				if ($restaurant_avg_delivery['status'] != 200) {
					if ($restaurant_avg_delivery['status'] == 503) {
						return response()->json(
							$restaurant_avg_delivery, $restaurant_avg_delivery['status']);
					}

					return response()->json(
						$restaurant_avg_delivery["message"], $restaurant_avg_delivery['status']);
				}
				$restaurant_avg_delivery_time_data = $restaurant_avg_delivery['message']['data']['restaurant_info'];
			}
			else{
				$redis_data = unserialize(Redis::get('restaurants_avg_delivery_time_value'));
				$last_updated_time = Carbon::parse($redis_data['last_updated_time']);
				$time_diff = $current_time->diffInHours($last_updated_time,true);
				if($time_diff > 6){
					$restaurantFullUrl = Config::get('config.order_base_url')."/fetch/avg/delivery/time";
					$restaurant_avg_delivery = RemoteCall::getSpecific($restaurantFullUrl);
					if ($restaurant_avg_delivery['status'] != 200) {
						if ($restaurant_avg_delivery['status'] == 503) {
							return response()->json(
								$restaurant_avg_delivery, $restaurant_avg_delivery['status']);
						}

						return response()->json(
							$restaurant_avg_delivery["message"], $restaurant_avg_delivery['status']);
					}
					$restaurant_avg_delivery_time_data = $restaurant_avg_delivery['message']['data']['restaurant_info'];
				}
				else{
					$restaurant_avg_delivery_time_data = $redis_data['restaurant_info'];
				}

			}
		}
		foreach ($restaurantBranchs as $key => $restaurantBranch) {
			$restaurantBranch['distance'] = '';
			$restaurantBranch['duration'] = '';

			$branch_id = $restaurantBranch['id'];
			if (!empty($google_matrix_response) && $google_matrix_response[  $branch_id ] !== false ) {
				foreach ($restaurant_avg_delivery_time_data as $_restaurant_avg_delivery_time_data){
					if($_restaurant_avg_delivery_time_data['restaurant_id'] == $restaurantBranch['id']){
						$restaurantBranch['distance'] = $google_matrix_response[  $branch_id ]['distance'];
						if(is_null($_restaurant_avg_delivery_time_data['timediff']) || $_restaurant_avg_delivery_time_data['timediff'] == 0 || $_restaurant_avg_delivery_time_data['timediff'] < 0){
							$time = $threshold_delivery_time ;
						}
						else{
							$time = $_restaurant_avg_delivery_time_data['timediff'];
						}
						$max_time = round((((int)$google_matrix_response[ $branch_id ]['duration']/60 + $time)));
						$min_time = $max_time - 10;

						$duration = "$min_time-$max_time" .' mins';
						$restaurantBranch['duration'] = $duration;
					}
				}

				// default duration if historic stats is not available
				if( $restaurantBranch['duration'] == '' ){
					$max_time = round((((int)$google_matrix_response[ $branch_id ]['duration']/60 + 40)));
					$min_time = $max_time - 10;

					$duration = "$min_time-$max_time" .' mins';
					$restaurantBranch['duration'] = $duration;

					$restaurantBranch['distance'] = $google_matrix_response[  $branch_id ]['distance'];
				}
			}

		}

		$parameter = $restaurantBranchRepo['parameter'];
		$restaurantBranchs->withPath("/search/branch?" . $parameter);
		$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
			'status' => '200',
			'data' => $restaurantBranchs
		]));
		return response()->json([
			'status' => '200',
			"data" => $restaurantBranchs
		], 200);

	}

	private function changeToUTC($time, $timezone)
	{

		$date = Carbon::createFromFormat('Y-m-d H:i:s', $time, $timezone);
		$date->setTimezone('UTC');
		$date = $date->toDateTimeString();
		$day = date('l', strtotime($date));
		$time = date('H:m:i', strtotime($date));
		$dateInfo = ["date" => $time, "day" => $day, "full_date" => $date];
		return $dateInfo;
	}

	public function checkDistrict($city_id, $requestedDistricts)
	{
		$base_url = config::get("config.location_url");
		$fullUrl = $base_url . "/public/city/$city_id/district?limit=9999";
		$district = RemoteCall::getSpecific($fullUrl);
		if ($district["status"] != "200") {
			return $district;
		}
		$requested = $requestedDistricts;
		$available = array();
		foreach ($district['message']['data']['data'] as $key => $data) {
			$available[] = $data['id'];
		}
		$intersectArray = array_intersect($available, $requested);
		if (count($requested) == count($intersectArray)) {
			return true;
		} else {
			return false;
		}
	}

	public function timeListOfOpenRestaurantHours($branch_id)
	{
		try {
			$restaurantBranch = $this->restaurantBranch->getSpecificRestaurantBranch($branch_id);
		} catch (ModelNotFoundException $ex) {
			$this->logStoreHelper->storeLogError(array('Restaurant Reservation', [
				'status' => '404',
				"message" => "Restaurant branch could not be found"
			]));
			return response()->json([
				'status' => '404',
				"message" => "Restaurant branch could not be found"
			], 404);
		}
		$today_date = Carbon::now($restaurantBranch['timezone']);
		$today = $today_date->format('l');
		$now_time = $today_date->format("H:i:s");
		$today_date = $today_date->format('Y-m-d');

		$tomorrow_date = Carbon::tomorrow($restaurantBranch['timezone']);
		$tomorrow = $tomorrow_date->format('l');
		$tomorrow_date = $tomorrow_date->format('Y-m-d');

		try {
			$restaurantHoursToday = $this->restaurantHour->getSpecificRestaurantHourByIdRestaurantIdDay($branch_id, $today);
			foreach ($restaurantHoursToday as $restaurantHourToday) {
				$restaurantHourToday['opening_time'] = $this->changeToTimezoneOfBranch($restaurantHourToday['opening_time'], $restaurantBranch['timezone']);
				$restaurantHourToday['closing_time'] = $this->changeToTimezoneOfBranch($restaurantHourToday['closing_time'], $restaurantBranch['timezone']);
				if ($restaurantHourToday['closing_time'] < $restaurantHourToday['opening_time']) {
					if ($restaurantHourToday['closing_time'] < "03:00:00") {
						$restaurantHourToday['closing_time'] = "23:59:59";
					}
				}
				$restaurantHourToday['opening_time'] = $this->convertToNearestFifteenMinuteTime($restaurantHourToday['opening_time']);
			}

			$restaurantHoursTomorrow = $this->restaurantHour->getSpecificRestaurantHourByIdRestaurantIdDay($branch_id, $tomorrow);
			foreach ($restaurantHoursTomorrow as $restaurantHourTomorrow) {
				$restaurantHourTomorrow['opening_time'] = $this->changeToTimezoneOfBranch($restaurantHourTomorrow['opening_time'], $restaurantBranch['timezone']);
				$restaurantHourTomorrow['closing_time'] = $this->changeToTimezoneOfBranch($restaurantHourTomorrow['closing_time'], $restaurantBranch['timezone']);
				if ($restaurantHourTomorrow['closing_time'] < $restaurantHourTomorrow['opening_time']) {
					if ($restaurantHourTomorrow['closing_time'] < "03:00:00") {
						$restaurantHourTomorrow['closing_time'] = "23:59:59";
					}
				}
				$restaurantHourTomorrow['opening_time'] = $this->convertToNearestFifteenMinuteTime($restaurantHourTomorrow['opening_time']);
			}
		} catch (ModelNotFoundException $ex) {
			$this->logStoreHelper->storeLogError(array('Restaurant Reservation', [
				'status' => '404',
				"message" => "Restaurant hour could not be found"
			]));
			return response()->json([
				'status' => '404',
				"message" => "Restaurant hour could not be found"
			], 404);
		}

		$time_list = [];
		$time_list['today']['time'] = [];
		$time_list['today']['label'] = "Today";
		$time_list['today']['date'] = $today_date;
		$time_list['tomorrow']['time'] = [];
		$time_list['tomorrow']['label'] = "Tomorrow";
		$time_list['tomorrow']['date'] = $tomorrow_date;
//        $time_list['today']['time'][]

		$today_day_time = [];
		$today_evening_time = [];
		$open_flag = false;

//        return $restaurantHoursToday;
		foreach ($restaurantHoursToday as $restaurantHour) {
			$time = $restaurantHour['opening_time'];
			$time = $this->increaseTimeByFifteenMinute($time);
			$count = 0;
			if ($restaurantHour['shift'] == "Day" && $restaurantHour["closed"] == 0) {
				if ($restaurantHour['opening_time'] < $now_time && $now_time < $restaurantHour['closing_time']) $open_flag = true;
				$counter_flag = 0;
				while ($restaurantHour['closing_time'] > $time) {
					if ($now_time < $time) {
						$today_day_time[] = $time;
					}
					$prev_time = $time;
					$time = $this->increaseTimeByFifteenMinute($time);
					if ($time < $prev_time) break;

				}
//                return "test";
//                }
			} elseif ($restaurantHour['shift'] == "Evening" && $restaurantHour["closed"] == 0) {
				if ($restaurantHour['opening_time'] < $now_time && $now_time < $restaurantHour['closing_time']) $open_flag = true;
				while ($restaurantHour['closing_time'] > $time) {
					if ($now_time < $time) {
						$today_evening_time[] = $time;
					}
					$prev_time = $time;
					$time = $this->increaseTimeByFifteenMinute($time);
					if ($time < $prev_time) break;
				}
			}
		}

		$today_time = collect($today_day_time)->merge(collect($today_evening_time))->values();
		$today_time = collect($today_time)->sort()->unique()->values();
		if (count($today_time) > 0) {
			if ($open_flag == true) {
				$time_list['today']['time'][] = "ASAP";
			}
			foreach ($today_time as $data) {
				$time_list['today']['time'][] = $this->changeTimeFormat($data);
			}
		}

		$tomorrow_time = [];
		foreach ($restaurantHoursTomorrow as $restaurantHour) {
			$time = $restaurantHour['opening_time'];
			$time = $this->increaseTimeByFifteenMinute($time);
			if ($restaurantHour['shift'] == "Day" && $restaurantHour["closed"] == 0) {
				while ($restaurantHour['closing_time'] > $time) {
					$tomorrow_time[] = $time;
					$prev_time = $time;
					$time = $this->increaseTimeByFifteenMinute($time);
					if ($time < $prev_time) break;
				}
			} elseif ($restaurantHour['shift'] == "Evening" && $restaurantHour["closed"] == 0) {
				while ($restaurantHour['closing_time'] > $time) {
					$tomorrow_time[] = $time;
					$prev_time = $time;
					$time = $this->increaseTimeByFifteenMinute($time);
					if ($time < $prev_time) break;
				}
			}
		}
		$datas = collect($tomorrow_time)->sort()->unique()->values();
		foreach ($datas as $data) {
			$time_list['tomorrow']['time'][] = $this->changeTimeFormat($data);
		}


		$time_list_return = [];
		$next_day = 0;
		if (count($time_list['today']['time']) > 1) {
			$time_list_return[0] = $time_list['today'];
			if (count($time_list['tomorrow']['time']) > 0) {
				$time_list_return[1] = $time_list['tomorrow'];
			}
		} else {
			if (count($time_list['tomorrow']['time']) > 0) {
				$time_list_return[0] = $time_list['tomorrow'];
			} else {
				$next_day = 1;
			}
		}

		if ($next_day == 1) {

			$day3_date = Carbon::now($restaurantBranch['timezone'])->addDays(2);
			$day3 = $day3_date->format('l');
			$day3_date = $day3_date->format('Y-m-d');

			$day4_date = Carbon::now($restaurantBranch['timezone'])->addDays(3);
			$day4 = $day4_date->format('l');
			$day4_date = $day4_date->format('Y-m-d');

			try {
				$restaurantHoursDay3s = $this->restaurantHour->getSpecificRestaurantHourByIdRestaurantIdDay($branch_id, $day3);
				foreach ($restaurantHoursDay3s as $restaurantHourDay3) {
					$restaurantHourDay3['opening_time'] = $this->changeToTimezoneOfBranch($restaurantHourDay3['opening_time'], $restaurantBranch['timezone']);
					$restaurantHourDay3['closing_time'] = $this->changeToTimezoneOfBranch($restaurantHourDay3['closing_time'], $restaurantBranch['timezone']);
					if ($restaurantHourDay3['closing_time'] < $restaurantHourDay3['opening_time']) {
						if ($restaurantHourDay3['closing_time'] < "03:00:00") {
							$restaurantHourDay3['closing_time'] = "23:59:59";
						}
					}
					$restaurantHourDay3['opening_time'] = $this->convertToNearestFifteenMinuteTime($restaurantHourDay3['opening_time']);
				}
				$restaurantHoursDay4s = $this->restaurantHour->getSpecificRestaurantHourByIdRestaurantIdDay($branch_id, $day4);
				foreach ($restaurantHoursDay4s as $restaurantHourDay4) {
					$restaurantHourDay4['opening_time'] = $this->changeToTimezoneOfBranch($restaurantHourDay4['opening_time'], $restaurantBranch['timezone']);
					$restaurantHourDay4['closing_time'] = $this->changeToTimezoneOfBranch($restaurantHourDay4['closing_time'], $restaurantBranch['timezone']);
					if ($restaurantHourDay4['closing_time'] < $restaurantHourDay4['opening_time']) {
						if ($restaurantHourDay4['closing_time'] < "03:00:00") {
							$restaurantHourDay4['closing_time'] = "23:59:59";
						}
					}
					$restaurantHourDay4['opening_time'] = $this->convertToNearestFifteenMinuteTime($restaurantHourDay4['opening_time']);
				}
			} catch (ModelNotFoundException $ex) {
				$this->logStoreHelper->storeLogError(array('Restaurant Reservation', [
					'status' => '404',
					"message" => "Restaurant hour could not be found"
				]));
				return response()->json([
					'status' => '404',
					"message" => "Restaurant hour could not be found"
				], 404);
			}


			$time_list['day3']['time'] = [];
			$time_list['day3']['label'] = $day3;
			$time_list['day3']['date'] = $day3_date;
			$time_list['day4']['time'] = [];
			$time_list['day4']['label'] = $day4;
			$time_list['day4']['date'] = $day4_date;

			$third_day_time = [];
			foreach ($restaurantHoursDay3s as $restaurantHour) {
				$time = $restaurantHour['opening_time'];
				$time = $this->increaseTimeByFifteenMinute($time);
				if ($restaurantHour['shift'] == "Day" && $restaurantHour["closed"] == 0) {
					while ($restaurantHour['closing_time'] > $time) {
						$third_day_time[] = $time;
						$prev_time = $time;
						$time = $this->increaseTimeByFifteenMinute($time);
						if ($time < $prev_time) break;
					}
				} elseif ($restaurantHour['shift'] == "Evening" && $restaurantHour["closed"] == 0) {
					while ($restaurantHour['closing_time'] > $time) {
						$third_day_time[] = $time;
						$prev_time = $time;
						$time = $this->increaseTimeByFifteenMinute($time);
						if ($time < $prev_time) break;
					}
				}

			}
			//$this->changeTimeFormat($time);
			$datas = collect($third_day_time)->sort()->unique()->values();
			foreach ($datas as $data) {
				$time_list['day3']['time'][] = $this->changeTimeFormat($data);
			}


			$day4_time_list = [];
//            $time_list['day4']['time'][]
			foreach ($restaurantHoursDay4s as $restaurantHour) {
				$time = $restaurantHour['opening_time'];
				$time = $this->increaseTimeByFifteenMinute($time);
				if ($restaurantHour['shift'] == "Day" && $restaurantHour["closed"] == 0) {
					while ($restaurantHour['closing_time'] > $time) {
						$day4_time_list[] = $time;
						$prev_time = $time;
						$time = $this->increaseTimeByFifteenMinute($time);
						if ($time < $prev_time) break;
					}
				} elseif ($restaurantHour['shift'] == "Evening" && $restaurantHour["closed"] == 0) {
					while ($restaurantHour['closing_time'] > $time) {
						$day4_time_list[] = $time;
						$prev_time = $time;
						$time = $this->increaseTimeByFifteenMinute($time);
						if ($time < $prev_time) break;
					}
				}
			}
			$datas = collect($day4_time_list)->sort()->unique()->values();
			foreach ($datas as $data) {
				$time_list['day4']['time'][] = $this->changeTimeFormat($data);
			}

			if (count($time_list['day3']['time']) > 1) {
				$time_list_return[0] = $time_list['day3'];
				if (count($time_list['day4']['time']) > 0) {
					$time_list_return[1] = $time_list['day4'];
				}
			} else {
				if (count($time_list['day4']['time']) > 0) {
					$time_list_return[0] = $time_list['day4'];
				}
			}


		}

		return response()->json([
			'status' => '200',
			"data" => $time_list_return
		], 200);

	}


	public function convertToNearestFifteenMinuteTime($time)
	{
		$parsed = date_parse($time);
		if ($parsed['minute'] > 0 && $parsed['minute'] < 15) $parsed['minute'] = 15;
		if ($parsed['minute'] > 15 && $parsed['minute'] < 30) $parsed['minute'] = 30;
		if ($parsed['minute'] > 30 && $parsed['minute'] < 45) $parsed['minute'] = 45;
		if ($parsed['minute'] > 45 && $parsed['minute'] < 60) {
			$parsed['hour']++;
			$parsed['minute'] = 0;
		}
		$time = $parsed['hour'] * 3600 + $parsed['minute'] * 60;
		$date = gmdate('H:i:s', $time);
		return $date;
	}

	public function increaseTimeByFifteenMinute($time)
	{
		$parsed = date_parse($time);
		$time = $parsed['hour'] * 3600 + $parsed['minute'] * 60;
		$time = $time + 900;
		$date = gmdate('H:i:s', $time);
		return $date;
	}

	public function changeToTimezoneOfBranch($time, $timezone)
	{
		$parsed = date_parse($time);
		$now = Carbon::now($timezone);
		$diff = $now->offset;
		$time = $parsed['hour'] * 3600 + $parsed['minute'] * 60;
		$time = $time + $diff;
		$date = gmdate('H:i:s', $time);
		return $date;
		//return date('h:i:s a', strtotime($date));
	}

	public function changeTimeFormat($time)
	{
		$parsed = date_parse($time);
		$now = Carbon::now();
		$time = $parsed['hour'] * 3600 + $parsed['minute'] * 60;
		$date = gmdate('h:i A', $time);
		return $date;
	}

	public function ResExportDetails($branch_id, Request $request)
	{
		//RoleChecker::hasRole("Admin");
		$request['lang'] = Input::get('lang', 'en');

		try {
			$restaurantBranchs = $this->restaurantBranch->getSpecificBranchForExport($branch_id);

			if (count($restaurantBranchs) == 0) {
				throw new \Exception();
			}
		} catch (\Exception $ex) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Requested Restaurant Branch could not be found'
			]));
			return response()->json([
				'status' => '404',
				"message" => 'Requested Restaurant Branch could not be found'
			], 404);
		}

		try {
			$restaurantBranch = $restaurantBranchs;

			$restaurantBranch['shipping_methods'] = $restaurantBranch->shippingMethod()->get();
			$restaurantBranch['payment_methods'] = $restaurantBranch->paymentMethod()->get();
			$reviews = $restaurantBranch->restaurantReviews();
			$restaurantBranch['average_rating'] = round($reviews->avg("rating"), 1, PHP_ROUND_HALF_DOWN);
			if ($restaurantBranch["average_rating"] == null) {
				$restaurantBranch["average_rating"] = 0;
			}
			$restaurantBranch['review_count'] = $reviews->count();

			/**
			 * Translation of branch
			 */
			$translation = $this->restaurantBranchTranslation->getAllRestaurantBranchTranslationByLangCode($restaurantBranch->id, $request['lang']);
			if (count($translation) == 0) {
				$translation = $this->restaurantBranchTranslation->getAllRestaurantBranchTranslationByLangCode($restaurantBranch->id, "en");
			}
			if (count($translation) != 0) {
				$restaurantBranch['lang_code'] = $translation[0]->lang_code;
				$restaurantBranch['name'] = $translation[0]->name;
				$restaurantBranch["address"] = $translation[0]->address;
			}
			/**
			 * Delivery Zone of branch
			 */
			$deliveryZone = $this->deliveryZone->getAllDeliveryZone($restaurantBranch->id, null);
			if (count($deliveryZone) != 0) {
				$restaurantBranch['delivery_zone'] = $deliveryZone->makeHidden(['id', 'branch_id']);
			}
			$restaurantBranch['restaurant_open'] = false;
			/**
			 * Restaurant Hour of branch
			 */

			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '200',
				'message' => $restaurantBranch
			]));

			/**
			 * Check if restaurant is open or closed
			 */
//todo timezone conversion
//            $nepal = Carbon::now('Asia/Kathmandu','H:i');
//            printf("nepal time ".$nepal. "<br>");
//            $utc = $nepal->setTimeZone(new \DateTimeZone('UTC'));
//            echo $utc;


//            $now = Carbon::now();
//            $parsed = date_parse($now);
//            $time = $parsed['hour']*3600+$parsed['minute']*60+$parsed['second'];
//            dd($parsed);
//            $now = Carbon::now('Asia/Kathmandu');
//            $diff = $now->offset;
//            $time = $parsed['hour']*3600+$parsed['minute']*60;
//            $time= $time - $diff;
//            $current = gmdate('H:i', $time);
//            echo  $current;
//
//dd("test");

			return response()->json([
				'status' => '200',
				'data' => $restaurantBranch
			], 200);

		} catch (\Exception $e) {
			$this->logStoreHelper->storeLogInfo(array("RestaurantBranch", [
				'status' => '404',
				'message' => 'Requested Restaurant Branch could not be found'
			]));
			return response()->json([
				'status' => '404',
				'message' => 'Requested Restaurant Branch could not be found'
			], 404);
		}


	}

	private function manualPagination($limit, $data, $count)
	{
		$page = Input::get('page', 1);
		$paginate = $limit;
		$offSet = $limit * ($page - 1);
//        dd($data);
		$itemsForCurrentPage = array_slice($data, $offSet, $paginate, false);
		return new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, $count, $paginate, $page);
	}

	private function googleDistanceMatrix($url){
// Get cURL resource
		$curl = curl_init();
// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $url
		));
// Send the request & save response to $resp
		$resp = curl_exec($curl);
// Close request to clear up some resources
		curl_close($curl);
		$array_result = json_decode($resp, true);

		if (!$resp || empty($array_result) || $array_result['status'] !== 'OK' || !isset($array_result['rows'][0]['elements'])) {
			return false;
		}
		$results = [];
		foreach ($array_result['rows'] as $key => $google_result){
			if(isset($google_result['elements'][0]['status']) && $google_result['elements'][0]['status'] == 'OK'){
				$results[$key] = [
					'distance' => $google_result['elements'][0]['distance']['text'],
					'duration' => $google_result['elements'][0]['duration']['value']
				];
			}
			else{
				$results[$key] = false;
			}
		}


		return $results;

//        foreach ($array_result['rows'][0]['elements'] as $google_result){
//            if($google_result['status'] == 'OK'){
//                $results[] = [
//                    'distance' => $google_result['distance']['text'],
//                    'duration' => $google_result['duration']['value']
//                ];
//            }
//            else{
//                $results[] = false;
//            }
//        }
//        return $results;

	}


}