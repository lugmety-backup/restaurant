<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 7/7/17
 * Time: 1:05 PM
 */

namespace App\Http;

use RemoteCall;
use Illuminate\Support\Facades\Config;
class LocationServiceFacade
{
    /**
     * @param $country_id
     * @param $languageTranslations
     * @return array
     */
    public function checkLanguageCode($country_id, $languageTranslations){
        $url=Config::get("config.country_url");
        /**
         * Calls location service to get language translation based on $country_id
         * @param $url
         * @param $country_id
         * @return \Illuminate\Http\Response
         */
        $locationResponseFromServiceCommunication=RemoteCall::getSpecific($url.$country_id);
        if($locationResponseFromServiceCommunication["status"]!="200"){
            $data = [
                "status" => $locationResponseFromServiceCommunication["status"],
                "message" => $locationResponseFromServiceCommunication["message"]->message,

            ];
            return $data;
        }

        foreach ($locationResponseFromServiceCommunication["message"]["data"]["language"] as $language){
            $languages[] = $language['code'];
        }
        $countLanguage=count($languages);
        foreach ($languageTranslations as $key => $translation) {
            $langlist[] = $key;
        }
        /*
         * if input lang_code count is not equal to obtained country lang_code count ,then it would not compare array since count is not same
         * */
        $countLangList = count($langlist);
        $countIntersect = count($intersectArray = array_intersect($languages,$langlist));
        if($countLangList !== $countLanguage || $countLanguage!==$countIntersect) {
            $data = [
                "status"=>"422",
                "message"=>"Input Translation does not match with specified country language code"
            ];
            return $data;

        }
        /*
         * compare input languages array with obtained country language array count
         * */


        $data = [
            "status" => 200,
            "message" => true
        ];

        return $data;
    }
}