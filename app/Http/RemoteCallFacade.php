<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/25/2017
 * Time: 10:51 PM
 */

namespace App\Http;

use GuzzleHttp\Exception\ConnectException;
use Illuminate\Support\Facades\Config;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * Class RemoteCallFacade
 * @package App\Http
 */
class RemoteCallFacade
{
    private $guzzle;

    /**
     * RemoteCallFacade constructor.
     * @param Client $guzzle
     */
    public function __construct(Client $guzzle)
    {
        $this->guzzle = $guzzle;
    }

    /**
     * @param $url
     * @return array
     */
    public function getSpecificWithoutOauth($url){
        try{
            $result = $this->guzzle->get($url,[
                'headers' => [
                    'Accept' => 'application/json',
                ]
            ]);
            $user=json_decode((string) $result->getBody(), true);

            return ["message"=>$user,"status"=>$result->getStatusCode()];
        }
        catch(ConnectException $ex){
            $res= ["message"=>["message"=>"Error Connecting to Service","status"=>503],"status"=>503];
            return $res;
        }
        catch (RequestException $e)
        {
            $res= ["message"=>json_decode($e->getResponse()->getBody()->getContents(),true),"status"=>$e->getResponse()->getStatusCode()];
            return $res;
        }

    }

    /**
     * @param $url
     * @return array
     */
    public function getSpecific($url){
        try{
            $accessToken=Config::get('config.access_token');
            $result = $this->guzzle->get($url,[
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => $accessToken,
                ]
            ]);
            $user=json_decode((string) $result->getBody(), true);

            return ["message"=>$user,"status"=>$result->getStatusCode()];
        }
        catch(ConnectException $ex){
            $res= ["message"=>"Error Connecting to Service","status"=>503];
            return $res;
        }
        catch (RequestException $e)
        {
            $res= ["message"=>json_decode($e->getResponse()->getBody()->getContents()),"status"=>$e->getResponse()->getStatusCode()];
            return $res;
        }

    }

    /**
     * @param $url
     * @return array
     */
    public function getSpecificByToken($url, $token){
        try{
            $accessToken=$token;
            $result = $this->guzzle->get($url,[
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer '.$accessToken,
                ]
            ]);
            $user=json_decode((string) $result->getBody(), true);

            return ["message"=>$user,"status"=>$result->getStatusCode()];
        }
        catch(ConnectException $ex){
            $res= ["message"=>"Error Connecting to Service","status"=>503];
            return $res;
        }
        catch (RequestException $e)
        {
            $res= ["message"=>json_decode($e->getResponse()->getBody()->getContents()),"status"=>$e->getResponse()->getStatusCode()];
            return $res;
        }

    }

    /**
     * call oauth service to use post request without access token
     * @param $url
     * @param $request
     * @return array
     */
    public function storeWithoutOauth($url, $request){
        try{
            $result =  $this->guzzle->post($url,[
                'headers' => [
                    'Accept' => 'application/json',
                ],
                'form_params' => $request

            ]);

            $user=json_decode((string) $result->getBody(), true);

            return ["message"=>$user,"status"=>$result->getStatusCode()];
        }
        catch(ConnectException $ex){
            $res= ["message"=>[
                "status" => "503",
                "message"=>"Error Connecting to Service"],"status"=>503];
            return $res;
        }
        catch (RequestException $e)
        {
            $res= ["message"=>json_decode($e->getResponse()->getBody()->getContents(),true),"status"=>$e->getResponse()->getStatusCode()];
            return $res;
        }
    }

    /**
     * @param $url
     * @param $request
     * @return array
     */
    public function store($url, $request){
        try{
            $accessToken=Config::get('config.access_token');
            //dd('test');

            $result =  $this->guzzle->post($url,[
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => $accessToken,
                ],
                'form_params' => $request

            ]);

            $user=json_decode((string) $result->getBody(), true);

            return ["message"=>$user,"status"=>$result->getStatusCode()];
        }
        catch(ConnectException $ex){
            $res= ["message"=>"Error Connecting to Service","status"=>503];
            return $res;
        }
        catch (RequestException $e)
        {
            $res= ["message"=>json_decode($e->getResponse()->getBody()->getContents(),true),"status"=>$e->getResponse()->getStatusCode()];
            return $res;
        }
    }

    /**
     * @param $url
     * @param $request
     * @return array
     */
    public function update($url, $request){
        $accessToken=Config::get('config.access_token');
        try{
        $result = $this->guzzle->put($url,[
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $accessToken,
            ],
            'form_params' => $request]);

            $user=json_decode((string) $result->getBody(), true);

            return ["message"=>$user,"status"=>$result->getStatusCode()];
        }

        catch(ConnectException $ex){
            $res= ["message"=>"Error Connecting to Service","status"=>503];
            return $res;
        }

        catch (RequestException $e)
        {
            $res= ["message"=>json_decode($e->getResponse()->getBody()->getContents(),true),"status"=>$e->getResponse()->getStatusCode()];
            return $res;
        }
    }


    /**
     * @param $url
     * @param $request
     * @return array
     */
    public function delete($url, $request){

        $accessToken=Config::get('config.access_token');
        //dd('test');
        try{
            $result = $this->guzzle->post($url,[
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => $accessToken,
                ],
                'form_params' => $request]);
            $user=json_decode((string) $result->getBody(), true);
            return ["message"=>$user,"status"=>$result->getStatusCode()];
        }
        catch(ConnectException $ex){
            $res= ["message"=>"Error Connecting to Service","status"=>503];
            return $res;
        }
        catch (RequestException $e)
        {
            $res= ["message"=>json_decode($e->getResponse()->getBody()->getContents(),true),"status"=>$e->getResponse()->getStatusCode()];
            return $res;
        }
    }

    public function deleteSpecific($url){

        $accessToken=Config::get('config.access_token');
        //dd('test');
        try{
            $result = $this->guzzle->delete($url,[
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' =>$accessToken,
                ]]);
            $user=json_decode((string) $result->getBody(), true);
            return ["message"=>$user,"status"=>$result->getStatusCode()];
        }
        catch(ConnectException $ex){
            $res= ["message"=>"Error Connecting to Service","status"=>503];
            return $res;
        }
        catch (RequestException $e)
        {
            $res= ["message"=>json_decode($e->getResponse()->getBody()->getContents(),true),"status"=>$e->getResponse()->getStatusCode()];
            return $res;
        }
    }
}