<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/25/2017
 * Time: 10:51 PM
 */

namespace App\Http;


use GuzzleHttp\Exception\ConnectException;
use Illuminate\Support\Facades\Config;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Redis;

/**
 * Class RoleCheckerFacade
 * @package App\Http
 */
class SettingsFacade
{
    private $guzzle;
    private $redis;

    /**
     * RemoteCallFacade constructor.
     * @param Client $guzzle
     */
    public function __construct(Client $guzzle)
    {
        $this->guzzle = $guzzle;
    }

    /**
     * @param $url
     * @return array
     */
    public function getSettings($name){
        $data=Redis::get($name);
        if($data != null)
        {
            //dd($data);
            $unserialized_data=@unserialize($data);
            if($unserialized_data == true){
                $data=$unserialized_data;
            }
            $setting['data']['slug']=$name;
            $setting['data']['value']=$data;
            return ["message"=>$setting,"status"=>200];
        }
        $url=Config::get('config.settings_url').$name;
        //dd($url);
        try{
            //$accessToken=Config::get('config.access_token');
            $result = $this->guzzle->get($url."?secret=hazesoft",[
                'headers' => [
                    'Accept' => 'application/json',
                ]
            ]);
            $setting=json_decode((string) $result->getBody(), true);
            Redis::set($setting['data']['slug'],$setting['data']['value']);
            $unserialized_setting=@unserialize($setting['data']['value']);
            if($unserialized_setting == true) {
                $setting['data']['value'] = $unserialized_setting;
            }
            return ["message"=>$setting,"status"=>$result->getStatusCode()];
        }
        catch(ConnectException $ex){
            $res= ["message"=>"Error Connecting to Service","status"=>503];
            return $res;
        }
        catch (RequestException $e)
        {
            $res= ["message"=>json_decode($e->getResponse()->getBody()->getContents()),"status"=>$e->getResponse()->getStatusCode()];
            return $res;
        }

    }

    public function getErrorMessage($name){
        $data=Redis::get($name);
        if($data != null)
        {
            $data = unserialize($data);
            return ["message"=>$data,"status"=>200];
        }
        $url=Config::get('config.error_message_url').$name;
        //dd($url);
        try{
            //$accessToken=Config::get('config.access_token');
            $result = $this->guzzle->get($url,[
                'headers' => [
                    'Accept' => 'application/json',
                ]
            ]);
            $error_message=json_decode((string) $result->getBody(), true);
            $value = serialize($error_message['data']);
            Redis::set($name,$value);
            $data=Redis::get($name);
            if($data != null)
            {
                $data = unserialize($data);
                return ["message"=>$data,"status"=>200];
            }
        }
        catch(ConnectException $ex){
            $res= ["message"=>"Error Connecting to Service","status"=>503];
            return $res;
        }
        catch (RequestException $e)
        {
            $res= ["message"=>json_decode($e->getResponse()->getBody()->getContents()),"status"=>$e->getResponse()->getStatusCode()];
            return $res;
        }

    }

    public function getErrorMessageOrDefaultMessage($name, $defaultMessage, $statusCode, $lang)
    {
        try {
            $data = Redis::get($name);
            if ($data != null) {
                $data = unserialize($data);
                return response()->json([
                    "status" => "$statusCode",
                    "message" => $data["$lang"]
                ], $statusCode);
            }
            $url = Config::get('config.error_message_url') . $name;
            $result = $this->guzzle->get($url, [
                'headers' => [
                    'Accept' => 'application/json',
                ]
            ]);
            $error_message = json_decode((string)$result->getBody(), true);
            if ($error_message['status'] != "200") throw new \Exception();
            $value = serialize($error_message['data']);
            Redis::set($name, $value);
            $data = Redis::get($name);
            $data = unserialize($data);
            return response()->json([
                "status" => "$statusCode",
                "message" => $data["$lang"]
            ], $statusCode);

        } catch (\Exception $ex) {
            return response()->json([
                "status" => "$statusCode",
                "message" => $defaultMessage
            ], $statusCode);
        }
    }

    public function getErrorMessageOrDefaultMessageWithPayload($name, $defaultMessage, $statusCode, $lang, $payload)
    {
        try {
            $data = Redis::get($name);
            if ($data != null) {
                $data = unserialize($data);
                return response()->json([
                    "status" => "$statusCode",
                    "message" => $data["$lang"],
                    "data" => $payload
                ], $statusCode);
            }
            $url = Config::get('config.error_message_url') . $name;
            $result = $this->guzzle->get($url, [
                'headers' => [
                    'Accept' => 'application/json',
                ]
            ]);
            $error_message = json_decode((string)$result->getBody(), true);
            if ($error_message['status'] != "200") throw new \Exception();
            $value = serialize($error_message['data']);
            Redis::set($name, $value);
            $data = Redis::get($name);
            $data = unserialize($data);
            return response()->json([
                "status" => "$statusCode",
                "message" => $data["$lang"],
                "data" => $payload
            ], $statusCode);

        } catch (\Exception $ex) {
            return response()->json([
                "status" => "$statusCode",
                "message" => $defaultMessage,
                "data" => $payload
            ], $statusCode);
        }
    }

}