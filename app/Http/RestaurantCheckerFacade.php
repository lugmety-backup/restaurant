<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 5/17/17
 * Time: 1:18 PM
 */

namespace App\Http;

use App\Http\Controllers\Controller;

use App\Repo\RestaurantBranchInterface;
use App\Repo\RestaurantHourInterface;
use Carbon\Carbon;


class RestaurantCheckerFacade extends Controller
{
    protected $restaurantBranch, $restaurantHour;

    /**
     * RestaurantCheckerFacade constructor.
     * @param RestaurantBranchInterface $restaurantBranch
     * @param RestaurantHourInterface $restaurantHour
     */
    public function __construct(RestaurantBranchInterface $restaurantBranch,RestaurantHourInterface $restaurantHour)
    {
        $this->restaurantBranch = $restaurantBranch;
        $this->restaurantHour = $restaurantHour;
    }

    /**
     * This function is used to check if the restaurant is active or not.
     * If restaurant is active, returns true else false.
     * @param $restaurantId
     * @return bool
     */
    public function check($restaurantId)
    {
        $restaurant = $this->restaurantBranch->getSpecificRestaurantBranchForPublic($restaurantId);

        if(isset($restaurant[0]) && $restaurant[0]['status']){

            return true;

        }
        return false;


    }

    /**
     * This function is used to check if the restaurant is open or closed at current time.
     * Checks if the restaurant is open or closed
     * @param $restaurantId
     * @return bool
     */
    public function checkOpenRestaurant($restaurantId)
    {
        /**
         * Check if the restaurant is active or not.
         */
        if($this->check($restaurantId))
        {
            /**
             * Gets the dayname of today.
             */
            $day = Carbon::now()->format( 'l' );
            /**
             * Fetch data of given restaurant branch id and dayname from RestaurantHourRepo.
             *
             */

            $restaurantHour = $this->restaurantHour->getSpecificRestaurantHourByIdRestaurantIdDay($restaurantId, $day);

            /**
             * The strtotime() function parses an English textual datetime into a Unix timestamp.
             * Change all the time in Unix timestamp using strtotime().
             * Compare Unix timestamps to determine if the restaurant branch is open or closed.
             */
            $now = strtotime(Carbon::now());
            $n = gmdate(Carbon::now()->format("H:i:s"));
            /**
             * There may be different shifts for a day in a restaurant branch.
             * So foreach loop is used until the timestamps are matched or data is finished.
             * If timestamp is matched, function returns true, else returns false.
             */
            foreach ($restaurantHour as $hour)
            {
                $opening_time = strtotime($hour['opening_time']);

                $closing_time = strtotime($hour['closing_time']);

                if($hour['closed'] == 1){
                    continue;
                }

                if($opening_time <= $now && $now <= $closing_time)
                {
                    return true;
                }elseif ($hour['opening_time'] >$hour['closing_time']){
                    if($hour['opening_time'] <= $n || $hour['closing_time']>= $n){
                        return true;
                    }
                }
            }
            return false;


        }
        else{
            return false;
        }
    }

    public function checkOpenRestaurantAndPreOrder($restaurantId)
    {
        try {

            /**
             * Check if the restaurant is active or not.
             */
            $restaurant = $this->restaurantBranch->getSpecificRestaurantBranchForPublic($restaurantId);
            $res['slug'] = $restaurant[0]['slug'];
            $res['country_id'] = $restaurant[0]['country_id'];
            $res['city_id'] = $restaurant[0]['city_id'];
            $res['tax_type'] = $restaurant[0]['tax_type'];

            if (isset($restaurant[0])) {

                $translation = $restaurant[0]->branchTranslation()->get();

                foreach ($translation as $trans) {
                    $resName[$trans['lang_code']] = $trans['name'];
                }
                $res['resName'] = $resName;
                if($restaurant[0]['pre_order'] == 1)
                {
                    return $res;
                }



                /**
                 * Gets the dayname of today.
                 */
                $day = Carbon::now()->format('l');
                /**
                 * Fetch data of given restaurant branch id and dayname from RestaurantHourRepo.
                 *
                 */

                $restaurantHour = $this->restaurantHour->getSpecificRestaurantHourByIdRestaurantIdDay($restaurantId, $day);

                /**
                 * The strtotime() function parses an English textual datetime into a Unix timestamp.
                 * Change all the time in Unix timestamp using strtotime().
                 * Compare Unix timestamps to determine if the restaurant branch is open or closed.
                 */
                $now = strtotime(Carbon::now());
                $n = gmdate(Carbon::now()->format("H:i:s"));
                /**
                 * There may be different shifts for a day in a restaurant branch.
                 * So foreach loop is used until the timestamps are matched or data is finished.
                 * If timestamp is matched, function returns true, else returns false.
                 */
                foreach ($restaurantHour as $hour) {

                    $opening_time = strtotime($hour['opening_time']);

                    $closing_time = strtotime($hour['closing_time']);

                    if($hour['closed'] == 1){
                        continue;
                    }

                    if ($opening_time <= $now && $now <= $closing_time) {
                        return $res;
                    }elseif ($hour['opening_time'] >$hour['closing_time']){
                        if($hour['opening_time'] <= $n || $hour['closing_time'] >= $n){
                            return $res;
                        }
                    }



                }
            }
            return false;


        }catch (\Exception $ex)
        {
            return false;
        }


    }

    public function checkPreOrderForRestaurantAsap($restaurantId){
        return $currentDate = Carbon::now();
//        $addtime = $currentDate->addMinutes(20);
//        $addtimeInHis = $addtime->format("H:i:s");
//        $day = $currentDate->format("l");
//        $restaurantHour = $this->restaurantHour->getSpecificRestaurantHourByIdRestaurantIdDay($restaurantId, $day);
//        $now = strtotime($currentDate->format("H:i:s"));
//
//        $filteredDate =  collect($restaurantHour)->where("closed",0);
//        $getDay = false;
//        $count = count($filteredDate);
//        if($count== 0) $getDay;
//
//        /**
//         * There may be different shifts for a day in a restaurant branch.
//         * So foreach loop is used until the timestamps are matched or data is finished.
//         * If timestamp is matched, function returns true, else returns false.
//         */
//
//        foreach ($filteredDate as $hour)
//        {
//            $opening_time = strtotime($hour['opening_time']);
//
//            $closing_time = strtotime($hour['closing_time']);
//            if($opening_time < $now && $now < $closing_time)
//            {
//                if($count == 1 && ($opening_time < $now) && (strtotime($addtimeInHis)) < $closing_time ) {
//                    $getDay = $addtimeInHis;
//                }
//                else{
//                    $getDay = $hour['opening_time'];
//                }
//
//                $getDay = Carbon::parse($currentDate->toDateString()." $getDay");
//                break;
//            }
//        }
////        return $currentDate->toDateString()." $getDay";
////       foreach ($filteredDate as $date){
////           $shift[] =$date["shift"];
////       }
////       return $shift = array_unique($shift);
//
////       foreach ($days as $day){
////           $getDay = collect($filteredDate)->where("day",$day)->first();
////           if($getDay) break;
////
////       }
//        return $getDay;
    }

    /**
     * This function is used to check if the restaurant is open or closed at current time.
     * Checks if the restaurant is open or closed
     * @param $restaurantId
     * @return bool
     */
    public function checkPreOrderForRestaurant($restaurantId,$dateInfo)
    {
        $day = $dateInfo["day"];
        /**
         * Fetch data of given restaurant branch id and dayname from RestaurantHourRepo.
         *
         */

        $restaurantHour = $this->restaurantHour->getSpecificRestaurantHourByIdRestaurantIdDay($restaurantId, $day);
        /**
         * The strtotime() function parses an English textual datetime into a Unix timestamp.
         * Change all the time in Unix timestamp using strtotime().
         * Compare Unix timestamps to determine if the restaurant branch is open or closed.
         */
        $now = strtotime($dateInfo["date"]);
        /**
         * There may be different shifts for a day in a restaurant branch.
         * So foreach loop is used until the timestamps are matched or data is finished.
         * If timestamp is matched, function returns true, else returns false.
         */
        foreach ($restaurantHour as $hour)
        {

            $opening_time = strtotime($hour['opening_time']);

            $closing_time = strtotime($hour['closing_time']);

            if($hour['closed'] == 1){
                continue;
            }
            if($opening_time > $closing_time)
            {
                if($opening_time <= $now || $now <= $closing_time)
                {
                    return true;
                }
            }

            if($opening_time <= $now && $now <= $closing_time)
            {
                return true;
            }


        }
        return false;

    }


}