<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * Class BranchTranslation
 * @package App
 */
class BranchTranslation extends Model
{
    public $timestamps = false;
    protected $guarded=['id'];
    protected $table='branch_translation';
    protected $hidden=['branch_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantBranch(){
        return $this->belongsTo('App\RestaurantBranch','branch_id');
    }

    /**
     * @param $value
     */
    public function setLangCodeAttribute($value){
        $this->attributes['lang_code'] = strtolower($value);
    }
}
