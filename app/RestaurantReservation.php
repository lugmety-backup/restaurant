<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 7/7/2017
 * Time: 1:53 PM
 */

namespace App;

use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;

class RestaurantReservation extends Model
{

    protected $table = "restaurant_reservations";
    protected $fillable = ['restaurant_id','user_id',"full_name","email",'phone','booking_date','booking_time','seats_count','special_instruction','status'];
    protected $hidden = ["pivot"];

    public  function restaurantBranches(){
        return $this->belongsToMany('App\RestaurantBranch');
    }

}