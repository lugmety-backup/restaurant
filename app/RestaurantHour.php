<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * Class RestaurantHour
 * @package App
 */
class RestaurantHour extends Model
{

    protected $table='restaurant_hours';
    protected $fillable=["branch_id","shift","opening_time","closing_time","day","closed","difference_checker"];
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurantBranch(){
        return $this->belongsTo('App\RestaurantBranch','branch_id');
    }
/*
 * return store date into am pm
 * */
    /**
     * @param $value
     * @return false|string
     */
//    public function getOpeningTimeAttribute($value){
//        return date('h:i:s a', strtotime($value));
//    }

    /**
     * return store date into am pm
     * @param $value
     * @return false|string
     */
//    public function getClosingTimeAttribute($value){
//        return date('h:i:s a', strtotime($value));
//    }

    public function setShiftAttribute($shift){
        $this->attributes["shift"]=ucfirst($shift);
    }
}
