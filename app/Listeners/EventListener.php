<?php

namespace App\Listeners;

use App\Events\EmailEvent;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class EventListener implements ShouldQueue
{
    public $event;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  EmailEvent  $event
     * @return void
     */
    public function handle(EmailEvent $event)
    {
        $this->event = $event;

        try {
            \Amqp::publish('service1', serialize($this->event->array), [
                'queue' => 'notice',
                'exchange_type' => 'direct',
                'exchange' => 'amq.direct',
            ]);
        }
        catch (\Exception $ex){
            $this->failed($ex->getMessage());
        }
    }

    public function failed($ex)
    {
        Log::info($ex);
    }
}
