<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 7/7/2017
 * Time: 1:53 PM
 */

namespace App;

use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;

class Cuisine extends Model
{

    protected $table = "cuisine";
    protected $fillable = ["status","icon","country_id"];
    protected $hidden = ["pivot"];
    public function cuisineTranslations(){
        return $this->hasMany('App\CuisineTranslation',"cuisine_id");
    }

    public  function restaurantBranches(){
        return $this->belongsToMany('App\RestaurantBranch');
    }

    public function branches(){
        return $this->belongsToMany('App\RestaurantBranch','cuisine_restaurant_pivot','cuisine_id','restaurant_id');
    }


    public function cuisineBranchSorting(){
        return $this->hasMany('App\Models\CuisineBranchSorting',"cuisine_id")->orderBy("sort","asc");
    }

    public function cuisineBranch(){
        return $this->belongsToMany('App\RestaurantBranch','cuisine_branch_sorting', 'cuisine_id', 'branch_id');
    }

    public function getIconAttribute($value)
    {
        if(empty($value)){
            return $value;
        }
        return Config::get("config.image_service_base_url_cdn").$value;
    }

}