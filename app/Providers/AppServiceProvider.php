<?php

namespace App\Providers;

use App\Repo\BranchSortingInterface;
use App\Repo\CuisineInterface;
use App\Repo\CuisineTranslationInterface;
use App\Repo\CustomerRestaurantInterface;
use App\Repo\DeliveryZoneInterface;
use App\Repo\Eloquent\BranchSortingRepo;
use App\Repo\Eloquent\CuisineRepo;
use App\Repo\Eloquent\CuisineTranslationRepo;
use App\Repo\Eloquent\CustomerRestaurantRepo;
use App\Repo\Eloquent\DeliveryZoneRepo;
use App\Repo\Eloquent\FeaturedRestaurantSortingRepo;
use App\Repo\Eloquent\FoodSizeRepo;
use App\Repo\Eloquent\FoodSizeTranslationRepo;
use App\Repo\Eloquent\PaymentMethodRepo;
use App\Repo\Eloquent\PivotRepo;
use App\Repo\Eloquent\RestaurantBranchRepo;
use App\Repo\Eloquent\RestaurantBranchTranslationRepo;
use App\Repo\Eloquent\RestaurantCategoryRepo;
use App\Repo\Eloquent\RestaurantHourRepo;
use App\Repo\Eloquent\RestaurantRepo;
use App\Repo\Eloquent\RestaurantReservationRepo;
use App\Repo\Eloquent\RestaurantTranslationRepo;
use App\Repo\Eloquent\RestaurantUsersRepo;
use App\Repo\Eloquent\ReviewRepo;
use App\Repo\Eloquent\ShippingMethodRepo;
use App\Repo\Eloquent\UserFavouriteRepo;
use App\Repo\FeaturedRestaurantSortingInterface;
use App\Repo\FoodSizeInterface;
use App\Repo\FoodSizeTranslationInterface;
use App\Repo\PaymentMethodInterface;
use App\Repo\PivotInterface;
use App\Repo\RestaurantBranchInterface;
use App\Repo\RestaurantBranchTranslationInterface;
use App\Repo\RestaurantCategoryInterface;
use App\Repo\RestaurantHourInterface;
use App\Repo\RestaurantInterface;
use App\Repo\RestaurantReservationInterface;
use App\Repo\RestaurantTranslationInterface;
use App\Repo\RestaurantUsersInterface;
use App\Repo\ReviewInterface;
use App\Repo\ShippingMethodInterface;
use App\Repo\UserFavouriteInterface;
use App\RestaurantBranch;
use App\RestaurantReservation;
use FastRoute\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Validator;
use Illuminate\Support\Facades\Config;
use App\Repo\AddonAddonCategoryInterface;
use App\Repo\AddOnCategoryInterface;
use App\Repo\AddOnCategoryTranslationInterface;
use App\Repo\AddOnInterface;
use App\Repo\AddOnTranslationInterface;
use App\Repo\Eloquent\AddonAddonCategoryRepo;
use App\Repo\Eloquent\AddOnCategoryRepo;
use App\Repo\Eloquent\AddOnCategoryTranslationRepo;
use App\Repo\Eloquent\AddOnRepo;
use App\Repo\Eloquent\AddOnTranslationRepo;
use App\Repo\Eloquent\FoodAddonCategoryRepo;
use App\Repo\Eloquent\FoodCategoryRepo;
use App\Repo\Eloquent\FoodRepo;
use App\Repo\Eloquent\FoodTranslationRepo;
use App\Repo\FoodAddonCategoryInterface;
use App\Repo\FoodCategoryInterface;
use App\Repo\FoodInterface;
use App\Repo\FoodTranslationInterface;
use App\Repo\Eloquent\CategoryRepo;
use App\Repo\CategoryInterface;
use App\Repo\CategoryTranslationInterface;
use App\Repo\Eloquent\CategoryTranslationRepo;
use App\Http\Facades\RestaurantCheckerFacade;
use App\Http\Facades\AuthCheckerFacade;
/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    protected $branch;

    /**
     * AppServiceProvider constructor.
     * @param $branch
     */

    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    public function boot()
    {

        /**
         * takes the google cloud credential
         */
        $array = [
            "type" =>env('STACK_TYPE') ,
            "project_id" => env('STACK_PROJECT_ID'),
            "private_key_id" => env('STACK_PRIVATE_KEY_ID'),
            "private_key" => str_replace('\n', "\n", env('STACK_PRIVATE_KEY')),
            "client_email" => env('STACK_CLIENT_EMAIL'),
            "client_id" => env('STACK_CLIENT_ID'),
            "auth_uri" => env('STACK_AUTH_URI'),
            "token_uri" => env('STACK_TOKEN_URL'),
            "auth_provider_x509_cert_url" => env('STACK_AUTH_PROVIDER_x509_CERT_URL'),
            "client_x509_cert_url" => env('STACK_CLIENT_X509_CERT_URL')
        ];
        /**
         * the path to create and store the json
         */
        $path = storage_path().'/stackdriver.json';
        /**
         * open the file
         */
        $fp = fopen($path, 'w');
        /**
         * store array in json with pretty print
         */
        fwrite($fp, json_encode($array,JSON_PRETTY_PRINT ));
        fclose($fp);


        $branch = new RestaurantBranch();
        Validator::extend('week_day_validation', function($attribute, $value, $parameters, $validator) {
            $test=in_array($value,["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]);
            if(!$test){
                return false;
            }
            return true;
        });

        Validator::extend('shift_validation', function($attribute, $value, $parameters, $validator) {
           $shift= Config::get('config.restaurant_hour_shift');

            $test=in_array($value,$shift);
            if(!$test){
                return false;
            }
            return true;
        });

        Validator::extend('day_shift_validation', function($attribute, $value, $parameters, $validator) {
            $shift= Config::get('config.restaurant_hour_shift');
            $check[0] = $shift[0];
            $test=in_array($value,$check);
            if(!$test){
                return false;
            }
            return true;
        });

        Validator::extend('domain_validation', function($attribute, $value, $parameters, $validator) {
            $domains = explode("@",$value);
            if(checkdnsrr($domains[1],"MX")){
                return true;
            }
            else{
                return false;
            }
        });

        Validator::extendImplicit('check_slug', function($attribute, $value, $parameters, $validator) use($branch) {
            $data =$validator->getData();
            if(!isset($data["country_id"])) return true;
            $countryId = $data["country_id"];
            $checkSlug =$branch->where([["country_id",$countryId],["slug",$value]])->first();
           if($checkSlug) return false;
           else return true;

        });

        Validator::extendImplicit('check_slug_update', function($attribute, $value, $parameters, $validator) use($branch) {
            $data =$validator->getData();
            $branchId = $parameters[0];
            if(!isset($data["country_id"]) || !isset($data["country_id"])) return true;
            $countryId = $data["country_id"];
            $checkSlug = $branch->where([["country_id",$countryId],["slug",$value],["id","!=",$branchId]])->first();
            if($checkSlug) return false;
            else return true;

        });


        /**
         * This validation is used to check if is_default="1" is present in only one array of food size.
         *
         */
        Validator::extend('check_default', function ($attribute, $value, $parameters, $validator){
            /**
             * Get values of is_default key and assign them in an array.
             *
             */
            $check = array_pluck($value, $parameters[0]);
            /**
             * Check if the array has "1".
             * Returns 1 for each "1" found in array and saves in array.
             */
            $filtered = array_where($check, function ($value, $key) {
               return ($value === "1");
            });
            /**
             * Count no of "1"s returned.
             * If no of count is equal to 1 return true else return false.
             */
            return count($filtered) == 1? true:false;
        });
    }


    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(RestaurantInterface::class,RestaurantRepo::class);
        $this->app->bind(RestaurantBranchInterface::class,RestaurantBranchRepo::class);
        $this->app->bind(RestaurantTranslationInterface::class,RestaurantTranslationRepo::class);
        $this->app->bind('roleChecker', 'App\Http\RoleCheckerFacade');
        $this->app->bind('settings', 'App\Http\SettingsFacade');
        $this->app->bind('remoteCall','App\Http\RemoteCallFacade');
        $this->app->bind("imageUploader",'App\Http\ImageUploadFacade');
        $this->app->bind(RestaurantBranchTranslationInterface::class,RestaurantBranchTranslationRepo::class);
        $this->app->bind(RestaurantUsersInterface::class,RestaurantUsersRepo::class);
        $this->app->bind(DeliveryZoneInterface::class,DeliveryZoneRepo::class);
        $this->app->bind(RestaurantHourInterface::class,RestaurantHourRepo::class);
        $this->app->bind(PaymentMethodInterface::class,PaymentMethodRepo::class);
        $this->app->bind(ShippingMethodInterface::class,ShippingMethodRepo::class);
        $this->app->bind(CuisineTranslationInterface::class,CuisineTranslationRepo::class);
        $this->app->bind(CuisineInterface::class,CuisineRepo::class);
        $this->app->bind(ReviewInterface::class,ReviewRepo::class);

        $this->app->bind(CategoryInterface::class,CategoryRepo::class);
        $this->app->bind(CategoryTranslationInterface::class,CategoryTranslationRepo::class);
        $this->app->bind(FoodTranslationInterface::class,FoodTranslationRepo::class);
        $this->app->bind(FoodInterface::class,FoodRepo::class);
        $this->app->bind(AddOnTranslationInterface::class,AddOnTranslationRepo::class);
        $this->app->bind(AddOnInterface::class,AddOnRepo::class);
        $this->app->bind(AddOnCategoryTranslationInterface::class,AddOnCategoryTranslationRepo::class);
        $this->app->bind(AddOnCategoryInterface::class,AddOnCategoryRepo::class);
        $this->app->bind(FoodCategoryInterface::class,FoodCategoryRepo::class);
        $this->app->bind(FoodAddonCategoryInterface::class,FoodAddonCategoryRepo::class);
        $this->app->bind(AddonAddonCategoryInterface::class,AddonAddonCategoryRepo::class);
        $this->app->bind("restaurantChecker",\App\Http\RestaurantCheckerFacade::class);
        $this->app->bind("authChecker",\App\Http\AuthCheckerFacade::class);
        $this->app->bind(RestaurantCategoryInterface::class, RestaurantCategoryRepo::class);
        $this->app->bind("LocationService",\App\Http\LocationServiceFacade::class);
        $this->app->bind(UserFavouriteInterface::class, UserFavouriteRepo::class);
        $this->app->bind(PivotInterface::class, PivotRepo::class);
        $this->app->bind(RestaurantReservationInterface::class, RestaurantReservationRepo::class);
        $this->app->bind(FoodSizeInterface::class, FoodSizeRepo::class);
        $this->app->bind(FoodSizeTranslationInterface::class, FoodSizeTranslationRepo::class);
        $this->app->bind(CustomerRestaurantInterface::class, CustomerRestaurantRepo::class);

        $this->app->bind(BranchSortingInterface::class,BranchSortingRepo::class);
        $this->app->bind(FeaturedRestaurantSortingInterface::class,FeaturedRestaurantSortingRepo::class);
    }
}
